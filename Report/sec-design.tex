\subsection{Architectual Design}
\subsubsection{Overall Design}
Our design primarily consists of 5 modules, which are interconnected as illustrated in figure 1. Starting from the Avalon Memory-Mapped (Avalon-MM), we use a set of Avalon On-Chip (OC) FIFOs to interface with the Avalon Streaming (Avalon-ST). The TX OC FIFO takes as input an Avalon-MM write slave interface, and as output an Avalon-ST source. On the other hand, the RX OC FIFO has as input an Avalon-ST sink interface, and its output is an Avalon-MM read slave  interface.\\\\
Each of the OC FIFOs is connected to an Avalon Dual-Clock FIFO (DCFIFO) which is synchronized to the HPS clock on the one side (input for the TX DCFIFO and output for the RX DCFIFO) and to the XAUI PHY clock on the other side (input for the RX DCFIFO and output for the TX DCFIFO). \\\\
Before being directed to the MAC module, the data outputted by the TX DCFIFO is first buffered in a TX buffer module which is used in order to make sure that the 64 bits ST data is  written in one clock cycle along with the valid signal (as expected by the XAUI PHY module). \\\\
The next stage is the MAC module itself. This is Altera’s 10-Gbps Ethernet (10GbE) Media Access Controller (MAC) IP core, which has on one side the 64-bit wide Avalon-ST interface, and on the other side the 72-bit wide SDR XGMII interface. Both interfaces run at a 156.25 MHz frequency.  \\\\
The last module in the chain is the XAUI PHY module. This is Altera's XAUI PHY IP Core which has a 72-bit data (single data rate - SDR XGMII) interface to the application layer, at 156.25 Mbps and a serial interface that consists of 4 x 3.125 Gbps lanes connected to the Dual XAUI board through the HSMC connectors. \\\\
Mounted on the Altera Cyclone V board, we have a daughter card which is the Dual XAUI to SFP+ High Speed Mezzanine (HSMC) board. This board interfaces with the XAUI module on the FPGA through the HSMC connectors on the one side, and with the network through SFP+ optical modules, on the other side.\\
\begin{center}
\includegraphics[scale=0.55]{images/design.jpg}\\
\small
figure 1 - Hardware Design Schematic\\
\normalsize
\end{center}
\subsubsection{Dual XAUI HSMC Board}
We use the Dual XAUI to SFP+ HSMC for the actual communication with the network. This board contains 2 full duplex 10G SFP+ channels with a XAUI backend interface. The XAUI interfaces will be attached to the HSMC side of the card and the SFI side of the interface will be attached to the SFP+ optical modules on opposite side of the board. The main device on the daughter board is the BCM8727, a dual-channel 10-GbE SFI-to-XAUI transceiver. It is fully compliant with the 10-GbE IEEE 802.3aq standard and incorporates an Electronic Dispersion Compensation (EDC) equalizer which supports SFP+ line-card applications. Figure 2 describes the connections between the BCM8727 and the Cyclone V board through the HSMC connectors.\\
\begin{center}
\includegraphics[scale=0.55]{images/bcm.png}\\
\small
figure 2 - Dual XAUI board and its connections\\
\normalsize
\end{center}
The BCM8727 interfaces, on the one hand, with the XAUI PHY through a 4-lane XAUI interface (4 x 3.125 Gbps). On the other hand, it uses the SFI protocol to connect and communicate with the SFP+ optical modules. \\
\subsubsection{XAUI PHY}
The Altera XAUI PHY IP Core implements the IEEE 802.3 Clause 48 specification to extend the operational distance of the XGMII interface and reduce the number of interface signals. The XAUI PHY module accepts 72-bit data (single data rate - SDR XGMII) from the application layer at either 156.25 Mbps or 312.5 Mbps. The serial interface runs at either 4 x 3.125 Gbps or 4 × 6.25 Gbps (DDR XAUI option).\\\\
In our design, the XAUI PHY module communicates through the HSMC connectors with the daughter board to receive 4 lanes of serial data at 3.125 Gbps each and outputs a 72-bit SDR XGMII signal at 156.25 MHz towards the MAC module (or the other way round, for transmission). The 72-bit data are obtained by adding a one-bit control to each byte of the arriving packets.\\
Figure 3 represents the XAUI PHY module and its interconnections with the rest of our system. As we can see, there are two main components inside the XAUI PHY IP core, which it uses to manipulate data at the low-level physical layer: the PMA (Physical Medium Attachment) and the PCS (Physical Coding Sublayer).\\
\begin{center}
\includegraphics[scale=0.55]{images/xaui.png}\\
\small
figure 3 - XAUI PHY module and its interfaces\\
\normalsize
\end{center}
\newpage
The XAUI PHY has two in/out interfaces:\\ 
\begin{enumerate}
\item The XAUI PCS interface to the FPGA fabric uses a SDR XGMII interface. This interface implements a simple version of Avalon-ST protocol. The interface does not include ready or valid signals; consequently, the sources always drive data and the sinks must always be ready to receive data. According to the configuration parameters that we chose, the application interface in our case runs at 156.25 Mbps and the data in only driven on the rising edge of clock. To meet the bandwidth requirements, the datapath is eight bytes wide with eight control bits (one per data byte), hence the 72 bits obtained (instead of 64 bits).
\item The other interface of the XAUI module is the Transceiver Serial Data Interface. This interface has 4 lanes of serial data for both the TX and RX interfaces. It runs at 3.125 GHz in the case of our design. There is no separate clock signal because it is encoded in the data.
\end{enumerate}
\subsubsection{10GbE MAC}
The 10-Gbps Ethernet (10GbE) Media Access Controller (MAC) IP core is a configurable component that implements the IEEE 802.3-2008 specification. The IP core offers several modes (according to the Ethernet speed). We use the ``10M-10G mode'' which uses the Avalon-ST interface on the client side and SDR XGMII (for the 10G mode) on the network side.
The 10GbE MAC IP core handles the flow of data between a client and Ethernet network through a 10-Gbps Ethernet PHY. On the transmit path, the MAC accepts client frames and constructs Ethernet frames by inserting various control fields, such as checksums before forwarding them to the PHY. Similarly, on the receive path, the MAC accepts Ethernet frames via a PHY, performs checks, and removes the relevant fields before forwarding the frames to the client.
Figure 4 summarizes the internal blocks and the interfaces to the core.\\
\begin{center}
\includegraphics[scale=0.55]{images/mac.png}\\
\small
figure 4 - MAC module - its internal organization and its interfaces\\
\normalsize
\end{center}
The core is a composition of three blocks: MAC receiver (MAC RX), MAC transmitter (MAC TX), and Avalon-MM bridge. The MAC RX and MAC TX handle data flow between the client and Ethernet network. The Avalon-MM bridge provides a single interface to all Avalon-MM interfaces within the MAC, which allows a host to access 32-bit configuration and status registers, and statistics counters.\\\\
On the client-side interface of the MAC, we have the ST data coming from/going to the DCFIFOs. In the MAC, the Avalon-ST interface acts as a sink in the transmit datapath and source in the receive datapath.\\\\
The network-side interface of the 10GbE MAC implements the SDR version of the XGMII protocol. On this side, the MAC is connected to XAUI PHY module. The data bus carries the MAC frame; the most significant byte occupies the least significant lane.\\\\
\textit{Note}: It is worth mentioning that in our design, the Altera 10GbE MAC IP core worked to some extent only. More precisely, when using any loopback system that did not include the OC FIFOs, the system worked properly, meaning we could use \texttt{tcpdump} and/or \texttt{tcpreplay} commands (on the desktop) to transmit and receive packets. In this configuration, we managed to transmit/receive the expected data, using the Altera 10GbE MAC IP core, and the whole system as long as it did not inolve the OC FIFO’s, i.e. the interface with the Avalon-MM.\\\\
In spite of our attempts to troubleshoot the problem (by using several TCL scripts for faster testing, signal tapping, trying other possible simple configurations for the MAC, etc.) we could not manage to solve the issue. For this reason, we used the FAST MAC module from the previous project instead of the Altera IP core. When the FAST MAC module is used, the system works front to back, both in transmission and reception (i.e. starting from the Avalon-MM interface and receiving the packet with \texttt{tcpdump} on the desktop, for transmission, and sending a packet from the desktop using \texttt{tcpreplay} and receiving the packet on the Avalon-MM interface).\\
\subsubsection{TX buffer}
The XAUI PHY module expects to receive 64 bits data at once (in one clock cycle), along with one valid signal for all of the 64 bits. On the other hand, the Altera MM\_to\_ST converter takes some clock cycles to organize the 64 bits stream data when the input is 32 bits. As a result (and also because of the higher latency of software compared to hardware), the 64-bit data outputted from the TX DC FIFO is sometimes broken down into chunks and the valid pulse signal is only sent at the end. But this scenario is not accepted by the XAUI PHY. For this reason, we use the TX FIFO module, from the previous project, which holds the output data from the TX DC FIFO and transmits the 64 bits when ready, in one clock cycle, along with the valid signal.\\
\subsubsection{DC FIFOs}
Altera provides FIFO functions through the parameterizable single-clock FIFO (SCFIFO) and dual-clock FIFO (DCFIFO) megafunction IP cores. The FIFO functions are mostly applied in data buffering applications that comply with the first-in-first-out data flow in synchronous or asynchronous clock domains.\\\\
For our design, we used two DCFIFOs - one for transmit and one for receive - to prepare the 64 bits input data arriving at the input clock frequency, for a 64 bits output data transmitted at the output clock frequency.\\\\
We also enabled the \textbf{Use packets} feature for the DCFIFOs in conformance with the OC FIFOs that they communicate with, which are configured to receive and transmit packet-oriented data.\\
\subsubsection{On-Chip Memory FIFOs}
The on-chip FIFO memory core is a configurable component used to buffer data as well as provide flow control. The FIFO can operate with a single clock or with separate clocks for the input and output ports. The input interface to the FIFO may be an Avalon-MM write slave or an Avalon-ST sink. The output interface can be an Avalon-ST source or an Avalon-MM read slave. The data is delivered to the output interface in the same order that it was received at the input interface, regardless of the value of channel, packet, frame, or any other signals.\\\\
The on-chip FIFO has four possible configurations:\\
\begin{enumerate}
\item Avalon-MM write slave to Avalon-MM read slave
\item Avalon-ST sink to Avalon-ST source
\item Avalon-MM write slave to Avalon-ST source
\item Avalon-ST sink to Avalon-MM read slave
\end{enumerate}
In our system, we used the last two configuration, to interface, from the Avalon-MM, with the Avalon-ST.\\
\subsubsection{Avalon-MM write slave to Avalon-ST source}
The TX OC MEM (as noted on figure 1) is an on-chip FIFO with the ``Avalon-MM write slave to Avalon-ST source'' configuration. In this mode, the FIFO's input is an Avalon-MM write slave with a width of 32 bits. The Avalon-ST output (source) data width must also be 32 bits. Several output interface parameters are configurable, including: \textbf{bits per symbol}, \textbf{symbols per beat}, \textbf{width of the channel}, and \textbf{error signals}. In the Tutorials section, we mention the settings for these parameters in the case of our design.\\\\
The FIFO performs the endian conversion to conform to the output interface protocol.\\\\
In this mode (``Avalon-MM write slave to Avalon-ST source''), the on-chip FIFO has an additional parameter - \textbf{Enable packet data} - that allows to specify whether or not we are using packet data.\\
\begin{center}
\includegraphics[scale=0.55]{images/mm_st_mem.png}\\
\small
figure 5 - Avalon-MM to Avalon-ST Memory Map\\
\normalsize
\end{center}
If \textbf{Enable packet data} is off, the Avalon-MM write master writes all data at address offset 0 repeatedly to push data into the FIFO.\\\\
If \textbf{Enable packet data} is on, the Avalon-MM write master starts by writing the SOP, error (optional), channel (optional), EOP, and empty packet status information at address offset 1. Writing to address offset 1 does not push data into the FIFO. The Avalon-MM master then writes packet data to the FIFO repeatedly at address offset 0, pushing 8-bit symbols into the FIFO. Whenever a valid write occurs at address offset 0, the data and its respective packet information is pushed into the FIFO. Subsequent data is written at address offset 0 without the need to clear the SOP. Rewriting to address offset 1 is not required each time if the subsequent data to be pushed into the FIFO is not the end-of-packet data, as long as error and channel do not change.\\\\
At the end of each packet, the Avalon-MM master writes to the address at offset 1 to set the EOP bit to 1, before writing the last symbol of the packet at offset 0. The write master uses the empty field to indicate the number of unused symbols at the end of the transfer. If the last packet data is not aligned with the \textbf{symbols per beat}, the empty field indicates the number of empty symbols in the last packet data. For example, if the Avalon-ST interface has \textbf{symbols per beat} of 4, and the last packet only has 3 symbols, the empty field will be 1, indicating that one symbol (the least significant symbol in the memory map) is empty.\\
\subsubsection{Avalon-ST sink to Avalon-MM read slave}
The RX OC MEM (as noted on figure 1) is an on-chip FIFO with the ``Avalon-ST sink to Avalon-MM read slave'' configuration. In this mode, the FIFO's input is an Avalon-ST sink and the output is an Avalon-MM read slave with a width of 32 bits. The Avalon-ST input (sink) data width must also be 32 bits. Similarly to the previous on-chip FIFO configuration, the same ST interface parameters (this time as input) are configurable. Refer to the Tutorials section for a detailed specification of these parameters in the case of our design. The FIFO performs the endian conversion to conform to the output interface protocol. \\\\
Just as before, in the ``Avalon-ST sink to Avalon-MM read slave'', the on-chip FIFO has an additional parameter - \textbf{Enable packet data} - that allows to specify whether we are using packet data.\\
\begin{center}
\includegraphics[scale=0.55]{images/st_mm_mem.png}\\
\small
figure 6 - Avalon-ST to Avalon-MM Memory Map\\
\normalsize
\end{center}
If \textbf{Enable packet data} is off, read data repeatedly at address offset 0 to pop the data from the FIFO.\\\\
If \textbf{Enable packet data} is on, the Avalon-MM read master starts reading from address offset 0. If the read is valid, that is, the FIFO is not empty, both data and packet status information are popped from the FIFO. It is important to highlight that \emph{before} reading status information at address offset 1, it is necessary to \emph{first} do the data read at address offset 0 to make sure it is valid (i.e. FIFO not empty), otherwise the packet status information is not yet popped from the FIFO so it is too early to try to read its correct value. The packet status information is obtained by reading at address offset 1. Reading from address offset 1 does not pop data from the FIFO. The \texttt{error}, \texttt{channel}, \texttt{SOP}, \texttt{EOP} and \texttt{empty} fields are available at address offset 1 to determine the status of the packet data read from address offset 0. The empty field indicates the number of empty symbols in the data field. For example, if the Avalon-ST interface has symbols-per-beat of 4, and the last packet data only has 1 symbol, then the empty field will be 3 to indicate that 3 symbols (the 3 least significant symbols in the memory map) are empty. \\\\
In our design, we enabled the packet data feature for both OC FIFOs in order to have a simpler design and avoid the use of Altera’s packet-to-byte and byte-to-packet modules.\\
\subsubsection{Loopbacks}
In order to incrementally test each subset of the system, we extensively used the loopback mechanism, meaning if the output of the system matches its inputs then we can confirm that that system works correctly.\\\\
Figure 1 illustrates how many loopback modules we used and where in the whole design we inserted them in order to test basically each stage before moving to the next one.
\newpage
\subsection{Timing Design}
\subsubsection{XAUI PHY}
On the network-side (the side that interfaces the Dual XAUI daughter card through the HSMC interfce) it has transmit and receive serial interfaces that run at 4 × 3.125 Gbps. That is, it has 4 serial transmit lanes and 4 serial receive lanes that run at 3.125 Gbps.\\\\
The XAUI PHY is fed a 156.25 MHz reference clock produced by an external PLL. It uses this clock as a reference for clock data recovery (the clock is recovered from the serial input data) and for its transmit PLL. It outputs a 156.25 MHz clock (produced by clock recovery) associated with the client-side receive data and accepts a 156.25 MHz clock associated with the client-side transmit data. In our project we used the XAUI's 156.25 MHz output clock as input to its transmist clock, as well as input to the transmist and receive clock of the MAC. This configuration ensures synchronization between all interfaces. A block diagram showing the various clocks associated withe XAUI PHY is shown in figure 7.\\
\begin{center}
\includegraphics[scale=0.45]{images/xaui_timing_4.png}\\
\footnotesize
figure 7. XAUI Input and Output Clocks\\
\normalsize
\end{center}
On the client-side (the side that interfaces with the 10GBE MAC), it has SDR XGMII transmit and receive interfaces that provide 72 bits at 156.25 Mbps. The XGMII interface is an implementation of the Avalon-ST interface that lacks valid and ready signals, so data is transmitted/received on every rising clock edge. The XAUI PHY treats the datapath as two interleaved 32-bit data buses. An example of the transformation from the original data to the interleaved data is shown in figure 8.\\
\begin{center}
\includegraphics[scale=0.45]{images/xaui_timing_3.png}\\
\footnotesize
figure 8. XAUI Data Transformation\\
\normalsize
\end{center}
Because of the interleaving the start of frame transmission can start with byte 0 or byte 5. The timing diagram for byte 0 start of frame transmission is shown in figure 9, while the timing diagram for byte 5 start of frame transmission is shown in figure 10.$^{1}$\\
\begin{center}
\includegraphics[scale=0.350]{images/xaui_timing_1.png}\\
\footnotesize
figure 9. XAUI Timing Diagram (byte 0 start of frame)\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.350]{images/xaui_timing_2.png}\\ 
\footnotesize
figure 10. XAUI Timing Diagram (byte 5 start of frame)\\
\normalsize
\end{center}
\subsubsection{10GbE MAC}
As mentioned above, the input to the transmit and receive clock of the MAC is the 156.25MHz clock produced by the XAUI PHY. The network-side (the side that interfaces with the XAUI PHY) has SDR XGMII transmit and receive interfaces that transmit/receive 72 bits at 156.25 MHz. Timing diagrams showing the transmission of 8 values on the network-side is shown in figure 11 and figure 12. Timing diagrams showing the receiving of 8 values on the client-side is shown in figure 13 and figure 14.$^{2}$\\
\begin{center}
\includegraphics[scale=0.4]{images/mac_timing_1.png}\\
\footnotesize
figure 11. MAC Network-Side Timing Diagram\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.4]{images/mac_timing_2.png}\\
\footnotesize
figure 12. MAC Network-Side Timing Diagram (detailed)\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.4]{images/mac_timing_3.png}\\
\footnotesize
figure 13. MAC Client-Side Timing Diagram\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.4]{images/mac_timing_4.png}\\
\footnotesize
figure 14. MAC Client-Side Timing Diagram (detailed)\\
\normalsize
\end{center}
The interfaces on the client-side (the side that interfaces with the dual-clock FIFOs) has Avalon-ST transmit and receive interfaces that transmit/receive 64 bits at 156.25 MHz. The Avalon-ST is high bandwidth, low latency, and has (optional) packet support. It uses a ready signal to indicate when it is capable of receiving data and a valid signal to indicate when it is ready to transmit data. Its packet support uses a signal to indicate the start of packet (SOP) as well as a signal to indicate end of packet (EOP). A timing diagram for the Avalon-ST interface is shown in figure 15.$^{3}$\\
\begin{center}
\includegraphics[scale=0.30]{images/avalon_st_timing.png}\\
\footnotesize
figure 15. Avalon-ST Timing Diagram\\
\normalsize
\end{center}
\subsubsection{Tx Buffer}
The input clock to the Tx buffer is also the 156.25 MHz output clock from the XAUI PHY. The Tx buffer uses the Avalon-ST interface to transmit and receive data at 156.25 MHz. See figure 15 for an Avalon-ST timing diagram.\\
\subsubsection{DC FIFOs}
The transmit DC FIFO receives data from the client-side (the side connected to the transmit on-chip FIFO) at 50 MHz on an Avalon-ST interface and transmits data to the network-side (the side connected to the MAC) at 156.25 MHz on an Avalon-ST interface. The receive DC FIFO receives data from the network-side (the side connected to the MAC) at 156.25 MHz on an Avalon-ST interface and transmits data to the client-side (the side connected to the receive on-chip FIFO) at 50 MHz on an Avalon-ST interface. For a discussion of the Avalon-ST interface consult the MAC section.\\
\subsubsection{On-Chip Memory FIFOs}
The On-Chip FIFOs interface with HPS/JTAG Master through the Lightweight HPS-to-FPGA Bridge, as well as interface with the DC FIFOs. The client-side of the transmit on-chip FIFO (the side that interfaces with the HPS/JTAG Master) has an Avalon-MM interface and receives 32 bits at 50 MHz. The network-side of the transmit on-chip FIFO (the side connected to the transmit DC FIFO) has an Avalon-ST interface and transmits 32 bits at 50 MHz. The network-side of the receive on-chip FIFO (the side connected to the receive DC FIFO) has an Avalon-ST interface and receives 32 bits at 50 MHz. The client-side of the receive on-chip FIFO (the side that interfaces with the HPS/JTAG Master) has an Avalon-MM interface and transmits 32 bits at 50 MHz. For a discussion of the Avalon-ST interface consult the MAC section. The Avalon-MM interface provides memory-mapped read and write interfaces for master and slave components. The Avalon-MM interface associated with the both the transmit and receive FIFOs are considered slaves because the HPS/JTAG Master act as the master. Like the use of the Avalon-ST interface, the Avalon-MM interface provides packet support. Given that it is memory-mapped, status bits in registers are set to indicate start of packet (SOP) and end of packet (EOP). The SOP bit is set when the first 4 bytes (32 bits) of a packet are written or read and the EOP bit is set when the last bytes of a packet are written or read. Because the last bytes of a packet may not be aligned to 4 bytes, two additional status bits (EMPTY) are used to indicate the actual number of bytes being written or read. A timing diagram for the Avalon-MM interface is shown in figure 16.\\
\begin{center}
\includegraphics[scale=0.30]{images/avalon_mm_timing.png}\\
\footnotesize
figure 16. Avalon-MM Timing Diagram\\
\normalsize
\end{center}
\newpage
\subsection{Network Driver}
We wrote our driver in both an incremental and iterative manner. We started by writing a simple Linux character/miscellaneous driver capable of writing to and reading from memory mapped interfaces on the FPGA. This driver was heavily influenced by a driver from CSEE W4840 Lab 3 at Columbia University. The goal of this driver was to familiarize us with Linux drivers and provide the framework for writing to and reading from transmit and receive buffers in our final network driver. We then wrote a simple Linux network driver that was devoid of hardware details (it simulated all hardware in software). This driver was heavily influenced by snull.c, a driver developed by O'Reilly \& Associates as part of a Linux network driver tutorial. The goal of this driver was to familiarize us with Linux network drivers and provide the overall framework for our final network driver. We then used the simple character driver and the simple network driver to write a more complete network driver. In addition to using our previous drivers as reference, we also used Altera's Triple-Speed Ethernet MAC driver.\\\\
At load time several parameters of the driver can be set, including the pool size and timeout length. The pool size represents the size of the packet pool, which is a linked lists of in-flight packet structs used by the driver for transmit and receive and the timeout length represents the time the kernel will wait on the hardware before timing out. If the parameters are not set at load time they are set to default values. Additionally, the values of the parameters are always exposed through the sysfs so that the user can review them. An example of loading the driver with and without setting the parameters is shown below.\\
\begin{verbatim}
Without Parameters
$ insmod blazepps_ethernet

With Parameters
$ insmod blazepps_ethernet pool_size="16" timeout="10"
\end{verbatim}
The network driver is loaded as a platform driver, so when it is loaded its probe function is called. The probe function allocates and network device and then initializes and registers it. The MAC address and transmit/receive buffer resources are obtained from the driver's entry in the device tree.\\
\begin{verbatim}
blazepps_ethernet: blazepps_ethernet@20{
	compatible = "altr,blazepps_ethernet";
	reg = < 0x20 0x8 0x28 0x8>;
	reg-names = "tx_fifo", "rx_fifo";
	local-mac-address = [ 48 40 48 40 48 40 ];
};
\end{verbatim}
The network device is actually allocated as an Ethernet device, which means that the kernel initializes many of its fields. This results in the interface automatically being named "ethx," were x is the next available eth interface number. For example, if the system already has an eth0 interface, the interface associated with the network device registered by our driver will be eth1. Below we show the output of ifconfig after loading the network driver and upping the interface.\\
\begin{verbatim}
eth1      Link encap:Ethernet  HWaddr 48:40:48:40:48:40  
          inet addr:192.168.1.103  Bcast:192.168.1.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
\end{verbatim}
When an application, such as ping, uses the interface to send a packet the kernel calls the network driver transmit function. The transmit function passed an sk\_buff, which contains the contents of the packet. The network driver then writes the packet to the packet to the transmit buffer, setting the SOP, EOP, and EMPTY status bits as necessary. As a debugging feature, the driver writes all data that it writes to the transmit buffer to the system message buffer (to see the system message buffer simply use the dmesg command). When it is done writing the packet to the transmit buffer it simulates an interrupt indicating that packet transmission is complete. The reason this interrupt is simulated is because the hardware does not currently have interrupt capabilities.\\\\
Because the hardware does not have interrupt capabilities it is difficult to test true receiving, so a combination of hardware loopback and receive interrupt simulation was to test the receive pathway of the driver. More specifically, we enabled loopback between the transmit and receive buffers in hardware and simulate a receive interrupt after transmission (the process of transmission was described above). This means that soon after the driver writes the packet to the transmit buffer, it will arrive at the receive buffer and the driver will be notified that there is a packet to receive. When the network driver receives the receive interrupt it first reads the packet in from the receive buffer. As a debugging feature, the driver writes all data that it reads from the receive buffer to the system message buffer. After reading in the packet it packages it in an sk\_buff and passes it to the upper layers.\\\\
The network driver also keeps statistics on the packets it transmits and receives, which are stored in a net\_device\_stats struct. Applications, such as ping can access and report the stats.