/******************************************************************************
 * TxRx Test
 *
 * Description:
 *
 * 	This program tests the txrx_fifos driver
 *
 * Authors:
 *
 * 	Christopher Campbell (cc3769@columbia.edu)
 * 	Valeh Valiollahpour Amiri (vv2252@columbia.edu)
 * 	
 * Last Modified:
 *
 * 	April 4th, 2015
 * ***************************************************************************/

#include <stdio.h>
#include "txrx_subsys.h"
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

/* Comment next line if the Enable Packet Data 
 * feature is disabled.
 */
#define EN_PACKET_DATA
#define NUM_PKTS_READ 20
//#define PIO

int txrx_subsys_fd;

int main(int argc, const char *argv[])
{
	int i;
	char txrx_choice;
	static const char filename[] = "/dev/txrx_subsys";
	unsigned int pio_clr, pio_set;
	/* pkt_info contains information related to 
	 * SOP, EOP, empty, error and channel. It is used 
	 * when the enable packet data feature is used.
	 */
	unsigned int pkt_info;
	/* Hold data read in receive mode */
	unsigned int data_read;
	
	if (argc!= 2) {
		printf("usage: %s 0 for tx, 1 for rx\n", argv[0]);
		exit(1);
	}
	else 
		txrx_choice = atoi(argv[1]);

	printf("%d\n", txrx_choice);

	if ( (txrx_subsys_fd = open(filename, O_RDWR)) == -1 ) {
		fprintf(stderr, "could not open %s\n", filename);
		return -1;
	}
  
#ifdef PIO
	/* Clear pio */
	pio_clr = 2;
	if (ioctl(txrx_subsys_fd, PIO_CLR, &pio_clr)) {
		perror("ioctl(PIO_CLR) failed");
		return;
	}

	/* Set pio */
	pio_set = 2;
	if (ioctl(txrx_subsys_fd, PIO_SET, &pio_set)) {
		perror("ioctl(PIO_SET) failed");
		return;
}
#endif

	/**************************** TRANSMIT SOME DATA ****************************/
	if (!txrx_choice) {

#ifndef EN_PACKET_DATA
		/* Send: SOP, 21 bytes of data, EOP, valid mask for the last byte, last byte of data */
		unsigned int tx_data[] = {0x7a7a7a7a, 
					  0x01005e00, 0x00010017, 0xf293c2d5, 0x08004500, 
					  0x0046417c, 0x00000111, 0x032d803b, 0x14c2e000,
					  0x00010272, 0x02720032, 0x2fd6534e, 0x51554552,
					  0x593a6468, 0x63703437, 0x2e63732e, 0x636f6c75,
					  0x6d626961, 0x2e656475, 0x3a725a77, 0x6472533a,
					  0x78737672,  
					  0x7b7b7b7b,  0xffffffff, 0xA916C57B};
  
		for (i=0; i<26; i++) {
			if (ioctl(txrx_subsys_fd, TX_DATA, &tx_data[i])) {
				perror("ioctl(TX_DATA) failed");
				return;
			}
		}
#else
		/* Set SOP bit in the packet_info register */
		pkt_info = 0x00000001;
		if (ioctl(txrx_subsys_fd, CONTROL, &pkt_info)) {
			perror("ioctl(CONTROL) failed");
			return;
		}
		/* Send 21 bytes of data */
		unsigned int tx_data[] = {0x01005e00, 0x00010017, 0xf293c2d5, 0x08004500, 
					  0x0046417c, 0x00000111, 0x032d803b, 0x14c2e000,
					  0x00010272, 0x02720032, 0x2fd6534e, 0x51554552,
					  0x593a6468, 0x63703437, 0x2e63732e, 0x636f6c75,
					  0x6d626961, 0x2e656475, 0x3a725a77, 0x6472533a,
					  0x78737672, 
					  // last byte of data
					  0xA916C57B};
		for (i=0; i<21; i++) {
			if (ioctl(txrx_subsys_fd, TX_DATA, &tx_data[i])) {
				perror("ioctl(TX_DATA) failed");
				return;
			}
		}
		/* Set EOP bit and the two empty bits for the last byte in the packet_info register, 
		 * then send the last byte of data 
		 */
		pkt_info = 0x00000002;
		if (ioctl(txrx_subsys_fd, CONTROL, &pkt_info)) {
			perror("ioctl(CONTROL) failed");
			return;
		}
		if (ioctl(txrx_subsys_fd, TX_DATA, &tx_data[21])) {
			perror("ioctl(TX_DATA) failed");
			return;
		}
#endif       
	}

	/**************************** RECEIVE SOME DATA ****************************/
	else {
		for (i=0; i<NUM_PKTS_READ; i++) {
			if (ioctl(txrx_subsys_fd, RX_DATA, &data_read)) {
				perror("ioctl(RX_DATA) failed");
				return;
			}
			printf("data received = %04x\n", data_read);
		}
	}

	return 0;	
}
