/*
 * Device driver for interacting with a packet processors Tx/Rx FIFOs
 *
 * A Platform device implemented using the misc subsystem
 *
 * Authors:
 *
 *	Christopher Campbell (cc3769@columbia.edu)
 *	Sheng Qian (sq2168@columbia.edu)
 *	Valeh Valiollahpour Amiri (vv2252@columbia.edu)
 *
 * Last Modified:
 *
 *	April 11th, 2015
 *
 * Build Instructions:
 *
 *	"make" to build
 *	insmod txrx_subsys.ko
 *
 *	Check code style with
 *	checkpatch.pl --file --no-tree txrx_fifo.c
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include "txrx_subsys.h"

#define DRIVER_NAME "txrx_subsys"
#define CLR_OFFSET 0x14
#define SET_OFFSET 0x10
#define CSR_OFFSET 0x4

//#define PIO

/*
 * Information about our device
 */
struct txrx_dev {
	struct resource res_rx, res_tx, res_pio;
	void __iomem *virtbase_rx, *virtbase_tx;
#ifdef PIO
	void __iomem *virtbase_pio;
#endif
	u32 tx_data;
	u32 rx_data;
        u32 csr_data;
#ifdef PIO
	u32 pio_clr;
	u32 pio_set;
#endif
} dev;

/*
 * Tx
 *
 * Writes data to Tx FIFO
 */
static void tx(u32 tx_data)
{
	iowrite32(tx_data, dev.virtbase_tx);
	dev.tx_data = tx_data;
}

/*
 * Rx
 *
 * Reads data from Rx FIFO
 */
static void rx(void)
{
	dev.rx_data = ioread32(dev.virtbase_rx);
}

/*
 * Control
 *
 * Write to control & status registers
 */
static void control(u32 csr_data)
{
	iowrite32(csr_data, dev.virtbase_tx+CSR_OFFSET);
	dev.csr_data = csr_data;
}

/*
 * Status
 *
 * Read control & status registers
 */
static void status(void)
{
	dev.csr_data = ioread32(dev.virtbase_rx+CSR_OFFSET);
}

#ifdef PIO
/*
 * Pio clear and set
 *
 * Writes clear and set values in the pio register
 * (clr_set==0) => pio clear
 * (clr_set==1) => pio set 
 *
 */
static void pio_clr_set(u32 value, u8 clr_set)
{
	if (clr_set==0) {
		iowrite32(value, dev.virtbase_pio + CLR_OFFSET);
		dev.pio_clr = value;
	}
	if (clr_set==1) {
		iowrite32(value, dev.virtbase_pio + SET_OFFSET);
		dev.pio_set = value;
	}
}
#endif

/*
 * Handle ioctl() calls from userspace:
 *
 * write data to Tx FIFO - read data from Rx FIFO
 */
static long txrx_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{
	unsigned int txrx;
	unsigned int csr;
	//unsigned int pio_clr, pio_set;

	switch (cmd) {
	case (TX_DATA):
		if (copy_from_user(&txrx, (unsigned int *) arg, sizeof(int)))
			return -EACCES;
		tx(txrx);
		break;

	case (RX_DATA):
		rx();
		txrx = dev.rx_data;
		if (copy_to_user((unsigned int *) arg, &txrx, sizeof(int)))
			return -EACCES;
		break;

	case (CONTROL):
		if (copy_from_user(&csr, (unsigned int *) arg, sizeof(int)))
			return -EACCES;
		control(csr);
		break;

	case (STATUS):
		status();
		csr = dev.csr_data;
		if (copy_to_user((unsigned int *) arg, &csr, sizeof(int)))
			return -EACCES;
		break;
#ifdef PIO
	case (PIO_CLR):
		if (copy_from_user(&pio_clr, (unsigned int *) arg, sizeof(int)))
			return -EACCES;
		pio_clr_set(pio_clr, 0);
		break;

	case (PIO_SET):
		if (copy_from_user(&pio_set, (unsigned int *) arg, sizeof(int)))
			return -EACCES;
		pio_clr_set(pio_set, 1);
		break;
#endif

	default:
		return -EINVAL;
	}

	return 0;
}

/* The operations our device knows how to do */
static const struct file_operations txrx_subsys_fops = {
	.owner = THIS_MODULE,
	.unlocked_ioctl = txrx_ioctl,
};

/* Information about our device for the "misc" framework -- like a char dev */
static struct miscdevice txrx_subsys_misc_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = DRIVER_NAME,
	.fops = &txrx_subsys_fops,
};

/*
 * Initialization code: get resources (registers)
 */
static int __init txrx_subsys_probe(struct platform_device *pdev)
{
	int ret;

	/* Register ourselves as a misc device: creates /dev/txrx_subsys */
	ret = misc_register(&txrx_subsys_misc_device);

	/* Rx registers */
	ret = of_address_to_resource(pdev->dev.of_node, 0, &dev.res_rx);
	if (ret) {
		ret = -ENOENT;
		goto out_deregister_rx;
	}
	if (request_mem_region(dev.res_rx.start, resource_size(&dev.res_rx),
			       DRIVER_NAME) == NULL) {
		ret = -EBUSY;
		goto out_deregister_rx;
	}

	dev.virtbase_rx = of_iomap(pdev->dev.of_node, 0);
	if (dev.virtbase_rx == NULL) {
		ret = -ENOMEM;
		goto out_release_rx;
	}

	/* Tx registers */
	ret = of_address_to_resource(pdev->dev.of_node, 1, &dev.res_tx);
	if (ret) {
		ret = -ENOENT;
		goto out_deregister_tx;
	}
	if (request_mem_region(dev.res_tx.start, resource_size(&dev.res_tx),
			       DRIVER_NAME) == NULL) {
		ret = -EBUSY;
		goto out_deregister_tx;
	}

	dev.virtbase_tx = of_iomap(pdev->dev.of_node, 1);
	if (dev.virtbase_tx == NULL) {
		ret = -ENOMEM;
		goto out_release_tx;
	}

#ifdef PIO
	/* Pio registers */
	ret = of_address_to_resource(pdev->dev.of_node, 0, &dev.res_pio);
	if (ret) {
		ret = -ENOENT;
		goto out_deregister_pio;
	}
	if (request_mem_region(dev.res_pio.start,
			       resource_size(&dev.res_pio),
			       DRIVER_NAME) == NULL) {
		ret = -EBUSY;
		goto out_deregister_pio;
	}
	dev.virtbase_pio = of_iomap(pdev->dev.of_node, 0);
	if (dev.virtbase_pio == NULL) {
		ret = -ENOMEM;
		goto out_release_pio;
	}
#endif

	return 0;

#ifdef PIO
 out_release_pio:
	release_mem_region(dev.res_pio.start, resource_size(&dev.res_pio));
 out_deregister_pio:
#endif

 out_release_tx:
	release_mem_region(dev.res_tx.start, resource_size(&dev.res_tx));
 out_deregister_tx:
 out_release_rx:
	release_mem_region(dev.res_rx.start, resource_size(&dev.res_rx));
 out_deregister_rx:
	misc_deregister(&txrx_subsys_misc_device);
	return ret;
}

/* Clean-up code: release resources */
static int txrx_subsys_remove(struct platform_device *pdev)
{
	iounmap(dev.virtbase_rx);
	release_mem_region(dev.res_rx.start, resource_size(&dev.res_rx));
	iounmap(dev.virtbase_tx);
	release_mem_region(dev.res_tx.start, resource_size(&dev.res_tx));
#ifdef PIO
	iounmap(dev.virtbase_pio);
	release_mem_region(dev.res_pio.start,
			   resource_size(&dev.res_pio));
#endif
	misc_deregister(&txrx_subsys_misc_device);
	return 0;
}

/* Which "compatible" string(s) to search for in the Device Tree */
#ifdef CONFIG_OF
static const struct of_device_id txrx_subsys_of_match[] = {
	{.compatible = "altr,txrx_subsys"},
	{},
};

MODULE_DEVICE_TABLE(of, txrx_subsys_of_match);
#endif

/* Information for registering ourselves as a "platform" driver */
static struct platform_driver txrx_subsys_driver = {
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(txrx_subsys_of_match),
	},
	.remove = __exit_p(txrx_subsys_remove),
};

/* Called when the module is loaded: set things up */
static int __init txrx_subsys_init(void)
{
	pr_info(DRIVER_NAME ": init\n");
	return platform_driver_probe(&txrx_subsys_driver, txrx_subsys_probe);
}

/* Called when the module is unloaded: release resources */
static void __exit txrx_subsys_exit(void)
{
	platform_driver_unregister(&txrx_subsys_driver);
	pr_info(DRIVER_NAME ": exit\n");
}

module_init(txrx_subsys_init);
module_exit(txrx_subsys_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Christopher Campbell, Sheng Qian, Valeh Valiollahpour Amiri");
MODULE_DESCRIPTION("TxRx Subsystem");
