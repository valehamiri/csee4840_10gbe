variable s_path ""
set s_path [lindex [get_service_paths master] 0]
open_service master $s_path

set tx_oc_fifo 0x20
set tx_oc_fifo_ctrl 0x24
set rx_oc_fifo 0x28
set rx_oc_fifo_ctrl 0x2C
set num 20

set SOP   0x00000001
set EOP   0x00000006
set EMPTY 0x0000000C

set sop_expr   "*** SOP ***"
set eop_expr   "*** EOP ***"
set empty_expr "*** empty bits ***"

puts "*** Begin Write ***";
master_write_32 $s_path $tx_oc_fifo_ctrl $SOP
for {set i 0} {$i < [expr $num]} {incr i} {
    master_write_32 $s_path $tx_oc_fifo $i;
}
master_write_32 $s_path $tx_oc_fifo_ctrl $EOP
master_write_32 $s_path $tx_oc_fifo 0xA916C57B
puts "*** End Write ***";

puts "*** Begin Read ***";
for {set i 0} {$i < [expr $num + 1]} {incr i} {
    puts "########i######";
    puts $i;

    puts "Data bits";
    set a [master_read_32 $s_path $rx_oc_fifo 1]
    puts $a;

    set b [master_read_32 $s_path $rx_oc_fifo_ctrl 1]
    puts "Status bits"
    puts $b;    
    if { $b & $SOP } {
	puts $sop_expr;
    }
    if { $b & $EOP } {
	puts $eop_expr;
	set c [expr ($b & $EMPTY) >> 2]
	puts $empty_expr;
	puts $c;
    }
}
puts "*** End Read ***";

puts "End"
