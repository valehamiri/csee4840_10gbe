 module txbuff_rx_loopback(
    input   logic       clk,
    input   logic [3:0] rx_mac_out,
    output  logic [3:0] txbuff_in);

    always @(posedge clk) begin
        txbuff_in <= rx_mac_out;
    end
endmodule