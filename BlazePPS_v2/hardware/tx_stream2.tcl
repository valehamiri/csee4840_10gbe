variable infile 0
variable s_path ""
set s_path [lindex [get_service_paths master] 0]
open_service master $s_path

set src_base 0x20
set src_base_control 0x24

puts "sop"

#Start of packet
master_write_32 $s_path $src_base_control 0x00000001

#Source and destination address
#master_write_32 $s_path $src_base 0x000f5308
master_write_32 $s_path $src_base 0x08530f00
#master_write_32 $s_path $src_base 0xcffd0101
master_write_32 $s_path $src_base 0x0101fdcf

#Length
#master_write_32 $s_path $src_base 0x00000004
master_write_32 $s_path $src_base 0x04000000

#Payload
master_write_32 $s_path $src_base 0xffffffff

#End of packet
master_write_32 $s_path $src_base_control 0x00000003
#Last 4 bytes of payload
master_write_32 $s_path $src_base 0x1256ff42
