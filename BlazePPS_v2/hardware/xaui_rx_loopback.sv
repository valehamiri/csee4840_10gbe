module xaui_rx_loopback(
    input  logic clk,
    input  logic [71:0] rx_out,
    output logic [71:0] tx_in);

    always @(posedge clk) begin
        tx_in <= rx_out;
    end
endmodule
