// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
//pragma protect begin_protected
//pragma protect key_keyowner=Cadence Design Systems.
//pragma protect key_keyname=CDS_KEY
//pragma protect key_method=RC5
//pragma protect key_block
PT6ZOJVdvnF4oDCLFgPu9PRQ9dlKbhZJhlp6j4y23ZWgxICibevePMTQ1PUdUnDz
aRsqHeKpHwX6v1EaBmP8kStie1UR1zWEyT3Vhh9bOAjmsf7N7tZ72PKaya2oj14q
o6sPGzfcIkxqoH/KTyHVwgPb2McrcO3VOyExPrbhCmRWNdO+65260A==
//pragma protect end_key_block
//pragma protect digest_block
a0vOADz9oYqVc8OrDme8HsRDmY0=
//pragma protect end_digest_block
//pragma protect data_block
yT+kek2za8usw62yHF3JhEsBkxF1rrmzE3/WQcYZDAJ1lj/Lz5WKfAHiUH0SOUSP
WjS3GEOYMQDI2Qv1m8wHEXklz4VgNoQEPoCvTF7PwpKEJyFvtFLWFvAn4MUBwhUr
3SiQQ9x6ULS1FmhQM8pPc7SCHXeEzehQ2lYDgu5urikLjMLpx54vvEEPvfeDTNRV
DxgPGVNUTTh/heEGl+EGYsKPbM13O+XoPyJyM6PfzOVKwxWZUDhHP+lYQUvS8DrO
ml5uNQfwIPVUKskKouyA/JajRwQq3Uqt+gXBpOib8Vkb0ChGcVODdNghItiT4Cj3
JB+qHgOm3Q6p3dUhlHV02bmVEqgDEwWmZXrKx/tX9vnZ43y2RKV2ECPitGatkXwE
IdtBe3guvMYeUoOWXUo4+R2mc/R0LX6BRA3/QyWt5WKsekIe/S4x7upTgUIGOl4B
Waxp8tdEI/jr3WpPKr3QqlXiKdC0I/t6u75qwke7Z5DMlwLrJdN7+ChIScOyW2HW
9KVsxfGhaN63euwNd/cvZ4+RngFbqJi6dT41qTMYShUHY3jqAISCPA1YoYQm1r2F
5sLq9VBJBXsM4im5EnCQraFZqPzxxo6qul2niYX+GtqBzXYy1nOylPv+CRFDMVJH
9BTxq6L45x/IpYmd4SFeJiYU4Z3x0fCXkZxOSVYzxC20Y7AO3dbBi3JBOmSm3WF8
JzoI7T+peQRhnyiM5PxXHEboAzRmcmU0TEZXtCnZox9TJwpZ6zTml8C9LTJAG1qV
wkpPXfNBV2TE8BMy1hNJMZx+IfiBiS0HKNjxnzkAkHtVBJqrjtqqxvkLuIHm9r8H
TNJHLfX4GAkzdBo1q0NLXZ08AB3DntAG0vqzqnFF+zbnEPjcla6QaYg9NO+N6eNf
CdbNpweRgWyjR/zY5FclFIjCTnG6Mkmn049J27vp/QX5tNXKoZNOLR+Xhc2GWHWI
tSiUHvUFv4V+DRKfdi8upIP3LJTH0rWM6zXsCjoSt9CeV2/i0DEDoERUdRUmQ8OG
xnEfyMK9wWNOIU7V+zryqbkX/nEMuy/IMfdO7mn0nhhzrxqrveK0UtFNN3wdNWoa
VUlaJKnbI3PqVaKAfaCEDJ3qdPSnH9hNbqOfxwywVivlReYpPHVDflfMcT/4YeAf
gfKBxax46U9nQey34SCRsTOFV2BHdXpW1waY4oGLheVRsoXK2whV7XejojzauNIn
PbOpedREkDs1dqVqysz/lcjBh0bfAxJAFn1IJzWzOepxJttjmfFHyUj849apETZC
1fOmqqDJ6MuGK5q+Kz3w0wbbH8aMMyIuzsnDckefEyKX5SVLe6COx6EesQHdJaVb
dum8N1ekD27cBKtSMYB+1i3D8gqbSFl4BV+T7/HaaWx9FWtjWzes5uScVzlxzkuM
k99lg9E4Q1L17AxtLCccqZBvVn/dXSYqZYpISbdIfqKxGCaFdbbW4kVT25K91IG4
5sI5bagNDl5StcYrI3bQ4Ur1fZvhSDqKgvZphKVVQxE4DxWY4wh38xaYT6mWLTt6
UHGTUVbwdroQk4WS1sno4cXW1Ufb35DeZk0MKWbY7ccnQIJ5BCW6QIcKHt5vsSuh
rk8CNRDpoQyvn0PBKIafE3DNOZu064sacH2RIrX31ibdX0tUTD+dId58gneGNRAe
rSg3JJZHWxL2QBVbdMxeSGJNn8ij5wNc9iWxMAZ+KXrkYPRRQoqa5i8sP9j1H2O4
OIp5NfvTeSdCeHT8tjTnggeyyuXgolCOWdVX4ORfKUoHU9CV74OfZmsAHGpgj4I7
Yz9qI6l7wZIV4oCLncI4Gz9MoNalcFO7pMV9Cdd4gdLj07sOKAyErsXqH4khFaj7
ETU2TBgPJbNiuzqMLLacZkjc3D9xm7FZbIzyQ+doqV2L+Bet9HzNjRFQKgyIk5Os
X6l3LkmgBjd2ILeEWa/Cgstm9LrX/RkyucqceSxbvKHUcx5nxJH0BQb0MhQ4YcNS
iuDjF9GAAXE6OoXpbxOhAIO+jajkXSvdUqId9nrFzF/P1Twm9CUooAvTDjVX7i2G
xPe/0B3yOAxGK0ojOJv+UDlkFV3rFrk7bX9MGLwqr3gFPjYXELTEeZQt6uyHmT8a
KTjObjqMstC+cr4FKQ5oAQzZ0clZUBxxlhBGEMJEJVCS+0Lgldpx1sWY+pj03Vav
vNescMPLDbxeL/+yjgHSI8WIyEIThg8jicNXI/gg5fJsvR+mL3JQtEjKzmcz3yCc
VbF2kzLwjhcPXKxQiOOf5qn3S9EFQL+bdJyoMjQ9DxvhfDf4iuaK08rhFgq9Ee0d
QqpJ/janyh98V35PNsbnQrN1RqVhtac9HLiwYahwYNkj0yeM26EqlREUJylbrW/r
AXzKyuHSu4KdvXn9eYj1SlLsqOgdLGKuLkRPZ1kXtnlD9i7W9CDwIHBxIPpMCI/u
PBuwENdflsP/c7wu7nEgA0nhRBxJwWHemM7/FQEQkw6UiZk+mIItX6DAOqpilTlP
3keWAZBJIW2m4ZzlT2LV0H3TR6qoy6cbEzyOpxgxvh8FKZg0miCngTl+F0glrjYf
8U9y9n4J3mmA5BF0bjycksdkuOnoGValM7md6Mi5t2i4QpJzsUznEMTAxSEX9cXp
zQbkVzm+Fo4f/OJQtBno64dB6GP4VicU2wrbBNae2xnN/ok532t8MHpiN4WTdYlp
65oniqZl8fbDwIJmX++/gAPjt7fB1RCb7E+LOHbpZj6uS5MWdB5UlY62Id/jd8eJ
JwJgsU4Zc4a8G41030GhsdfbXIYBZxDGiPr3LYSQFNXM5pHMew+Qo3xRrvh2V70X
/Iq9RGj1OvIMhgsFVZfMsU6Bun4snjcwuhjOpLJLmVP43xvs+fKXeJMN0IVp0zhY
QqReiyB28jBVsWmJf3ZZvAFoX5oj8VVNUy+8p5/kYqlLh8DvdjnOu5dn2xNtoAiq
99zGzZ2OjmQezm9tbFqR2FYI/wDw8QULNQ0sY3noJlscvqrR0B0CAph9fP9Q6rE9
gXkdVE2N5nW8PqNeiSeWV8RsaJHsp6k/RMrhRDSiQxSTuRVm3qUUihCzIG1lYSXv
YF4/j82adhn2TztADOLgK4EeFGySuRQn2PfygdPFgkxtunMl/pKZZFJ1GhpglQDf
6DgQBwFjHmevAS8EZgE+NJlHhC4wSCO3YnbIxIpXdKWNlgEMOKkWO6TkpbyHSl9H
xFZRkDJOdewG6bU8USbXmiuDQZASdO4hvAUYaoQNbddrw3I4ufUbfc2h5dii6u0Q
3krsNi53M0MG/S8ctiKcazZDCh0F5a+9HsHoFhy78ieWG8y7gpjglCOeuK9Pufef
ozQ4V98mlQkboTb0zyxoEg==
//pragma protect end_data_block
//pragma protect digest_block
yVcbiTzxDLcIaVsPi8ZpxyIqq3s=
//pragma protect end_digest_block
//pragma protect end_protected
