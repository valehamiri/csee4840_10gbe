// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HT DVAH\3@'"J%%<0)B@7L[AU_><T(;]J^X ??&T&65^;<'^D,IY=P@  
HWWK'Y'&"ZD;F&6<S'[%\9V&X'A=YT4E1;^G"RLQ5%##-AL3TB.9E7   
HE"%.<J>'"2JPD2 U#U\76]@(%TQS/3GT2@R3EQU7U%]:'38\+QSMWP  
H:5Z(VNWE CJURJ<(@0E@H4"SB%=QD#@-(W["5416S4G9W0TLE*,TR0  
HH ZC1-#SYZ>?#N,]D:D'H0.*&P8DUKIH(1,5L8-S%9T993I< K!7H0  
`pragma protect encoding=(enctype="uuencode",bytes=5152        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@& 8?SDAX*8-LC3"$'_CWOK89WN=^-?,;#45+(IFEB38 
@PE3\:+TZEC\IES9^3M0(C2?>*9Z6SKE3861UL!S54'4 
@W'.MOK6I?3Z0A_T6@/',FKV-+,XPU="U@BAL[J"77T\ 
@4/P &;!([;'D*' TSNJ^'L3QYZ8"K3C^"AF3\*SQL8T 
@JTKN/GQAIG3B2XVE0!BU7DVON0P_.D-F%KGK6A$10H< 
@U^844E^L9^^G&&.J-.G&W9[[PJ>!?6$<AV=Q#<J4URX 
@;(8]T?'O=2L-<RMSI?)>4IRU,IZ#CJPR63G;@NFU(8X 
@PR99)SS\$8+$K4Z MQ5^]3W;(EFLY0&%!D-$U_A2K54 
@$WU+H)KKUCC8&A22 5-_MO8WW_0&?V,L>0&H@)XG*!( 
@???7"9W*>S8.Z=>>"%OQEUY6?7DL)P<N=5L #VJNRQ0 
@7*IX-U*H8]=]C;J]B]9Q)LK9+D$CI6,3!C4'= M&[M\ 
@@ N:T_*84GQK#=\8K-,74B\[WMMG>8J]X-BAL4$)4Y\ 
@&YHW96R^L34_G>;*'[8Q@,/O*>#3/9<&#Y;26X*:N]H 
@;$"HT$/I(!1%&V ZP:)]OTW='DS-W&KATYEE W7?5JL 
@F72>N''6DZQA<XK, >CCW@KY<JKBVE&2.Q['8B^UT)4 
@=28:L7M'#ZWVZ4RT7A:+\W2M22]$;195*C+5-W#72LX 
@!]0XRR/MKDM2G%@;V]&]+1*\K^#3^N\UF@L"&*53&YL 
@&OW+X$ J K#_I]#")1$C/L6P3AL8L?,=E60;7DD5KJL 
@F"!><?2ETSVB=J0]0U?IQD!:2R!,BT,C8BAJG8B@:#  
@AE0P!GR,F OG6?>==+E.*BPVK2]%TFN#%:3*^Q'']L< 
@08I92AH[-V<>1T;5E<"J5G'"4<Q/-!.=F<Z[.SA99^( 
@Y^\ Q4=,E#B7\1O\YSUU*"JR<"$JB7BLF$]TW)\PO=8 
@N_S&K.%,/CY,N,DB$YI,6SL? Q#5[?YXD_0#BD+>&(@ 
@7;IY0JT>4^78;4&FE6UH 4&,:+9SPP+NX6 SG8J4>BP 
@-[YJ<RBA0SQBL%.K)0/!!D,'WD^ ;<*\_E3'#9&4;W4 
@D9'V[M^A%RU)+2(@V.!99RSMQ3H$O':QGPHD#!^O4)L 
@-$]/)Y6\6*<X^,YLQ:QNO^$Q-$@D7L8#MR?A^>'0/9  
@L^/1=;I"L^S"E$!-ME>+CITTC/D: %NYI9ZD,-/HWL$ 
@%0[0Y:#_PJG*G';;\H\W=1H!5;PX BLWDU+T :E-50( 
@_V9B-^Y-H/<'HL8M6,!^.91VR1[(B(@%_/<*NC':&9@ 
@<P28$7(3Y!K3JXNLW^0S,G1F)K0U]E5P%GN<\N+2K;4 
@DVE*> 0'US'4HM\W"6OK@[^!S8F4R8_D2O'(L%;?Z.@ 
@'2X0IF[).7K7_25@# W8YM?8 MA&S*!&%/51/*ZW&ND 
@9,,J=H:Y\837D2!"GQFY)K-?O?1HO8VH[U#$;N@-7.4 
@(+"T!KGF,71B@0[I%P!8799-;1K]G<:,.YK6ME#DY=H 
@)S,)R_C%.;+*+*AM_XQ(TUX@:7.0Z(D'T ,Z^SUQP2, 
@2>Q&CF8MSWQ20X &%_X/Z_+()EL!GF!A>$U2:J=XFX$ 
@;Q^/^)2S5I!>/(D=KZWD+1Y"WC,B11>Y*G'HC36RUQ@ 
@?B[Y/Y4V</U'*+C))&E*+R0[OS"]_2#]V1S>%WU/O(  
@;N/-035[]_%8I9(]?<SB-SCPAPN4IXFU2BDE?VRKU5< 
@#L96P*ANBDN_Z]P;6-;E:+H02G1!PE.34RV+.A7%H@X 
@-[Q_US92T,GAQW<Z=7FLLB%B>PH_%F_F0Y#F'O[1H1( 
@V)J@YS5#'<5%Q!T5Z!WO[;XVP8DPA.LOQ"=6^"6H=A8 
@:,=E5!P]=O6#V"A"V="!REA'_X4!!VJU*'X2ZO,&,M( 
@%#K_+T7L]%^[PA;( "&EGB0AK<<I*K?'[W',%TWS)H\ 
@U:9/4:MWBX<$0 P!G?@LLS:(8)L']5B6]N1,^V['1VX 
@+=6']<L?-/[P85ZK11]W,&R\D!>KU]H.<G O!('EW[T 
@YBR$)#V%5FBH,47I%C?GY'97YS%G+&=/<)Z5@JQ #K( 
@(P" S@-[Z*(*,'!8VF,5\R<H.EW60K@Y+SSJ@RV!-JP 
@5+A$SG\N5-QWDMI71_@71W:#EE4J0;>3Y"(IZ"QT N0 
@^UBHH6$R#R-)H9 2 *0?=7!I[RS$I:DZT+\@3D$<C\  
@Q6%N6X3<NL4^[IS-\>O>?<9S:^GX?XI!960.$7^AAI$ 
@5?<VAAQ=E'=VEYLW"QT8&RG(+1CK/S)HIO@_U"CNV$0 
@!0!J"BY<"5K-9O[T84BJ0CFA9^(:I _H1YE$P/LG5"X 
@DV++\Y,KW/%\N)4\LZ5+G*Y%A?(+N9T4'R(%@IYSG6  
@*BA4#7+W,#V$@VN/:%SYD2ND9\DPJ'8N!M*D\WL8L$4 
@5H=2&[S['PM5<WG:RM(4<%N)O*H%YK=YG]?O<]>',6$ 
@RWLP-%MS?\Y[!^@:>%"V2:Z*&FKJG&._@279N!8.)X  
@(=6'&661MQO-;<,_=.>Z4<M!((R1H:I$X.@+ %Z(DP4 
@!\1TXB-3'5?2)L7\W/+V8_6H*>D,WMDMO:Z/(W@%?FH 
@0%I#]\RC;L*"Q^,?"!+C9>.FU1RR>=TU)($!CZ8LR,< 
@J1/]*UW8SL*];)IKUX38K:]MG$D$?TSYEFD%E^@VK\D 
@DG.@%L.W=''JY"Z\!?RZ_ZC)NT0" 'CU+]A':2C4?Y$ 
@31,O;:D;W+5<"Q(T$)WJV,H%Q9^J8UP1W EH%370IWT 
@W38]P7'.4_^,MO:3W0'2,= *:5#[.FC4Q\\BYLYC<BL 
@(49-4U@!J.=['1WVP!T9\*-CG95/[17VBI?2ON>90W< 
@(8&'X_ M3ON-HRDT0@\>F"0]"["<2J(1\R@VGC430!  
@P <)$[ Y8UU$66BW\S[5D8M T=DU'7?5BA5CD5GV\VD 
@@D,N/-").[8>2<<A7WJ4O!(GLH9GSOX [5_->=\'O8\ 
@%_1.5HUUS0K"RNU2\I]#(3PV$"B2^,0II(7VK$(W&8D 
@5AI*G$+C;+E@9G0$PPKW$YP/B!5LM:LORK.@Z,6*1S$ 
@83Z!6Z*9^Y>A(,^6(=O3/"+()$:Q+$ V^Z_6;](])LT 
@VRA70C18I[5EJK,X@$I;!H1O.+*L^UU5@0K8DX"!B?H 
@+;ZMA=83Z]FW/!<N?HL*&1-H#3K"T+N]J%<\H_G8!>\ 
@R&GT(V""G_4'OVT:YP"\^X<UB\Q#-]KM'5]8GD7*FED 
@F+3E2LZ\JA@KQE5'4^9?.U()#-4X*WA;/='XJ.";5VL 
@*\S, 4\"%JX?D521D4)YA;=A!%3A\ [>\) .J%)JJ80 
@O>3I3R!W;?J@3Q>=@]Z8*BN$6Y(-5.*>;).C>YY?(]< 
@^BAPV\9@?)CE1*YI,G"%RCP@J(:0CRAZBQB<]+W$T,$ 
@>-2XWF/E1,*</-K)/R]4]?J <@!BQ'3^+28+KKI\038 
@/1*E"WHJ4J^S&8N(7^WVT&^,?Q.714TR_*;3$(I!7F< 
@JF31ZZ?JXPM9;C8U%?J7I<>TV0.D4UIO=&59]_1NI7L 
@A3<Q+JC@-V"_+.X#P<K3C@ZL#7,_GXTZ4;5]TA*J4T$ 
@9GX3WM4A YA"L8Y;8?M+<9C#_-Y'B':^<:E_"8\A-6P 
@<E0)^#0DH3SVOJA+;%?+ S24*9WB]Q]G"+TFJM+UZ90 
@<T^5* >%F]&&4TNR*Z?0T'H*1++<-98CLKH1&$D4Z&0 
@[!]K^*WU;@E$-(#"EHWN\?E]W]159I ^(%"@MIHLW"L 
@"4TW2+)9=W,\9*>XH"P>D=#Y$@*5O(391PGAX$Q7P#< 
@7UB<;C9-1_5%Y.3A;E=!-3H OEA&Y#MYU;W21/;LES  
@17DS7.75*S"">@UA(N+@2TG:U,Y+[CB!&2>;M3BDPTP 
@E& H%3K%E4#%"9G/00QBYW]UPI^[-3;R+3SR$0X_'OT 
@8>'34-3]='75_9 Y\:"RF&\LYZZM9$IHR+.7I?C&\4\ 
@1NV&A.!,YB>IV(G;VR!+3K<9,NM.J>/WE7C_-0M]/=X 
@CCSWXW=JZP?#7N:OW57/0P&]=K>3#RSY <UIFEAIIR0 
@K0[M9+!6)]2><A2=_')J=R!H=GH0JPCL-<:-,$"$NEP 
@)K5_SA&(^D/IX1Y;:\44Y8=LER@TEZ X->6N6KS'LVP 
@IRC! 12RKX!;1Q%%7F5, C61+#0^D_I0U;*Q;$I&>1T 
@(J]_Q&CP=E:$R#O9U8F+S%B0_43>*'(H)R:;BY7#2VD 
@R:42]2#J\>ZQB2N_U(_X=XKG"%Y1_.,*I8C?]7=B0IH 
@=KE5GU^SV1[ BL22AFJ/:_ZJ+.+VAK$^94)77X+H^IL 
@G4_!7[F-/]X/MO66+S U#,EGP'V7HP-_D$\NV30>\3T 
@R=3CQB !5I48X6I/W,X :V6TGI9X#'7;,[RYR=M S,< 
@D](2=QW7D@FXS>9:-X92D#?K<WM59ZM36/@(P"VP%"D 
@<^4(,YBY=MDB0"S$^[V0*?L>FP[NEQ>AF1+N)H@(/G8 
@H)<1WG!VHE?2$*UF!Y'BNK,.", UA_/1-++9V.%H/.$ 
@@ 5P+ 9.QJ; 8ZJP'E>:5D0X)GW0 (?%\KD."7TQ$D\ 
@RK[R +\?RL(QG-)(WG2F+Y,MIRHI1OXAEKJSL).=M<  
@D<[230O#KO66>P8PP0F1334<K.TGE^]X9%P\,C(1N;$ 
@W:=I"IP>I&H;0DSHI7T*1R1^A>ZI;()5G$3\G>7G#R8 
@F +PF$+^3QR/PG:@^XL<\4@FM66R4;D'/@;35\8 _<D 
@,A/N%[ ;T3$F-1K Y26;--A#SET=AE!#&!)R$ZC5W:  
@.C^6[K.0[O<:GK+K"+:4Z%-W8@'F< Z*6.AG;R1F(\4 
@N.N?N]?F.J0'=8:=6_=33CY5-QZ'("3.-=U_W;+2.B, 
@0;5H!"V/:/W)H K.5/M*\YT!!EWQ_ED=':U"=VQ?C.8 
@K_%87@HMSM== %!U[#RO#6<E\AP"W>NX<X/-]$_X),@ 
@MPI5(1QQQ(=/-VPC(Z]SJF<12#[K$R;5FQ*L4JL3;G< 
@$3SG:-"[#0B3XF\$LW%R>K+KS9?G12$@7\F4Q+WUIT0 
@*B%\+TO'7RU64O9-JS;SH4-H,/9R5WE9LBVH_]FW2/$ 
@UEX*=.SC;(UGAV:N%^U'2-N'&!176K!)M;P!N.E.5E@ 
@,(%2!9]AR:K$PY>;H94^05%I; %J9L/<0?V[[ASHC!P 
@^+1+L/M::22O"PL/KTZ<KJC:E)#0CF(B:YJ\U.PF_V0 
@.6K;[JGKSSRX4LQ:W\2LD)4O>-0/4QRZ'K\YOX/0';8 
@)\&0<9 ,Q(>#/7,?)\VC.B)*NYT7+J+GIP14")KG4[, 
@;E:14V?N<XL!AQMXD=>H=<X>*QHG]TW@I(7"[(.=*SD 
@!U]!^'G_5M]*0]790RS/#JISE18YJQ*4(LIT5FN:_<( 
@P:;)>#Q7%1@!0@IL*,3[9@?<I$PB_0",MGIJKV;D%G, 
@XN2 &P+*N<T&E(I9:U9U3F>W$B+OP^9C*GN5[)YYW8  
@QMQB3=Z9+KW(##(LY?H%1TB]:PZ/WO%^(1L(#6+8^.@ 
@H%^3T7GO*A%XPZ;>N@ !(OAB7<-P'@&F;9E&$C);/+$ 
@=T<?/6BD!%]]"EO4!=O7V&@X]K.U6"E<%DM@::,&-+< 
@>FQHNK\__I*2*G!/=U?7(,=Z 06U-DB8+\* HSD[E:X 
@-8UG!7@1D8M UEUP624[%_98@*JA.RA7+9(P@X-$V;$ 
@;7;G)^.2FYS5F[212-SQUX4N.GM.D1F6L;#$&OO-(@X 
@?P68K!@R'QVZ-N6^>@FWCJ!I!W@G<-Z=0RDLOOYZI\, 
@M3?FER-.D=@,:M%:#%@!XI$]4FCG(QNPZ1)1&@Q]@8, 
@^P.([[<$*ZVW;&.DU.A%_M\"50 ^,1X<*O5B1,28CQ< 
@/3<@>3DKVL+\7=PXK^XYY)^\?QB [":[K.G)E238.., 
@=H3^U N81U>\2F;P:IY,@_JIS!^,3_L'UN5D]H/_K'L 
@P?'O6K-W5L:5%0-)F5:7&;\O(V"Z8.O=\:EQ: .BUOL 
@4!"ANUR[>L"PI_2Z89$%\O _89FK#PA[M]JM@3;$^J\ 
@,''+L *E$)66MI#"<21R3R4Y,DM%;J@MN$@U\:F9J+0 
@NZ!T)W5&_'_BP7@7'B!&=<'\GSII YO@5N-+/4$A('T 
@#>%NAQ^$*)HFS"A7P282/3"NWX2)E=O W<.=M^9O+4L 
@9Y$0271,;&=7>HPY62W*]%]I.F'415C<6OZSGR^A[3@ 
@YE*K)YJCU\$'VA<+V/7YR8#Q=?<05'+L8O2+9,]DCNL 
@Q_H!O-ZI3W+5\1*-(B_!F =SD "*/K]6"1%Z &SY:'0 
@[%![]=<XCV.\.5[WXJ&R@W?ZP\PKN8(U;&E<(^;<JN( 
@[B-X,Z$]5]10,\'4<]>AZYP/:+%R#5ZN'9]F$_/$2?P 
@N/$]VV6UO6(![H7U6<+I>^_,%;5QFM#JJBB/!F#_9T@ 
@"\XQIVA0=(S=('^I&\)Z9HT#"8YG6N[$-!559E+JNYD 
@D2%U9ZQ?KD':35-@_NZA<G9-^N#O *7S<B$A]QM]%V8 
@'W:O-[J6,R^_';/"SC;.&%+]SM)BEW6''BG_.T13$3P 
@0N*?<@.(V.G;J&UA-SFHU@5W,B,!7K8E,M49V"OX$70 
@:\4W>LL&1E/N=TZ4O4_;(<I.AF+ *#S]$9(R%IZ** , 
@S,U_;;&+#RB["PQSS%P,W(,.UD8%I-Y9ELPJY\@@.'\ 
@&GY.9, 09,-X;P#:7'G_RU_G4+R&0)(B':$))3GUO=< 
@0XI7E"S,%PU2HB(;R/C,JK[&EGRS5&6Q4XC&$?_$@M0 
@ _+EZ*,)0-6&12"DN9<8R:\6CWT8U8"D1H^C^*FP?8H 
@32G9"@X%;D*UACB^ O\'[O<&*_N9>AO:CO_ML**#L5T 
@7/,$RTH:P]K/Z]Q_L6V99.^B=770PI- Q=FUT,RI5=@ 
09(0W-.EZA.5!2*<:9G<,_@  
0C\+T/!#7Q3;;PO^@<_*L'0  
`pragma protect end_protected
