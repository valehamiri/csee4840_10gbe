// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H4Y>(1$;)4/7$Y4GT/E[;Z^!85;[TCM3-(H0LDIV?^K$=1JJ^@P6<9P  
H7)_@&*M;V>%%0:$]CT6"-(KU72_QUR.9S%8A!D]5Q>V:'[_,-KAD!0  
HW_&]L9%/99=A NB)U@T_DV5U1D/Y$BSWACP/G)/*0A'_F#J:!_K1:   
HV/=\:2F=*V :#VG/IRQ#\]^W.F (MO<PA2J'TWH<4U-Y((8<+,OXU   
H#U:,2ES@/P3:%_H$ZR I>3:AS/K^Q^*'+V;,K/W3_#BX,UT\W>[XN   
`pragma protect encoding=(enctype="uuencode",bytes=2304        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@^V3O;1U<];WRDZMW^%]E]LAQ4)U]2K3([#[Q&H??]$P 
@O>R88"-U5E>".<\%9Q(!@8C6=!DO<,T H,JB!$_<XL$ 
@A^_H+.8\7G; US%%#?27T'^G?\ :8C-X<P+Y<<1 W]P 
@=_*AX)=#N1P?0YFFF0>S0S7T4Q$G9YVI6Q?4V@X]8.L 
@$J!F1ZAAOK"?GBO#W@BGL=%U\0-^SQR%#ET@/L)*-Q8 
@K-!9!(03;85[S]G3B\0U8WGUID<D=P=;%/52DAQ:)L, 
@<1J6[5<(-0W+A[1904.CNJB7BU+,_$6("@:<@'6GAX4 
@X55_Q#%UL!7W>Q/JI?2D5#Y+C^]:$@PO6FU(I>+,[:8 
@IRO/HU4C?]Q07C<9_*VO6G%N-Y(8_.<HRH9''+OXZ/\ 
@IJI%@ \I^5\A0NZ(KMA(QY,WT?@1[@PLD?)Y*J]B)UP 
@1#O)X-H1:4UJ=S+;U'"J!5]#EW[$7,1U*FQ1'%V3/K, 
@)^^XA"B8 )&\TDZ<UFQF2LOEG%&L/Z0NS&]V_.*U ZD 
@<&C*09NF\]Q"85#(O%FYE YZJFIMFB)2]ZEKQ*+Z!+@ 
@\0.>QVPP -SDZ%KO2"P)TXA!VC#!P3?FUFTGHVM'>$X 
@6S^OLVY7+!('P;7I"!#V$CU\BO:B*1LV,#Q)6_KI/Z, 
@,L^5"R^EBH6EMP%K,-^!@M&9]%<7T?!QY$7?O%58.:< 
@XT8;8E4\<C5B!+\4 E2<1I3:)0$_=GLI/^$9I:+!IV, 
@]OWG>W%WH#*DUU-I[>LO*HG?B@.AYF$#I1SS@Z4$QJ\ 
@B_@AX(%OJ0; 1<WXT]Q[T2&AZG+M-@$LV5;[>NH-P_X 
@)0BVK2>A"%X7C#IR$[B81 AR&09XOC2V?RD%J MDGG4 
@5="0!]R*$0;.52")&])1+4$Z]URJ;*+-8Y3&_V-L97( 
@BWP!:7] ("8N]9K3="S:T++:19:2>P1H8*7\W\V.@!, 
@4--0UW3([5^[7TK"-RR/G'5KDIQ@^-I/[9%DI9)4RFL 
@[Q$0;*:1:)HJB"N-7YQ)G=+RR*:0!Y=H+B<3 A/5D9\ 
@<"X? ;9<VW=6_]T">QN5$KQB)-<9FJ*<@KFZE[N7(UX 
@1$VDYD4A.:I3Y?7-AD-KTS4J_(LU820^2;2L'N3Z:$H 
@%DO\!TA,<L VVZ46X,0K]A13,<=2Q4OF8J%041DCR(4 
@/!1$!ZB3KEMS:N82+V!B*?WA#\JEJF(W0JXA$XU#HD\ 
@YK":VD:-K!6EV-LD&,!0,\4FWRJOMQ$?\.-)'\""<:8 
@0K">R< EC?&^GQE6ZALM\K[F=9@<4]>PX-V+@O4[-:\ 
@>H!YFZQ,'#F.+U'$,&$8L6VZ(/B,_A.K4(.N ^U/< X 
@A,FZQ/871\,?Y_\YXT@'XC3" Y'74CQU(=E56+BN*/D 
@QSH"+S@LXZXC-. Y>RVI(>L^#AY9ZIH@F4Y"4T8^M \ 
@(Y+9*8E*K*)6)^1+8L(-D 1G7GVC!Y1!K2[_K1VRL_D 
@3=W*.$&WX4\7#20$/ED3IU<@4"3>XN3-N+Q1^["W_*\ 
@?$O3B^0COKB',X@'!N:,=[NWDY[U/RX#;5^G,24Z+(8 
@IEZK)<O^_T.*&.Q<T$_JM"@W/C]=.#C89FJSIO:/%C4 
@C0$X)%Q[-VJQ_:56+G:C-FP_];LPS!3=!:&\IW]5?94 
@4MN=A%!,I<B8]&='QB+RPM[_%2 5I9O74D!$J^%Q5\D 
@KM?"F-?R,?;3DZGW'W>*<D[-??+_N$(S;$"T^;.CS94 
@_96ED'>_B:B^F8'T2AI$_X7N!E IM(E/#2F-L-SJ[R0 
@J)K5]LM@S[IV.^.X Y)E4)* 8U>U_J@3BH=L>KXPXXH 
@>2&$7'P?P H<>5SE]L#OR;'"FCL[]7=I6=(_**3]G=T 
@V$X@B)2_H/7H?4\-YY()T 1I?(L^-=@\:'?>P)?_AYP 
@E3"ON%TB[4CMJ!0Q*7B8/4@7V:-<U-VWH&J"V]V4)X@ 
@.!:'Z0FK%>CB@,&K<&C\=2:@L_YQ.PZPJ4_8 (I+ X  
@7LR&:^H\K[H>H1V&2$@#SW-? %WA2MH!?)AIA_W$)Z$ 
@9U7N5^LT^ZJ4@JC,W,$&X?=G1R3SBFZYDC=?@'>0MO( 
@[,QK9R6RUF)$G1"B=PRA:>]^X:$,Q*;J/U,619OE@A, 
@KMF]??Z3)M,R[W=N_4]Z0FSZDR:8\,; G A!2R6EQ*4 
@)MZ2$O-!<\V? :>&A:;CKKN(Y^J:Z7@5T_H@<EKY'DT 
@7D\Y1VK4V.%M*?);D$*Y6*J(]L6R43E!7:O4#+304_< 
@ET!(,+)35VW11'H7CZ3FB=$.;NB6<66T:(3=V%7M6&X 
@6IFME']'%/- J1C4L/CHU8^Y! (4$&C+7J$%G>?M%1\ 
@*:MN9N2W!ESN.WH[UHPE,FGALIK:2*&-'2" %'Y'UM4 
@+)IKJ(RR\)._^?66;,*YF>6QH@YY\SQZW%>[*-(+P*0 
@_Q6^LK\*>2D&U/.*L4=1N=<6XB%GSU66";[-B>\U]:\ 
@]B[=T$P!6)K_[J:&V52:NUW0CZN50]?_AU5V,($XYQ4 
@;&EU1@2I<<NF'OO/.8- XR[=[S[.-[LI!CB5:R9<.M< 
@_HE0*JW8+4J!1(7\!9##&6%!C_.GOL8]L^=EH:D3;", 
@\MJ@C%O3U(X^0L.DX\)1;I00GM()"_$>K&15I:0G7O\ 
@'SCD#=]([WI_V+O7LF&J,=A$_$TEP1FT'F)#"J#WFOD 
@."ECC7[I,#M @>@KV%KG.75S,;+4BK"](URB1Y (MMX 
@CV0)+[)4&F^T=Z_EC?4!HPU$&_0A9'7D5P 6!<_?8"4 
@20A&4Q"45L%NC3<I)<-M@,\5U PX?35:+=0Z+TF)X], 
@'FS_/  X*1&$-P[5X#Y05#,AP<+_-A4?E6^H@6 D(Z8 
@Y$.7CQ+XV:69]U]H7&&A0=ND$:>0/YU@T"L+3(#K4R( 
@+#J/#[;GK@+ Z.[?MFOL*^ ?=#-9?'>FE!?R_2/L1"H 
@^>&WM%M,1*(.&,>9*'009+T@(1(+]I$=W/S"BP@[(1, 
@^5YS*H3UPM2,I=!O=I+6DXMGRR%9\M_65"F2ARQ'7X4 
@Q',8S-AP4C_O4"N@2K7_J*@9IKOC:BY$*P&M>..Q<AT 
0!6!6N$0P-#(.S,"%-40FSP  
04ZGU6.TVU#V=LR^?<<3DW   
`pragma protect end_protected
