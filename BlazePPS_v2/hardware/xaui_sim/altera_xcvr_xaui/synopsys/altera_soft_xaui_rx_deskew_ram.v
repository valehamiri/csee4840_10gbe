// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H@$4"\,2 9,O 6RON>^_4\/3@M^+N:S 6)9&N?3U:M_8B%.L!.><%40  
H@'LTAW;1]U>I(NW%PLN.S:[HM<=^Z#' ;&W/'?CF9>R&'\C-]4)UU@  
HQ]?Q5.R^:>LO&8;8M+1UK[[Q=-/D)X7Z1Q$0B&-HP=!O@3P1&>'A1P  
H6D,._4GS]!2CO:U>L.:9L92WD[K][D!T3OAUYG.AX*R6P<'R&+8\V   
HOJ>Q(F$4F6OV6GX'E6*/N.;$]/U7B1NWR: /.HFB:^3YOQK4!QJU?0  
`pragma protect encoding=(enctype="uuencode",bytes=2192        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@AE!&M*S*WN"*-2]+5SZD3;C;;E/V%PL8B26@%UT^%O, 
@_Y)[?J]QY><*85'/(O^*,]Z GQD:)/ZFTMNM^I^82S\ 
@1AB4CZ_S308HB5'9$!.RMM[^*SHCOO Z4U'RX)9V.N\ 
@O.7PHQYT3234!+!4W P*H@_,4YK6F^#))^E_T3RT3F  
@W0,26Z)LR8?)[6M.'(H_K^WL)W(M/4XH"VAP>R1_+-\ 
@K1:3<-IW+.C@_J\&;6[4C"V^N7%!;L4)!%?W#K?TO"P 
@5+1ZE)(T]8T@XU9_!,_C>&6TMHRJ% ]P;>B->[^YQ-D 
@W*0+US  :DT2RD&AEHCOB'6)FY9FZ37F<;5-C9-J9*( 
@X/@EZ;2&TO=+&EAQ2>6Z.F?F/%,[5,+SP4U^L"=&(K, 
@&($KB^1#?][HL4!*=,M:K@SCU#P\;_SU(:ZB1K(X@+0 
@+ ]TM'9E-8K:3D3"U^#QI7TW)PP9RVF*)MEZH?%1B4X 
@\.-P)AL8I,AHV(?3_RE=?/M*'!=]2$43VVF.M!"G#\  
@Q#26B@4]+U_7DCCI.ZSGG,6>!7!,++R3L"30@S.P>\T 
@\[;6* PYF8LZ6)%;A<1.('#S;",0A"B<J>! ,.HSR(, 
@;O-L(9O9Y0Y3V[MG;6!\_61-,Q:&167\41*4$M'7SK$ 
@:/*>28 <P_G#"U.#:F7,@V"=/\K?FO:9OWL-Z7/P\+0 
@?3HS1.^O*JWF'JI2-M>I[BU&6&R['+5OJ)ZN ;?]>@D 
@UKXE49V<5/O7FR.M+0/LLXJ,QW*%J<)HX>DSB1:E1J  
@MWXN6[].?'3'V<IZ A4@R>.S&@Z;)[YL]!M3+%I@"PL 
@;J:\O:IQ)])<HS]>S<JH#-%((X?SGC:H>?(T_UKZ9Z  
@E!XPL%_":4NC /Q/(SK1)!\@PEF\J/F,4LJ5O70,/3\ 
@/DBOO9D;(82S]>AA"=&H,%J+H'+EIPS3V4LV-Z:A]%8 
@CM7];%I"CUSH%Y>DZ!QS"HL-D*)VQ$)*P.%!I3*NT!\ 
@1N,>U23 ]Z26H Q!3>I98'57S+##CS X]MNZ5NFOOQH 
@D+H-U4%>=@K9IACOJ?)?ND+CK*%A$[T1JQ?J@[T'Z'T 
@7X@2MKS2#'J@KI\AZ;CT*RA$H5-3W$W@6]U;A^FZ;Y, 
@R7C><@*;@A_;75U.C4KY,\%;\/Y7MI8 N-7M-A]_J/< 
@S/^W?9:Z0SW%?6\[RV<])ST<\I8J=#D)V )[?4.Z?Z  
@9PQU-)1_?44AYY;,_KT ]VIN*]$F2:AUN5R7-GA-WX4 
@#,$Z:' .(1-_5"I6Z$1<TED32(F"T" !15B*'B%!Q\@ 
@DJ:^2QP&2UND2L07%3DY:**OVQ,GU02JLO?JJYOZX6  
@8T<: 4IFLPH,]BJJ0Z<49Y?U'EJR9_6-5J9S:Z"2&4X 
@M!,+ 9M9'KLQK'U\+UF#DD'U$$!9U+D@QK:E1X$@S-@ 
@?X>[RVJ6< (1IE\*SGH\+X?$J>@S6@C(1O6QX13T!7P 
@T/W)IG5XDB0_J?TKF?VWW.).[E^5R1L%+TJ(IQ+I'>4 
@0FE>(8($[']9'CM8+>)0D2FS&K3\)9NVRU0A3?E<6 D 
@_[VB)97;XJPM&][D^>JU)W!,&7C\2<FI+OX_FQK-<!( 
@0[X8 [*%X?D!&=8&"!)5!7+DI=_4G$2(4$*30+_<^O\ 
@7E*6S+1R6'*R&WB3[RHJ=:4A9PZ,V5Q$YK$$]=8BKIT 
@-;/K)<I:)1/HZ"+JULV1JW=KA4>].L7G5<FHE+4>P(L 
@;+#*K\&F;K=:=L0\1-H-=AJ-A"Q30&+;$U;\#GP&FJ< 
@T#CZ"5F?J=]D+=%:P=B&.M: %&8YGB8,'LC 2-:!&F  
@Y93#LI>>__G>%165N2<-:?!+^?DDR9).(C3GRM6;9#H 
@+5<VLQ#29/8$)@?R'E/TO1>M'R[/?9JER#P\US>Z6;\ 
@NVH8PB1P3G'4HUN_C.$2\M.8LZA&,8-4O\QTO/?$-.L 
@UZ;Z^;36LD#5:%L_"5G2>5HH-\RW8EE7G>GL"DJ?8F\ 
@R]J!]/[\[?$$AI.MY+A*B0=P2:>RU]=:MJ9>R,*NL&X 
@T'5?QDQ8M#RX]CQH@>3<O*L%/-;BC@?#+EAE94%  T0 
@J^A[YZ=5ECUF:.%C-WVBY(&>2YJ095ZO6XWF%,3C#2< 
@_G"#9Z!6MI@[$!C/D)?VX+B?W^?W[!,LLM]3N!?<&"\ 
@">@VW4P."<\#\33+*$T+#M%#?D^.BO.;+UT6-(A7RRP 
@YW#&/8?/6Q&U0"/T?M(\"<WC8FXO1,*XV?$I;H[!OK8 
@H_W7L<)"OA7 %XVUD,5*9N$KV5=FJ\VDEI#'FD- C-\ 
@0/\!MXJ'HE+W6@V781[Z!!,44QK,T$ <L4D%I#KCL/$ 
@JQ'4@'$D17BWIKIP5)NE R5^HK(@D^7#RW1\R8: S4@ 
@TN27'I_+T8!()GBXA:IH>]/ ,ZO1K>)#-SZ&#1Z@TD, 
@$CA87(!4<V8B9/2<_V/JJOO3RAX0&Q8&)15HZ>LUQU( 
@]][3^S]Q8*ZH+E2G$[CZT]G91?<$LN^ $ &B\"YP(^T 
@<,!G\"0R33\@6&[0_@"GH(Z9:JMS,O6# <J)O:;2M(H 
@=C^J/]-2K#7F* -> 42L-/PS_!^.^K.K^FJHIM,E/]  
@P;E7)P3V_79%MM%\)2.D,9X!+'MS=%B'<"P6%M8LVPT 
@1+IH];0GXXC;\8>7TVF$?V3\3_%Z[9Y<YJDP])_)P)P 
@OI;$;\%??Y*X--L4=?&(M@;964NB''&[9VY;$%CEMV4 
@H_Q?=[DTZ[WA;T+^9;P)B+@QMDRS$O9?RQ_E=)!:8VX 
@/$ "/.$VP;!W3(NLF35]XC/B)*SF=GOK6LM.ILFPAG, 
@ P:U&C"._09\E#67"==8T.29#E,"!,"R^1L<$>51:,0 
@%6U6POLI+(5"PH4W<;81P@F>N86%E=ZAY_?'HTXNQ4D 
@6/B%8(\R 92JEEVZHYOX*<J@M^*U/G,\ @!0I&$C[<H 
0+;4.>#Y]XW^,K#-D8P<T!P  
`pragma protect end_protected
