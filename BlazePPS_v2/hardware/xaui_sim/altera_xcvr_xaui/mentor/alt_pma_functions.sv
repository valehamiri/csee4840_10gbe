// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:52:01 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
qWfQho2cbMxLqge7GU0PoDg9JiT/4H/d6sv/wps1VADldXiR5HoPS8uiCNMM4J6u
1kUGjL9QMN5aN7OGnK2kovfqtYVH5BppUJEvdqsO6dq5OJRPpmJ9z7FKcyoWFsQ+
lTc9YaYxBi3D2xKyvbiPDWg8y2tl/dCdPv0oUfR8vH4=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2496)
i796Lhm7R951a+GK2s2/Yz7/b/M/4SA/pwy4njBF2ZH5f98rOEAAEgYdhsV/k1FF
8GGpSpTpuRDsc/MembTs/aKyO1okX0OcgtUx/Z0atezf5lHDoWztxKb3UTOFNbSf
Dgdb1RFyL4Ie3roSqPxbBNaJVAyH36tvIbp9ABn9AsL9PEghuwhKm+zgy3RZ0MSr
yTso2EUF0IrzItqxmap6OHdrweERgy0uqkjjEKr+0LjlF/2yYoarZK/R5JH2OCz3
CPxFJntJ3ro/QvCrqjhR/eArr00Iqm1TMmU/Q8eF9gAdc2IbqJQyLB1lgH/ESPJ3
xKir+iGd7TQYqNXQ7HBigV1xDYCGvilMzDHtO0aZlYJEwqCmtcApXkk5GLCJJYQp
WpZMUU5uaZ8Y3op9BQi7YBuZOnhukpRmexGd4Qdz6eQpNUjL5PtsriN4Nl09aeOG
UWsToyjAwmhatIfH0JhovO8Z8hKkH8zqpSIMZcOec72LdZEffbMxw1f3Y8EJyOPW
yHRDZWLJZhgI5mGT/YdXc/mWNCYF+8/YBQ+5MbYpfbCLtYXxpfUOJabxIgHWocGs
D2Takvo3sKFLtY6JmWaNZgRKDmV4Q2/8crSw+RoZRBskP8EtTRvrpjXnDW+tWEPI
BBHXc0Yz5shzeS6jZHXUQKlwIQnGIV0BSNXPn5s//syXgOJ6deWoUzb6rMGp1BVU
pBkJugARqUW7IB6bluTwKsqThQ4Uqq6Af+U0H7qFIAZx7ek7na8VXoVv5kOIwWr5
0ZqQYvDV+3rePVQwyHH/kLXDEUwDSkb4oNzqmeCmCAbDqkk2v86e9kiThXyeYX/W
nLp52jwQG7nDmmFl+yLDQ2qrDqYaZVjgul08QKx8FiNLPg4ez2EUrayzmOyrhh64
PFiTj9cNyAQ0Fb3WslXEgMFpR9xKHFm2P7ivCXwR3UNFCwQYCYR3JMCbP9GDlUAP
g81GhYXPJl4JSLJUAIoOCb/qsNCJ+hsyWNfAKtpYCEZD/wLTRTHDO/6PGlrGRK/1
88l73yJ+4EDBrFW+YvkB6Yz0pQDL/Th5czG7tyvcIEYP4vZddwx3zfgS9+iMNUkK
cbiWcbSKafCim7SDZpAVKNHmY3/1bKSiyglcu/GUa2exJtruUZWPiZFvurteljvO
kTd/7wB9ywU5Bu9cx8t+gaQQa49TtmWNY4S2BsVUAjxr1QbhyhnQ07rhVRd4HQ2M
pEWd2/yyszgqEvcxFcweMJc09B53KajRGOUtGodNm/6SM3Z9L4kLsgt6wlgmbxer
ZztFjyqqSKF5ucbu5+gjkp1T5q3pm9xPnEVTww+TyGWBDck+t7Tqo84lDto1IGcU
wGTSFDpqOD/ivm7eIYQ751l3+ETSdGVnjU5BHc6eOdZnparEhfDWjITtHUHWDAn0
D1sUpHKdGTbvXXxamC7jSzgoXVNmePQjREsYELSa1n9XmIFUh41shQKFPG7TPVyu
Sn3eRaWP4JDwp9lEqpyn6JNzbofArn1HeIRVq+aq9b/ilQZVAOQ3hkls2o8I76LW
3ldWvPDzCzYwzC2jO3iTuWF3V2DU6+02v+RHCHvmcJFx3pwjMyx/l3RI1lhd64Hy
yjC3jLB3tRLT+9b8NtNKbhB0twsd7CBPIwxJBTrZIne81A2FecIUHZphgC4SSWev
BMFQpSNWQgj4310Q3BXIThjvSFG5CbHyvNsawwX8VzFvXk7yiNzSvgIBvr3hUmrA
vOsGe6zj/b54YAtA1atCIRf2nTd/bnbl/p+TCTlA6BpI6bL313agYY3KH6dYm3s/
H9wwiDArhZtjRsqt859GbH8ua/no3T+A0VY5cR0+AkyZ4giJ7utqVGM/Xqca2Znn
CFIYmBofNvozZ2ZGBciuhJ7C8/2rdiS46YbfterdC6iQ6xQi2x/4e+0diUa7tT6O
2vPyWX/SAEhNp09PH9QSq40QBmCxQwLxjDXZYRveXVt5Ckmv8N4I1R8EjckwxY3v
LznmZ1xqma+2ESqn8liHSIbAc7H9V5Ci7rC0x4VH4WpdWRrdIcauFZW5m2T06Cgk
dmjGT3YgK8niko55eEHmyLWUPLl+GSKGHs/PAm9mhC2X16dfciseY2aLhZIcg+hP
bCPPyZSU/hgI5ay9XX4hi4Kak9KamVV7nD/7tjZNE7ib3lhhnNdT44XKYoTD9pzx
PkShwVjOBdjQYlzgqjFkYPhXfjcYRmis4UprFciaFIdXrGfgTUGXAaHookCvE/fu
SPH59LyPiWTE2f7FVKi5qkpyhHV7Rsh4shCp5GcwixhG77k6yVxVeSsQnx9PTIk0
1unMlzIlvfT/jv7Xxcsuhj6SoEhND5EC/h3AS8SgS6MuppBzQgsB2Jxdyd0zkYSZ
ithjoTSJwbPGv1dOLhiokRYGyw++09mXx+mKNvU8A99zb2N7RHgnruTLuhjOWHhb
fyt94UUT0d785KpuFFrfHXb/mYLiuE+gzYeZP5KIZAAvHXTuPKCwxQQRiTRjVaQY
TCYG5mbjIVWK/WlvnLeMHJovaFKnZR/wGEkNvh12xDvmagAi+3swFkNtgEQlF4nb
ofQYuWCMkMfiNxDldl06dLFmXCyia4+VvTqAsxCX1NPNDbGn+bEjnEDR3dH/lzcc
M1qhPiXHB40YhW71PC2aWFjfKwFNK13/gCQXCTCKyK1Y4QAtmGv23k9NqCtEe8fc
5/l27nqY2t5VpD2+yTYqhh/XF5F2j8CCL2kt8FWEhG254sgO7bMntEEYSKQu2wIz
PaathMqnBfTQZ584s2O+I3LsH3bwkYnTOjEp4d6tvpafHgIE3ew+bAdsIuD1yYGY
5lKs5cj5tmAQGOxmEOhyDR2s4iZ29ow6yVGN0nXN9bKWiKXbvJG2G72BhUJwjC7v
NvKaEWmW1hCYJGjjq4fMJUDDH4jAAISC81m2K9A1PXf4nBg9q3vPAfvbIqlUnwK5
Cig8lvhcY7vHyPaiUQMJEy+z7yoGKgUJuESRmkhAUXOCMEqulzxSB/UatmQao4Ht
JROUuTStT1wD/Bh64yqU/O/8tfV6Du1ULGXI9fUSCJVIJyZJQD9A+T7evcFfH2sy
X/O5dUjXw6Ve7NPMiux5ySU5M93fh4ViwqZDsIRm/ru1ERS6p9fpsWDRAcvf0e5K
BUToj3NWVj4s6A4z19k/AG94JmmRpJ732K9RjJmaYQGoX/j0NuA2kQrA8h8gQ9gR
y16qfTzkhJd4GHqFXnmyCIN4LF9vGkB8G/+9sp5eZ+OdZPVGqgJSr3rVEJ8cvM9W
0GCtlPRAWogFrbAk6vM9juhUahtg/6L3WTpNIJe9vCaqbm5974KnwrAwKcV0X1yg
`pragma protect end_protected
