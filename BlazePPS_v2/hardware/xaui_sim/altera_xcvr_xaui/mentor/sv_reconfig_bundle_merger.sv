// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:52:02 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
ahn1gYJCAJ2sdvki8pXRxxizF9XTI3+Gv+FTjvHEMhQP57LMX1Hqt+W0VS6r1Cbe
tK7ClvI1ZkodyKzw5t3hPJWk1LcM7uePQX3WB2Msa9PkEXYd67JLebJpH1OBhquw
hSPFw9kzb6TCX0gYjl8hVrSGdrUSHtNGdGDZdWII2pg=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3264)
lNZt7926usgRS+JmTj7mAFo5fHgp+w5TsEUhtFS8snrLxgZdM3WALKx/GPNJi2t+
+qL1TqAAWddON7FTVogrj35haa9p+BRWuixg6n9Yr5bjsfym1jRv8x0PbcufLopk
MJrh0122QN0KrqHTHAIxSuNFMxF7cFTlFXsL1KL2KHwKXcLBdzSAOk/TmQaQ2VHs
8md7GkbtNU85p+U/4halKzdCXTjzpQ5eHgMehJUfZatJC/9LTGkk2hWHvHamhy9b
9D1uPbC/ozZYTjWS05Vmg98Mz9w3L9lDDWymubUzs7AxSJgQ6fnvXMNqHdXvAbYJ
CTCQXAaoSjdrG8QFc2GObws4YwgIR7Q39RYR68B9iHcP8HuKacRslSysIXs876gU
KtjL7+z1LKnJpuIfWZSg9gp8zvUfQWlNyb6iQ4t5iuGC0NTsnweFkTsj9ZcUUEYr
qPPfp1qdYR+FCVrSKkeeC3EfqCr5j1+MSEvG868T4B8O+QrKggqyFg9oOhc2+b2c
yQUr7fMJVyv0KpfikmmoW3rDvkukxQrynB9vPWQhBH0mV9xGRpWY59U7DMxf83Pl
o0KPj7f31YaKjcGbXuVdKNvdz0YW4+HQPDhZJxAaT9SKa97z4rWm7riN4NayjfRx
39CpmSeXm8YpoeAvXNE1rdeR0p+C7IwINhutefMgar4pjmhgiaxhCIlgzrD4w3vh
ALTvHD7RF+Il7sOUc8nEzB1jyYhlc/b1sBQIbKb+hO9byanauYCnMAW3/fIqErQ9
5ofU8tX3hlVo+5MCrEGuZluOljaeLT5JVsL5yLfQtzAAHGPe1/wl2UH3rq/0meqe
Cu37MU79k/qYsvGO8J5HhxVVdw5iNmofXN47h0yqT4hMPn3I68U7mVtc711fOWdk
BYJHhwQzeKRX2H9uK6HrWg1HfrXQfkHVpbXJhqTQKfHLUJoYEQGWQ2wIETk7YCds
xmflhi79nZKgTrLGoCQSPHPUDY9ypnUw7g5jlzZ8zzKoNxLhI3UhcGl3JJiUkkaK
1Tmxql2gZYTgIjxOtJnjo+qJg/7eC0K5ZVsJSJalPcP35FDZoVk4CVafPF9lV6ai
q2Fim81+DISTLsvmc7MCEwRpfIL1blavifrQ0cRkrYtGRLU+G0Yd2jXOSkZ2dBr1
OLsY78CfwYH5NK/ix6WiYRcraN4Qpg3Sx1tIGekKaAznyID84GMyoURrOX/lZk10
zq0nXiT9aGrkqjYFFC1LzdsNAwiVUIfwbxKh8mdQWzT2KThhbD2ftAr90uJIqTbZ
UKkenrhqZqN7zXiB7oZgmjvx8lCH4PBxFzo7vdhyLErDMuyvm9eofWCOmuociL5R
e1xjeW8i6VU5hGIkt/KeIGk67zM+aa5OHSLHiuFpsVMoLrCwwCEBEDVLUruMHMdk
tK+6vd8VsDZX5BqsJt1k1aIM+QFq68EugZaYqfrsaNbQDAAaahuyScceJN4KXX1/
bI0Qx73vvk4CxtlRXu4L1ZIh8P/ik8YGNczCtXieL7b2Q+yzP/xYftpYI4grVUPR
dzX2ZKgavek93orU9SjB4jaJfvoZ6DkkGPIm1MxAKUsdmBVcJ/F82jvRhxhAdJcs
gPpIelRXEbS6BLCXTn7mxyZst17bqvzvSKZi2UnbYKuVE/AxzxLLt9hJA4OKYQIN
dYtNF9YIQtNGybKkYOXa114KIrS0YbWTrnqlW/9jmX9ZYQms7qb7sMuvqlPMut4A
dJAJPkQ8knasBGvO/t0GQUKRczNJCXf3XT6agAOin62iRZoAcgkkHZzRJOmWdpvn
Vbr8Ah4cJ5fabDHFRpeTCsp1NsV9gaPHFpLx2PR7ZH8tWAtTW/eIVp/ChNgGu1rp
jinUFftEMnlv6oTOQmym9vb7rcff18Elo9XF5Rzo6o/XcXMXqfECrvJHsar4VV0D
8EoNh85TVgl7kMAQhWTz+dzvBgaVP+DCO1j1GVC9WbZiqCTYyv3gfSGaNGHScZPw
hpT6zjZ4QJh44GjbJgwFkvhS4pzXsXqfTWGp59c0cCRvECprhnOssY+0UX11XY1P
qoXVUqfIF1xBhQ3fgyG3CfIcfwDoeN2xGK94s0skWlj10zNutu0f0MFWC+Apxr6K
BrUn7vXLGgybJeZVLsbt+yO+Hw+pgtCLNCC5RT4pdc53te/tbMa0IIXtD/u5l6sm
obyO9yQFCI+itnkFxGDK/9yxhBcOeDIeF/jWtXuFbUxsQUeJmQJyMYRadE2oXzbW
0Zsn2YOs4g5FVdTowUE2F7MDgwr2Hm1GyMEFvqclCnNwynsAPJfBRszKOKcpNAiB
IYy/8QdTpzXNOv0m0m+m4nsM83nxCy8Zh9gJYFjfOs3GYtbrwtiMKP7dbSfRXv5i
vEJiHrc+gQtnbPzDN+Zj7ZqHBdojJToqtX5elxPQJcad4sa+bJKELY2sXfTUK+rW
KcY2V9qX7otGy4aaqmKl7E+Q+PEvzK68KAinxf54lgddxL/pTyihe6HOtZV8pDzG
6YJhxfTKKDcJTTSG+xhW0SnhEfR7dj6dRmpWbOIbzpMsy13mAKV64HlcPO9arP0/
sX4m2wJO6J6s2h6loWDmBDTFRtokLwbbiqw9q39PjpAJK2FDtxpWrFREDhYVZwXp
2T7ari+0wxX7A0+LE4ODkw42JnaRSTRVcT9IdySG+qgKtuAmmiqNggODMkHIsEN4
/gti9utK96jD70eopMhxNHqnd2qlveQ4ieGzlkuUYEJr4gCbp+NXJRg3CMg1kIMp
6sfESyAO1Z1gVdky2rkj3A4mIafrlich0KwaQS1rizdJt9DDVE9PIQ11IXbN737h
xrDWtDjb4y//tWwJ6rwHx3P3EguOLflPbQrUykZ+d13ErYJu7jtQwAUkuV51es6P
kb6MER6/eDlJqhG/g8u2IEiCZyoK3JSkP8mdVnhaAKREGwe4uogZTglxVqC+eiI4
7v5mTInKnhtAc/Y7we7iJP2Mkba4+A/bvzZUqLM07Ibh782zogcTvYHga2MhVibf
gw8HCy2jz0x4zO7wgMS+zRtrcO7QnsjGkTzWgDJhWUozlRv3Wy+ghnbBSw+FVnQF
aYzGg1f5ds9M31/JWpWJLlmnR/ii3VvcWswkWqH8Z9zUwfScjfG7ts2oQh2APtZd
EzgLJZlslXhOQGqry2nppmlyFVKr7oxkbCff1d1X/WdMZfrHYPZUXtcrcSYRIxOH
gzYtw1GnnpbhCuwsH5eKSf0qaDBl8lhCiVLDuzxTEmT+BYt8IeeBLqBHquJ3OdTt
JuPxx/U35EhTvYyDv42NygvvPDlMUe8JW4X9FTIxQdsIcCHf8vVkhLxDnhL5Tb6Q
M69Rd8h7oYSBSgruiKMX//gW9weZ0Tb5pjskufu7AG2Iw59BL9W/bxhe8e7wVaX9
Bje35uceylCkKUeU4ykqFKQ/jWYjWkkIrftAicKa3KOL/D/rFGDKOPELsplMKxTx
MivT3wI6lxTf7MbkYa0TwvFDi+pnVKeaH2FtXXblQlMQNSYCSTRnKYX0GtkX5Tak
HnrfZq7y7+pPJf5DEffnBJskzZrrcanaH5UPp7a6mg6tecKraL4mJtlOxWQpLlkv
RpfAI52jCTaHf8MWcczeLNQM4QH8rytZmPNhwXLHtRhXRww4002UnlYcBFJjtlCM
O30jA8jfDXE2gCiAiuDcMgID/gT0kI+W7mTR1X+4Ne5b/FK1qkk1klur1HRG5uMT
8/1/z9NLLau5jtopEoNiZre7rjhXEJLSKPhicUz/sCl8y9lxOoLLBRf6O8dUqPkj
SoZZSZ6q8cAEJxJRAR6t6hoMPaWZW9Xh3uz3EJz98FH3jFxA0aSYI+jn0D6yzcYZ
+t+O5RdRCHO1lr7Z5zDTG5zD+Mhd8Gr7WyX5MMczCcYuMhM4V6MQ1PwCNg0zaFrh
S9XPH/vk95xBgYyYomcEwAvscp5e8n+j8DpG0ueWeVIZuo94IL/EerhqvZURYkNh
gq+2WJnpH3AJY1TEPAeG2uTi7XhtB7DHXiBIO+4n3YddpWptZNbj1fktTgt92/Wp
UK7mGAWZSSD3+gbPTBy4WcF/YeAhxgXoSxVivA3GGCl/8xp1UitUfdDei4LMEZGu
iOJqihqkDt78MSydQ5jr0TnIrL7ezZz8SDa0QoIzK8nH6skBgHBz3JalwBiBaODr
RIBMjvyQ6+yGT2GHoiiFpnlI6zK+f3jLtIzRV9TM/gSwtIVXIE9KpgtID+HS/y7p
LjR6DXTMybklekUeqT72MeP+GGpKL1XoqUD0qGq0NnNzlgwybvecBxE4Je9v+jAf
h+5pM5QFYpzLWroQ9AemKAu5U7io95YzwsVeKuYfdQ736iKD8jf0DGyzKRf+WcHM
`pragma protect end_protected
