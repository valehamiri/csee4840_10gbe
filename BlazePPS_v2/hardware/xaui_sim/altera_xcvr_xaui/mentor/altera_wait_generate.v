// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:52:02 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
KF1tmD/ZLvkKdo6WvB1rcnOEN8YcZjuGCRObsf+IuEEjWJ+xz7cK8f/0bOa5sVAE
i9dVxesTYu+X2wGR1mUQPzPP9lXuQtjrYpGDM3kHH7qZCdu2feIpiyYM1Kt7XwhN
B1JDz/ozaW+7FwBxJpdkefjN7ybX8sPaVTVXm3x8kLA=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2336)
T2307gxAYNyrZaN3XJqLm/ZDWjOSkJl0GW/0god7Pk1w3jW7A1YEeC96nfwcQQB1
/nTxIwcOQ+wYbYgZI6WJbE3oNnznaqdLszSZWz5eZ1pAPJr7BeMSUjshEW1CPN2l
GKy6E+RvVVWv8joUjGQqW6FOtpWx2Ky3Uj3l0rDabl19gcVMAGj5VljWDzxdxPMO
XoXpQ2sNL4fEiwc1Qg3+OVRJkipD7ptFMRYa/+wRLgw+D//KUpuJNZXTFMds583H
M1xsX192DWt5acixCit0FdGMAE8KQ/rwPHjQjMz/q+Ep5FUbvT7hJ+JPtfO+s06R
dSTmjbYVpTJdhT/eCbeXz54P/BL9tT2RjVqmvsA2oNreYDsrK733MdNyMetj30Lh
WU0BAcQk1s+sbezCDsvbSLRqu7VCUoruF6Ul+DT6nVucUf5aCJ9vn6yZLBLT4vGI
XaQJsg+19tfEGp/T8II34QG5izvkyM9c1v0HuTXH3fP1mMJzvemoNsUvtqdCXZFQ
QjBdUcmRnyKRE+Rsfnhhxdqm/sdiZ30msn4bvS79WgmQUdkX4IXUSYhsbgXsBDyt
0L6IGFfD0AolmGJ4g5uir5xTzxEi1UWrh4Dr4nYMlAyVZqW2l13PSwQH7CONsAag
CTwlpL6iq4nuSjbpx5CgSXZSkY4hMbHM0Xyw3tOdyH7gJOhdX8kI3/KpRdGk9nJ4
XNmj/xC8HU/c3Xs/cnMhwS55vg7EgMDkpuVUG4VzYhpAWnfobGLPb0v1bM92Sdvz
4WcvrV74Fo5ZKJNOvRlKlElqc/Ycgy33DYoLox7ga0aPmGQgKr4F8F8JiBfTMmYR
MjA6l/5DWQEc/rENRW99QhJSO4+PwZ60uTSoF9Je00ztgWuhhEyhJsL60N98oh3h
Pi3GQIoODy6jx/jZyUnS/UgbUDwn7AR3XI6tiiHbSjGu6mp8bVgZsVjoH4Nf894R
ufvtH9gce9qSRd29pkxmKAVsZ8Mvs6YfH7+hSejuNpyOFoN/IkOc736VkasvXJUm
XtRR63QDuFlKjWGUKM5074iij8X2VcYepa4/TQU0JPlzpivuiNafxPrqWuTEhdSj
RmP26c4E83XQY6lrwp6jqi5mt5h75UWag84KsoXLHTNnZ/de7bS+0k44BoLwdidu
wCmw9gc/DuvP46+OROJyynepQhVvkcVF6YYQ+sjjkZ1N4/kZ4MoChxUMQhmwfsyO
kdmAvV91uLFceOdrEUdzbqxJ2O081ZO2cMMPsGv1XUHGLzSq+XEcLr9mvwAPhbmU
JCUN+xmFficBF6kklGzSMUje0grlYr1fn6kd4cerMynQ/BbJ6i6MI7iuNQqR+6E3
P1Io+idHzR3gcVZqcPHQ7Fwx6/LENhjAIuxPh59XvPFcNzu5GCQewQlcylOktcJI
AiCWu1bg54+GHrfTWHOtCHy/W/Zisx6hG2M5mjPopcs17ST1kKeylqM+bOu5iOB4
cJjmd6KkIiQToCBxX02ezfqwOOQR5gmUFChdom/SvyY+gGv2KjxKRmqu5bt0dZyj
OTXMMU+4PB0thO3BeQ7EyaGz0RiSJMOY6yT9xvpQQU1moMK8SzUfIrrkEDQbClJr
JaOXYthxb2i10FhMF8qBmVqTExbrnXAS9rvaj7wTrnlwBv/8bnAWMA0jyJ/GKzpL
61uYSjT9fqvOHr2YrKLY4sXBO8+oFRFccEIXfWXq2j8HT0ajbXeocTzRhgRiCfSj
4qPsT8/dveoPqZZ/nLa6bJpBCUsSD0qJ+lYUR1kUj2R6cAP+WHJcFs/da7p90GD1
c8q5LvCYkjHJsRU5sMWGmXOqvmRQOWpdp8QgXIGA5U1YEXFCISDlQHCSZsNxcGS+
Dyp3+BBhaA4Fjj2hC0ysJrlLpzJdo2D+7s+F6nADP98Yri/ctthd7FiUgwrMxaTh
GxhQ6GruPLltiE/YS6fzRT9SWOw6PDa0zs6jHYqiXEVmsyWJa1rPgJl902pe3htk
YYX6jY050a8vw9boEdSTihvscCYopzmHzEPNSzE0zjJDmNWQ1d0vEosIN7fMeLZS
tIFv8+wk7TTCPyDN4kI4P+I6YNE02xh1HVcqrkal1MMRDoVveiYr0rhxnopq4CbH
NbD7LhhxQOwxEGxMaDa+0W/TEYLKI22zbNcplmPJ+y+hlfKvuTm/1rcQ4vzPe6BI
9lsZVCQnI5GaMVAiuMylXcoWzgRJY5aCE2k+gB87l2KR/1aw5vNfnNLhhpU0BKfu
yE/7IYuxNULTAHF6V6VpKAtbQeFnx+Mwe9Pwu8OK2p/ZI6lv1MDAUul57tI9ZujG
WqYZG8MzFq4D3f6/mlPi9FZQ1NPKz6ZY6YLasJ0z3vOgBXz6diNpCAnSX2ReOnFu
r1ckQf3YivI8Ve+PKX4kxWQbvLJCEaQ8JuzMA+wtFr+oy2hcT/BvNpCk1Wpx+Ku3
NvWgTTxb5SA6z/oAwS4tqUhv875M9xrb7WtZF8cJsJfi48KszvCzxxdROsd3Y/Er
K98py4xIgLUDFPYvn07ChXDoSl4NzaLga8Kaniy+3qlSHmCvlKl/56s71bcuQpZe
CpgThTwb/NhgnPCPtH9HIlrvHlJPNJSdwH/MeChIqRw7p8LZYpods2D/zI0pkkz5
qzi47qolax9PoYvLp4n30BV3PzpuqNKqydoJC21guEwjUp5UK+f/NZGRaodSQcRm
A5IjNlbU4fRGZGX/syy7nvC9hSygalt4YVeA/AGR+QRY4HwemQ75PYEPx6xQGj60
Ad1LEXJat22lsiXRgF6nqAWKP2YyRFTZJT6sDtmKc+rz3hv82NGtGRnQQSx1TXRT
EXqg2aEUj36L+cQAvEYt2/2dDBkWmriUNJ8xwFZM7SZ5XmJwRxSzYpKEsYQCHC+B
ypSzvLCEqWZI2XTvkWUG2XNrR9awig7T9HI3SWiNsqPVqNtPsJBmcTopT+viewIT
CPTA2V/n7zKD4DxfOiY9umJB3MWTrAviOgeJ1S1wTuWZOHpWvCX7LjGCIfKITuE+
vnPCQrPpPxcLYlzpw5NkEw8DyP+54JRRhRNehZ1Q5giqYiKa90deAKseJDI95nnE
YU6MTWUENdkO9j6V5k+gg+8hLROTptdezgDW881oPrQ=
`pragma protect end_protected
