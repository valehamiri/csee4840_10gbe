// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:52:02 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
gA3mjHqhFrSoF7u0ijj6c34CzKZfVFdUq8Jdf8bStt6k69j4KUlWtCP8kuL8pmWl
BITvqij17z5ug842Dd8Eec9Pb4K7tLzuOxsl4hec5UMS9yYwFlgH2z9o/YdZDQHn
z/q3svRShAziNTS6hiCSRxc9g3kHr0VRV8JA1PVZkzk=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2656)
HNO1vOydOKGsRhefLN+fU0wMlaQlN/vxpSAjVmKjzTbydTSsn9w0Ckx/dImQrBxM
WOxOpzKMpW+Xl/zFZA7aDDdFM4aJ8ueeAJLonTvqOUJHRx6kOaf4KmNgvBVv59q0
LTtdDhGfdmFqxuD2NC4GawTMGwLawVBZLJ0w54Co2A+wC/XRWF/mL42YYuo6PxZu
4dDGb6YStGL6pSJacuzUJ27+36iBB8B4WWgMLe21sN3FZG2a6eMKhEtjYqv/a3bm
7aIuDZWHf+R6azxeF+uLU42UOesu4xUe522a3esEabR4jyUC45iJ3pOrRDQ318Ql
FwI/jpSHHYgadB5cJ9Q+/9467BFLib8BMr04xgalzymVEBn6zljR9q/uw8R8Q8c3
m8fAXXmWDgop1KruVYyZdtbbacqJC4FsJP99gYhft8DH7yw7RMqH4MH8dG4rWS/E
R5TZKwELPMPGLP6nN2pwgtl6RfU+++7LN1VLueboScyUQTWR8Qm1AFt3vv9+LzFK
uMrxE5NS0iMEdB8Hli7ZW3v87awyejqOHJoeyu8rvb8BSROwrGfFoQ0Oui6biY2H
ZiSUs6AcKG3X6SezyJhx/xpJGHUPnU7Q2IwjXmYCvp641expCTBjsyGHXEdn2fFE
1/JYD400vu9KOBZ00RhQa44oIBH3xbFR9opG4MuyPMYUGK9wap79AUASJIKCjfFj
CZiklmbuKnOTBNBQSFxB3ZW/++JTDHgu9XvLJBrMXDoXCU9CZ3D8CRrEobdcaZWl
CXqv+7AA38reP+nKrAIoJQSpu88Fj0to5bMJW2dej9FCPEAWS1kgfaWc3QfegpmW
ED/lPwSfMblBBeaDm2o5GBJcRsRhrsN0nZN2PTAAoT2YpmrfJYlZg5hMvpvnLoRl
Nde2m9PnfrLNTvotAkDWwxdsfQ43s8IBSMc9byAEiRUjQ3wlQgc8ChWihav/HEA/
EKuICYLtRU5xNlw6NJAiqwKNAo8ksquwVSp/NWVjZjXdwoY/0ZT1/Uv3J7chWoYj
fsUhHPR7TpNJI4ZhK7j3mPxAdvle+cjs8sJfbiMq9KEyd3TO13xad7Tk/IotR/k2
kaZddxp6W1Os11ry8kQkmk5CKyrvwEnqCZEzGYifAbIEPZ86Pvx9z5RgvP54pxg/
YqG5jgPBWYnwhc1imkbtZhaZRsmfyExJNPHw4ZU6aRbmr6CeqTsZ4TUFjalit57Z
GjC9j13zkKl6V7ewYmlvkoHUDQgBbw6AHX1PZTQZfIef+gE00CunLuTZ+jkdKrIG
CyaEkmhh8t+WabBIaaIeuUNiy1mFRy4WB8zB3Hy/rlsDaaZliFCC5xHoCFJPteLt
C4AM8WqQ7jyDfd3/55uGQT8khb6rIdae3GuD5FMIYvlbwKt4WJfOHyUsjaUrjuWT
jfTwXjj5Hq0PowEvIvbEpCStIJoqnKDuH2jJiuIuKjvEAJOrNjbsf5XX/7NvE/8S
nMwNxJfwOD/smK8OEaxma285sA6mrV22gljLexpJ+HJjDoLWbOCCzeYS68xBmUPS
C/6PPx8YVNx7loJI/srpeT/FtMpi2aF/Ouid/nR9xLMzzOJ2lyWhrqp/kskolGPY
XZ85VQ9SVICgwPb3tTPgGE/xWWAWMtJ2ul3vUSaAk60TNpEaNVJ83yERbV/i+Nry
mxQGY/Se2hOkq9K/tvtansx+lcr5sT9XSpm8lP9LMcBlisbu6FueWJPsrRtXPzCe
mNB9FhDzt2OJhw2WvmVwxYffTdfD4fk4gvm/yYZihvZ/sZbtPoiSJWAaagN0HF0p
zQbe1K5gK+lDYwsXPiHhtlKkGbSuoQw0LQl31eh2o33E4VzqwsmjonB95iWQ/g+G
OJDiJPFnVKCZva1IoEwMPY63hKUZKDd5J7qJIunAKxHeXbAFnDgqV1wrZvF4nOwU
xoupqbTgTIIeDxK3MVhADzEJCT6sypZxSljXcbCQ9s/SeFmV045PiJVX+wW+lR/x
f7Z2XdbEbtCOFY3kDb+RW+BS0lgfOESSiPPrCqCxregY9E1zZKVBcmXaqTivZxgG
TvPNhJ6ogubP3DALaMDP5W81F9FOzsFkfU9t5uF1YGhcVUWGLP15Nr7YgpVajOB7
Tjx66VHlI+QlUuct9JtVesMyF0v+Fd5i2RfC86CVkVP+vtjus+gRmxVujCdpOIzV
eZxurhzLm515ZC4EZiZLR+x/H663sXcxwCRgiaa56+HMAP6y5QPDtNTnaLOZ0Rx7
ZqimNsPxSKPEBqmUmDIPmLit0DaRrOXh7+GuKSCM/tdM64gh/Z4qgP6fl4Vmzu1t
bOfIM5O3ZpB9hEOdiDjh96xYIE2CsvtNCxCXkybqFF/VQTE3LJD2jPHZ6oCu8AU7
aSQz5VY8zNwZxUN73A3GFUJv40itJWIaeBuSC2UbUMd5kFCZnCxRKh1cUq1lDWcl
RixBYruw+VpfUJrb1mcEcL2tH14ih3LW4nvTzDHD83YZNAy+dZBR6EbZJ2U+ntmP
eWDhTSFtJiu3/70naEcwK6ggRVV1pNT2B7rQ6zjc07pYqOH2RV1FYdxyNvfrpOFK
ZkAhtuA8o34F8nUNaqjZHHOQUywdIvwNXr2JvE0jOCl8RTNnQr9FMhpjKAFNSaN0
RJEgyRuO8hFFALKXlwfWokN7Jl9S1XIhjNoEmVc6eWI9jaDY/74yGC5tiy6g4x+6
fZYP1O2jg81DPgVE2MZmhBqRqr/z53qp4msNm94je4GWdPPjdpF8A2gdZirOE7UI
8xGn9PTOMDcnEYQZMg80aAAmChb7h1smnVEqY0es9Sq47JsYCV7h0D6pZGZ3cdSm
TArQ2dUwk2StjCCAr7btgQLc8GY7TS1f8BGcykuepUGopdHW+W190abUtMmD5sxH
UEd1OdbjhCDCzT2wP2pi1RBPx1xT3Qi53CeN1E4etbBqndHGueuag/ydeiXSgPvs
DOWl3CVppdIGBZQzjMbA/0g5Y/yqRbeHOI7RNFQLJ78ptE4c0LUAODBSqWV+Wvji
aeNjV6n3SAz5Xl20M1b5+JAIJ1U+s3m06XaymTsMEWv3X/9s7JN7QWuKP5KRjink
wjRvcm6C2zlqA8NHynahOmmN4NSqISSZA1e3/nd9ftyeIgrLUjxLwPHuC/5pDXtX
5C57qb8HLT2feCwxNGCAlDrI+gChda5Z9RRXlttQoWAB2aTAyw0CUpS3jnqD1Slg
rRpubJJ99F+exdsjf7ZEDUBPHFfDUroa3gnMQnATri4e+t8iFCkybVD2t7S/+F+Q
TVo7s2hUw7I5T+W4c1CJlfzkOfCZVhYph+odM+bggLZOEnLeOUJXWYhq8u2by5qW
AvZBNWqFgNof2RaEqnxYhxHN8aiKIueGfY2UxUHV6WxPqNg4GxA7LPOy3TvYl17i
DoWc6V2SnAqObQOiW7KJpxhjAc9AQKRBsaEorC0Jm0opsFYxDGz37O9ZavjheAMk
h7Bm74CKSOh6YlvXxBcrb9FdukkPq+giZYrxrYdbD0tG7exh9uWl3W89UqV77fGT
cPA7LXfKT9aNyYIsarYIxQ==
`pragma protect end_protected
