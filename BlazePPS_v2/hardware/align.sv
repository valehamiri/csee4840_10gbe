module xgmii(
  input logic clk, 
	input logic [71:0] xgmiidata,// input data
	input logic reset,	
	output logic flag,
	
	output logic [71:0] xgmii//output data
	); 
	
	logic flag2;
	logic [35:0] buff;
	logic[7:0] xgmiicontrol;
	assign xgmiicontrol = {xgmiidata[44], xgmiidata[53],xgmiidata[62], xgmiidata[71],xgmiidata[8], xgmiidata[17], xgmiidata[26], xgmiidata[35]};
	
	initial begin
		flag<=0;
		flag2<=0;
		buff<=0;
	end
	always@(posedge clk) begin
		if(reset) begin
			flag<=0;
			buff<=0;
		end
		else begin
		  if(flag==0 && xgmiicontrol !=8'h1F) begin
			 xgmii<=xgmiidata;
			
		  end
		  if(xgmiicontrol == 8'h1F)begin
			 xgmii[34:27]	<=	8'h07;
			 xgmii[25:18]	<=	8'h07;
			 xgmii[16:9]   <=   8'h07;
			 xgmii[7:0]	<=   8'h07;
			 xgmii[70:63]	<=	8'h07;
			 xgmii[61:54]	<=   8'h07;
			 xgmii[52:45]	<=	8'h07;
			 xgmii[43:36]	<=	8'h07;	
			 {xgmii[44], xgmii[53],xgmii[62], xgmii[71],xgmii[8], xgmii[17], xgmii[26], xgmii[35]} <=8'hFF;
			 buff<=xgmiidata[71:36];
			 flag<=1;
			 flag2<=1;
		  end
		  if(flag2)begin
					  {xgmii[44], xgmii[53],xgmii[62], xgmii[71],xgmii[8], xgmii[17], xgmii[26], xgmii[35]} <=8'h01;
					  flag2<=0;
		  end
		  if(flag==1)begin
			 xgmii[35:0]<=buff;
			 xgmii[71:36]<=xgmiidata[35:0];
			 buff<=xgmiidata[71:36];
		  end
		  if(flag==1 && xgmiicontrol == 8'hFF)begin
			 flag<=0;
		  end
		end
	end
endmodule
