module xaui_tx_loopback(
    input   logic       clk,
    input   logic [3:0] tx_out,
    output  logic [3:0] rx_in);

    always @(posedge clk) begin
        rx_in <= tx_out;
    end
endmodule
