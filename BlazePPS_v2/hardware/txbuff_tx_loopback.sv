 module txbuff_tx_loopback(
    input   logic       clk,
    input   logic [3:0] txbuff_out,
    output  logic [3:0] rx_dc_in);

    always @(posedge clk) begin
        rx_dc_in <= txbuff_out;
    end
endmodule