module fast_mac_rx_loopback(
    input  logic        clk,
    input  logic [63:0] rx_out,
    output logic [63:0] txbuff_in);

    always @(posedge clk) begin
        txbuff_in <= rx_out;
    end
endmodule
