module fast_mac_tx_loopback(
    input  logic        clk,
    input  logic [71:0] tx_out,
    output logic [71:0] swap_in);

    always @(posedge clk) begin
        swap_in <= tx_out;
    end
endmodule
