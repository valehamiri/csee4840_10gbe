// ============================================================================
// Copyright (c) 2013 by Terasic Technologies Inc.
// ============================================================================
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development 
//   Kits made by Terasic.  Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use 
//   or functionality of this code.
//
// ============================================================================
//           
//  Terasic Technologies Inc
//  9F., No.176, Sec.2, Gongdao 5th Rd, East Dist, Hsinchu City, 30070. Taiwan
//  
//  
//                     web: http://www.terasic.com/  
//                     email: support@terasic.com
//
// ============================================================================
// ============================================================================
//
// Major Functions:	SoCKit_Default
//
// ============================================================================
// Revision History :
// ============================================================================
//  Authors: Valeh Valiollahpour Amiri (vv2252@columbia.edu)
//           Christopher Campbell (cc3769@columbia.edu)
//           Sheng Qian (sq2168@columbia.edu)
//           Yuanpei Zhang (yz2727@columbia.edu) 
//
//  Last Mod. Date: May 14th, 2015
// ============================================================================

//`define ENABLE_HPS
`define ENABLE_MDIO

module SoCKit_Top(

`ifdef ENABLE_HPS
    /////////HPS/////////
    HPS_CLOCK_25,
    HPS_CLOCK_50,
    HPS_CONV_USB_n,
    HPS_DDR3_A,
    HPS_DDR3_BA,
    HPS_DDR3_CAS_n,
    HPS_DDR3_CKE,
    HPS_DDR3_CK_n,
    HPS_DDR3_CK_p,
    HPS_DDR3_CS_n,
    HPS_DDR3_DM,
    HPS_DDR3_DQ,
    HPS_DDR3_DQS_n,
    HPS_DDR3_DQS_p,
    HPS_DDR3_ODT,
    HPS_DDR3_RAS_n,
    HPS_DDR3_RESET_n,
    HPS_DDR3_RZQ,
    HPS_DDR3_WE_n,
    HPS_ENET_GTX_CLK,
    HPS_ENET_INT_n,
    HPS_ENET_MDC,
    HPS_ENET_MDIO,
    HPS_ENET_RESET_n,
    HPS_ENET_RX_CLK,
    HPS_ENET_RX_DATA,
    HPS_ENET_RX_DV,
    HPS_ENET_TX_DATA,
    HPS_ENET_TX_EN,
    HPS_FLASH_DATA,
    HPS_FLASH_DCLK,
    HPS_FLASH_NCSO,
    HPS_GSENSOR_INT,
    HPS_I2C_CLK,
    HPS_I2C_SDA,
    HPS_KEY,
    HPS_LCM_D_C,
    HPS_LCM_RST_N,
    HPS_LCM_SPIM_CLK,
    HPS_LCM_SPIM_MISO,
    HPS_LCM_SPIM_MOSI,
    HPS_LCM_SPIM_SS,
    HPS_LED,
    HPS_LTC_GPIO,
    HPS_RESET_n,
    HPS_SD_CLK,
    HPS_SD_CMD,
    HPS_SD_DATA,
    HPS_SPIM_CLK,
    HPS_SPIM_MISO,
    HPS_SPIM_MOSI,
    HPS_SPIM_SS,
    HPS_SW,
    HPS_UART_RX,
    HPS_UART_TX,
    HPS_USB_CLKOUT,
    HPS_USB_DATA,
    HPS_USB_DIR,
    HPS_USB_NXT,
    HPS_USB_RESET_PHY,
    HPS_USB_STP,
    HPS_WARM_RST_n,
`endif /*ENABLE_HPS*/

    /////////KEY/////////
    KEY,

    /////////LED/////////
    LED,

    /////////OSC/////////
    OSC_50_B3B,
    OSC_50_B4A,
    OSC_50_B5B,
    OSC_50_B8A,

    /////////RESET/////////
    RESET_n,

    /////////SI5338/////////
    SI5338_SCL,
    SI5338_SDA,

    ///////// SW /////////
    SW,
          
    ///////////hps//////////
    memory_mem_a,
    memory_mem_ba,
    memory_mem_ck,                         
    memory_mem_ck_n,                     
    memory_mem_cke,                      
    memory_mem_cs_n,                     
    memory_mem_ras_n,                    
    memory_mem_cas_n,                    
    memory_mem_we_n,                     
    memory_mem_reset_n,                  
    memory_mem_dq,                       
    memory_mem_dqs,                      
    memory_mem_dqs_n,                    
    memory_mem_odt,                      
    memory_mem_dm,                       
    memory_oct_rzqin,                    
    hps_io_hps_io_emac1_inst_TX_CLK,     
    hps_io_hps_io_emac1_inst_TXD0,       
    hps_io_hps_io_emac1_inst_TXD1,       
    hps_io_hps_io_emac1_inst_TXD2,       
    hps_io_hps_io_emac1_inst_TXD3,       
    hps_io_hps_io_emac1_inst_RXD0,       
    hps_io_hps_io_emac1_inst_MDIO,       
    hps_io_hps_io_emac1_inst_MDC,        
    hps_io_hps_io_emac1_inst_RX_CTL,     
    hps_io_hps_io_emac1_inst_TX_CTL,     
    hps_io_hps_io_emac1_inst_RX_CLK,     
    hps_io_hps_io_emac1_inst_RXD1,       
    hps_io_hps_io_emac1_inst_RXD2,       
    hps_io_hps_io_emac1_inst_RXD3,       
    hps_io_hps_io_qspi_inst_IO0,         
    hps_io_hps_io_qspi_inst_IO1,         
    hps_io_hps_io_qspi_inst_IO2,         
    hps_io_hps_io_qspi_inst_IO3,         
    hps_io_hps_io_qspi_inst_SS0,         
    hps_io_hps_io_qspi_inst_CLK,         
    hps_io_hps_io_sdio_inst_CMD,         
    hps_io_hps_io_sdio_inst_D0,          
    hps_io_hps_io_sdio_inst_D1,          
    hps_io_hps_io_sdio_inst_CLK,         
    hps_io_hps_io_sdio_inst_D2,          
    hps_io_hps_io_sdio_inst_D3,          
    hps_io_hps_io_usb1_inst_D0,          
    hps_io_hps_io_usb1_inst_D1,          
    hps_io_hps_io_usb1_inst_D2,          
    hps_io_hps_io_usb1_inst_D3,          
    hps_io_hps_io_usb1_inst_D4,          
    hps_io_hps_io_usb1_inst_D5,          
    hps_io_hps_io_usb1_inst_D6,          
    hps_io_hps_io_usb1_inst_D7,          
    hps_io_hps_io_usb1_inst_CLK,         
    hps_io_hps_io_usb1_inst_STP,         
    hps_io_hps_io_usb1_inst_DIR,         
    hps_io_hps_io_usb1_inst_NXT,         
    hps_io_hps_io_spim0_inst_CLK,        
    hps_io_hps_io_spim0_inst_MOSI,       
    hps_io_hps_io_spim0_inst_MISO,       
    hps_io_hps_io_spim0_inst_SS0,        
    hps_io_hps_io_spim1_inst_CLK,        
    hps_io_hps_io_spim1_inst_MOSI,       
    hps_io_hps_io_spim1_inst_MISO,       
    hps_io_hps_io_spim1_inst_SS0,        
    hps_io_hps_io_uart0_inst_RX,         
    hps_io_hps_io_uart0_inst_TX,         
    hps_io_hps_io_i2c1_inst_SDA,         
    hps_io_hps_io_i2c1_inst_SCL,         
    hps_io_hps_io_gpio_inst_GPIO00,
		  
    ////////////HMSC///////////////////////////
    HSMC_CLKIN_p,
	 HSMC_XAUI_RX_p0,
	 HSMC_XAUI_TX_p0,
    MDC2,	
    MDIO2,
    MDC1,
    MDIO1,
    PRTAD02,
    PRTAD01,
    PRTAD4,
    PRTAD3,
    PRTAD2,
    PRTAD1,
    TXONOFF1,
    TXONOFF2,
    OPINLVL,	
    OPOUTLVL,
    PHYRESET,
    USER_LED_G,
    USER_LED_R,
    CONFIG1_1,
    CONFIG0_1,
    CONFIG1_2,		
    CONFIG0_2,
    SS338_CLKIN,
    GPIO0_1,
    GPIO1_1,
    GPIO0_2,
    GPIO1_2,
    SER_BOOT,
    SMBSPDSEL1,
    SMBSPDSEL2,				
    SMBWEN,			
    NVMA1SEL,
    NVMPROT,	
    OPRXLOS2,
    OPTXFLT2,
    SFP_TXDIS2,
    SFP_TXRS20,
    LASI2		
);
		  
	  
//=======================================================
//  PORT declarations
//=======================================================

`ifdef ENABLE_HPS
    ///////// HPS /////////
    input                                            HPS_CLOCK_25;
    input                                            HPS_CLOCK_50;
    input                                            HPS_CONV_USB_n;
    output [14:0] 				      				 HPS_DDR3_A;
    output [2:0] 				      				 HPS_DDR3_BA;
    output                                           HPS_DDR3_CAS_n;
    output                                           HPS_DDR3_CKE;
    output                                           HPS_DDR3_CK_n;
    output                                           HPS_DDR3_CK_p;
    output                                           HPS_DDR3_CS_n;
    output [3:0] 				     				 HPS_DDR3_DM;
    inout [31:0] 				     				 HPS_DDR3_DQ;
    inout [3:0] 					     			 HPS_DDR3_DQS_n;
    inout [3:0] 					     			 HPS_DDR3_DQS_p;
    output                                           HPS_DDR3_ODT;
    output                                           HPS_DDR3_RAS_n;
    output                                           HPS_DDR3_RESET_n;
    input                                            HPS_DDR3_RZQ;
    output                                           HPS_DDR3_WE_n;
    input                                            HPS_ENET_GTX_CLK;
    input                                            HPS_ENET_INT_n;
    output                                           HPS_ENET_MDC;
    inout                                            HPS_ENET_MDIO;
    output                                           HPS_ENET_RESET_n;
    input                                            HPS_ENET_RX_CLK;
    input [3:0] 					         		 HPS_ENET_RX_DATA;
    input                                            HPS_ENET_RX_DV;
    output [3:0] 				      				 HPS_ENET_TX_DATA;
    output                                           HPS_ENET_TX_EN;
    inout [3:0] 					                 HPS_FLASH_DATA;
    output                                           HPS_FLASH_DCLK;
    output                                           HPS_FLASH_NCSO;
    input                                            HPS_GSENSOR_INT;
    inout                                            HPS_I2C_CLK;
    inout                                            HPS_I2C_SDA;
    inout [3:0] 					     		     HPS_KEY;
    output                                           HPS_LCM_D_C;
    output                                           HPS_LCM_RST_N;
    input                                            HPS_LCM_SPIM_CLK;
    inout                                            HPS_LCM_SPIM_MISO;
    output                                           HPS_LCM_SPIM_MOSI;
    output                                           HPS_LCM_SPIM_SS;
    output [3:0] 				     				 HPS_LED;
    inout                                            HPS_LTC_GPIO;
    input                                            HPS_RESET_n;
    output                                           HPS_SD_CLK;
    inout                                            HPS_SD_CMD;
    inout [3:0] 					    			 HPS_SD_DATA;
    output                                           HPS_SPIM_CLK;
    input                                            HPS_SPIM_MISO;
    output                                           HPS_SPIM_MOSI;
    output                                           HPS_SPIM_SS;
    input [3:0] 					    			 HPS_SW;
    input                                            HPS_UART_RX;
    output                                           HPS_UART_TX;
    input                                            HPS_USB_CLKOUT;
    inout [7:0] 					      			 HPS_USB_DATA;
    input                                            HPS_USB_DIR;
    input                                            HPS_USB_NXT;
    output                                           HPS_USB_RESET_PHY;
    output                                           HPS_USB_STP;
    input                                            HPS_WARM_RST_n;
`endif /*ENABLE_HPS*/

    ///////// KEY /////////
    input [3:0] 					     			 KEY;

    ///////// LED /////////
    output [3:0] 				                     LED;

    ///////// OSC /////////
    input                                            OSC_50_B3B;
    input                                            OSC_50_B4A;
    input                                            OSC_50_B5B;
    input                                            OSC_50_B8A;

    
    ///////// RESET /////////
    input                                            RESET_n;

    ///////// SI5338 /////////
    inout                                            SI5338_SCL;
    inout                                            SI5338_SDA;

    ///////// SW /////////
    input [3:0] 					      			 SW;

    /////////hps pin///////
    output wire [14:0] 				     		 memory_mem_a;                          
    output wire [2:0] 				     		 memory_mem_ba;                         
    output wire 					    			 memory_mem_ck;                         
    output wire 					    			 memory_mem_ck_n;                       
    output wire 					    			 memory_mem_cke;                        
    output wire 					    			 memory_mem_cs_n;                       
    output wire 					    			 memory_mem_ras_n;                      
    output wire 					    		    memory_mem_cas_n;                      
    output wire 					    			 memory_mem_we_n;                       
    output wire 					    			 memory_mem_reset_n;                    
    inout  wire [31:0] 				     		 memory_mem_dq;                         
    inout  wire [3:0] 				      	 memory_mem_dqs;                        
    inout  wire [3:0] 				      	 memory_mem_dqs_n;                      
    output wire 					     			 memory_mem_odt;                        
    output wire [3:0] 							 memory_mem_dm;                         
    input  wire 					     			 memory_oct_rzqin;                      
    output wire 					     		    hps_io_hps_io_emac1_inst_TX_CLK;       
    output wire 					     			 hps_io_hps_io_emac1_inst_TXD0;         
    output wire 					     			 hps_io_hps_io_emac1_inst_TXD1;         
    output wire 					     			 hps_io_hps_io_emac1_inst_TXD2;         
    output wire 					     			 hps_io_hps_io_emac1_inst_TXD3;         
    input  wire 					     			 hps_io_hps_io_emac1_inst_RXD0;         
    inout  wire 					     			 hps_io_hps_io_emac1_inst_MDIO;         
    output wire 					     			 hps_io_hps_io_emac1_inst_MDC;          
    input  wire 					     		    hps_io_hps_io_emac1_inst_RX_CTL;       
    output wire 					     			 hps_io_hps_io_emac1_inst_TX_CTL;       
    input  wire 					     			 hps_io_hps_io_emac1_inst_RX_CLK;       
    input  wire 					     			 hps_io_hps_io_emac1_inst_RXD1;         
    input  wire 					     			 hps_io_hps_io_emac1_inst_RXD2;         
    input  wire 					     			 hps_io_hps_io_emac1_inst_RXD3;         
    inout  wire 					     			 hps_io_hps_io_qspi_inst_IO0;           
    inout  wire 					     			 hps_io_hps_io_qspi_inst_IO1;           
    inout  wire 					     			 hps_io_hps_io_qspi_inst_IO2;           
    inout  wire 					     			 hps_io_hps_io_qspi_inst_IO3;           
    output wire 					     			 hps_io_hps_io_qspi_inst_SS0;           
    output wire 					     			 hps_io_hps_io_qspi_inst_CLK;           
    inout  wire 					     			 hps_io_hps_io_sdio_inst_CMD;           
    inout  wire 					     			 hps_io_hps_io_sdio_inst_D0;            
    inout  wire 					     			 hps_io_hps_io_sdio_inst_D1;            
    output wire 					     			 hps_io_hps_io_sdio_inst_CLK;           
    inout  wire 					     			 hps_io_hps_io_sdio_inst_D2;            
    inout  wire 					     			 hps_io_hps_io_sdio_inst_D3;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D0;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D1;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D2;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D3;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D4;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D5;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D6;            
    inout  wire 					     			 hps_io_hps_io_usb1_inst_D7;            
    input  wire 					     			 hps_io_hps_io_usb1_inst_CLK;           
    output wire 					     			 hps_io_hps_io_usb1_inst_STP;           
    input  wire 					     			 hps_io_hps_io_usb1_inst_DIR;           
    input  wire 					     			 hps_io_hps_io_usb1_inst_NXT;           
    output wire 					     			 hps_io_hps_io_spim0_inst_CLK;          
    output wire 					     			 hps_io_hps_io_spim0_inst_MOSI;         
    input  wire 					     			 hps_io_hps_io_spim0_inst_MISO;         
    output wire 					     			 hps_io_hps_io_spim0_inst_SS0;          
    output wire 					     			 hps_io_hps_io_spim1_inst_CLK;          
    output wire 					     			 hps_io_hps_io_spim1_inst_MOSI;         
    input  wire 					     			 hps_io_hps_io_spim1_inst_MISO;         
    output wire 					     			 hps_io_hps_io_spim1_inst_SS0;          
    input  wire 					     			 hps_io_hps_io_uart0_inst_RX;           
    output wire 					     			 hps_io_hps_io_uart0_inst_TX;           
    inout  wire 					     			 hps_io_hps_io_i2c1_inst_SDA;           
    inout  wire 					     			 hps_io_hps_io_i2c1_inst_SCL;           
    inout  wire 					     			 hps_io_hps_io_gpio_inst_GPIO00;        
	
	
    ////////HSMC///////////////////////
    input 											 HSMC_CLKIN_p;
    input     logic 	[3:0]                       HSMC_XAUI_RX_p0;
	 output   logic 	[3:0]                       HSMC_XAUI_TX_p0;
    output logic          						 MDC2;	
    inout  logic         						 MDIO2;
    output logic          						 MDC1;
    inout  logic         						 MDIO1;
    output logic          						 PRTAD02;
    output logic          						 PRTAD01;
    output logic          						 PRTAD4;
    output logic          						 PRTAD3;
    output logic          						 PRTAD2;
    output logic          						 PRTAD1;
    output logic          						 TXONOFF1;
    output logic          						 TXONOFF2;
    output logic          						 OPINLVL;
    output logic          						 OPOUTLVL;
    output logic          						 PHYRESET;



    output logic 	[7:0] 							 USER_LED_G;
    output logic 	[7:0] 							 USER_LED_R;


    output logic            						 CONFIG1_1;
    output logic            						 CONFIG0_1;
    output logic            						 CONFIG1_2;		
    output logic            						 CONFIG0_2;
    output logic            						 SS338_CLKIN;
    					 
    inout  logic           							 GPIO0_1;		
    inout  logic           							 GPIO1_1;
    inout  logic           							 GPIO0_2;
    inout  logic           							 GPIO1_2;
    output logic            						 SER_BOOT;
    output logic            						 SMBSPDSEL1;
    output logic            						 SMBSPDSEL2;				
    inout  logic           						     SMBWEN;			
    				 
    inout  logic           							 NVMA1SEL;
    inout  logic           							 NVMPROT;
    							 
    				 
    input  logic           							 OPRXLOS2;
    input  logic           							 OPTXFLT2;
    input  logic           							 SFP_TXDIS2;
    input  logic 									 SFP_TXRS20;
    input  logic           							 LASI2;
	
	
    //=======================================================
    //  REG/WIRE declarations
    //=======================================================

    // MAC

    logic  mac_tx_ready;
    logic mac_rx_ready;

    // Make the FPGA reset cause an HPS reset
    reg [19:0] 					            hps_reset_counter = 20'h0;
    reg 						     		         hps_fpga_reset_n = 0;
       
    	
    wire        							        MDIN1;
    wire        							        MDOEN1;
    wire        							        MDO1;
    logic   									     RESET_N;                    // S5 active low
    wire 										     clk_buff0;
    wire 										     clk_buff1;
    wire 										     clk_buff2;
    wire  [71:0]						           link;
    wire 										     link_clk;

    logic [71:0]   				              xgmii_buff ;
    logic [71:0]   				              xgmii_rx_dc;
    logic [71:0]   				              xgmii_tx_dc;
    logic [71:0]   				              xgmiialigned;
    logic [31:0] 					              crclink;
    logic 										     eop;
    logic 									        sop;
    logic 										     valid;
    logic 										     ready;
    logic 										     checksum_err;
    logic [2:0] 						           empty;
    logic [63:0] 						           data;
    logic [1:0] 							        state;

    logic 										     eoptx;
    logic 										     soptx;
    logic 										     readytx;
    logic [2:0] 						           emptytx;
    logic [63:0] 						           stdata;
    logic 										     validtx;
    logic [31:0] 						           crclinktx;
    logic [31:0] 						           cctx;
    logic [63:0] 						           xgmiirevtx;
    logic [71:0] 						           xgmiitx;
    logic [71:0] 						           tx_buff;

    logic [63:0] 						           databuff;
    logic										     validbuff;
    logic										     readybuff; 
    logic										     sopbuff;    
    logic										     eopbuff;    
    logic [2:0] 							        emptybuff;
    parameter idle = 0, trig =1, lock =2;
 
 
    //=======================================================
    //  Structural coding
    //=======================================================


    // +-------------------------------------------------------------------
    // +
    assign CONFIG0_1 = 1'b1;	// Configure BCM8727 from EEPROM
    assign CONFIG1_1 = 1'b0;
    assign CONFIG0_2 = 1'b1;
    assign CONFIG1_2 = 1'b0;
    assign SS338_CLKIN	= 1'b0;

    // BCM8272C allow spi-rom to be removed 
    // 1 = Boot microcode from spi proms
    assign SER_BOOT  = 1'b0;	
    assign SMBSPDSEL1 = 1'b0; 
    assign SMBSPDSEL2 = 1'b0; 
    assign SMBWEN    = 1'b1;
    assign GPIO0_1   = 1'b0;
    assign GPIO1_1   = 1'b0;
    assign GPIO0_2   = 1'b0;
    assign GPIO1_2   = 1'b0;


    // __________________________________________________________________
    // 1: EEPROM Slave Addr 52, 0: 50 addr: 
    // During deassertion of BCM8727 reset, 
    // latched into bit 10 of register 1.8002h 
    assign NVMA1SEL = 1'b1;

    // when high protect non volatile memory 
    assign NVMPROT  = 1'b0;			

    // MDIO ports connection
`ifdef ENABLE_MDIO
    assign MDIO1 = !MDOEN1? MDO1 : 1'bz;
    assign MDIN1 = MDIO1;
`else
    assign MDC1	 = 1'bz;
    assign MDIO1 = 1'bz;
`endif
    assign MDC2	 = 1'bz;
    assign MDIO2 = 1'bz;

    // +-------------------------------------------------------------------
    // +TXONOFF2


    assign PHYRESET =  (KEY[1]); // S4 active low
    // assign STOPMON  = ~KEY[2]; // S3 active high

    assign {PRTAD4,PRTAD3,PRTAD2,PRTAD1,PRTAD01} = 5'b00000;
    assign  PRTAD02 = 1'b0;
    // +
    assign TXONOFF1 = 1'b1;
    assign TXONOFF2 = 1'b1;
    assign OPOUTLVL = 1'b0;				// 0 for active low OPTXENB/OPTXRST
    assign OPINLVL  = 1'b1;				// 1 for active high OPRXLOS/TXONOFF

    assign USER_LED_R = 8'b00001111;
    assign USER_LED_G = 8'b11110000;

    //assign LED[0] = SFP_TXRS20;
     //assign LED[1] = SFP_TXDIS2;
    assign LED[0] = mac_tx_ready;
    assign LED[1] = mac_rx_ready;
	
    always @(posedge OSC_50_B4A) begin
        if (hps_reset_counter == 20'h ffffff) hps_fpga_reset_n <= 1;
            hps_reset_counter <= hps_reset_counter + 1;
	end      
				
    // Logic for resetting modules using KEY[0]  
	always @(posedge OSC_50_B4A)
        begin
            case(state)
                idle:               
                    if (KEY[0]==0)
                        state <= trig;
                    else
                        state <= idle;
                trig:
                    if (KEY[0]==0)
                        state <= lock;
                    else
                        state <= idle;
                lock:
                    if (KEY[0]==0)
                        state <= lock;
                    else
                        state <= idle;
			endcase
		end
	always @(state)
        begin
            case (state)
                idle: 
                    RESET_N <= 1;
                trig:		
					RESET_N <= 0;
				lock:
					RESET_N <= 1;
			endcase
		end

//=======================================================
//  Modules Connection
//======================================================

// PLL REFERNCE  CLOCK FOR  XAUI
	pll p1(.refclk(OSC_50_B4A), .rst(0), .outclk_0(clk_buff0), .locked());

// XAUI  PHY	  
xaui test(
	.pll_ref_clk			(clk_buff0),          //        pll_ref_clk.clk
	.xgmii_tx_clk		    (link_clk),          //        xgmii_tx_clk.clk
	.xgmii_rx_clk		    (link_clk),          //        xgmii_rx_clk.clk
	.xgmii_rx_dc			(xgmii_rx_dc),        //        xgmii_rx_dc.data
	.xgmii_tx_dc			(tx_buff),            //        xgmii_tx_dc.dataxaui
	.xaui_rx_serial_data	(HSMC_XAUI_RX_p0),    //        xaui_rx_serial_data.export
	.xaui_tx_serial_data	(HSMC_XAUI_TX_p0),    //        xaui_tx_serial_data.export
	.rx_ready				(LED[2]),             //        rx_ready.export
	.tx_ready				(LED[3]),             //        tx_ready.export
	.phy_mgmt_clk		    (OSC_50_B4A),        //        phy_mgmt_clk.clk
	.phy_mgmt_clk_reset		(!RESET_N),        //        phy_mgmt_clk_reset.reset
	.phy_mgmt_address	    (3'h000),            //        phy_mgmt.address
	.phy_mgmt_read           (0),              //        .read
	.phy_mgmt_readdata       (8'hFFFFFFFF),    //        .readdata
	.phy_mgmt_write          (0),              //        .write
	.phy_mgmt_writedata      (8'h00000000),    //        .writedata
	.phy_mgmt_waitrequest    (0),              //        .waitrequest
	.reconfig_from_xcvr      (),               //        reconfig_from_xcvr.data
	.reconfig_to_xcvr        ()                //        reconfig_to_xcvr.data
); 

	
// SWAP	 
xgmii swap (
	.clk(link_clk), 
	.xgmiidata(xgmii_rx_dc),      // input data
	.reset(!RESET_N),	
	.flag(),
	.xgmii(xgmiialigned)          // output data
);

// RX FAST MAC
mac_rx rx (
	.clk(link_clk), 
	.xgmiidata(xgmiialigned),     // input data
	.reset(!RESET_N),
	.c(crclink),
	
	.data(data),//output data
	.empty(empty),
	.sop(sop),
	.eop(eop),
	.valid(valid),
	.ready(ready),
	.newcrc(crclink),
	.checksum_err(checksum_err)
);

// TX  BUFFER
txbuffer buffer(
	.clk(link_clk),
	.reset(!RESET_N),
	.stdata(stdata),              // stream_src.data
	.valid(validtx),              // .valid
	.sopin(soptx),                // .startofpacket
	.eopin(eoptx),                // .endofpacket
	.emptyin(emptytx),            // .empty
	
	.databuff(databuff),    
	.validout(validbuff), 
	.readybuff(readybuff),  
	.sopout(sopbuff),    
	.eopout(eopbuff),     
	.emptybuff(emptybuff),
	.readytx(readytx)             // .ready	
);

// TX FAST MAC	 
mac_tx tx (
	.clk(link_clk), 
	.stdata(databuff),            // input data
	.reset(!RESET_N),
	.c(crclinktx),
	.sopin(sopbuff),
	.eopin(eopbuff),
	.valid(validbuff),
	.ready(readybuff),
	.empty(emptybuff),												
	.xgmiirev(xgmiirevtx),				
	.xgmii(tx_buff),
	.newcrc(crclinktx),
	.cc(cctx)
); 

// LOOPBACKS
/*
// XAUI TX LOOPBACK
xaui_tx_loopback xaui_tx_loop (
    .clk(link_clk),
    .tx_out(HSMC_XAUI_TX_p0),
    .rx_in(HSMC_XAUI_RX_p0,)
);

// XAUI RX LOOPBACK
xaui_rx_loopback xaui_rx_loop (
    .clk(link_clk),
    .rx_out(xgmii_rx_dc),
    .tx_in(tx_buff)
); 
 
//  FAST MAC TX LOOPBACK
fast_mac_tx_loopback fast_mac_tx_loop (
    .clk(link_clk),
    .tx_out(tx_buff),
    .swap_in(xgmii_rx_dc)
);

//  FAST MAC RX LOOPBACK
fast_mac_rx_loopback fast_mac_rx_loop (
    .clk(link_clk),
    .rx_out(data),
    .txbuff_in(databuff)
);

// Tx Buffer TX Loopback
txbuff_tx_loopback tx_buff_loop (
    .clk(link_clk),
    txbuff_out(databuff),
    rx_dc_in(data)
);

// Tx Buffer RX Loopback
txbuff_tx_loopback tx_buff_loop (
    .clk(link_clk),
    rx_mac_out(data),
    txbuff_in(stdata)
);
*/

///////////////////////////////////////////QSYS:MM/ST  CONVERTER & HPS///////////////////////////////////////////////	 
blazepps u0 (
	.clk_clk                                  (OSC_50_B4A),                      //                clk.clk
	.reset_reset_n                            (hps_fpga_reset_n),                //                reset.reset_n
	.memory_mem_a                             (memory_mem_a),                    //                memory.mem_a
	.memory_mem_ba                            (memory_mem_ba),                   //                .mem_ba
	.memory_mem_ck                            (memory_mem_ck),                   //                .mem_ck
	.memory_mem_ck_n                          (memory_mem_ck_n),                 //                .mem_ck_n
	.memory_mem_cke                           (memory_mem_cke),                  //                .mem_cke
	.memory_mem_cs_n                          (memory_mem_cs_n),                 //                .mem_cs_n
	.memory_mem_ras_n                         (memory_mem_ras_n),                //                .mem_ras_n
	.memory_mem_cas_n                         (memory_mem_cas_n),                //                .mem_cas_n
	.memory_mem_we_n                          (memory_mem_we_n),                 //                .mem_we_n
	.memory_mem_reset_n                       (memory_mem_reset_n),              //                .mem_reset_n
	.memory_mem_dq                            (memory_mem_dq),                   //                .mem_dq
	.memory_mem_dqs                           (memory_mem_dqs),                  //                .mem_dqs
	.memory_mem_dqs_n                         (memory_mem_dqs_n),                //                .mem_dqs_n
	.memory_mem_odt                           (memory_mem_odt),                  //                .mem_odt
	.memory_mem_dm                            (memory_mem_dm),                   //                .mem_dm
	.memory_oct_rzqin                         (memory_oct_rzqin),                //                .oct_rzqin
	.hps_io_hps_io_emac1_inst_TX_CLK 	      (hps_io_hps_io_emac1_inst_TX_CLK), //    			   .hps_0_hps_io.hps_io_emac1_inst_TX_CLK
	.hps_io_hps_io_emac1_inst_TXD0 		      (hps_io_hps_io_emac1_inst_TXD0),   //                .hps_io_emac1_inst_TXD0
	.hps_io_hps_io_emac1_inst_TXD1   	      (hps_io_hps_io_emac1_inst_TXD1),   //                .hps_io_emac1_inst_TXD1
	.hps_io_hps_io_emac1_inst_TXD2   		  (hps_io_hps_io_emac1_inst_TXD2),   //                .hps_io_emac1_inst_TXD2
	.hps_io_hps_io_emac1_inst_TXD3   		  (hps_io_hps_io_emac1_inst_TXD3),   //                .hps_io_emac1_inst_TXD3
	.hps_io_hps_io_emac1_inst_RXD0   		  (hps_io_hps_io_emac1_inst_RXD0),   //                .hps_io_emac1_inst_RXD0
	.hps_io_hps_io_emac1_inst_MDIO   		  (hps_io_hps_io_emac1_inst_MDIO),   //                .hps_io_emac1_inst_MDIO
	.hps_io_hps_io_emac1_inst_MDC    		  (hps_io_hps_io_emac1_inst_MDC),    //                .hps_io_emac1_inst_MDC
	.hps_io_hps_io_emac1_inst_RX_CTL 		  (hps_io_hps_io_emac1_inst_RX_CTL), //                .hps_io_emac1_inst_RX_CTL
	.hps_io_hps_io_emac1_inst_TX_CTL 		  (hps_io_hps_io_emac1_inst_TX_CTL), //                .hps_io_emac1_inst_TX_CTL
	.hps_io_hps_io_emac1_inst_RX_CLK 		  (hps_io_hps_io_emac1_inst_RX_CLK), //                .hps_io_emac1_inst_RX_CLK
	.hps_io_hps_io_emac1_inst_RXD1   		  (hps_io_hps_io_emac1_inst_RXD1),   //                .hps_io_emac1_inst_RXD1
	.hps_io_hps_io_emac1_inst_RXD2   		  (hps_io_hps_io_emac1_inst_RXD2),   //                .hps_io_emac1_inst_RXD2
	.hps_io_hps_io_emac1_inst_RXD3   		  (hps_io_hps_io_emac1_inst_RXD3),   //                .hps_io_emac1_inst_RXD3
	.hps_io_hps_io_qspi_inst_IO0     		  (hps_io_hps_io_qspi_inst_IO0),     //                .hps_io_qspi_inst_IO0
	.hps_io_hps_io_qspi_inst_IO1     		  (hps_io_hps_io_qspi_inst_IO1),     //                .hps_io_qspi_inst_IO1
	.hps_io_hps_io_qspi_inst_IO2     		  (hps_io_hps_io_qspi_inst_IO2),     //                .hps_io_qspi_inst_IO2
	.hps_io_hps_io_qspi_inst_IO3     		  (hps_io_hps_io_qspi_inst_IO3),     //                .hps_io_qspi_inst_IO3
	.hps_io_hps_io_qspi_inst_SS0     		  (hps_io_hps_io_qspi_inst_SS0),     //                .hps_io_qspi_inst_SS0
	.hps_io_hps_io_qspi_inst_CLK     		  (hps_io_hps_io_qspi_inst_CLK),     //                .hps_io_qspi_inst_CLK
	.hps_io_hps_io_sdio_inst_CMD     		  (hps_io_hps_io_sdio_inst_CMD),     //                .hps_io_sdio_inst_CMD
	.hps_io_hps_io_sdio_inst_D0     		  (hps_io_hps_io_sdio_inst_D0),      //                .hps_io_sdio_inst_D0
	.hps_io_hps_io_sdio_inst_D1      		  (hps_io_hps_io_sdio_inst_D1),      //                .hps_io_sdio_inst_D1
	.hps_io_hps_io_sdio_inst_CLK    		  (hps_io_hps_io_sdio_inst_CLK),     //                .hps_io_sdio_inst_CLK
	.hps_io_hps_io_sdio_inst_D2   		      (hps_io_hps_io_sdio_inst_D2),      //                .hps_io_sdio_inst_D2
	.hps_io_hps_io_sdio_inst_D3      		  (hps_io_hps_io_sdio_inst_D3),      //                .hps_io_sdio_inst_D3
	.hps_io_hps_io_usb1_inst_D0      		  (hps_io_hps_io_usb1_inst_D0),      //                .hps_io_usb1_inst_D0
	.hps_io_hps_io_usb1_inst_D1      		  (hps_io_hps_io_usb1_inst_D1),      //                .hps_io_usb1_inst_D1
	.hps_io_hps_io_usb1_inst_D2     		  (hps_io_hps_io_usb1_inst_D2),      //                .hps_io_usb1_inst_D2
	.hps_io_hps_io_usb1_inst_D3     		  (hps_io_hps_io_usb1_inst_D3),      //                .hps_io_usb1_inst_D3
	.hps_io_hps_io_usb1_inst_D4     		  (hps_io_hps_io_usb1_inst_D4),      //                .hps_io_usb1_inst_D4
	.hps_io_hps_io_usb1_inst_D5      		  (hps_io_hps_io_usb1_inst_D5),      //                .hps_io_usb1_inst_D5
	.hps_io_hps_io_usb1_inst_D6     		  (hps_io_hps_io_usb1_inst_D6),      //                .hps_io_usb1_inst_D6
	.hps_io_hps_io_usb1_inst_D7     		  (hps_io_hps_io_usb1_inst_D7),      //                .hps_io_usb1_inst_D7
	.hps_io_hps_io_usb1_inst_CLK    		  (hps_io_hps_io_usb1_inst_CLK),     //                .hps_io_usb1_inst_CLK
	.hps_io_hps_io_usb1_inst_STP    		  (hps_io_hps_io_usb1_inst_STP),     //                .hps_io_usb1_inst_STP
	.hps_io_hps_io_usb1_inst_DIR    		  (hps_io_hps_io_usb1_inst_DIR),     //                .hps_io_usb1_inst_DIR
	.hps_io_hps_io_usb1_inst_NXT    		  (hps_io_hps_io_usb1_inst_NXT),     //                .hps_io_usb1_inst_NXT
	.hps_io_hps_io_spim0_inst_CLK   		  (hps_io_hps_io_spim0_inst_CLK),    //                .hps_io_spim0_inst_CLK
	.hps_io_hps_io_spim0_inst_MOSI  		  (hps_io_hps_io_spim0_inst_MOSI),   //                .hps_io_spim0_inst_MOSI
	.hps_io_hps_io_spim0_inst_MISO  		  (hps_io_hps_io_spim0_inst_MISO),   //                .hps_io_spim0_inst_MISO
	.hps_io_hps_io_spim0_inst_SS0   		  (hps_io_hps_io_spim0_inst_SS0),    //                .hps_io_spim0_inst_SS0
	.hps_io_hps_io_spim1_inst_CLK   		  (hps_io_hps_io_spim1_inst_CLK),    //                .hps_io_spim1_inst_CLK
	.hps_io_hps_io_spim1_inst_MOSI  		  (hps_io_hps_io_spim1_inst_MOSI),   //                .hps_io_spim1_inst_MOSI
	.hps_io_hps_io_spim1_inst_MISO  		  (hps_io_hps_io_spim1_inst_MISO),   //                .hps_io_spim1_inst_MISO
	.hps_io_hps_io_spim1_inst_SS0    	 	  (hps_io_hps_io_spim1_inst_SS0),    //                .hps_io_spim1_inst_SS0
	.hps_io_hps_io_uart0_inst_RX     		  (hps_io_hps_io_uart0_inst_RX),     //                .hps_io_uart0_inst_RX
	.hps_io_hps_io_uart0_inst_TX     		  (hps_io_hps_io_uart0_inst_TX),     //                .hps_io_uart0_inst_TX
	.hps_io_hps_io_i2c1_inst_SDA     		  (hps_io_hps_io_i2c1_inst_SDA),     //                .hps_io_i2c1_inst_SDA
	.hps_io_hps_io_i2c1_inst_SCL     		  (hps_io_hps_io_i2c1_inst_SCL),     //                .hps_io_i2c1_inst_SCL
	.xaui_clk_clk                             (link_clk),        
	.xaui_reset_reset_n                       (RESET_N),

   .stream_src_data                         (stdata),                 //  stream_src.data
   .stream_src_valid                        (validtx),                //            .valid
   .stream_src_ready                        (readytx),                //            .ready
   .stream_src_startofpacket                (soptx),        //            .startofpacket
   .stream_src_endofpacket                  (eoptx),          //            .endofpacket
   .stream_src_empty                        (emptytx),                //            .empty
   .stream_sink_data                        (data),                // stream_sink.data
   .stream_sink_valid                       (valid),               //            .valid
   .stream_sink_ready                       (ready),               //            .ready
   .stream_sink_startofpacket               (sop),       //            .startofpacket
   .stream_sink_endofpacket                 (eop),         //            .endofpacket  
   .stream_sink_empty                       (empty),   //.empty
);		  
endmodule

