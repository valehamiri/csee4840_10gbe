// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H)]+X= V9/7$?P#+CA(--:E <B_V:_FQ-T@Q@%B->5]:_4PX ^>M6:P  
H'_74&??I/9%><?$S!>=>BWLW.*M;&D?HN;PTF1>/8DE@X@1<9$0#6P  
HIRIZF6W-.;\'D%4Z0P0B2(W9G%NN$7M^O!"H=4)(/A;*Y"2'TT4*6@  
HI-45,&..CNP%4Q&$W,*LW-Q_1W6Q)SUGM:[RY92>MM^H-97;H,YI]   
H3EJ;_(+,Z4@<4FDM5O(3N \/>V!I+C\^*-H>9C#$]N<GG$H":B$[!@  
`pragma protect encoding=(enctype="uuencode",bytes=2112        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@580P;3)*A4EA?3((L:.8?C$7(MJ@F'\"VM/'V931(:\ 
@.(56_)AP6"-"8^ #.+ CC8GYW:D)I,H$&!-/:QSAI9P 
@[].+0<2?F<V-$#1EG*C,>$#OC.%J[/.+IFRB86J25'< 
@ND0Y6BV)5-Z#>5'8C/*TD%IR@"5JR,Z!+[%6N?,GYTL 
@&'J ')3ML4H^@T2%,KXQKMGD%$8+H."U%AGAA+TI>6H 
@62G\5,8-4=:8=2L"91?/9U5 V[PG"HC,-;S4Z8%#]G4 
@EDAU-L2?"FUURCW+0A*BFI("Z/44 XHR8>'HJD@\,3D 
@6DO,V.U8R&79I=N$3GW[:PT<JW*<7X#(-@W-X@UQ82$ 
@V_VOHT?,]H^7(EF3#R?X^WI"GZ<QAEXA[.&-,I[^(.< 
@5CVJ9L>]O"@*=F))=(173N.M"(B<+HUT9+B?%X RCEP 
@K"F9$$=DE8,QG-_-O8 M]*&*V\RA?</.Y^"'H.ABCD\ 
@6?:F)M!*$:%> ]F":S!F<ZP'EYV-<'0ZJM!'S+-(6ZX 
@HKLIF[,L@&47K>S#49GGN(788>S(Z3E(S4Q\3'B,#:0 
@7V$!5JTE!HD+"YSJ@$20%,&6^G)W[)V*0=7M4 HP\_D 
@L]][YXG^/L>P1[LN*>V 2M\'(B^%>A+Y67]MQ?W=ZI0 
@1R38-JR'Y],=8BPR1V%WK[L\J4@Z/7W^,]X<YE@921, 
@X5"LIB!/IU]D^ZD-C)J^SWX>7HGD>^M:M:;XP3X/<C  
@^\$=> /)DTPT%'?!A.%,%^^4/0!TB3;L4RZ!Z2<']CP 
@"CL?1K 7^ZSE;$;L&@HZX%%\-B<V&L7*8_:]J;:.B8T 
@P]E^/%D6!D=1.</D$=(KH2<<%7W<4"W UXQ@EV.$7WX 
@:JUQ+%7/Z??>"61])7&<*\#GY #75S,8/]&G#S]:D98 
@ZG),X].:[VH#*9HTE1J3OE#V) 2!Z^#T[T5=^78%:XX 
@5%7T?PA'1P& 2Y]_(9VL;[RHFN2/Z/<T>J^EGC&YWP( 
@]-I'(:-7H%?^E5:(."TFY-?I3YVH5:JUX F^L(X61E@ 
@!9L"GK/L<N!]7J+"WH(@%&_ $Q3B2Y-#&JA39COV)80 
@G]:(]JE1C.5/A/DZ["^%TG,S=KOGUO_V<M:.L<("894 
@0!F#XS0')Q<J>16I)'CO2)PFLQ9A?L\?UPV-#1% KK( 
@]>3)S^.Q/P/K+-L0^H?YF,VG86#;0.M JJ6AU1M3)18 
@4X(="0P4V(US1A_L$DP$<=QO/"29W?S.=B&/>YAR0N, 
@JVGS+S^]"8='%9XK,0FK(;85DK&5Q+SR&@.JT 6- $$ 
@4S@F+<O'UY*"PP0]6&_1IQ"]"@J,*PSZ?)3PNC3C=B@ 
@P3H"(EZV:RUVTB@%#O)4&H8=T=E5CXU/B63Q/'YN+;D 
@L2PX0.E&KLHC2O2MQ^EFYXMJ&F9E+FIC&$,.@" 1G@T 
@PM]_FHGA0C,U@G*3#X^<,44O2&'WXV@\8:@X5.?9QS\ 
@JPX-M9))88.@R$+,3X+Y"9%A!*8"'R=7"?E@5G&YD:, 
@:BJQ%^PQO[!L(RTWJS!I+M*KZ_BXI$6M!QI3GP&+!C\ 
@?F!2AS/-9^O*RL N:';O;63:1I7$\[I-_K)Y/$,T?DL 
@<-GN37*^+1'54FUZU8&5-Y9[917_OK&P1ZUJ@APG\A  
@P4=@PJ[K8%SMH;SRL\P+>"*0Y/X'=Q*G.:/[8TX(KW4 
@*8U+]-U$&[VIM^)6L=/"RW!C2=J>].VL>9*$*/N+^U< 
@KM]-1L,D6%=M2,4EC$7<O2W8O,I<T$P ITM+9TNO\W0 
@[X+?DQ "YP@*A\*^2*WI((ZBCO^73$@F Y>FGYA4H-L 
@@(-A@ ;Z&^P#)S4SZ+1CM14>?$*RN'MQ2][3<YEZ!X, 
@?U_?&HV3$2TAAA(U6QWS=B+1'O"?7]KAKX1[5>4D=I@ 
@?Y=7S=SAF'_1VAB(\*OW*_: !]9V:'R9@#,];#C"B&D 
@XO-<FFY\GT-<,$(- @U+"0%JF,QJ#J6LQFH#(!\=+!\ 
@%L?GVF@1BBNL/.JR@@AC;"FT+&KL7WG[J2\NVA\$PI  
@IT?3/XD0<MS3H0ZIC.>%_5P?N"%QAZ10%VJ3KP-LS_0 
@6R8'R"2Y!ZJH>;?Q_%=E*NN-TS%%2;UP5;F9]65/^9H 
@XD).-B3[<[AH)E>H"@VZ4HZ1HZM0H4C[L49D_I/AZ;  
@PS%J# (Z4$\L;M40*#D_J@C"/Y7E0FM+:R%JHJM..VL 
@OZ< E])RJ@ @SP=%V$RI!PV$S<;(JH4#]Y/X#E*9'I< 
@"BE"TT4O5R*91Y#R=9K;T;[2%= RZ0A&!W%! S2O[/L 
@E*?!DU^]UN(2IX*,$3$":JF<9-/MPFJ<*2[YV@%$ABX 
@M819^5I$)A(0EK-B?.2F->>=RSFJ ;L+!*N^+@_K)>P 
@N@\TO)H>@46T#O?TL6 /N=>CMJVT>32@Z,CUN_B3- < 
@8 /&WNF*-%\?3[KB-\AL@1TC=(LD8X)JMD8G<"$+3+, 
@)$8GLFCA"TB(6'[K$/V^B!E<&.SG^?LVE;4)LR!*O0< 
@QVCZTV8LEC&I/XV&Q2A!&?3QT=&7+Z8^5KL(HXJ' @< 
@JHSO9/MT>P[/5S0/.^B4\;J;-6&R 21[%K4URZ-;?$T 
@.YV]WTQ^7%\?  F1]'][VVB0PRJ+51DP/-S"4$1@2*, 
@,"Y<R3L'&5+]U7?V4KGO1,T2JP%Y*;8"-NN+R(.!E:T 
@/2=)4PR%)A:DW,)M%E://L?$%?"%WAY.NIE]U:DBJ8  
@:$!"BO5CZS?RS(M\H71R^VVM?;78;'MJ"))KI0_$B\4 
@B+&"@J@2I-DM5;UK:QOC?_">*%ZD 8^B+>!TYR\M(PT 
0<FLUF(T>#*+$&FW(H(+LV   
0O)B(K#>>?'G,/S[Z,I.M3@  
`pragma protect end_protected
