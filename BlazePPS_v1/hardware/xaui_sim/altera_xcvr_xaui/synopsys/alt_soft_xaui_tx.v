// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H2E  &$DEMV[#4*8A"\H!9Y31*= 3FK<%2RZ2(O*M1+B><F4(YM.2&   
H ),8^'#U]5LLBMZP<)>!U@B"&$)JV@[9->V^W:7_YXG[U1]R]=(^8@  
H:I_<9SZ6VP2%M(N+]\P:Y@_"'(L*8FX!6BO6QYU<XM[._S';-:59.0  
H0Y?!GG9 SM,?\10>G_T"*@/$>D-^NWO&;T;\K_107(*.T5[PLA6WB@  
HNEQ!69JJSAEPN=@AE/RJ4^0&P"F8ZHQ]W1B7A\P_$G^[F0QQ_<!=;0  
`pragma protect encoding=(enctype="uuencode",bytes=2944        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@^[B?L&&GI/MJ]BJPE\*79*& ?!W,"Q1C[4IQ%"55O @ 
@#/5#4G,N3O^F@/<//=EB,SXR'!E;'665[$8,&C"HIH8 
@ =)5$KG'4J$C30-+J%*[\;5K27H?(9Z(&?CVRA7DH$8 
@68DB0BP! MX"YV45K=WN\Q1; V%R!.R@C%AL1C1P/1P 
@:['FVP+"8N8O14)0DA*3\C18*%^HYQ-O68&&9$PJ#0X 
@!X'N3&8GG &8Q^"">!*Y79[OXXY?; P7E5"Y5BI*?J\ 
@9 )SGWG#ALF.@U2H%W XU]^8*Z_SL!D2!!DN;Q^9)"T 
@(*X]2[$EKC&O1&JI*)J ^]#ZI=;,/X&[P!EDO^;!(#\ 
@+I?*Y))/XX>(V!&3Z./DP%-F#BZH["RAZ:. -JOZ+OL 
@,'!^Z4>OWT5[0O%N=_0FJX]_3*-[GNF*S^VQP]]4D;\ 
@-LO-O,=[TVD(,7N]V\*,XL*^'$0\ND7--ANR$# 7,6$ 
@BYG+W4R(PDC' 78 +:&_9X4<2./%2C^JAA\O22>(K.0 
@:R0DQ0%0+$S"KY>\7GU09 $ZH?:@N8I\?-GNS*;#4Z  
@_T$3$5TCN):W=5+I.9;S\O.%P7.7<H/RC@;Q:FZ5DE\ 
@7.*&M2)B>;-VTR]:\*Q>5YY-',#6J J 1,MB6V;ST\@ 
@'V2TY((6^GZNW?;J";!,'CB!C>AS$. X=B4KF_H,L.$ 
@#Y0RB2 XN\HE0L5):$9"0CX<)6]LKW<;[PG$F8%ZW\\ 
@.M/*Y5-]O13.Z>0]4$HXWLM7(N:*B -]=?5&>TNMD/\ 
@038]29PSN1!UAYNY^&&8;(@6LZ:+8D"\VWB13/X.9SH 
@!OGX<.FZSC-DG^AP:01'A3L-(J51!/9V2@I__/'Z.&4 
@LK7J%(:HT5(R\WVMB24QD-!!_9U-WTP!FI_-K<%%N[( 
@NK>?WOA?@U#I0'C1&BGPK]KF <5S;F/8Q3HPM\H) 7T 
@7=$:O,<?8QE51+30EA#)L7/84BZ I=-M^&Z"M#6HZ3< 
@1& ?Y,5U/.MU6Q(MW48(,8*W=F>R*F5NVOO\*:Q,ELH 
@K3U=ZW<$ZH.T?T4E!_4452;XTO&:YA)\C<^. 885Y(L 
@F8AH=+ES?*HVV*P=OF02OA(H8,DRV^-DH-Z^+NQ7OQ\ 
@558M^R=:;>[0UKO0+)V.Q L3_X!"<T=NG:3W 6"I_W( 
@M!T(P7,F ^8]&.:#6=9/**B3N'/SM?N;U')+N?/LI L 
@0'TFAJK^,QS4?H6A5T1QN^49JH_FY@&047[GY&,Q9WL 
@O6><'E?<IOWO\[[#QL55I7H/+!=Z=[31,"(QQ-M<]Q  
@\%64=_HD.3_6E=\O]AY-!ZNVP%CL0GV*:B>HOT0>3W8 
@FQ GBHQ^=#2[17YS^,AH-Z9Q3GR1@2+A$[BL@[U@R?H 
@O>F_&ZZ406Q&=CO'2;OA;T8 \E2FY:K:Q_/5WB ;=^  
@*^%[VCL1T!>3U#!I0038TT"%L)(.: E4& !LH?&>T6P 
@\+YJ)3&Y*)0WLSWS(D.T:S!V^W-_O$QZ 'K)EE+BG$D 
@^JMHYP\Z^B=6\[<@39%#+XE(=_ ].37F6&FWY,H/DNP 
@>L*N(E,5D-FCXAN$*&_BL3OTIA7:ZK6!_?]CBYGT_WH 
@>0G@B'\;%B1'V.; 4=Y7KNATIP1!X])\)FL>2&^_@OD 
@.ROGD8B#._J00H@:?B8?$Z.",6P)M)6W2,X+1]746K  
@_@3W1P<AY$%#6R=Y[\93;].#:N2V#1]]F4)7H*45AQ( 
@S96;S,_7M(#5K8\ER6O,*5TK=MC.W^&RQ1DSUZ@.X\L 
@-R=2@('()D@'=+&&RJU+\ [>:!X V$:BO5/T_A.>U%D 
@3B8E\CD4)S0M2BVN2QE5$;&VFPT/Q3!8605WY5BFFI  
@09,\3V'V5F5'P/9)/APJA1$YZ&@!Z 'QO>K68@66YYH 
@*;2&AQYX,/&K"ODI[WP:?QMS<L#/%/,%( [PR*RI6/X 
@?,+UJ#N37GM,$$V'8O;LP!,N7GC7-EJ:#+#Z!-MD,.4 
@\07E]MY;/&/HRLAQWP8]II ,1DNQ^'K &EM3$4>'"W\ 
@0-QL3Y1.[O>\&<5?'(C$@>(TT"M?Q6,EIDY(+,J<* 4 
@%2_/R-+!8M37]#WK*=0T(^1&]!P;PN&FU6KVK83.7H  
@Q*SW_"O?.E.D,Y6I$-A*#WN!LL13[Y"HUR2PD6^WYOD 
@XO5_FGY8*T#BT]YOY%Y0=F_K;&O$/Y$B,RA]HJYSK'( 
@(MD7<OAN^-3TV]-9 5#SW4(H533 L,!.W/3A,HW21!( 
@XH=7E&REQ%MCY(^=G/P"4-!@T%)H_]Z%93U 1,U]?M4 
@5C,'9W"7K$]A2W(HBJM!/._.V8@R7L\3+1V]_B'C9S, 
@?$,!Z^PBQI4*FN%K"5:N.CO9/A0*:UX ,'YU3)Y5^/\ 
@(*!K9W1]7O5%!&W\"XLI,0(&K,B>55*/[6CS9LQ?&>@ 
@[*3D!+1F?WOVEC#)16J4<79F%EXO&^2>*H9V:$,R0 X 
@QK=\*=+H H!+?@0'E$BB!A#Z5N@N %W?F)</R7"3KV0 
@5.J\/!'LF:-F/>NG4/5?@_AH""T$58:2@7SOU7&43Z, 
@5$6Q3G3$1@G:*RJE&?YT$>0ZY)W C@?&D9LUM%0)2M( 
@%FR2?LXCE@)2J9D <'R8%4SB96H1*/K3\F0(P>VQ-'P 
@K#N@0K-ENF3"_AW35N<.SS@@BZWA:3'H&?M!1B!<K.< 
@HVTXX0Y'+;XG+/+488.:;0Z])#:!$2#?:*NG:;W\&20 
@^4BF,S*WZ+CL")# GV<-(#I7'X'I !H).I,%H>)^2:P 
@;9/3Y'8EZW>)0]ZI9R.>F69[#TK1"M709F0>*DEXJ+X 
@/]$,F%M''UJ\5O9@PFY(@]\4;-U'Y"W,,/LH#$I!@L, 
@U6FUTR'N&1T[>7J(V+FTS.VC?"JTN@/8/4&>\U6\X60 
@O>8T:N=:03UDW1,2+/,'\G3ZE.2+Q8G^=@"D'V_^#BD 
@,P\0ZE<,T.CT%&UW4IV2WY7QWMLX!'$L;?/,#U2C%Q< 
@L/>/N'*PGQ)<>7<8FK%N:-QH[LQ@S2V>1ZJ'3RDT$(0 
@^_C;TX3EJ*%\H2G520LY-TQOGSJHM4^)F/EJ:OL2_A< 
@93]Q$>.6UR<. 8[?[Q(3FD\AP]#-GG\NH=,0Z0ZKRC4 
@(N.>$02N5&D((=K=F0>T7:[-7L8%A0@&J$UM;UY0"CT 
@X*E];$14\EFI%^9SRF.%(BP/=50@AY:B?">(ADP]A$@ 
@Q*8>"/K,DGGN;]]JL/GB!S2FEJ#>A*;+(@V92A[GWU< 
@U)R]W35G;3JS.[2>V;(%0Q:?L@@0D5<![=AX#^C@Q1P 
@".ZE#FCEKSX:W%8@TV%6$ME74:RUDMTZ5:U]AC>GM44 
@Z4NW\2O'E;#N(\A##DW1(<>"E!68;Z>XBD?+'@_ ODD 
@B<A;T!'_O76H+;.XV>",&4_^96G60P>3^C&I'P)ON!4 
@K!:V);ZJ$2WK+JF_8M1F2]W<:0\*G81"9"\YH24&F3D 
@5B-'+S4SYM-WUZBF;5;_\LDB( 0-!$ZFCVA:8G^2YDH 
@A75'!J[W]:_EY^-DF-S(GQ[5ZG\B8OMCHOWP2(EJ!"  
@&K.!<?(1N8(HR3SZTM"7^+#J-\,TFC3<:P9;+P/@EB4 
@$^^>>\ZD/=K[ L1,A;.8:MG"W.X63EU<5 C=@MBH;)< 
@D8 -VZV8K 1S@B=OTA7+HZ/27OE]"8 P>_EOU=["ARD 
@U-BR!1XF7PJ\X2O\SCH""1L?;U(,B7X3YSL"=X,\V40 
@Q_A91M\61,B$>Z6JFC&""YY32H%O@6+RGJ*K8KP0#]0 
@AGVB&7FNL3'(G59+?^27JY0@>B5EQ1H]]IZO$?'=35@ 
@1Y]SUXF>K=X!3LPY+$^I@#-L<SEWQ-+OZ#BH/6_ZNV@ 
@#PLK4CL"Y?CK4ZR6#P,%1/3L7K:_-?#/0>%]SZI^,%H 
@ S\ >,#1 UZ!H=Z?6V/#/\BV7V+0 PQ)+O^\OB9.X2$ 
0BJ7HPSY\O4O]+S:7V=)PR0  
0+)VM!9K(,O@^11-9VIM@+   
`pragma protect end_protected
