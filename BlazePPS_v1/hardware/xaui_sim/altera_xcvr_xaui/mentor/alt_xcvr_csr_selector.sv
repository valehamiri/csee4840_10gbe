// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:52:02 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
IQhXMmO4uFhQ2You1bNkaNiCNw3oQ2F9jqVZHVI6+WSeHFHPBYm12WJewAx/1k11
RC8J+aquFJ3kjWspkmm+MkCYeqPmRD13xOAnoO06w7tS0XOvqCB6xTUR8e3liTQ0
nr3JHFc6H+t0v8mEAtmflkpJGFtajnTJeTQTLGR7lsg=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5088)
Rw0oSR1ln9XIQ9rrhKvf0zCvBpeDzDmkhJiYqK3uP3J0ZwMilmxp5TzNaYSicBYw
JrgxkCG4MjQTgdgCwzhLb+4Mrg0sfus9ouOUYf4jXluuFoFir21zF7Uh5qjb6Wdp
epgce3gw19TVZwES6bG5QyhDe93jOQ1HNedpxdgU9Pseqis0WufaQRuS94fs1PB5
qm/MXmnGEhrRRvD04tc5Vz1xomWoKavRnlsvJkAAR37Wy37RkshtCbIKIODzC/qa
Oww0sPDLO+FCMRwq4ihBWzdoO2RX1beKI3XyRH08U0iZaqYcsZe2Yf3HA84UR1Mp
BTPWfUVjLvY7wf2rXz3/UOAb/K+eJhp6OhlmpDgJom40MW8YGRK/BxmJPXKTcuhX
3FBc8/AtX1ZEno4Mfj0DafXVoonfyvwWJNQaTfcaAc+31T1dnQ2pzEx8FEBEIy5S
rCH/vgsu4yBRA23X5XwCEpjwH2T/xYgJFt6rzW03n8LbaIAZhgondYbpPb6AWxEd
R2N20kj2X8n/0dxlTEDH6Vsfp+ioh6iVXq+WZjAEGn2bfoQzzu7Pds+kTnSc42XJ
o7TEr0I6Cu8C3P4peSzT2punm+TVE+5Y6mXwYQcISBLtbPRwHxqAmvpzO/z/NxSv
Dp9dmXkjGjIcNGtWLFSzomfG5WBsM6ZjitpAAbKr0dvbNOAn06dczzrtsJimRtKF
BL7hlDVEOigbny1uPB0lv2PKSbzuUrct/Zh4hQcGCSdtU4M8EMzONbR/4agm2zSh
L/uJQxthhIcyVXfzzmis/WygKh/Wz3elbSEHph90yKQrbEwzXBnHQ624UMFRaEQ1
zKA5/K/p9v8mGbrYLN3Ff5eDb33fa7dAe7hq9NOAmfgikNCh9W38cvaB1vtnBCd2
0l+EF1ZnKR7J+03PibCzVFhz617mUpMjU5e1oY47O8iUq1Hi6wabvuvo0RWUmEL9
HKeOxbn7Y9BNvTWwYsiMD+jYAILKuXjhO76I4+BK0Ey0q8G6BP3nHj2MIdfdRpLo
LbBOXS3a+keEW7sKV1JOrXiXEsGve5JB3HQCEDBwZdhiiL41cZzCkGM0UFzbzx+u
Obw6TLJKE8TQhUjqMn0lz+G4eMlekCFeEX+1Rc4pibLti6CT3uz8jEd7LqKy6f7J
q1c/fdFGFlmjpYDDr5pJ25zr4RuEqin60PEwIsOkmpk/Dazayhs5d8DEJaQuLbwx
FnqKxAjoQ9oA1po7ZyYNityB9GKcHzA2YtFGB17r9MIf8YhBNaSs/Ulrfo8ctoZL
tFmxFIEp4rEWcpxLugddtmDDXQhckevBxQv4u7KGTnF0L6b7GFX13TorDY55+oc+
gyklJxP9/ApYsUqMwG0SoL5UiSrXMJieKW0xPXV6SNxH4586jSeA4+X5KXQsvYS3
FjE5kSBO/qISENZAihuLKV4vqQd4lzrO7N7BSoupH0If65F6xs7YytI4SL+rm02R
albDkp00BOE4ryYJo3vxefdwDOXQ9cewl+TDkC2wvIt8pQQ7rRIp6zkbvNheOjcb
uyM8BuBYpPjjgAHcbNUs7IFiiDgzeZ0EfoRDnnEEzAOoDsiR6Jcs0Eaisvx1UIEi
APG/p1VwjVu+zfNFM0S3CUiLsgK3AagIR2UCktbHLdSkKNgDDbEWtSEZ0t/O6vIR
ejhqPbvi6U0XEIEQVVbVpPT3EDcO4FTr+8To8DvA/6yRaYbLKTi+CyeEaGTG1dYD
6vbMB/cKnQFlbWOMT55tqrSBi5r5SjPq/B6iSLp3VMLlcuBIc8NvUDEcpoLnGBuh
MfQoWcYMhe6f/L+Lrzwn4E3LSc3dHi8Q+4kv52D4wniNbr0lvnXOHhxjORWg8XYF
5T4PreXGS+Qk9jVYCvECI1c8zv6kftQdSuKFNePnbs4IBCcKe7J9vEFGo5bSjzEl
p7uem9S6Saqv6CYw93CbBo9wJP81ovP4Y4sb83kmPpSTLWlU6cZj8gizexXeQ2eP
xIeqK0snPQ/w8ur15CFC/1wF654GSanUp389bc5UKd7Q+yZyVxogUmMLiUHHGI19
61cyiJL+1BorLLaUix7NsvnBx6GtPikyRaV7Z+takViYVN8J35kYYv+K5hNH9be7
CneL7/jf4VxT2jA1Fg3Xls9z6xleeKwogSbEwub1b7dGYtZ5e2ni9H1tYHgzFQxi
a8Fa8Ev4KdcHPCzuG3fhDr9ZPy7MkTx4Oztn9verWwDcsA3bfxdBIUk3CQegb3dw
SWBwJQUf5DyPcYyh0zAg8hBSQDkwRbvGkGbvngKoupGj4ke1950XJWq3yr4JbD4w
fuiJ/uJP04JTc1JNZygbi1hiwQxJdmmoerSx7Xc0+aBxRckhsPA4mKjp9eBe8Zow
Ef3AJzBRzB6Fcbs9B97p3kCR1X8VAzLAhsFxRktHM2iZL+g9b/W9ScjtuWNw5Bv8
xRdeXAqjG2ydGn0TVO55/9Xh/pXpO6RHN4lfzH3Lm/y9fEiyD3BQwvGwlEuLQATu
3Wl5XQT/UF8m3bwENySRQuJs67/9oJ55uawq/tvHTRsJZb8BDuYdSdpQJeZvCroG
DUkIR5C6ZJG4jM3fxLqZYTGPMGCMpe7he/tzBkfVEWNlwf4dWukOXllt5WRLOCGu
6YHRxNxjzl261aYs7XTnUcOfDkAvz9JpfejLYWESHF9bLYxtLAmevorR6p9rJvkG
wit3EOVvidIQ0AMbZkSbeTNQ7C7fNCN7bc7tDLZlPFiZKnGEmrraTPBdHC+1WObn
wiLVQl45TyWuad2KarsRDJxLDLDM/0OUgZyNhRrav36DXGiM7JbK6E6R0ZemwUqZ
u2Ds89D/jh4IDBz9P/nJ195Gv8c0U52tkROYCQZnTbnvVp3JZFhD2qftcZ0KE+bq
DB1bCmA9XOqExASlN8NXShf7XSYs5weEu/b2LQFj3tylOQMqeBX9UtAKkVVrxa7C
2ja+6NAydFNnLoqaJDHHM5anPc+kHapoVbVKX/lJpCYAF63xPiweWDMSNY5bRna5
PoZis0XzLzdRSXurtAdOjg3m5RQNjdOWchv6xXa7XAcJ9uHKX+ENAc4TinbKSVm1
eGzaLXcFOYdui/Bj2xC2/CU+05D23aOK2UhxQG+TSuwVaXl9Ce/BsGYIFloK/iSU
3/Z6eyHRRaNdHY5et4tMSYUSW6WDUCZjEuQEw7JMuUfOjU+WHE63i9g7igKnID02
Qal/jEk2gBT8BMvt9M3xzWez2r4OuUqi/Xrd1D1yuuXeSzyPgx5NRVNnsxwc/kYr
/PofEFC/mW/P+HmlBnqDmxlzUe4v3wWFCvZPp9Z5Oys6zslyC+sBibbFezkHr0Rc
fJXNq2mtK0OqZdgqwrv7ZdVVVxhTSUsmJDTBVoZnbWgQTsZ1yR/x7gQnJ4uAPL6n
4h+TTRu/hNFQnX8kIN6H6Hct3leY5hKQ1TwBmh0qilNxbpXyrIcxBNEB1GZ8ZV0W
U0Rc0f4OqahpQcPeZcQ4BsgylVdqaNFhfLa4Xsed7DXvVFYxjgpfz5GrzDWZw8hD
HWPZbGyV9ZZx720NIxEL25lg67yzCiyOtUuD450RnieBvzU4Fz3rgC98acS95wGH
HuZ+MaUYFKxn0OmUP5KjgH0Dki/Nve5aM2j+zLqsw93deJ/veMB9Sf+1diGP9XCY
twGtzY3E4QGZ1Py5nIcG0DQQzYHgQvpNIL4F9M7i88SXcOSgBy2Uinn2a29PnfiT
IwSQYnY7u+Yy5sGqnvQTwLe31egBbSlo8GLuzQOgV+MVxReHHMGuualKwoGovzTQ
zRGKsJizBV9uaK3CO2CfF26IYzTWRHtcrkPyYNR87BFaY84ryHBr3ZTBQ/ZvTbxP
jPX11asjny1B3d72DshAjhfLshA4jbuyy+jY/d5DZBt20ZaA0yyo2uSLCHX30njc
+B2xOSba71VtTu+6cg/BjNQFqg9QJKLJlOp4EQfglfHF3L391pVWKLdQ+WSt1iLD
y/LItXQSmoIenRG39CLOLH2w/nL1jLxMLt4+XMWc8RjXPbymMKELDzEXucS8Z+4l
ovuCOYYeWxjhs9P1m/7jYEkxfb4HApm3hrhziMkPfUMQkYtg/IzuA38LKI433HHO
zrmM59JwtL82L7PeazSjcg3VhG+Ah1RXaHKy2WcWQZXwTSp2GDHOYD78cvEQ2ehm
OFfSX6vOmGQ8glg0nLOBkAFfPaGMqkA/SIJuR+zhw+QNgcR1kVgcs66g9aWXC1FZ
NNV/EPjfDgkv7WBghEiIsoILkuIuQlUg2rb/VL1k3ns51IeYkPSB1KNsO1uKYc/q
iCz0kDurAeKxWWjxR9KqziWMGmwCg06woPTvNkxK16eMkCgHN2fmmT/t3W+fTcN3
sCd1R+ggFSo8Z+gZ12oVcrhC9W2uelEr/HpfLgrspFjjifU8gs96Y2NgBqmTbX/z
BgL+sjaXPYQwIBelzCvcep1S/LFWO2I36Gx2qz+HWTQQz173TNa1JO8UUmHf1KBu
xkoMdthCTmCeE9arhM3lt9mzoFto09+SYgnzlbx3dT2WjTQ0YuMksMGfhSdm5H+A
k9PneCU6YdbR+FBgLB1LqN0tkKwsmhljJOsRMOhv2+glZWbbrRPRIL7fG9gUI20C
bCndkz8RHxuN8JpBrdJGA5O0sdEPW3aAafHHb5IbMJstq4Uqe3MX3q9UsBYfp3Lj
HPRG5ZA8KCKoXyZ6ymmtY/im5znaXcQ87Qxq1fyaWtC0t+Z46rOW7TBrrE1sxag6
cBkLdkHARnEAIyLOlopJr62pqLeOmx3jf4j1xyhTfsu0rYw9QttwPopc0QuEzfxe
iomXx8rKh87aypjziDm7Wb1ZRGNt3JQ2+BnFiBfVTtdmOqbdlNM2o/v9onMG4Q1j
gjnG6PGNPA7be6h/KJR+mJAyHoh/+WzyMkfY0MB9twrNoz+Lh+B+dAHZzXqtgUww
GNZIXbqCZZi4/3ei9vTYdws2oc9ukV3zeHP1eUh77O/UY/5kP7sTCQ6dRV1ouFbI
WlewnIIZAxZMSII2SDkcStagVsxCdy0kRqRGCRYkXrL/m7+KpvKOPmkshp2t3JV9
qEgtgJPaJ8fY8r/mJttyrhdbRZxZuyYIkdekxYmr8x6ec3mob9o20K5JXPsOhb0f
vbIJyZtROo7tWp4zVnHGZL7mbSEfoKybmRiOGGqbAWObM765uHWB4nOlcWdaWJnf
3OuxkwwKaATlnt9TXO8kXWTkl/MgCKZRHmsC3kqIJHldyydIrqr2kmr+kAfVO145
L9mmnChJeqt3r79h5dBvcjO1fWLo3DX8BC9EKFwWiatpDu+Som8jh1nV7FubZ/AU
DK5A7mnGDwicXqXU9wnJKCENvO4qxQDAK1xKQW/0xhTHZcJGJwi0DAq4FFn4NXtd
X2K8K59pGsTznKyBQP0YWyYxuuftjWN1D2dQ1rhPKQLLZtohXwh90ZRJM93kLXnE
Q+oBCFwqmUNgRawNYxC0mIj7v1YZWPX65fS5STBejqZeKWfK4jkTrvrDKqYC5mGc
3rCHJsJ9yQIY+t7YxM+W1O/TAR0JJldNafFR5KcK1Cbvo2qfqu3/pl4hL/LJxW2V
zkIaT5D9jjLdvWMpI6U8DiJbf1RXpe7ryHEyXD6BXZboSzcMi5NkvXT9sIW4tSMg
9SEkdOemdchg2HG7B5iAWziulpks4CLaN05e/VQtZYIfC01fdyZmPdT10ce/o3pM
XuD8ta8gpLYOiGnimH3Vxi/IODSZ52qtyufyBfAP2GH1sZq7yTIULRHiFz4HxNI/
btkidnavJ3EJUPHMgFCqmiLoAwSYSTBtdOKB24wzjPIV3I0688OZqwtK5MQnaGd7
xQt8Jni7YQ6ktTUcQ9vzPFw2yBAXBRYSrgtY4u2XY4YygV5rrxBrlQ/lhJN+JT6W
GgWyE8UO6A//bdpWh7ZD0q975JWllI/5XzzGi/ez8dg3oJU79I1/UmRVNRZIX3/B
e4DGXSVNOqXqWIpxbzVAu+inUBdVsb/+SHXTxXhG9TxxrRqdkuxUqsyW5oDAT4aS
nRbP2NrrtpS5pW5uZx5lxvwU2pJS2cWaBjsIVqEvihK41RA0yzZ+M45cdlnmDmmL
lMRlGH1jqn4/K+3az5myhYVX6nltPzPIJn59or/rIvkSUri9CRuZinZ6wxkhxmps
BgSMIoJKbwVhGw9TNyZr7Oo21Oy9H5dnLQHUY4l5nGOmRxnziARoLd2nDXenoWLk
zcwZ/msghZ6g3CExBDq44HlXry1N+FesUwdq79uvnw8nmKzfzxIqKdByoyIKrBAk
QIAuBzS5KHHihGyotqsOtm0UF4Sw+nWwvj/Fib770yQ+8HU1nVJxiCAYFaCJ4P5A
0fjJDCNuGGXx8MfZVIhNQwmsUpY5JMBd0zjMXRz7xrOrgsriGhCDoidjB9zNzOyr
CCYW0s83J7zcfc5EfHQclT/eFvgjdMRyXFYKDpqJGrwJdpribUVNuZip4Ki5zB+l
Szr15X+4bEhyHtBeIcWtsMuHzGtDk12FsyECjYOBS8+pMUyBR/GFQqYBeLW6XjdS
QeVVWL/XLOc/6kvUFNfosyP087nWG5TgycCIq9Aj4sg9akeJ3AqQiBSec/PXfrwL
YyHmtpNkBr4cPcmnbmXXVDt3DZcBZda6AqkFi7mA400U+IOi8rmLdDD6GMhaO3BR
fi/lguHwuFNulKqEOVWuSaFvW31UD0g86uCAxArta+so3LpgxUWZYOnCpZxa61S/
VzTSGHjF+ybxkjmGK9jMnUzm3niZfMl+72v6vkmw6OhVXvbcLONVJ32c/GbB03O2
`pragma protect end_protected
