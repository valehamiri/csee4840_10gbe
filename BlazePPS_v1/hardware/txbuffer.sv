module txbuffer(
				input logic clk,
				input logic reset,
				input  logic  [63:0] stdata,                 //  stream_src.data
				input  logic valid,                //            .valid
				input  logic sopin,        //            .startofpacket
				input  logic eopin,          //            .endofpacket
				input  logic [2:0] emptyin,                //            .empty
				
				output  logic  [63:0] databuff,    
            output  logic validout, 
            output  logic readybuff,  
				output  logic sopout,    
            output  logic eopout,     
            output  logic [2:0]emptybuff,
				output  logic readytx                //            .ready	
);

				logic [7:0] cnt;
				logic [7:0] cnt2;
				logic [0:190][63:0] fifo;
				logic flag;
				logic sendflag;
				logic validbuff;
				logic sopbuff;    
				logic eopbuff;  

				assign readytx  = ! sendflag;

				
				
				
		always @(posedge clk)
				begin
						validout	 <=validbuff;
                  sopout    <=sopbuff;
                  eopout    <=eopbuff;
				end
	
	
			initial 
				begin
							databuff <= 64'b0;
							cnt <= 8'b0;
							cnt2<=8'b0;
							sendflag <= 0;
				end

		always @(posedge clk) 
				begin
						if (reset == 1)
							begin
								databuff <= 64'b0;
								cnt <= 8'b0;
								cnt2<=8'b0;
								sendflag <= 0;
						    end 
						else 
							begin
									/////////////read from dc fifo//////////////
									if (valid && readytx)
											begin
												fifo[cnt][63:0] <= stdata;
												cnt <= cnt +1;
												flag <=1;
											end
									
									if (eopin && valid)
											begin
												sendflag <= 1;
									end
									/////////////////write to mac///////////////
									else if (sendflag)
										begin
											if (flag)
													begin
														sopbuff <=1;
														flag <=0;
														databuff <= fifo[cnt2][63:0];
														cnt2 <= cnt2+1;
														cnt <= cnt -1;
														validbuff <=1;
											end 
											
											if (cnt ==0)
													begin
															eopbuff  <=0;
															sendflag <=0;
															validbuff <=0;
															databuff <= fifo[cnt2][63:0];
											end 
											else if (cnt ==8'b1)
													begin
														eopbuff<=1;
														databuff <= fifo[cnt2][63:0];
														cnt2 <= cnt2+1;
														cnt <= cnt -1;
											end 
											else if (flag ==0)
													begin
														databuff <= fifo[cnt2][63:0];
														cnt2 <= cnt2+1;
														cnt <= cnt -1;
														sopbuff <=0;
											end
									 else 
										begin
											databuff <= 64'b0;
											cnt2 <= 8'b0;
										end
								end
				end
		end
endmodule


