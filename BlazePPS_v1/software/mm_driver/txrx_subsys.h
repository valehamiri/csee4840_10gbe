#ifndef _TXRX_SUBSYS_H
#define _TXRX_SUBSYS_H

#include <linux/types.h>
#include <linux/ioctl.h>

#define TXRX_MAGIC 'q'

#define TX_DATA _IOW(TXRX_MAGIC,  1, unsigned int *)
#define RX_DATA _IOR(TXRX_MAGIC, 2, unsigned int *)
#define CONTROL _IOW(TXRX_MAGIC, 3, unsigned int *)
#define STATUS  _IOR(TXRX_MAGIC, 4, unsigned int *)
#define PIO_CLR _IOW(TXRX_MAGIC,  5, unsigned int *)
#define PIO_SET _IOW(TXRX_MAGIC,  6, unsigned int *)

#endif
