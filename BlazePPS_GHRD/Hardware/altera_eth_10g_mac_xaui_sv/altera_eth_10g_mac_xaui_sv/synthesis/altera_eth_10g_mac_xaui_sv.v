// altera_eth_10g_mac_xaui_sv.v

// Generated using ACDS version 13.1 162 at 2015.04.12.15:24:58

`timescale 1 ps / 1 ps
module altera_eth_10g_mac_xaui_sv (
		input  wire         ref_clk_clk,                           //                    ref_clk.clk
		input  wire         ref_reset_reset_n,                     //                  ref_reset.reset_n
		input  wire         tx_clk_clk,                            //                     tx_clk.clk
		input  wire         tx_reset_reset_n,                      //                   tx_reset.reset_n
		output wire         xgmii_rx_clk_clk,                      //               xgmii_rx_clk.clk
		output wire         avalon_st_rxstatus_valid,              //         avalon_st_rxstatus.valid
		output wire [39:0]  avalon_st_rxstatus_data,               //                           .data
		output wire [6:0]   avalon_st_rxstatus_error,              //                           .error
		output wire [1:0]   link_fault_status_xgmii_rx_data,       // link_fault_status_xgmii_rx.data
		input  wire [3:0]   rx_serial_data_export,                 //             rx_serial_data.export
		output wire [3:0]   tx_serial_data_export,                 //             tx_serial_data.export
		output wire         rx_ready_export,                       //                   rx_ready.export
		output wire         tx_ready_export,                       //                   tx_ready.export
		input  wire [63:0]  tx_sc_fifo_in_data,                    //              tx_sc_fifo_in.data
		input  wire         tx_sc_fifo_in_valid,                   //                           .valid
		output wire         tx_sc_fifo_in_ready,                   //                           .ready
		input  wire         tx_sc_fifo_in_startofpacket,           //                           .startofpacket
		input  wire         tx_sc_fifo_in_endofpacket,             //                           .endofpacket
		input  wire [2:0]   tx_sc_fifo_in_empty,                   //                           .empty
		input  wire         tx_sc_fifo_in_error,                   //                           .error
		output wire [63:0]  rx_sc_fifo_out_data,                   //             rx_sc_fifo_out.data
		output wire         rx_sc_fifo_out_valid,                  //                           .valid
		input  wire         rx_sc_fifo_out_ready,                  //                           .ready
		output wire         rx_sc_fifo_out_startofpacket,          //                           .startofpacket
		output wire         rx_sc_fifo_out_endofpacket,            //                           .endofpacket
		output wire [2:0]   rx_sc_fifo_out_empty,                  //                           .empty
		output wire [5:0]   rx_sc_fifo_out_error,                  //                           .error
		output wire         mdio_mdc,                              //                       mdio.mdc
		input  wire         mdio_mdio_in,                          //                           .mdio_in
		output wire         mdio_mdio_out,                         //                           .mdio_out
		output wire         mdio_mdio_oen,                         //                           .mdio_oen
		output wire         avalon_st_txstatus_valid,              //         avalon_st_txstatus.valid
		output wire [39:0]  avalon_st_txstatus_data,               //                           .data
		output wire [6:0]   avalon_st_txstatus_error,              //                           .error
		input  wire [18:0]  mm_pipeline_bridge_address,            //         mm_pipeline_bridge.address
		output wire         mm_pipeline_bridge_waitrequest,        //                           .waitrequest
		input  wire         mm_pipeline_bridge_read,               //                           .read
		output wire [31:0]  mm_pipeline_bridge_readdata,           //                           .readdata
		input  wire         mm_pipeline_bridge_write,              //                           .write
		input  wire [31:0]  mm_pipeline_bridge_writedata,          //                           .writedata
		input  wire         mm_clk_clk,                            //                     mm_clk.clk
		input  wire         mm_reset_reset_n,                      //                   mm_reset.reset_n
		output wire [367:0] reconfig_from_xcvr_data,               //         reconfig_from_xcvr.data
		input  wire [559:0] reconfig_to_xcvr_data,                 //           reconfig_to_xcvr.data
		output wire         reconfig_busy_reconfig_busy,           //              reconfig_busy.reconfig_busy
		output wire [559:0] reconfig_to_xaui_reconfig_to_xcvr,     //           reconfig_to_xaui.reconfig_to_xcvr
		input  wire [367:0] reconfig_from_xaui_reconfig_from_xcvr, //         reconfig_from_xaui.reconfig_from_xcvr
		output wire         master_reset_reset                     //               master_reset.reset
	);

	wire         merlin_master_translator_avalon_universal_master_0_waitrequest;            // mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_waitrequest -> merlin_master_translator:uav_waitrequest
	wire   [2:0] merlin_master_translator_avalon_universal_master_0_burstcount;             // merlin_master_translator:uav_burstcount -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_burstcount
	wire  [31:0] merlin_master_translator_avalon_universal_master_0_writedata;              // merlin_master_translator:uav_writedata -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_writedata
	wire  [31:0] merlin_master_translator_avalon_universal_master_0_address;                // merlin_master_translator:uav_address -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_address
	wire         merlin_master_translator_avalon_universal_master_0_lock;                   // merlin_master_translator:uav_lock -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_lock
	wire         merlin_master_translator_avalon_universal_master_0_write;                  // merlin_master_translator:uav_write -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_write
	wire         merlin_master_translator_avalon_universal_master_0_read;                   // merlin_master_translator:uav_read -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_read
	wire  [31:0] merlin_master_translator_avalon_universal_master_0_readdata;               // mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_readdata -> merlin_master_translator:uav_readdata
	wire         merlin_master_translator_avalon_universal_master_0_debugaccess;            // merlin_master_translator:uav_debugaccess -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_debugaccess
	wire   [3:0] merlin_master_translator_avalon_universal_master_0_byteenable;             // merlin_master_translator:uav_byteenable -> mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_byteenable
	wire         merlin_master_translator_avalon_universal_master_0_readdatavalid;          // mm_interconnect_0:merlin_master_translator_avalon_universal_master_0_readdatavalid -> merlin_master_translator:uav_readdatavalid
	wire         mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_waitrequest; // eth_10g_design_example_0:mm_pipeline_bridge_waitrequest -> mm_interconnect_0:eth_10g_design_example_0_mm_pipeline_bridge_waitrequest
	wire  [31:0] mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_writedata;   // mm_interconnect_0:eth_10g_design_example_0_mm_pipeline_bridge_writedata -> eth_10g_design_example_0:mm_pipeline_bridge_writedata
	wire  [18:0] mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_address;     // mm_interconnect_0:eth_10g_design_example_0_mm_pipeline_bridge_address -> eth_10g_design_example_0:mm_pipeline_bridge_address
	wire         mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_write;       // mm_interconnect_0:eth_10g_design_example_0_mm_pipeline_bridge_write -> eth_10g_design_example_0:mm_pipeline_bridge_write
	wire         mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_read;        // mm_interconnect_0:eth_10g_design_example_0_mm_pipeline_bridge_read -> eth_10g_design_example_0:mm_pipeline_bridge_read
	wire  [31:0] mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_readdata;    // eth_10g_design_example_0:mm_pipeline_bridge_readdata -> mm_interconnect_0:eth_10g_design_example_0_mm_pipeline_bridge_readdata
	wire         jtag_master_master_waitrequest;                                            // mm_interconnect_0:jtag_master_master_waitrequest -> jtag_master:master_waitrequest
	wire  [31:0] jtag_master_master_writedata;                                              // jtag_master:master_writedata -> mm_interconnect_0:jtag_master_master_writedata
	wire  [31:0] jtag_master_master_address;                                                // jtag_master:master_address -> mm_interconnect_0:jtag_master_master_address
	wire         jtag_master_master_write;                                                  // jtag_master:master_write -> mm_interconnect_0:jtag_master_master_write
	wire         jtag_master_master_read;                                                   // jtag_master:master_read -> mm_interconnect_0:jtag_master_master_read
	wire  [31:0] jtag_master_master_readdata;                                               // mm_interconnect_0:jtag_master_master_readdata -> jtag_master:master_readdata
	wire   [3:0] jtag_master_master_byteenable;                                             // jtag_master:master_byteenable -> mm_interconnect_0:jtag_master_master_byteenable
	wire         jtag_master_master_readdatavalid;                                          // mm_interconnect_0:jtag_master_master_readdatavalid -> jtag_master:master_readdatavalid
	wire         mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_waitrequest;           // alt_xcvr_reconfig_0:reconfig_mgmt_waitrequest -> mm_interconnect_0:alt_xcvr_reconfig_0_reconfig_mgmt_waitrequest
	wire  [31:0] mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_writedata;             // mm_interconnect_0:alt_xcvr_reconfig_0_reconfig_mgmt_writedata -> alt_xcvr_reconfig_0:reconfig_mgmt_writedata
	wire   [6:0] mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_address;               // mm_interconnect_0:alt_xcvr_reconfig_0_reconfig_mgmt_address -> alt_xcvr_reconfig_0:reconfig_mgmt_address
	wire         mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_write;                 // mm_interconnect_0:alt_xcvr_reconfig_0_reconfig_mgmt_write -> alt_xcvr_reconfig_0:reconfig_mgmt_write
	wire         mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_read;                  // mm_interconnect_0:alt_xcvr_reconfig_0_reconfig_mgmt_read -> alt_xcvr_reconfig_0:reconfig_mgmt_read
	wire  [31:0] mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_readdata;              // alt_xcvr_reconfig_0:reconfig_mgmt_readdata -> mm_interconnect_0:alt_xcvr_reconfig_0_reconfig_mgmt_readdata
	wire         rst_controller_reset_out_reset;                                            // rst_controller:reset_out -> [alt_xcvr_reconfig_0:mgmt_rst_reset, merlin_master_translator:reset, mm_interconnect_0:jtag_master_clk_reset_reset_bridge_in_reset_reset, mm_interconnect_0:merlin_master_translator_reset_reset_bridge_in_reset_reset]

	altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0 #(
		.starting_channel_number      (0),
		.interface_type               ("Soft XAUI"),
		.data_rate                    ("3125 Mbps"),
		.xaui_pll_type                ("AUTO"),
		.BASE_DATA_RATE               (""),
		.en_synce_support             (0),
		.use_control_and_status_ports (0),
		.external_pma_ctrl_reconf     (0),
		.recovered_clk_out            (0),
		.number_of_interfaces         (1),
		.reconfig_interfaces          (1),
		.use_rx_rate_match            (0),
		.tx_termination               ("OCT_100_OHMS"),
		.tx_vod_selection             (4),
		.tx_preemp_pretap             (0),
		.tx_preemp_pretap_inv         ("false"),
		.tx_preemp_tap_1              (0),
		.tx_preemp_tap_2              (0),
		.tx_preemp_tap_2_inv          ("false"),
		.rx_common_mode               ("0.82v"),
		.rx_termination               ("OCT_100_OHMS"),
		.rx_eq_dc_gain                (0),
		.rx_eq_ctrl                   (0)
	) eth_10g_design_example_0 (
		.mm_clk_clk                      (mm_clk_clk),                                                                //                     mm_clk.clk
		.mm_reset_reset_n                (mm_reset_reset_n),                                                          //                   mm_reset.reset_n
		.tx_clk_clk                      (tx_clk_clk),                                                                //                     tx_clk.clk
		.tx_reset_reset_n                (tx_reset_reset_n),                                                          //                   tx_reset.reset_n
		.mm_pipeline_bridge_address      (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_address),     //         mm_pipeline_bridge.address
		.mm_pipeline_bridge_waitrequest  (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_waitrequest), //                           .waitrequest
		.mm_pipeline_bridge_read         (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_read),        //                           .read
		.mm_pipeline_bridge_readdata     (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_readdata),    //                           .readdata
		.mm_pipeline_bridge_write        (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_write),       //                           .write
		.mm_pipeline_bridge_writedata    (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_writedata),   //                           .writedata
		.ref_clk_clk                     (ref_clk_clk),                                                               //                    ref_clk.clk
		.ref_reset_reset_n               (ref_reset_reset_n),                                                         //                  ref_reset.reset_n
		.xgmii_rx_clk_clk                (xgmii_rx_clk_clk),                                                          //               xgmii_rx_clk.clk
		.reconfig_from_xcvr_data         (reconfig_from_xcvr_data),                                                   //         reconfig_from_xcvr.data
		.reconfig_to_xcvr_data           (reconfig_to_xcvr_data),                                                     //           reconfig_to_xcvr.data
		.avalon_st_rxstatus_valid        (avalon_st_rxstatus_valid),                                                  //         avalon_st_rxstatus.valid
		.avalon_st_rxstatus_data         (avalon_st_rxstatus_data),                                                   //                           .data
		.avalon_st_rxstatus_error        (avalon_st_rxstatus_error),                                                  //                           .error
		.link_fault_status_xgmii_rx_data (link_fault_status_xgmii_rx_data),                                           // link_fault_status_xgmii_rx.data
		.rx_serial_data_export           (rx_serial_data_export),                                                     //             rx_serial_data.export
		.tx_serial_data_export           (tx_serial_data_export),                                                     //             tx_serial_data.export
		.rx_ready_export                 (rx_ready_export),                                                           //                   rx_ready.export
		.tx_ready_export                 (tx_ready_export),                                                           //                   tx_ready.export
		.tx_sc_fifo_in_data              (tx_sc_fifo_in_data),                                                        //              tx_sc_fifo_in.data
		.tx_sc_fifo_in_valid             (tx_sc_fifo_in_valid),                                                       //                           .valid
		.tx_sc_fifo_in_ready             (tx_sc_fifo_in_ready),                                                       //                           .ready
		.tx_sc_fifo_in_startofpacket     (tx_sc_fifo_in_startofpacket),                                               //                           .startofpacket
		.tx_sc_fifo_in_endofpacket       (tx_sc_fifo_in_endofpacket),                                                 //                           .endofpacket
		.tx_sc_fifo_in_empty             (tx_sc_fifo_in_empty),                                                       //                           .empty
		.tx_sc_fifo_in_error             (tx_sc_fifo_in_error),                                                       //                           .error
		.rx_sc_fifo_out_data             (rx_sc_fifo_out_data),                                                       //             rx_sc_fifo_out.data
		.rx_sc_fifo_out_valid            (rx_sc_fifo_out_valid),                                                      //                           .valid
		.rx_sc_fifo_out_ready            (rx_sc_fifo_out_ready),                                                      //                           .ready
		.rx_sc_fifo_out_startofpacket    (rx_sc_fifo_out_startofpacket),                                              //                           .startofpacket
		.rx_sc_fifo_out_endofpacket      (rx_sc_fifo_out_endofpacket),                                                //                           .endofpacket
		.rx_sc_fifo_out_empty            (rx_sc_fifo_out_empty),                                                      //                           .empty
		.rx_sc_fifo_out_error            (rx_sc_fifo_out_error),                                                      //                           .error
		.mdio_mdc                        (mdio_mdc),                                                                  //                       mdio.mdc
		.mdio_mdio_in                    (mdio_mdio_in),                                                              //                           .mdio_in
		.mdio_mdio_out                   (mdio_mdio_out),                                                             //                           .mdio_out
		.mdio_mdio_oen                   (mdio_mdio_oen),                                                             //                           .mdio_oen
		.avalon_st_txstatus_valid        (avalon_st_txstatus_valid),                                                  //         avalon_st_txstatus.valid
		.avalon_st_txstatus_data         (avalon_st_txstatus_data),                                                   //                           .data
		.avalon_st_txstatus_error        (avalon_st_txstatus_error)                                                   //                           .error
	);

	altera_eth_10g_mac_xaui_sv_jtag_master #(
		.USE_PLI     (0),
		.PLI_PORT    (50000),
		.FIFO_DEPTHS (2)
	) jtag_master (
		.clk_clk              (mm_clk_clk),                       //          clk.clk
		.clk_reset_reset      (~mm_reset_reset_n),                //    clk_reset.reset
		.master_address       (jtag_master_master_address),       //       master.address
		.master_readdata      (jtag_master_master_readdata),      //             .readdata
		.master_read          (jtag_master_master_read),          //             .read
		.master_write         (jtag_master_master_write),         //             .write
		.master_writedata     (jtag_master_master_writedata),     //             .writedata
		.master_waitrequest   (jtag_master_master_waitrequest),   //             .waitrequest
		.master_readdatavalid (jtag_master_master_readdatavalid), //             .readdatavalid
		.master_byteenable    (jtag_master_master_byteenable),    //             .byteenable
		.master_reset_reset   (master_reset_reset)                // master_reset.reset
	);

	altera_merlin_master_translator #(
		.AV_ADDRESS_W                (19),
		.AV_DATA_W                   (32),
		.AV_BURSTCOUNT_W             (3),
		.AV_BYTEENABLE_W             (4),
		.UAV_ADDRESS_W               (32),
		.UAV_BURSTCOUNT_W            (3),
		.USE_READ                    (1),
		.USE_WRITE                   (1),
		.USE_BEGINBURSTTRANSFER      (0),
		.USE_BEGINTRANSFER           (0),
		.USE_CHIPSELECT              (0),
		.USE_BURSTCOUNT              (0),
		.USE_READDATAVALID           (0),
		.USE_WAITREQUEST             (1),
		.USE_READRESPONSE            (0),
		.USE_WRITERESPONSE           (0),
		.AV_SYMBOLS_PER_WORD         (4),
		.AV_ADDRESS_SYMBOLS          (1),
		.AV_BURSTCOUNT_SYMBOLS       (1),
		.AV_CONSTANT_BURST_BEHAVIOR  (0),
		.UAV_CONSTANT_BURST_BEHAVIOR (0),
		.AV_LINEWRAPBURSTS           (0),
		.AV_REGISTERINCOMINGSIGNALS  (0)
	) merlin_master_translator (
		.clk                      (mm_clk_clk),                                                       //                       clk.clk
		.reset                    (rst_controller_reset_out_reset),                                   //                     reset.reset
		.uav_address              (merlin_master_translator_avalon_universal_master_0_address),       // avalon_universal_master_0.address
		.uav_burstcount           (merlin_master_translator_avalon_universal_master_0_burstcount),    //                          .burstcount
		.uav_read                 (merlin_master_translator_avalon_universal_master_0_read),          //                          .read
		.uav_write                (merlin_master_translator_avalon_universal_master_0_write),         //                          .write
		.uav_waitrequest          (merlin_master_translator_avalon_universal_master_0_waitrequest),   //                          .waitrequest
		.uav_readdatavalid        (merlin_master_translator_avalon_universal_master_0_readdatavalid), //                          .readdatavalid
		.uav_byteenable           (merlin_master_translator_avalon_universal_master_0_byteenable),    //                          .byteenable
		.uav_readdata             (merlin_master_translator_avalon_universal_master_0_readdata),      //                          .readdata
		.uav_writedata            (merlin_master_translator_avalon_universal_master_0_writedata),     //                          .writedata
		.uav_lock                 (merlin_master_translator_avalon_universal_master_0_lock),          //                          .lock
		.uav_debugaccess          (merlin_master_translator_avalon_universal_master_0_debugaccess),   //                          .debugaccess
		.av_address               (mm_pipeline_bridge_address),                                       //      avalon_anti_master_0.address
		.av_waitrequest           (mm_pipeline_bridge_waitrequest),                                   //                          .waitrequest
		.av_read                  (mm_pipeline_bridge_read),                                          //                          .read
		.av_readdata              (mm_pipeline_bridge_readdata),                                      //                          .readdata
		.av_write                 (mm_pipeline_bridge_write),                                         //                          .write
		.av_writedata             (mm_pipeline_bridge_writedata),                                     //                          .writedata
		.av_burstcount            (3'b001),                                                           //               (terminated)
		.av_byteenable            (4'b1111),                                                          //               (terminated)
		.av_beginbursttransfer    (1'b0),                                                             //               (terminated)
		.av_begintransfer         (1'b0),                                                             //               (terminated)
		.av_chipselect            (1'b0),                                                             //               (terminated)
		.av_readdatavalid         (),                                                                 //               (terminated)
		.av_lock                  (1'b0),                                                             //               (terminated)
		.av_debugaccess           (1'b0),                                                             //               (terminated)
		.uav_clken                (),                                                                 //               (terminated)
		.av_clken                 (1'b1),                                                             //               (terminated)
		.uav_response             (2'b00),                                                            //               (terminated)
		.av_response              (),                                                                 //               (terminated)
		.uav_writeresponserequest (),                                                                 //               (terminated)
		.uav_writeresponsevalid   (1'b0),                                                             //               (terminated)
		.av_writeresponserequest  (1'b0),                                                             //               (terminated)
		.av_writeresponsevalid    ()                                                                  //               (terminated)
	);

	alt_xcvr_reconfig #(
		.device_family                 ("Stratix V"),
		.number_of_reconfig_interfaces (8),
		.enable_offset                 (1),
		.enable_lc                     (1),
		.enable_dcd                    (0),
		.enable_dcd_power_up           (1),
		.enable_analog                 (1),
		.enable_eyemon                 (0),
		.enable_ber                    (0),
		.enable_dfe                    (0),
		.enable_adce                   (0),
		.enable_mif                    (0),
		.enable_pll                    (0)
	) alt_xcvr_reconfig_0 (
		.reconfig_busy             (reconfig_busy_reconfig_busy),                                     //      reconfig_busy.reconfig_busy
		.mgmt_clk_clk              (mm_clk_clk),                                                      //       mgmt_clk_clk.clk
		.mgmt_rst_reset            (rst_controller_reset_out_reset),                                  //     mgmt_rst_reset.reset
		.reconfig_mgmt_address     (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_address),     //      reconfig_mgmt.address
		.reconfig_mgmt_read        (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_read),        //                   .read
		.reconfig_mgmt_readdata    (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_readdata),    //                   .readdata
		.reconfig_mgmt_waitrequest (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_waitrequest), //                   .waitrequest
		.reconfig_mgmt_write       (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_write),       //                   .write
		.reconfig_mgmt_writedata   (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_writedata),   //                   .writedata
		.reconfig_to_xcvr          (reconfig_to_xaui_reconfig_to_xcvr),                               //   reconfig_to_xcvr.reconfig_to_xcvr
		.reconfig_from_xcvr        (reconfig_from_xaui_reconfig_from_xcvr),                           // reconfig_from_xcvr.reconfig_from_xcvr
		.tx_cal_busy               (),                                                                //        (terminated)
		.rx_cal_busy               (),                                                                //        (terminated)
		.cal_busy_in               (1'b0),                                                            //        (terminated)
		.reconfig_mif_address      (),                                                                //        (terminated)
		.reconfig_mif_read         (),                                                                //        (terminated)
		.reconfig_mif_readdata     (16'b0000000000000000),                                            //        (terminated)
		.reconfig_mif_waitrequest  (1'b0)                                                             //        (terminated)
	);

	altera_eth_10g_mac_xaui_sv_mm_interconnect_0 mm_interconnect_0 (
		.mm_clk_clk_clk                                                   (mm_clk_clk),                                                                //                                           mm_clk_clk.clk
		.jtag_master_clk_reset_reset_bridge_in_reset_reset                (rst_controller_reset_out_reset),                                            //          jtag_master_clk_reset_reset_bridge_in_reset.reset
		.merlin_master_translator_reset_reset_bridge_in_reset_reset       (rst_controller_reset_out_reset),                                            // merlin_master_translator_reset_reset_bridge_in_reset.reset
		.jtag_master_master_address                                       (jtag_master_master_address),                                                //                                   jtag_master_master.address
		.jtag_master_master_waitrequest                                   (jtag_master_master_waitrequest),                                            //                                                     .waitrequest
		.jtag_master_master_byteenable                                    (jtag_master_master_byteenable),                                             //                                                     .byteenable
		.jtag_master_master_read                                          (jtag_master_master_read),                                                   //                                                     .read
		.jtag_master_master_readdata                                      (jtag_master_master_readdata),                                               //                                                     .readdata
		.jtag_master_master_readdatavalid                                 (jtag_master_master_readdatavalid),                                          //                                                     .readdatavalid
		.jtag_master_master_write                                         (jtag_master_master_write),                                                  //                                                     .write
		.jtag_master_master_writedata                                     (jtag_master_master_writedata),                                              //                                                     .writedata
		.merlin_master_translator_avalon_universal_master_0_address       (merlin_master_translator_avalon_universal_master_0_address),                //   merlin_master_translator_avalon_universal_master_0.address
		.merlin_master_translator_avalon_universal_master_0_waitrequest   (merlin_master_translator_avalon_universal_master_0_waitrequest),            //                                                     .waitrequest
		.merlin_master_translator_avalon_universal_master_0_burstcount    (merlin_master_translator_avalon_universal_master_0_burstcount),             //                                                     .burstcount
		.merlin_master_translator_avalon_universal_master_0_byteenable    (merlin_master_translator_avalon_universal_master_0_byteenable),             //                                                     .byteenable
		.merlin_master_translator_avalon_universal_master_0_read          (merlin_master_translator_avalon_universal_master_0_read),                   //                                                     .read
		.merlin_master_translator_avalon_universal_master_0_readdata      (merlin_master_translator_avalon_universal_master_0_readdata),               //                                                     .readdata
		.merlin_master_translator_avalon_universal_master_0_readdatavalid (merlin_master_translator_avalon_universal_master_0_readdatavalid),          //                                                     .readdatavalid
		.merlin_master_translator_avalon_universal_master_0_write         (merlin_master_translator_avalon_universal_master_0_write),                  //                                                     .write
		.merlin_master_translator_avalon_universal_master_0_writedata     (merlin_master_translator_avalon_universal_master_0_writedata),              //                                                     .writedata
		.merlin_master_translator_avalon_universal_master_0_lock          (merlin_master_translator_avalon_universal_master_0_lock),                   //                                                     .lock
		.merlin_master_translator_avalon_universal_master_0_debugaccess   (merlin_master_translator_avalon_universal_master_0_debugaccess),            //                                                     .debugaccess
		.alt_xcvr_reconfig_0_reconfig_mgmt_address                        (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_address),               //                    alt_xcvr_reconfig_0_reconfig_mgmt.address
		.alt_xcvr_reconfig_0_reconfig_mgmt_write                          (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_write),                 //                                                     .write
		.alt_xcvr_reconfig_0_reconfig_mgmt_read                           (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_read),                  //                                                     .read
		.alt_xcvr_reconfig_0_reconfig_mgmt_readdata                       (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_readdata),              //                                                     .readdata
		.alt_xcvr_reconfig_0_reconfig_mgmt_writedata                      (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_writedata),             //                                                     .writedata
		.alt_xcvr_reconfig_0_reconfig_mgmt_waitrequest                    (mm_interconnect_0_alt_xcvr_reconfig_0_reconfig_mgmt_waitrequest),           //                                                     .waitrequest
		.eth_10g_design_example_0_mm_pipeline_bridge_address              (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_address),     //          eth_10g_design_example_0_mm_pipeline_bridge.address
		.eth_10g_design_example_0_mm_pipeline_bridge_write                (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_write),       //                                                     .write
		.eth_10g_design_example_0_mm_pipeline_bridge_read                 (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_read),        //                                                     .read
		.eth_10g_design_example_0_mm_pipeline_bridge_readdata             (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_readdata),    //                                                     .readdata
		.eth_10g_design_example_0_mm_pipeline_bridge_writedata            (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_writedata),   //                                                     .writedata
		.eth_10g_design_example_0_mm_pipeline_bridge_waitrequest          (mm_interconnect_0_eth_10g_design_example_0_mm_pipeline_bridge_waitrequest)  //                                                     .waitrequest
	);

	altera_reset_controller #(
		.NUM_RESET_INPUTS          (1),
		.OUTPUT_RESET_SYNC_EDGES   ("deassert"),
		.SYNC_DEPTH                (2),
		.RESET_REQUEST_PRESENT     (0),
		.RESET_REQ_WAIT_TIME       (1),
		.MIN_RST_ASSERTION_TIME    (3),
		.RESET_REQ_EARLY_DSRT_TIME (1),
		.USE_RESET_REQUEST_IN0     (0),
		.USE_RESET_REQUEST_IN1     (0),
		.USE_RESET_REQUEST_IN2     (0),
		.USE_RESET_REQUEST_IN3     (0),
		.USE_RESET_REQUEST_IN4     (0),
		.USE_RESET_REQUEST_IN5     (0),
		.USE_RESET_REQUEST_IN6     (0),
		.USE_RESET_REQUEST_IN7     (0),
		.USE_RESET_REQUEST_IN8     (0),
		.USE_RESET_REQUEST_IN9     (0),
		.USE_RESET_REQUEST_IN10    (0),
		.USE_RESET_REQUEST_IN11    (0),
		.USE_RESET_REQUEST_IN12    (0),
		.USE_RESET_REQUEST_IN13    (0),
		.USE_RESET_REQUEST_IN14    (0),
		.USE_RESET_REQUEST_IN15    (0),
		.ADAPT_RESET_REQUEST       (0)
	) rst_controller (
		.reset_in0      (~mm_reset_reset_n),              // reset_in0.reset
		.clk            (mm_clk_clk),                     //       clk.clk
		.reset_out      (rst_controller_reset_out_reset), // reset_out.reset
		.reset_req      (),                               // (terminated)
		.reset_req_in0  (1'b0),                           // (terminated)
		.reset_in1      (1'b0),                           // (terminated)
		.reset_req_in1  (1'b0),                           // (terminated)
		.reset_in2      (1'b0),                           // (terminated)
		.reset_req_in2  (1'b0),                           // (terminated)
		.reset_in3      (1'b0),                           // (terminated)
		.reset_req_in3  (1'b0),                           // (terminated)
		.reset_in4      (1'b0),                           // (terminated)
		.reset_req_in4  (1'b0),                           // (terminated)
		.reset_in5      (1'b0),                           // (terminated)
		.reset_req_in5  (1'b0),                           // (terminated)
		.reset_in6      (1'b0),                           // (terminated)
		.reset_req_in6  (1'b0),                           // (terminated)
		.reset_in7      (1'b0),                           // (terminated)
		.reset_req_in7  (1'b0),                           // (terminated)
		.reset_in8      (1'b0),                           // (terminated)
		.reset_req_in8  (1'b0),                           // (terminated)
		.reset_in9      (1'b0),                           // (terminated)
		.reset_req_in9  (1'b0),                           // (terminated)
		.reset_in10     (1'b0),                           // (terminated)
		.reset_req_in10 (1'b0),                           // (terminated)
		.reset_in11     (1'b0),                           // (terminated)
		.reset_req_in11 (1'b0),                           // (terminated)
		.reset_in12     (1'b0),                           // (terminated)
		.reset_req_in12 (1'b0),                           // (terminated)
		.reset_in13     (1'b0),                           // (terminated)
		.reset_req_in13 (1'b0),                           // (terminated)
		.reset_in14     (1'b0),                           // (terminated)
		.reset_req_in14 (1'b0),                           // (terminated)
		.reset_in15     (1'b0),                           // (terminated)
		.reset_req_in15 (1'b0)                            // (terminated)
	);

endmodule
