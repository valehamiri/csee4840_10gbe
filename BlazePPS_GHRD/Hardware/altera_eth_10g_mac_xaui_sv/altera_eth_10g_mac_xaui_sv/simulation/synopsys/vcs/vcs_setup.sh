
# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 13.1 162 linux 2015.04.12.15:24:39

# ----------------------------------------
# vcs - auto-generated simulation script

# ----------------------------------------
# initialize variables
TOP_LEVEL_NAME="altera_eth_10g_mac_xaui_sv"
QSYS_SIMDIR="./../../"
QUARTUS_INSTALL_DIR="/opt/altera/13.1/quartus/"
SKIP_FILE_COPY=0
SKIP_ELAB=0
SKIP_SIM=0
USER_DEFINED_ELAB_OPTIONS=""
USER_DEFINED_SIM_OPTIONS="+vcs+finish+100"
# ----------------------------------------
# overwrite variables - DO NOT MODIFY!
# This block evaluates each command line argument, typically used for 
# overwriting variables. An example usage:
#   sh <simulator>_setup.sh SKIP_ELAB=1 SKIP_SIM=1
for expression in "$@"; do
  eval $expression
  if [ $? -ne 0 ]; then
    echo "Error: This command line argument, \"$expression\", is/has an invalid expression." >&2
    exit $?
  fi
done

# ----------------------------------------
# initialize simulation properties - DO NOT MODIFY!
ELAB_OPTIONS=""
SIM_OPTIONS=""
if [[ `vcs -platform` != *"amd64"* ]]; then
  :
else
  :
fi

# ----------------------------------------
# copy RAM/ROM files to simulation directory

vcs -lca -timescale=1ps/1ps -sverilog +verilog2001ext+.v -ntb_opts dtm $ELAB_OPTIONS $USER_DEFINED_ELAB_OPTIONS \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v \
  $QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim.sv \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/stratixv_atoms_ncrypt.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/stratixv_atoms.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/stratixv_hssi_atoms_ncrypt.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/stratixv_hssi_atoms.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/stratixv_pcie_hip_atoms_ncrypt.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/stratixv_pcie_hip_atoms.v \
  $QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_rsp_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_rsp_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_cmd_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_cmd_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_id_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_addr_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_rsp_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_rsp_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_cmd_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_cmd_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_id_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_addr_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_rsp_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_rsp_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_cmd_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_cmd_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_id_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_addr_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_rsp_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_rsp_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_cmd_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_cmd_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_id_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_addr_router.sv \
  $QSYS_SIMDIR/submodules/altera_avalon_st_handshake_clock_crosser.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_clock_crosser.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_base.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_rsp_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_rsp_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_cmd_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_cmd_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_id_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_addr_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_lc_lb_timing_adapter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_line_splitter_timing_adapter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_line_lb_timing_adapter.v \
  $QSYS_SIMDIR/submodules/altera_eth_loopback.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_lc_splitter_timing_adapter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rxtx_timing_adapter_pauselen_tx.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rxtx_timing_adapter_pauselen_rx.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_txrx_timing_adapter_link_fault_status_export.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_txrx_timing_adapter_link_fault_status_rx.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rx_st_error_adapter_stat.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_packet_overflow_control.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_crc_pad_rem.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_crc_rem.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_packet_stripper.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_stage.sv \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_frame_status_merger.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rx_timing_adapter_frame_status_out_frame_decoder.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rx_st_timing_adapter_frame_status_in.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_lane_decoder.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_link_fault_detection.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_10g_rx_register_map.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_link_fault_generation.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_out_0.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_in.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_xgmii_termination.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_packet_formatter.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_delay.sv \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_10gmem_statistics_collector.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_status_output.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_status_in.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_error_adapter_stat.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_frame_decoder.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_frame_decoder.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_splitter.sv \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_crc.v \
  $QSYS_SIMDIR/submodules/synopsys/crc32.v \
  $QSYS_SIMDIR/submodules/synopsys/gf_mult32_kc.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_address_inserter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_mux_flow_control_user_frame.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_pause_ctrl_error_adapter.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_ctrl_gen.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_controller.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_gen.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_beat_conversion.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_pkt_backpressure_control.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_pad_inserter.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_packet_underflow_control.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_eth_10g_tx_register_map.v \
  $QSYS_SIMDIR/submodules/altera_avalon_mm_bridge.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_rsp_xbar_mux_001.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_rsp_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_rsp_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_mux_001.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_demux_001.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_burst_adapter.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_address_alignment.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_traffic_limiter.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_reorder_memory.sv \
  $QSYS_SIMDIR/submodules/altera_avalon_sc_fifo.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_id_router_001.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_id_router.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_addr_router_001.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_addr_router.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_slave_agent.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_burst_uncompressor.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_master_agent.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_slave_translator.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master_p2b_adapter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master_b2p_adapter.v \
  $QSYS_SIMDIR/submodules/altera_avalon_packets_to_master.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_packets_to_bytes.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_bytes_to_packets.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master_timing_adt.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_jtag_interface.v \
  $QSYS_SIMDIR/submodules/altera_jtag_dc_streaming.v \
  $QSYS_SIMDIR/submodules/altera_jtag_sld_node.v \
  $QSYS_SIMDIR/submodules/altera_jtag_streaming.v \
  $QSYS_SIMDIR/submodules/altera_pli_streaming.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_idle_remover.v \
  $QSYS_SIMDIR/submodules/altera_avalon_st_idle_inserter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0.v \
  $QSYS_SIMDIR/submodules/altera_eth_mdio.v \
  $QSYS_SIMDIR/submodules/altera_eth_fifo_pause_ctrl_adapter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_pa_pg_after_timing_adapter.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_pa_pg_before_timing_adapter.v \
  $QSYS_SIMDIR/submodules/altera_avalon_dc_fifo.v \
  $QSYS_SIMDIR/submodules/altera_dcfifo_synchronizer_bundle.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac.v \
  $QSYS_SIMDIR/submodules/altera_xcvr_functions.sv \
  $QSYS_SIMDIR/submodules/alt_pma_functions.sv \
  $QSYS_SIMDIR/submodules/altera_xcvr_xaui.sv \
  $QSYS_SIMDIR/submodules/hxaui_csr_h.sv \
  $QSYS_SIMDIR/submodules/hxaui_csr.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_mgmt2dec_phyreconfig.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_mgmt2dec_xaui.sv \
  $QSYS_SIMDIR/submodules/alt_pma_ch_controller_tgx.v \
  $QSYS_SIMDIR/submodules/alt_pma_controller_tgx.v \
  $QSYS_SIMDIR/submodules/alt_reset_ctrl_lego.sv \
  $QSYS_SIMDIR/submodules/alt_reset_ctrl_tgx_cdrauto.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_resync.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_csr_common_h.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_csr_common.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_csr_pcs8g_h.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_csr_pcs8g.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_csr_selector.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_mgmt2dec.sv \
  $QSYS_SIMDIR/submodules/altera_wait_generate.v \
  $QSYS_SIMDIR/submodules/altera_xcvr_reset_control.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reset_counter.sv \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_pcs.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_reset.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_8b10b_dec.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_channel_synch.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_deskew.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_deskew_channel.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_deskew_ram.v \
  $QSYS_SIMDIR/submodules/synopsys/altera_soft_xaui_rx_deskew_ram.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_invalid_code_det.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_parity.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_parity_4b.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_parity_6b.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_rate_match.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_rate_match_ram.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_rl_chk_6g.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_sm.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_tx.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_tx_8b10b_enc.v \
  $QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_tx_idle_conv.v \
  $QSYS_SIMDIR/submodules/synopsys/l_modules.v \
  $QSYS_SIMDIR/submodules/synopsys/serdes_4_unit_lc_siv.v \
  $QSYS_SIMDIR/submodules/synopsys/serdes_4_unit_siv.v \
  $QSYS_SIMDIR/submodules/synopsys/serdes_4unit.v \
  $QSYS_SIMDIR/submodules/sxaui.v \
  $QSYS_SIMDIR/submodules/sv_xcvr_xaui.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_low_latency_phy_nr.sv \
  $QSYS_SIMDIR/submodules/sv_pcs.sv \
  $QSYS_SIMDIR/submodules/sv_pcs_ch.sv \
  $QSYS_SIMDIR/submodules/sv_pma.sv \
  $QSYS_SIMDIR/submodules/sv_reconfig_bundle_to_xcvr.sv \
  $QSYS_SIMDIR/submodules/sv_reconfig_bundle_to_ip.sv \
  $QSYS_SIMDIR/submodules/sv_reconfig_bundle_merger.sv \
  $QSYS_SIMDIR/submodules/sv_rx_pma.sv \
  $QSYS_SIMDIR/submodules/sv_tx_pma.sv \
  $QSYS_SIMDIR/submodules/sv_tx_pma_ch.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_h.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_avmm_csr.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_avmm_dcd.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_avmm.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_data_adapter.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_native.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_plls.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_10g_rx_pcs_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_10g_tx_pcs_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_8g_rx_pcs_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_8g_tx_pcs_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_8g_pcs_aggregate_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_common_pcs_pma_interface_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_common_pld_pcs_interface_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_pipe_gen1_2_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_pipe_gen3_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_rx_pcs_pma_interface_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_rx_pld_pcs_interface_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_tx_pcs_pma_interface_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_hssi_tx_pld_pcs_interface_rbc.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_custom_native.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_arbiter.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_m2s.sv \
  $QSYS_SIMDIR/submodules/altera_reset_controller.v \
  $QSYS_SIMDIR/submodules/altera_reset_synchronizer.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0.v \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_h.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_dfe_cal_sweep_h.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cal_seq.sv \
  $QSYS_SIMDIR/submodules/alt_xreconf_cif.sv \
  $QSYS_SIMDIR/submodules/alt_xreconf_uif.sv \
  $QSYS_SIMDIR/submodules/alt_xreconf_basic_acq.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_analog.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_analog_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xreconf_analog_datactrl.sv \
  $QSYS_SIMDIR/submodules/alt_xreconf_analog_rmw.sv \
  $QSYS_SIMDIR/submodules/alt_xreconf_analog_ctrlsm.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_offset_cancellation.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_offset_cancellation_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon_ctrl_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon_ber_sv.sv \
  $QSYS_SIMDIR/submodules/ber_reader_dcfifo.v \
  $QSYS_SIMDIR/submodules/step_to_mon_sv.sv \
  $QSYS_SIMDIR/submodules/mon_to_step_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_reg_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sweep_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sweep_datapath_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_oc_cal_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_pi_phase_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_step_to_mon_en_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_adapt_tap_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_ctrl_mux_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_local_reset_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sim_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_adapt_tap_sim_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_adce.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_adce_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_adce_datactrl_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_sv.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_cal.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_control.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_datapath.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_pll_reset.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_eye_width.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_align_clk.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_get_sum.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_cal_sim_model.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_mif.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_reconfig_mif.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_reconfig_mif_ctrl.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_reconfig_mif_avmm.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_pll.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_reconfig_pll.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_reconfig_pll_ctrl.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_soc.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_ram.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_direct.sv \
  $QSYS_SIMDIR/submodules/sv_xrbasic_l2p_addr.sv \
  $QSYS_SIMDIR/submodules/sv_xrbasic_l2p_ch.sv \
  $QSYS_SIMDIR/submodules/sv_xrbasic_l2p_rom.sv \
  $QSYS_SIMDIR/submodules/sv_xrbasic_lif_csr.sv \
  $QSYS_SIMDIR/submodules/sv_xrbasic_lif.sv \
  $QSYS_SIMDIR/submodules/sv_xcvr_reconfig_basic.sv \
  $QSYS_SIMDIR/submodules/alt_arbiter_acq.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_basic.sv \
  $QSYS_SIMDIR/submodules/sv_reconfig_bundle_to_basic.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu.v \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_reconfig_cpu.v \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_reconfig_cpu_test_bench.v \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0.v \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_irq_mapper.sv \
  $QSYS_SIMDIR/submodules/altera_merlin_master_translator.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_addr_router.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_addr_router_001.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_id_router.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_id_router_001.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_demux.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_demux_001.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_mux_001.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_rsp_xbar_mux.sv \
  $QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_rsp_xbar_mux_001.sv \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master.v \
  $QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0.v \
  $QSYS_SIMDIR/altera_eth_10g_mac_xaui_sv.v \
  -top $TOP_LEVEL_NAME
# ----------------------------------------
# simulate
if [ $SKIP_SIM -eq 0 ]; then
  ./simv $SIM_OPTIONS $USER_DEFINED_SIM_OPTIONS
fi
