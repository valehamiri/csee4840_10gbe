
# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 13.1 162 linux 2015.04.12.15:24:39

# ----------------------------------------
# vcsmx - auto-generated simulation script

# ----------------------------------------
# initialize variables
TOP_LEVEL_NAME="altera_eth_10g_mac_xaui_sv"
QSYS_SIMDIR="./../../"
QUARTUS_INSTALL_DIR="/opt/altera/13.1/quartus/"
SKIP_FILE_COPY=0
SKIP_DEV_COM=0
SKIP_COM=0
SKIP_ELAB=0
SKIP_SIM=0
USER_DEFINED_ELAB_OPTIONS=""
USER_DEFINED_SIM_OPTIONS="+vcs+finish+100"

# ----------------------------------------
# overwrite variables - DO NOT MODIFY!
# This block evaluates each command line argument, typically used for 
# overwriting variables. An example usage:
#   sh <simulator>_setup.sh SKIP_ELAB=1 SKIP_SIM=1
for expression in "$@"; do
  eval $expression
  if [ $? -ne 0 ]; then
    echo "Error: This command line argument, \"$expression\", is/has an invalid expression." >&2
    exit $?
  fi
done

# ----------------------------------------
# initialize simulation properties - DO NOT MODIFY!
ELAB_OPTIONS=""
SIM_OPTIONS=""
if [[ `vcs -platform` != *"amd64"* ]]; then
  :
else
  :
fi

# ----------------------------------------
# create compilation libraries
mkdir -p ./libraries/work/
mkdir -p ./libraries/rsp_xbar_mux/
mkdir -p ./libraries/rsp_xbar_demux/
mkdir -p ./libraries/cmd_xbar_mux/
mkdir -p ./libraries/cmd_xbar_demux/
mkdir -p ./libraries/id_router/
mkdir -p ./libraries/addr_router/
mkdir -p ./libraries/crosser/
mkdir -p ./libraries/mm_interconnect_0/
mkdir -p ./libraries/lc_lb_timing_adapter/
mkdir -p ./libraries/line_splitter_timing_adapter/
mkdir -p ./libraries/line_lb_timing_adapter/
mkdir -p ./libraries/line_loopback/
mkdir -p ./libraries/lc_splitter_timing_adapter/
mkdir -p ./libraries/mm_interconnect_2/
mkdir -p ./libraries/mm_interconnect_1/
mkdir -p ./libraries/rxtx_timing_adapter_pauselen_tx/
mkdir -p ./libraries/rxtx_timing_adapter_pauselen_rx/
mkdir -p ./libraries/txrx_timing_adapter_link_fault_status_export/
mkdir -p ./libraries/txrx_timing_adapter_link_fault_status_rx/
mkdir -p ./libraries/rx_st_error_adapter_stat/
mkdir -p ./libraries/rx_eth_packet_overflow_control/
mkdir -p ./libraries/rx_eth_crc_pad_rem/
mkdir -p ./libraries/rx_eth_frame_status_merger/
mkdir -p ./libraries/rx_timing_adapter_frame_status_out_frame_decoder/
mkdir -p ./libraries/rx_st_timing_adapter_frame_status_in/
mkdir -p ./libraries/rx_eth_lane_decoder/
mkdir -p ./libraries/rx_eth_link_fault_detection/
mkdir -p ./libraries/rx_register_map/
mkdir -p ./libraries/tx_eth_link_fault_generation/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_out_0/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_in/
mkdir -p ./libraries/tx_eth_xgmii_termination/
mkdir -p ./libraries/tx_eth_packet_formatter/
mkdir -p ./libraries/tx_st_status_output_delay_to_statistic/
mkdir -p ./libraries/tx_eth_statistics_collector/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_status_output/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_status_in/
mkdir -p ./libraries/tx_st_error_adapter_stat/
mkdir -p ./libraries/tx_eth_frame_decoder/
mkdir -p ./libraries/tx_st_timing_adapter_frame_decoder/
mkdir -p ./libraries/tx_st_splitter_1/
mkdir -p ./libraries/tx_st_pipeline_stage_rs/
mkdir -p ./libraries/tx_eth_crc_inserter/
mkdir -p ./libraries/tx_eth_address_inserter/
mkdir -p ./libraries/tx_st_mux_flow_control_user_frame/
mkdir -p ./libraries/tx_st_pause_ctrl_error_adapter/
mkdir -p ./libraries/tx_eth_pause_ctrl_gen/
mkdir -p ./libraries/tx_eth_pause_beat_conversion/
mkdir -p ./libraries/tx_eth_pkt_backpressure_control/
mkdir -p ./libraries/tx_eth_pad_inserter/
mkdir -p ./libraries/tx_eth_packet_underflow_control/
mkdir -p ./libraries/tx_register_map/
mkdir -p ./libraries/tx_bridge/
mkdir -p ./libraries/rsp_xbar_mux_001/
mkdir -p ./libraries/cmd_xbar_mux_001/
mkdir -p ./libraries/cmd_xbar_demux_001/
mkdir -p ./libraries/burst_adapter/
mkdir -p ./libraries/limiter/
mkdir -p ./libraries/id_router_001/
mkdir -p ./libraries/addr_router_001/
mkdir -p ./libraries/eth_10g_design_example_0_mm_pipeline_bridge_translator_avalon_universal_slave_0_agent/
mkdir -p ./libraries/merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent/
mkdir -p ./libraries/eth_10g_design_example_0_mm_pipeline_bridge_translator/
mkdir -p ./libraries/p2b_adapter/
mkdir -p ./libraries/b2p_adapter/
mkdir -p ./libraries/transacto/
mkdir -p ./libraries/p2b/
mkdir -p ./libraries/b2p/
mkdir -p ./libraries/timing_adt/
mkdir -p ./libraries/jtag_phy_embedded_in_jtag_master/
mkdir -p ./libraries/eth_mdio/
mkdir -p ./libraries/eth_fifo_pause_ctrl_adapter/
mkdir -p ./libraries/pa_pg_after_timing_adapter/
mkdir -p ./libraries/pa_pg_before_timing_adapter/
mkdir -p ./libraries/dc_fifo_pause_adapt_pause_gen/
mkdir -p ./libraries/tx_sc_fifo/
mkdir -p ./libraries/eth_loopback_composed/
mkdir -p ./libraries/eth_10g_mac/
mkdir -p ./libraries/xaui/
mkdir -p ./libraries/rst_controller/
mkdir -p ./libraries/alt_xcvr_reconfig_0/
mkdir -p ./libraries/merlin_master_translator/
mkdir -p ./libraries/jtag_master/
mkdir -p ./libraries/eth_10g_design_example_0/
mkdir -p ./libraries/altera_ver/
mkdir -p ./libraries/lpm_ver/
mkdir -p ./libraries/sgate_ver/
mkdir -p ./libraries/altera_mf_ver/
mkdir -p ./libraries/altera_lnsim_ver/
mkdir -p ./libraries/stratixv_ver/
mkdir -p ./libraries/stratixv_hssi_ver/
mkdir -p ./libraries/stratixv_pcie_hip_ver/

# ----------------------------------------
# copy RAM/ROM files to simulation directory

# ----------------------------------------
# compile device library files
if [ $SKIP_DEV_COM -eq 0 ]; then
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v"                       -work altera_ver           
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v"                                -work lpm_ver              
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v"                                   -work sgate_ver            
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v"                               -work altera_mf_ver        
  vlogan +v2k -sverilog "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim.sv"                           -work altera_lnsim_ver     
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/stratixv_atoms_ncrypt.v"          -work stratixv_ver         
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/stratixv_atoms.v"                          -work stratixv_ver         
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/stratixv_hssi_atoms_ncrypt.v"     -work stratixv_hssi_ver    
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/stratixv_hssi_atoms.v"                     -work stratixv_hssi_ver    
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/stratixv_pcie_hip_atoms_ncrypt.v" -work stratixv_pcie_hip_ver
  vlogan +v2k           "$QUARTUS_INSTALL_DIR/eda/sim_lib/stratixv_pcie_hip_atoms.v"                 -work stratixv_pcie_hip_ver
fi

# ----------------------------------------
# compile design files in correct order
if [ $SKIP_COM -eq 0 ]; then
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_rsp_xbar_mux.sv"        -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_rsp_xbar_demux.sv"      -work rsp_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_cmd_xbar_mux.sv"        -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_cmd_xbar_demux.sv"      -work cmd_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_id_router.sv"           -work id_router                                                                                    
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0_addr_router.sv"         -work addr_router                                                                                  
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_rsp_xbar_mux.sv"                  -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_rsp_xbar_demux.sv"                -work rsp_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_cmd_xbar_mux.sv"                  -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_cmd_xbar_demux.sv"                -work cmd_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_id_router.sv"                     -work id_router                                                                                    
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2_addr_router.sv"                   -work addr_router                                                                                  
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_rsp_xbar_mux.sv"                  -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_rsp_xbar_demux.sv"                -work rsp_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_cmd_xbar_mux.sv"                  -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_cmd_xbar_demux.sv"                -work cmd_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_id_router.sv"                     -work id_router                                                                                    
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1_addr_router.sv"                   -work addr_router                                                                                  
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_rsp_xbar_mux.sv"                  -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_rsp_xbar_demux.sv"                -work rsp_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_cmd_xbar_mux.sv"                  -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_cmd_xbar_demux.sv"                -work cmd_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_id_router.sv"                     -work id_router                                                                                    
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0_addr_router.sv"                   -work addr_router                                                                                  
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_handshake_clock_crosser.v"                                                                         -work crosser                                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_clock_crosser.v"                                                                                   -work crosser                                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_base.v"                                                                                   -work crosser                                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_rsp_xbar_mux.sv"                              -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_rsp_xbar_demux.sv"                            -work rsp_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_cmd_xbar_mux.sv"                              -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_cmd_xbar_demux.sv"                            -work cmd_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_id_router.sv"                                 -work id_router                                                                                    
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0_addr_router.sv"                               -work addr_router                                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_mm_interconnect_0.v"                      -work mm_interconnect_0                                                                            
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_lc_lb_timing_adapter.v"                   -work lc_lb_timing_adapter                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_line_splitter_timing_adapter.v"           -work line_splitter_timing_adapter                                                                 
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_line_lb_timing_adapter.v"                 -work line_lb_timing_adapter                                                                       
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_loopback.v"                                                                                              -work line_loopback                                                                                
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed_lc_splitter_timing_adapter.v"             -work lc_splitter_timing_adapter                                                                   
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_2.v"                                -work mm_interconnect_2                                                                            
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_1.v"                                -work mm_interconnect_1                                                                            
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_mm_interconnect_0.v"                                -work mm_interconnect_0                                                                            
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rxtx_timing_adapter_pauselen_tx.v"                  -work rxtx_timing_adapter_pauselen_tx                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rxtx_timing_adapter_pauselen_rx.v"                  -work rxtx_timing_adapter_pauselen_rx                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_txrx_timing_adapter_link_fault_status_export.v"     -work txrx_timing_adapter_link_fault_status_export                                                 
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_txrx_timing_adapter_link_fault_status_rx.v"         -work txrx_timing_adapter_link_fault_status_rx                                                     
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rx_st_error_adapter_stat.v"                         -work rx_st_error_adapter_stat                                                                     
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_packet_overflow_control.v"                                                                      -work rx_eth_packet_overflow_control                                                               
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_crc_pad_rem.v"                                                                                  -work rx_eth_crc_pad_rem                                                                           
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_crc_rem.v"                                                                                      -work rx_eth_crc_pad_rem                                                                           
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_packet_stripper.v"                                                                                  -work rx_eth_crc_pad_rem                                                                           
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_stage.sv"                                                                                 -work rx_eth_crc_pad_rem                                                                           
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_base.v"                                                                                   -work rx_eth_crc_pad_rem                                                                           
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_frame_status_merger.v"                                                                          -work rx_eth_frame_status_merger                                                                   
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rx_timing_adapter_frame_status_out_frame_decoder.v" -work rx_timing_adapter_frame_status_out_frame_decoder                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_rx_st_timing_adapter_frame_status_in.v"             -work rx_st_timing_adapter_frame_status_in                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_lane_decoder.v"                                                                                 -work rx_eth_lane_decoder                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_link_fault_detection.v"                                                                         -work rx_eth_link_fault_detection                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_10g_rx_register_map.v"                                                                          -work rx_register_map                                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_clock_crosser.v"                                                                                   -work rx_register_map                                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_link_fault_generation.v"                                                                        -work tx_eth_link_fault_generation                                                                 
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_out_0.v"              -work tx_st_timing_adapter_splitter_out_0                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_in.v"                 -work tx_st_timing_adapter_splitter_in                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_xgmii_termination.v"                                                                            -work tx_eth_xgmii_termination                                                                     
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_packet_formatter.v"                                                                             -work tx_eth_packet_formatter                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_delay.sv"                                                                                          -work tx_st_status_output_delay_to_statistic                                                       
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_10gmem_statistics_collector.v"                                                                  -work tx_eth_statistics_collector                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_status_output.v"      -work tx_st_timing_adapter_splitter_status_output                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_splitter_status_in.v"          -work tx_st_timing_adapter_splitter_status_in                                                      
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_error_adapter_stat.v"                         -work tx_st_error_adapter_stat                                                                     
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_frame_decoder.v"                                                                                -work tx_eth_frame_decoder                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_stage.sv"                                                                                 -work tx_eth_frame_decoder                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_base.v"                                                                                   -work tx_eth_frame_decoder                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_timing_adapter_frame_decoder.v"               -work tx_st_timing_adapter_frame_decoder                                                           
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_splitter.sv"                                                                                       -work tx_st_splitter_1                                                                             
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_stage.sv"                                                                                 -work tx_st_pipeline_stage_rs                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_base.v"                                                                                   -work tx_st_pipeline_stage_rs                                                                      
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_crc.v"                                                                                          -work tx_eth_crc_inserter                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/crc32.v"                                                                                                   -work tx_eth_crc_inserter                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/gf_mult32_kc.v"                                                                                            -work tx_eth_crc_inserter                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_address_inserter.v"                                                                             -work tx_eth_address_inserter                                                                      
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_mux_flow_control_user_frame.v"                -work tx_st_mux_flow_control_user_frame                                                            
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac_tx_st_pause_ctrl_error_adapter.v"                   -work tx_st_pause_ctrl_error_adapter                                                               
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_ctrl_gen.v"                                                                               -work tx_eth_pause_ctrl_gen                                                                        
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_controller.v"                                                                             -work tx_eth_pause_ctrl_gen                                                                        
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_gen.v"                                                                                    -work tx_eth_pause_ctrl_gen                                                                        
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_pause_beat_conversion.v"                                                                        -work tx_eth_pause_beat_conversion                                                                 
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_pkt_backpressure_control.v"                                                                     -work tx_eth_pkt_backpressure_control                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_pad_inserter.v"                                                                                 -work tx_eth_pad_inserter                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_packet_underflow_control.v"                                                                     -work tx_eth_packet_underflow_control                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_eth_10g_tx_register_map.v"                                                                          -work tx_register_map                                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_clock_crosser.v"                                                                                   -work tx_register_map                                                                              
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_mm_bridge.v"                                                                                          -work tx_bridge                                                                                    
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work rsp_xbar_mux_001                                                                             
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_rsp_xbar_mux_001.sv"                                                   -work rsp_xbar_mux_001                                                                             
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_rsp_xbar_mux.sv"                                                       -work rsp_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_rsp_xbar_demux.sv"                                                     -work rsp_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work cmd_xbar_mux_001                                                                             
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_mux_001.sv"                                                   -work cmd_xbar_mux_001                                                                             
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_mux.sv"                                                       -work cmd_xbar_mux                                                                                 
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_demux_001.sv"                                                 -work cmd_xbar_demux_001                                                                           
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_cmd_xbar_demux.sv"                                                     -work cmd_xbar_demux                                                                               
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_burst_adapter.sv"                                                                                     -work burst_adapter                                                                                
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_address_alignment.sv"                                                                                 -work burst_adapter                                                                                
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_traffic_limiter.sv"                                                                                   -work limiter                                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_reorder_memory.sv"                                                                                    -work limiter                                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_sc_fifo.v"                                                                                            -work limiter                                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_base.v"                                                                                   -work limiter                                                                                      
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_id_router_001.sv"                                                      -work id_router_001                                                                                
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_id_router.sv"                                                          -work id_router                                                                                    
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_addr_router_001.sv"                                                    -work addr_router_001                                                                              
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0_addr_router.sv"                                                        -work addr_router                                                                                  
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_slave_agent.sv"                                                                                       -work eth_10g_design_example_0_mm_pipeline_bridge_translator_avalon_universal_slave_0_agent        
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_burst_uncompressor.sv"                                                                                -work eth_10g_design_example_0_mm_pipeline_bridge_translator_avalon_universal_slave_0_agent        
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_master_agent.sv"                                                                                      -work merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_slave_translator.sv"                                                                                  -work eth_10g_design_example_0_mm_pipeline_bridge_translator                                       
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master_p2b_adapter.v"                                                               -work p2b_adapter                                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master_b2p_adapter.v"                                                               -work b2p_adapter                                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_packets_to_master.v"                                                                                  -work transacto                                                                                    
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_packets_to_bytes.v"                                                                                -work p2b                                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_bytes_to_packets.v"                                                                                -work b2p                                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master_timing_adt.v"                                                                -work timing_adt                                                                                   
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_jtag_interface.v"                                                                                  -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_jtag_dc_streaming.v"                                                                                         -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_jtag_sld_node.v"                                                                                             -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_jtag_streaming.v"                                                                                            -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_pli_streaming.v"                                                                                             -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_clock_crosser.v"                                                                                   -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_pipeline_base.v"                                                                                   -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_idle_remover.v"                                                                                    -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_st_idle_inserter.v"                                                                                   -work jtag_phy_embedded_in_jtag_master                                                             
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_mm_interconnect_0.v"                                            -work mm_interconnect_0                                                                            
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_mdio.v"                                                                                                  -work eth_mdio                                                                                     
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_fifo_pause_ctrl_adapter.v"                                                                               -work eth_fifo_pause_ctrl_adapter                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_pa_pg_after_timing_adapter.v"                                   -work pa_pg_after_timing_adapter                                                                   
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_pa_pg_before_timing_adapter.v"                                  -work pa_pg_before_timing_adapter                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_dc_fifo.v"                                                                                            -work dc_fifo_pause_adapt_pause_gen                                                                
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_dcfifo_synchronizer_bundle.v"                                                                                -work dc_fifo_pause_adapt_pause_gen                                                                
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_sc_fifo.v"                                                                                            -work tx_sc_fifo                                                                                   
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_loopback_composed.v"                                        -work eth_loopback_composed                                                                        
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0_eth_10g_mac.v"                                                  -work eth_10g_mac                                                                                  
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_xcvr_functions.sv"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_pma_functions.sv"                                                                                               -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_xcvr_xaui.sv"                                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/hxaui_csr_h.sv"                                                                                                     -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/hxaui_csr.sv"                                                                                                       -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_mgmt2dec_phyreconfig.sv"                                                                                   -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_mgmt2dec_xaui.sv"                                                                                          -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/alt_pma_ch_controller_tgx.v"                                                                                        -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/alt_pma_controller_tgx.v"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_reset_ctrl_lego.sv"                                                                                             -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_reset_ctrl_tgx_cdrauto.sv"                                                                                      -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_resync.sv"                                                                                                 -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_csr_common_h.sv"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_csr_common.sv"                                                                                             -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_csr_pcs8g_h.sv"                                                                                            -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_csr_pcs8g.sv"                                                                                              -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_csr_selector.sv"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_mgmt2dec.sv"                                                                                               -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_wait_generate.v"                                                                                             -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_xcvr_reset_control.sv"                                                                                       -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reset_counter.sv"                                                                                          -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_pcs.v"                                                                                       -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_reset.v"                                                                                     -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx.v"                                                                                        -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_8b10b_dec.v"                                                                              -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_channel_synch.v"                                                                          -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_deskew.v"                                                                                 -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_deskew_channel.v"                                                                         -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_deskew_ram.v"                                                                             -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/altera_soft_xaui_rx_deskew_ram.v"                                                                          -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_invalid_code_det.v"                                                                       -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_parity.v"                                                                                 -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_parity_4b.v"                                                                              -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_parity_6b.v"                                                                              -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_rate_match.v"                                                                             -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_rate_match_ram.v"                                                                         -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_rl_chk_6g.v"                                                                              -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_rx_sm.v"                                                                                     -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_tx.v"                                                                                        -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_tx_8b10b_enc.v"                                                                              -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/alt_soft_xaui_tx_idle_conv.v"                                                                              -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/l_modules.v"                                                                                               -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/serdes_4_unit_lc_siv.v"                                                                                    -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/serdes_4_unit_siv.v"                                                                                       -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/synopsys/serdes_4unit.v"                                                                                            -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/sxaui.v"                                                                                                            -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_xaui.sv"                                                                                                    -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_low_latency_phy_nr.sv"                                                                                      -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_pcs.sv"                                                                                                          -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_pcs_ch.sv"                                                                                                       -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_pma.sv"                                                                                                          -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_reconfig_bundle_to_xcvr.sv"                                                                                      -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_reconfig_bundle_to_ip.sv"                                                                                        -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_reconfig_bundle_merger.sv"                                                                                       -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_rx_pma.sv"                                                                                                       -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_tx_pma.sv"                                                                                                       -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_tx_pma_ch.sv"                                                                                                    -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_h.sv"                                                                                                       -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_avmm_csr.sv"                                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_avmm_dcd.sv"                                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_avmm.sv"                                                                                                    -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_data_adapter.sv"                                                                                            -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_native.sv"                                                                                                  -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_plls.sv"                                                                                                    -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_10g_rx_pcs_rbc.sv"                                                                                          -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_10g_tx_pcs_rbc.sv"                                                                                          -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_8g_rx_pcs_rbc.sv"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_8g_tx_pcs_rbc.sv"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_8g_pcs_aggregate_rbc.sv"                                                                                    -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_common_pcs_pma_interface_rbc.sv"                                                                            -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_common_pld_pcs_interface_rbc.sv"                                                                            -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_pipe_gen1_2_rbc.sv"                                                                                         -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_pipe_gen3_rbc.sv"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_rx_pcs_pma_interface_rbc.sv"                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_rx_pld_pcs_interface_rbc.sv"                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_tx_pcs_pma_interface_rbc.sv"                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_hssi_tx_pld_pcs_interface_rbc.sv"                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_custom_native.sv"                                                                                           -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_arbiter.sv"                                                                                                -work xaui                                                                                         
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_m2s.sv"                                                                                                    -work xaui                                                                                         
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_reset_controller.v"                                                                                          -work rst_controller                                                                               
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_reset_synchronizer.v"                                                                                        -work rst_controller                                                                               
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_mm_interconnect_0.v"                                                                     -work mm_interconnect_0                                                                            
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_xcvr_functions.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_h.sv"                                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_resync.sv"                                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_h.sv"                                                                                             -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_dfe_cal_sweep_h.sv"                                                                                         -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig.sv"                                                                                               -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_sv.sv"                                                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cal_seq.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xreconf_cif.sv"                                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xreconf_uif.sv"                                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xreconf_basic_acq.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_analog.sv"                                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_analog_sv.sv"                                                                                     -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xreconf_analog_datactrl.sv"                                                                                     -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xreconf_analog_rmw.sv"                                                                                          -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xreconf_analog_ctrlsm.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_offset_cancellation.sv"                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_offset_cancellation_sv.sv"                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon.sv"                                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon_sv.sv"                                                                                     -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon_ctrl_sv.sv"                                                                                -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_eyemon_ber_sv.sv"                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/ber_reader_dcfifo.v"                                                                                                -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/step_to_mon_sv.sv"                                                                                                  -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/mon_to_step_sv.sv"                                                                                                  -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_sv.sv"                                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_reg_sv.sv"                                                                                    -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sv.sv"                                                                                    -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sweep_sv.sv"                                                                              -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sweep_datapath_sv.sv"                                                                     -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_oc_cal_sv.sv"                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_pi_phase_sv.sv"                                                                               -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_step_to_mon_en_sv.sv"                                                                         -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_adapt_tap_sv.sv"                                                                              -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_ctrl_mux_sv.sv"                                                                               -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_local_reset_sv.sv"                                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_cal_sim_sv.sv"                                                                                -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dfe_adapt_tap_sim_sv.sv"                                                                          -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_adce.sv"                                                                                          -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_adce_sv.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_adce_datactrl_sv.sv"                                                                              -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_sv.sv"                                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_cal.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_control.sv"                                                                                   -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_datapath.sv"                                                                                  -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_pll_reset.sv"                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_eye_width.sv"                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_align_clk.sv"                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_get_sum.sv"                                                                                   -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_dcd_cal_sim_model.sv"                                                                             -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_mif.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_reconfig_mif.sv"                                                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_reconfig_mif_ctrl.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_reconfig_mif_avmm.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_pll.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_reconfig_pll.sv"                                                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_reconfig_pll_ctrl.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_soc.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_ram.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_direct.sv"                                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xrbasic_l2p_addr.sv"                                                                                             -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xrbasic_l2p_ch.sv"                                                                                               -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xrbasic_l2p_rom.sv"                                                                                              -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xrbasic_lif_csr.sv"                                                                                              -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xrbasic_lif.sv"                                                                                                  -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_xcvr_reconfig_basic.sv"                                                                                          -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_arbiter_acq.sv"                                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_basic.sv"                                                                                         -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_arbiter.sv"                                                                                                -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_m2s.sv"                                                                                                    -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_wait_generate.v"                                                                                             -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_csr_selector.sv"                                                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/sv_reconfig_bundle_to_basic.sv"                                                                                     -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu.v"                                                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_reconfig_cpu.v"                                                                               -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_reconfig_cpu_test_bench.v"                                                                    -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0.v"                                                                          -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_irq_mapper.sv"                                                                                -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_reset_controller.v"                                                                                          -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_reset_synchronizer.v"                                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_master_translator.sv"                                                                                 -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_slave_translator.sv"                                                                                  -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_master_agent.sv"                                                                                      -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_slave_agent.sv"                                                                                       -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_burst_uncompressor.sv"                                                                                -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_avalon_sc_fifo.v"                                                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_addr_router.sv"                                                             -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_addr_router_001.sv"                                                         -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_id_router.sv"                                                               -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_id_router_001.sv"                                                           -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_demux.sv"                                                          -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_demux_001.sv"                                                      -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_arbitrator.sv"                                                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_mux.sv"                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_cmd_xbar_mux_001.sv"                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_rsp_xbar_mux.sv"                                                            -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/alt_xcvr_reconfig_cpu_mm_interconnect_0_rsp_xbar_mux_001.sv"                                                        -work alt_xcvr_reconfig_0                                                                          
  vlogan +v2k -sverilog "$QSYS_SIMDIR/submodules/altera_merlin_master_translator.sv"                                                                                 -work merlin_master_translator                                                                     
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_jtag_master.v"                                                                           -work jtag_master                                                                                  
  vlogan +v2k           "$QSYS_SIMDIR/submodules/altera_eth_10g_mac_xaui_sv_eth_10g_design_example_0.v"                                                              -work eth_10g_design_example_0                                                                     
  vlogan +v2k           "$QSYS_SIMDIR/altera_eth_10g_mac_xaui_sv.v"                                                                                                                                                                                                     
fi

# ----------------------------------------
# elaborate top level design
if [ $SKIP_ELAB -eq 0 ]; then
  vcs -lca -t ps $ELAB_OPTIONS $USER_DEFINED_ELAB_OPTIONS $TOP_LEVEL_NAME
fi

# ----------------------------------------
# simulate
if [ $SKIP_SIM -eq 0 ]; then
  ./simv $SIM_OPTIONS $USER_DEFINED_SIM_OPTIONS
fi
