// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
//pragma protect begin_protected
//pragma protect key_keyowner=Cadence Design Systems.
//pragma protect key_keyname=CDS_KEY
//pragma protect key_method=RC5
//pragma protect key_block
/nnbGJ+dZ4ltK6z+yfxxdgflC+qVTMj5x4Toq/aE053MGU2miysmj8El9Tk/Y6Jx
UVW3573Q1lGqcy8Q/m/fomm+FNy6pohce9ir6bc8Hh7zRj4AzHBBkJUx6ULA6Qe1
24Mr5hnc7/ZvpaBxJKQ2bwe/BBoZxxioygWSvtUUIW1ErXKBawG7Ng==
//pragma protect end_key_block
//pragma protect digest_block
O0DCozbf8ZSFCR9C3GydKHshCwE=
//pragma protect end_digest_block
//pragma protect data_block
t0cuaxEsgnklFVoESk9lql6zHUBJj0LRWC5ng392FtXc3NfAc+8Q9QXsNWb/tl4H
9QGzil49pFnR/AUAMt+qPPkd8vf3DheTAGosRN1ut4kVQfJcAmi2DUa2r9gniTsF
ODhFH85O8o7yf7BhLiY0rHYAVQK5NzZFLUbJseeMAL550CP8qHQSiLRoLlHblhXR
XjlA9IblCVsF09D7bEQXMTl3LVBhIZx/o1TWIiNF3yi7RBAIriU0F6G1Dra/7znq
ghwztydJfxx3Dz0FQi2vZKpaTW1A5S7ssz/muwfOZOKowzjXm3mg3IcGT6RzT9ig
D42Uzo3ZLrGtEOZWVTUhEPBIs4TGLHSeyqM66qbItQq7tGzQRlFL/5rWTsYtBLeM
f67kN88irZcx3rwbvMg7+pZroIFFSZeVvIFvecvQSiqz6TA9ZY7GNIsfitE3ASRR
01/x9iAimcaobuXkZ6hBL34e1XvDCu2ztGyAU0TXlykNa9dz6Dqd3COCk9Ij1a0g
VhZUjAlL0BxOulLEyyom63ySOM/sJbbMSLe+e+3qI/FbX0uHRzapW3CKxpTNtAP8
v70UZjpVzXn3P1Ksuc/RV+cRjapIcnjqkQIP4/K5tX44fR3ROmwMKGxpXqHrswtK
Z8F0X36KqP0Y/QCOOd3S/TV+CN5Ay06jbBen9GYgPA/7wLsejmBxdl0/IuiuVwAx
y+b52Y47W8M3MjlIJbtaCB/yxRtsaAsSqayK1byocJWots8ZoRUlo5O1gd9EwQsZ
C7Ya26nkXsURuBe9OGzkOpBKNt3/MB0AuhSJix3YLZqByF+DqhpsEFJO1/WvD66V
d+7QI+8b8M/8WJae215GmFmqVGVRNB0/HoJ3me1pe3RxgpU6v5n943UW24qISnC5
1eoJ06GeRhkoPeQBNRwXlzBHdo/DnZ8gqg1r9ExP0W8RgvQLFoxbsEnRR4ktNYsx
zb3VEFFT7RY9Nz2Rv16icW+X2/VeXg/o7K7C2MBvOWVCl/tnNQvnzAnH0N1KEfkk
tBrS54vHar/8wZr2gPJXlklPdiiM+u5xK8O0dd1dL57voZHsvYG1uRb8LmWQn10r
KzPClAMX3tEIbvrQkZfj+gGOAUNMBDsJ5Yn6IWvqlwE95pz+gbgfCmAalAzZ9V6A
GmVm3Wwo+NcDV2xq1ZuX4iEdHJbbYGU4CLl8Llt+mWsbHbTarDt0qMA6gm9uNkcu
IlOWc2sPO7Kn5ijJrlmj2Hbf2tFRY8zSZqeOwu1NQznzveFcY1wWYXssHP3a8y1z
erw3AGZ4bkqkakLdwx/iVdi4kAb5dSDBnLsoWmEpH9Iriwoiq9hhXT4pXXwGu6or
QOP5UzUbH8ZoS/siOZ9txxbtaXZo5Xbk6s0vJtjgTN1vM6rfiwyDGNQmZTJpwpIA
VLwojkPXWHAiNP4EmEuPXJE9pQiOU5s9OmdNtZ2sK4yaN4f2urFxg/c1pVzMfgks
xZ8I8afn6dmwhOUolh3qpTXlgFS3fhpHwmHDOGBzCFlnVDACd+5p4JcbbZOITk0y
G6hh9BkSEhAzzyXR1C3RTGFHDJP4qXL/XhB2WMIdQjPweEv7uZzXD8HpygOjOn9O
tlTPcVB6Rq6yYdSa3LeQO6hP8prTC/QA+VZ/wFYZYSZluJ1Khud606gZycc/cV8I
nVTuJxJ1VV93P26FT9nzCBLXX2zP23fZ2h9lRXoBWSL+L4du3cK6mPD9W0lINKj0
Uewo3VIrHvkHyN4Hxn4wpQdgQ4EdqHnKgLMny8j1Jk3PnMMTg7/o/3/OeOKU52an
P396GzXo2sEvmuLlTtWc7cqYHnalSC5s0IbkYIpgcIMkKq1iy/OIM8O9tEagq+BM
VmiTyImsBy8m2HEsxkaOMXpj/k/ytxbvVoI2bilfKt9Pmr9gRMIWzcrSSPurkKqK
PVZj1aVQEnZ4PyuxKP/gUXbqtZZVFjOQCxQmZhwCeMXnviw3r+UlFbdiCMpnMyUn
eOVcPMx4G23UrAWySYaKSxmBvJwOa3lk06LhonVPIYVm1gVOtPPHZ8rhUK9ZkyJw
w5CtopzQ3ekxJDZ8VhI1eh4D+YiPbty+EZtbNT5Ht9uYhNe0yHv7cNdVmNRZQJAQ
EhUi0GuPmegIer7BWrGPi6+gQN/YOOpKZ+U0HA8SVPKk/Jlnc1ne9KESuZ7tyhKT
e9ygdFJzOpf6wi/acxSmkIbwmvIipCQii5Z+ul0f/Wb8hGzzj+MZWTWH7B6NZCLj
w8ThT8H5xFwt5XhHSKViK7auO/3c60ww+xZjqqCPEkNJLMo8tgYXluA/dTHa3iTs
BPM3Sprm3Uk+GpExEG9h3jJh/TkW+rhoqVORunNSlkbFs2f3E8nd1MSVsUghxcqb
G/5krUyRBuN2WTPU3D2e/msaL7l/9TygWikAeAM5RAwwq0sNBERrbYm5HJ6j6FIo
ZDQp8kSZ53BXNLAMLp+ma02+wXg7QrdB0zQOgTahzWlBG3al9kDYOVKwiwMz9jau
hWKy1v9m97HuB32ValVOsdsuMUmW3AH8sv3+CEO6fFxxHKTgN0jmpTfB1fEawWQG
218p8w0vpDhScCuCQJk1GBKsNAqhhDYrINx5LboDO7FuIuOC95gvTiwSdhG1+s4j
s40laZeARQat+eElZZLRusqX9ySz+Ta6PMt2CI45bGsjuiXcB8tugviS4XaI887E
6v7PRXHRXqdZX6pSnTmwLLGl0FkRsIXRclwSIBEni2qYMbdnCqysVBT+ASNpqx5j
XyYj+c9D8AAvXYAJt5cKKnv+KCmHOmGn3c/eDDwRXMIJa82Hla03xL7m9nbvbN4e
q6EClfFKtz44BFglM8w76J4U6zlYlg6OA66paIwjU0LV345Cg1X6WP9Jvc25bZJk
iniFJ6Agy6ApwZKMLsGsmn4L93az6J59mB2sCccJNaR13BKmfElHszS6uC2jfxYQ
RBJtYq0TgfYKcpUE9B/T/JSDPwoy+ctJ3Cz1/Rd4dx/g+UotoqFvZW7OOjirvlkC
OCGM0+HepQDJbkw48SzCopjrud/K9NC9gP8nh3ZKSpwlMlTmdB5KvraYMaQPkcjl
zWrDyqaG1QC8BYFZjU0Lf6yoB2ve7wQwa7KFjomR6+Gj5DRQAqGlCSn5Em1v+pLv
o0vuqYnI2AbM5YO8xI9FXM/PGuDm2xyZ6qiEBjH/zyiv/sGFiwezWO1PFx5HmjOs
9Le7JOGVPaygSeG5QVy9L4hJZCcy5ZMfN/FOvwVN0sKOUrwosi1oWmTtwc1nOow7
+WN7GIJ86DOUTl/oiSqOf8YhSJfIuADc/IBl27qheRDt8BVJxo6TR8O+c1i/ZE8v
m8Kmgrl+ImfpijTtcXisWDkq+B8Vd0rpQoPagJC9eHBcBTqUcTJibSlN9JtjXJfw
RIj9QrQuo8XzY7XlMJXixLOk39yPNjEZdP/rMYLOIYeqL+2Bmmxa2P8+d3kq10kh
IEXuiEVxkMYs8e1eWxBNiqpz8Sb0u0CB+vCmHwFIPx0DBb1erdDo7yMt+48ULS1k
aShpujdza5jmpomIuVUIX+1unTlv6A9TGTK/1gzEh7bjGoQkQiYgRR+b4woCf/8a
VbInf+oPH/062My8hZpNVmecvK9R5WK2IdQ7KDnnQEd2MmqVI317V9FiiOXP5Pgf
QYDnrb/e+jU7jG8BbR1Uo6VtGpL1x3s4ZsTZOwJvBZPZKWwb4pTRgmDfgXT4ZURQ
it0NNFWbzOhnTJmjGSMSQikJjqaS8TpTxxnZGSLCzj0D5fjaXAnjoZmfqiZSo4MJ
md8SOBmT8zymNT9XmQqLlh6y5GfH9ksPSjTdn6xMJtrEhBVy5DW1UDOTvRFEYg0T
iBciJ/dFGVkLT2/rVJbZQFUHltAiX7B347MrEFMhLB/PSVNk9muGfIbBpiNg0ao+
e6lhQvFM68sz2+Utd8WcRBTiv4LQu7GN29Gwl+qDNgv/WQuEQ+9iwIwqJMzggZ2b
zuebKXQeFbSABTuvlpoEHiKBPRGUZy1Iu0V15Em1JvRn4zeMBASeLwDJvTmopupo
EOyTZazR6xxwib1BNYblQBzpgM/70ETDuHWRBdFNyQo0Cm4K7o0/OdIR9gT1Ec/r
VfVIUfIAsAJKY6bOsBYdi2wEfKRVAHMOlj9AU4WLKyDvWzt5mF7J6HF2MF76mfL9
LyguCZirlVx+Ec2s/vmDO6aFTcFyvO3gLmfZz8aiZ26QXalHXgCl//iAf/KQ8ObI
b1ykaCx1xO4bxDEUeipithehjyLe3xfVL3Zr2Pb7T3rMjnpqYD7YXuEGteQYacMG
ShNsuHR8jcsLdcyiUu8a6rxpVvSD1klArU1i9AJMc6FGOK+pvDBFudk5PEG5b4oD
35SddVY66KzhozRj4izlnRzWFAzU8tysl2lDlO0P9ZIBVO72KV849rSF6dr4rNpB
32DQyM4j8OcHBgGo0FQEgeCAtZmJfzGxbO4V98TNezDggrj66q2E4qEk5elXDxS9
gzltKGp8nrO2pEr0UrVtTivVOc1TUa1YqcZBUWF3c6o3KtkDpzYN74Xr53uVL7EA
GUAyc+EZenfjnRQaMpwdy+a9M4P6pC/aei1DA0VqFk2sIueOEGD1TcFVfcVTOME5
pEUVlrmYcwalmhoFXLMDeZ+SJsMq2eSTl27PnESLsXMPb0tcAyEhzZgPCw1ZUNZA
fHHoeccATuki2bB8xXH28bhIAu1MdPFdZWrieoRoreuMJGtlx9TyucQlCyQTbBPc
m6RV10tCJADZFUWdL0aTB2amnGFXoEuB+TxbGDyE+SR+gljot9yKmccPOeAK4vOo
vbaag+kh1idy95XCFmwZg4sTHuSrphf+SalJm/8rv7GVFT73gyyfncuWAYOqMGls
ZSG/Qsq/K14bMslU4PomOlFY/gt3+9vCnIHvTv2g5i8l6FxolRpEKIiw1yr9iQdK
CYCLfZeebFsGh3cKOiYDcbN4k7+zVPpNKuZsFOZJ6MFvkz7A14vaO0bgUC6sUg1s
tl1n4yRpQdaWlqbrLit53xuKX75FZ74UhOqkqxf+y+JIUzgEsE2GG74kKyZKC5Kd
KLGHuDPiWm63lDp3mJO/898iY9IaqsJQn87ax9Ynpfx9T68fWjhMjfM6b4aaZB6u
4oUwybDZ4IEDR2LBOdCOWvKkFISluPNiul9OkNvoJj4gYM5SB3FaUm1qVAN75ez6
zY9MgoaH29Qvf173bwGg9uujkNgWwMGDo7J6XycjbUEO+5ZD+U2PRevzO9OR9z3E
nbbDKt1uR/8Rib4DrAKEC5jydhghVnNfQ/pavodHAHOlv3GNvXY75TZ8x7z+JuQc
DjFiGfoasqQHAlFQ75YasdUoMMlZULNfvxYuHONs5cPkTIiY8CmyQHcxogWeQFqZ
8ZCK8jpb9K1q0JIX596n/2lnZs8UiD+MPZbBHAj7U1T50eSI1zVgYJnSGTtA2HCj
soQtlc2BOIIZLT9du5Qch+Df9GOGGQY/Im6qAanRH5JbL0HtAKnvbIdoQjFlLA4z
xkDiFwTGtZmacPI0b5tZw6sDNIW5SBNOHQl/wBRCHn2kLGfUsB5IqXMHlsbLkDHC
74RqM/GsBJN2RsOvFyePcopCUwRwiph6/3gqlvs1izhciif8r4uv2clBDTecC7Bh
23Sm9ZyFkufsPtoYhODETE+JwYyQgD/xc181CLK1Q1H0O1VjEAAe9fa3azCoG1LK
e7cmaIQjkIda9A+u69slYgh7jE1P8jY1kJWP69WzcP2yjR59Ws87YUkjRdQzm0KU
U3LTBWG4FIUJPRi/6nkw7E9Ks93b5ChAcPXr5Hyr8eMNfSwm/H/pTcSEEJl2DEc9
v9naLu+KKnDSF3NPTCv1ZJzwjM8qfeHnhg6OBqep+ZTRQy/EkQ6q47dKXQPbEkDt
cfSQcLtpT+qKMhoavDfQ9Ir04OQDhxITLlDrY86JLJHL+87UW9PmKhpT6nc7/q1O
a74OmpSpHWXTVnppePRh7nyl0Kq0r3ZROjOPc8QDX+iW/KAg7NbpY7NxPhEazu6Z
pFBbMRRetgx7Tg21hthON50DgEJljlKbBRkVR84vVg47GPBnpQ8IRIOPe6uG28mY
KQ7G9JkJfxG5E7RdlWZjHjJ4JdetbURswj/Y+UWpfn44w/mhO0tcCGg4bN5JpcId
QZE3Wl+WunYaX2Ov+pgKeYWsVE4pkMLHbGnRIp+USMumgqFGAmE6YpPfhHYZ2ZfM
n9ObqxaQ3NTOYIe3XclQhmz0qirAwtHnhwi9MXnZGd9xmuJSuCCHe+cdlDM5B773
hmvPcXORVhUBeh9BxwkGyxbUymDt9K4Hqvt8Kk0JKW6npomniKvmDJOaidLDifyC
eso+vD3I4IlukPxYMsTIYL2h108mzVrOA6jI9YRbbz1p2DpnvkrTSDPkOKmxfNA0
PYodKAY6dgOzurM7AZt4ZltzsQHS/SU4KDBmyV4d4BU9P61x8i2sgSIRBXmpHkpy
Sg6TOvr0sFgpKtPdybenZpCsL25mDOgspcEWdFnP1erb9PIu3FBwywwBsZyjAxK5
CUkxzSPCnBNj1BUYQkLT3iwGSDtudzFcuVipgnI65DAio6JjbcPKopf64vDa81Xk
Ft++vODBSQtj2axp8W6Beh8pdfLiRWzCkwUvOyZmm1r2utrun+SD5ZJzvoymy4su
Qf+1R88veR/Im+yoKEwtTOddkz4irUhe3grL0etOVS0LLXMt4Cg2nATEmtXvbY+j
Oa1Ngd5eCy7D0+VNO2el9lCU6z7/Wsge089qMznZl3IcuNLcGYnItzx0jZaM9Adh
B/5tKLanJ6kSf3lvqjFWuro/8UQ2Klgrg5PaUwzoGRSQsNz4+7TExvXaKdHh/Dtr
kzsIfhMC82WQ5x0ItZB9Ft0VGRhInbKHRegFuLN48JDJTokelAueWwuxvgL8GZcR
4LY7P0NnOUGCzOesrzaKfa9SMUAM9HBWFm34EKWpPkhnb6Ahw81fqRVBeSFtQtwh
vRclrdd64jYI8ygd46f7/plReEuLuhkfsHeaGc86xxrT1cvVjPrP/X+Vrpta/NOX
v31s5gAtdwA3QC74Tg9qu0gjTfSSmtpwrmAWIamo/ztgZ1wG0rIKh1d1qNnD4fj4
ZVBxfumNHXYaP8YJaCA7eLebOlb59rbCDHM+rVWPoh0K/jDbdi+gCywWmDyXO4PF
a0bJDg7IWoCpvhbXFu4v8VUnx8fuhmstrtwzTokizSx1nRzu55psnBntCXl29Xg0
PSexTkg/vQF1tFVYE1ZJHM3yO7jnpkQsXtXhjtxL8iW8N+tSD8Qxz1h6Zz0rN30C
JtpUpHXctCSBNiDBWs26JaF3gQxXRyQtHrbr52qqRoJ2MZWfRW74RJ5bb6cLJgp3
iFYj2z2uN2DVxcXkWyMSMSKVJUgT4oHbg4TJo9xd5+vVxY1MMpdC9J9SUy/eCsZS
lfL5RyFTQ0E+hZ9ZnXwBN8JRnVC57pmbTFd++XrXvfRYGFoMhcEhEASW5nzuZD0Q
+cS/WBru+ivspc5AAMlm1wRd7s9eD9jAcEVzybEG4YT942COPqjYXn2SgXEveHSw
Y+hHgSxIxKIxIjoXYZqDZLZG7JlWRtklW9kz0Vt1+8Cf0VlGd78ADm82/EmCB2eU
hRUx0X7fLnYNidJj0o5+M8iLV33gg6XaiuHSUUk1iPaOHRlUjs0wKblsUbjUcqTb
GkWzQl1SsuXOnZaqESqhIThEDJEmikogbjOH2Rbux/Oa3rHNar+CwH5vdSgx8Jeg
hiwX35OyytFPSHAOaMkya+kKbVDJX8VQlPG4sgPLvyAY4KcHcivLKmVocrObEwiI
HsrONzJwVLdYGZkSEDhwAcbtXCHReTNlenWb14CjvZbONHaRQGF/YTQthMiirUpM
mvfmBVqQhgqo0OPx30121CSUKa6KOv3fIR5mIOo5CJ77moA65F9dVL292vEjZXNg
XvG+/50xuwZyRhpGXpoSjMmrUUnO2zRB/h9VT7aVfDlC7kTi56l6EIST7Bv7UBWj
x9mX6T3toOWUv8LG8KrAdVICZCCBWlyYVI7zKXn+HumJDkoFu8QM+/Jfwft4xg1H
7r0oGLJKuXEiy0gLv5Ee09e/Hi5yYZ3F33/afHIKi+gEDIzpAg+czVBVNbG2N54t
HIupS/yQ2zDs7Q56uoCo8w62uGXHzI2JxrB6JmWyyMOheZzxvnKDrVEPF28iv0tX
bphD9fHiTFKjtS91OL+9fT2E7fyPapSFrJFc6S8tISkxyA0GD37/uqijwbZgCh8L
vL/9Twx+Hq2gJJS39TMoNQqE/v32Gja8YekwZhHll4jALWJKEbXyeLSnp0nVJQ5l
drM2x6RWYshzYOPixia3WvzEuIk5XAvnJrBJ1p2rvdEXOCPJtwYpa0gFGoJJ88AI
ALiYNnZkTOenVA20z7e1pP1ss74/7OJPCO6OoZyk43FrdaEkbVc/fe+Wpjv0y/vT
AZd3SChX9+TitaiACIplikSiBHQFnNzSiDRBFqqMLTWBLqJWRaH9ESe6ctJue+aU
yELtbliDFUlbL3p1+Cxkb0tjdh6BvvR5x8gUHE89+STSaREp3yTabuZ43I8rjw2u
4R15/rkGSZui5V4O118Mv1OcMEjEj1FtvTFkm/nmxgm97DZhBjr8d954Xx12dJtD
QKIKoH1A8fKIeL6IkpBnN2+uaKy9tAd57QuDad4KnObpS7U+R0EHUSXDIAHwrelx
yOYtLTj+DoH5EiqwuGd98EzpOTnBYsmgZ/8x94MhYvsJSwFeFCzNbHKoZtZKkjoY
GFLcI8uJSF3n0rjiGIrc+GhPZlNWK9l8wMMiG4wUHkOJU1SEYBzaEkWRkA97u78f
A5/z/l+RKf0esMpbpRi27aFuOxXfkvKTI+K5sPBRlUds1mR6/VFNiim/s1DyUF9b
8BOJM6QJg0HoPFe48AgKFBb7q6gDyankw9EWMAv7wIbiUeLSilqJhwvZyG3s9dqD
R+loz2JZGYXEuL1Iz/gV+bY+yCo65ByfEHsAjg3kFETERxFkvKgatsKnyXEKzE2r
FoZMmzNPXxWrOcRSp8FK39/o3hbSibYYz6CKZkm1Zq5xXiqroH9GCAL645PHhOMU
U/BUyFPkW/IcL8tILXLPK+vfbHmkDxoUp30DZ0gPEjvSWUR+c9oxj9YbzRgEAfDX
RoURdYIAjzfdz+ZNgkmSQuFE0/fL+4QFNxYKx2RndfRtQo1IS2tWarcL9BONmwQm
/YXDyFZnFD1W6s/l1sm+JYBjR9DGOwUXBPIpBjT2G3mynIo9gVEeQk7YPNNl1XSN
QG7LFxIzllnzSU9GnnBNeUFubjaAgPwX5LFSup/Dhy7P+xpTOZ3UBYULMqb7X5CD
uG5TIM7OBr7LwX663OZcyl+NYLQbGAmsu515G/d+5v3tL23c+bzv4V+KT4GyKe91
axmRqDsCXdfJXEkWRWKL1I/vljHcekCd1qPRjbh65HgO+lm75nDoBKVlBhb31RtC
BM4d3j75ndEJ8F2WUFOovOK/2ehH94iaxqZavshqiVldWnfUipYwIu1ZwYdxDVFJ
Oq2pWLAo5bEdZCw3tg69cFQQaH8R/AgGxlatEK+ePZfhEDq9teYl6Ary0eJSaHFw
2ch3n4y/aNYweYcFBZNC7qy10JG0I3MdMMs7TtgxOYQ1HkHYX5Y906eJThaNHIHE
As6svFiUCRA9ObL1Nmzi6LZXHkQot6xY51hh3IqbuSslRxy8AYPWAXTmTTYZWkfE
wAYAPaKNYHsrekRf5jtLlZmPhK0/ambKVcLjazivMrDpEUc2YwQa/eGXBCq31ozS
9FqLCx4BO2QIE+DsJBhPoSomXtfRcn4SnZqIU7ntBhQIEy7fiEIlKbeZa8t9nAeL
5mao7ixNoK4w5VC2rhF0R5enCAKDhg4r6JJntzjDadaTFLztyiYyom77wiV6PwOF
DP1iFB2vFrD/RvLqnXSE6snwHpAQcd3JGDKLM/AaNvYlsJWW/q3aBpIf7PSbKocO
p+r0bm9NZrh3dEJ1te1H1po+2pFHSa66KuETukEXnBdBqtI8rd45Nrhe7wx/AVdB
4Oa8sL3VAD4+SNQjjaDBckO4Ib7nwP3oo8tCalneab1YmRACMC9RYAXLQadZyvhH
l+M1wHubfzY8f87dc3nfWWln1Ja6TIe1mhjptCi5rF5IU9nDEZHoSIrn/lsTsbrn
urZ+79kRlcakK/0jOuXo1tVKZUvFgEMSEwsTAMm5KjGuqEY8TxByt8rJDK47bRro
P/eDpRfSVjcPiPbZg7m1Q/rhcPtIa5rbj5L3w5mn4ZoXaKRUix+zDZhwZH19GV7b
nunGMmRzWqzzlVadyOMrvafvOFSjaknOS2A0Ij+eMjfrIPP1uUlYk1b2MqouKTST
aby/l/jMdWJ+YS0xfMduk0RhIiSqltpONo56QqOMWVNnrs/1GPG0w6GgRtxb+LJE
laBUO8v1pq1mpKiL6/NigKLKcJmapiEEurUVD9e8A1OEyWz8dA5HOpNl1ksK8JxK
sIn9lOTarXNZuzX3DcQEuUUC151+1B4tRqCum798pLGhmi4GsGVyOHo0veWveEDT
pkiqaMurCktS/UhfEyey3t+I8BbpPp7nr/XHdyLYaH08aiSl8hBsZKGO9fGb5NDh
JKSd1Ppei9mH4d95tO8GiyJiIzTU+w3ub3uHTaRHsvefi5gibJdkESfeL0Q16q5Q
VayTB6RbKgkMBIwKm2m539sw3x4nx4qGoDxBk9pOUvHtLqWbbOGbxOszZP7AATNB
3MuZYzVrAw4EOi6WiHBn1EvuaTGFJk+PRt6Mg7N6xKzB20n6XizBW2qhvMvXX7wt
g9W+c/WZmyT2S8AR4QdX2h7g4hN8zmkZkWYd3KWoXFFpy/Tvd7oZ7kCvmKm24jcQ
xpcEqVElE8mj+mhmPgMcTLocbQtAwrEKeK4XGjXaIWo8HB0MbDLMJ5VIBmMr3cFF
flmch9rQZqWgyd4iylIS+ejDrXOnye7MoklOYxR9RiMUqwSKUmbWLmzUWrbJqhIw
Qv7TNZ0n27EwDp9LQgHwF4fS7lJqKoiSCsuh+vEamzbSNicQ4nfdQa1pXdqzAJT2
yfTGVsl8c5lfjuRiB3QZNqUCS3VxEZH6XoqQrPkCD8kFbvcTKK1OQWNT9d4Uf5zH
uG8ZURAxUuLznNxTh378IvBV7GlxOi9y2g3b6CqJs8pWy6jSPNNaiKA1Up8i3sfl
yzTsiTZ+h5apCRc376RppALHcw+VHJw5y1FseDyyvYmt6axV6hX2PJvrr+LKS10l
1lTAkBLiKUYhFqWfqgYGnGrwYjXZ6di/ZG2EZVlnIbmhfS00vqfSiZMlwT4UoUuL
h2zyt0h14jLBlCC9kh71zRuSLywoiFKNQNGRT0ugAQ7aYMogaClb8Z6VZcHtX45r
PbMXs+eJIp7PqY+/MevVxumh4Gr7XWyb8Zd+FPhMEEoHDtoYPw4YAQS41oD57Gux
SgncWMzuGl0vqduhroSPX69X9ZyMNpsBF+GPXfsobeN64ncYTJoHLd3ZFdagwQxW
LEaNIWfx1U8klaGOnVu9SGCHgLarbfL/9UrFzXEwSdfFERCHf6HAbCQ0iFQUzDIG
gIJ/gfAgrUdq8jWOH0fhfw==
//pragma protect end_data_block
//pragma protect digest_block
Gf/V+u2CnKEUsoIrSyt5c60wFUY=
//pragma protect end_digest_block
//pragma protect end_protected
