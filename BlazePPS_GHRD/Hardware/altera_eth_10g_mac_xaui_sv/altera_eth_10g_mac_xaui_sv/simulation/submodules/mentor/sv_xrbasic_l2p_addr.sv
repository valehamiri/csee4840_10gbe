// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:51:54 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
GzcphGcSF1oi4i0KJ9+VERn74kpnKmbBqgFOrfFIJmJCxDUbdMlwP/i0Mv2anrT+
ait6WLxm5Jh3lrf9w9mQ2RT/ykRkVJ5PN3+VksQ8D4Wnb1iKlAa+UOfiVs5OOlc2
/icsRRTQzctYNqOSP0rCSe4F7VMmI7+JyPb2LY7Z/J0=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2368)
WnZqwewPGKFmKAoUw4V7ev5KQjzOZ05nLjLBN4KjkwPBwgUY1dyTgyCHVpHHuVyQ
MRB2HSW10eR9XF3tdZU2B0mtPV3oQp7NRDyg5BUnlVwGMKIGc3Chmr6wWLfUEYkC
JdJqivUXMnVa/3QFZ0cNauwkZiMI21r5WL043uq47AbZf+XI99VQTuI4FgiCZU+K
+ySb4jIXms5J+XlHjpaZ6AHaVxTWfbE7BGIvLhGfWDjGkhybutTrC9xK8amoIRm8
KYDXyA+V5f+5JWk7aclEGWqBFK1AvEvMC3dRx4C75KzR/vJp2B7MDyGH9iVB5e7W
ItYcY7iDFy3DfCeogkFCAznR1gCbRhk8H6oLwc/d/iD+BBylP6Sce/itey1H4kqB
Iog9uz5leILHBSaIfaZTT3L0FuG5EI/cu1aAOzI6Sc+Vh9D/WtI+foTPLl+onrXw
rwyNhVib0TE/7pYQvw/8Md2ipWFeJjK40de/qL1rzmLSN/ECA+XL2Zd5oxA+4rdN
MHzqVMCt3vzRYQXMHE9bJxwsZdKKKXEg0PWalqkRmMSuF18IhmCAme0t8meg2MJZ
RBs+UnfHGY+LzMz8fyGrTea9RXMilHZ/S/h7Tkc2XhUnGqWudXH+PZkGr76KPBfz
q+dPuFNQ+EZJrdC86sk8tQ+b1Q1lPhPVt2TY7tjyHgNXpXVEC80Aok0i7wRArc4P
7azqzyq53nbYPZwFFFDmrqn76SkopyDsqRLcFP82yHpWS6V65ZlPwoqoc1oRDXIq
BJmOS2olmFv0cqvjATLNiaDCesB/ctmg+TbWDvjYpwSWwSvub9ViinQ4rV+6Hikl
B3wM9ymJlPW/VHgn6glgw4xznk4dctJZnu6CWhvxsKdn7yEojAJagAlLT/dYEWv1
B0ABLW5RjP5KOIvGyUD8De8uYX2zRwqUhAHgMPi07U+zcrJaHgQLXY69SwV503n5
f510pEvGT2oDqBvTHUa1RprMNu1fxcDpOxvdsR+FZRyEYUz2XGriYeqGYyxRu87O
Qe1qunbKuzPed90eXZIjXcoVHtN9Y2l/xlTeP4uqZotOQUsQzd7y3GTYnFZnle5o
cIdf9XYbjthYut21aevdqofLlCSZB+Q6A6rmwFkYXX1igyu8aimO9tzu3sS0E/wo
J5OcqIJjDNnpeR8y6AwthjfdobRWyc7apQx/DXNfSgo8h3h/Udb76sA0M9QH/0JN
Xu4uLO+GcPXqv/iRLxhU1xQ3rmpiy3eKFmIs1TVcVREw7AP/cpaeRk+3gDt6lHtw
0vBSix+kNTU10awv/ZemPenHBDjkIYHNqlKQxRBWEdyFDEvO9cLu6i0taQB/8qce
jo+ulgF7GaPvE2lA5QTp3JGKL8/8kpVO/jzbUc0ZCqcWMY5eiUebGF0iMfSIjemn
JntVQYrMngS5nIFLlMQfTI70POQEE82C6yTL6bLWjk1/8wlt+SdJhfO8sIlt/eDI
mQLJykB3spRVCqP/YHPrI9ejmElGywd8IVWauN+6TPmHKd+tf0efJqtl0R0BTQhD
/b4fFJplWkjWMK4C7duVPQ6ZASrazHjbBR29cIMltopPkBaanxTVhRbXM64fK1m/
JuyP3trmlXCST3oiYN3J0L4pw84ZlP4yi4dbI8BS7ZRZlUC8KR50OPp7hlFY5Po2
NhXgpnXnlLTEGAZizywTUn7p83VoyCYcCArHw74wJdLKld6ic2gDTtGk5KnZWhXO
JHS/PnBQJkdXzw5fw/7d+JqdxViQhXBRG2HVHuk3rxZm7eJq4o9CWF0h6Wqdx8Pm
j2P4WeZGFsYPV41YEQ5txMA+ZS8pXXwBlv8rcONkOO8HqiH2ZYTpvpr5hF7Dw6Mw
Qzh+g1UZNP/Dlo9/n2JQcrX7f2bqtjd1KMAyrqcJ/bqncV14Mf4U3jl+5bvD9nEL
IdJVD1WFsf9fucOyYqiY/ndqsmxpzINnXaIZtp7PvWTxnYr4pDuRONQJmTrrdZL2
XFWQ7gUXlKIAjf9ikpoLExoYw8X89AsWDw6NA+pJhr7zFjMiqLYergXPwiKh3Kmp
wfr6+6argc9KH6f4mdRWvqQBXDm8eF5UJl7jFeV2JOQAN2Vtxvd+gulsxF1g3uNk
k3o2c9T/ljxWh86h7IMQVLPZUYP3lqd1Q0W3WYpOYorG6So/qcSI/XL/NEKgl++3
dLJkzsk82N8MwcA/Wx94pkSBaG5uI1xAd8Xkwdu1I6qUqMGV1F8EvMh6VNfqkzba
QXFjFHeTWPW3/KPzaaM6kZAfggpJUhIeSWPUUpifZV3iOOuuF6nfNX9+NJzvNfX0
tw9XGD+Yz1gsD7YW9Tqr1cUZkEUb313RoMF7fGYRlcSl7zT6YmXUKHvFxOzBsPNe
OI4qdlDg4jFwWNXpOVi7YGZ66Domq+ImYs1Cs/tNF9yrV9uyMrCSkddA7PJNTIIH
qsVgOK2rHiKaL07COWszbgNlaLUwFgsigzf8/0IxTWGM437v1RAYQjPGOVIYs6j5
m3ayWNH1EDioJ1AuS39RmFAC0SSJ5i6Ohkkp5HX/MzX8tRzJ3utgI3oKx/85YGkZ
Muxjfv2bgEvDH5gy34Vs6G0k3AB0hB7zzfJCx3yq6OnsfFScsNAQjFYCJyf1IOVP
OLv8E2Z4O+V1jGL2tczCsHZqjL1Rmm8bDVSP3uP7zelSyqKY+6QNdEpjuRrn6d9P
TqIfrzpP/bczZM3X8t9XbcuPVBfGZtieExc7vqx43CdNBCn38Y94/jYs39uSf/Ub
p184RYozNO4cYXUOWR80xwyvWPZ2DQuJlI+xlr5e+UV+2/OHJRw1wF3pP+Z7bYle
l6rLYF9syapHAMBQk8jmBcZNExyU7SnW33T8g17zTDTl2ReZ/I6Wx1ho4jxcF0oG
lFNUOjKmghwcQrOz4bi3Ncmyu18OaxWauAPcIcUvLXoa3PHZIIZtQLQ5fmOVkJyp
VyuhXxFYYKoqVbbloqlohwN9UdXhz6UVhuEMDTJ3C6nZ30gs9aj5aGZQA2q3yfAZ
wkRga0d9k0pY83ETnJrKHMdSN6nZqhbH2wmHnf+EUOp3ELM2nHBuu/SmTHLiTEtp
pP5FhAQHe/ysHgcK6JStkXyXDldb8lYctJqhn0dRqawiwbMtjVYdDPJDV3/2Hrpc
UIB/sBoBbXi7fgWO48Uocw==
`pragma protect end_protected
