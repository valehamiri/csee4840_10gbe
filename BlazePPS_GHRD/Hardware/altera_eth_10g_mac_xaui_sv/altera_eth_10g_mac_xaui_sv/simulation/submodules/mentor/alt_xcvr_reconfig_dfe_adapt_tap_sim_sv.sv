// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:51:54 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
shkwc/WLzXE3BEL9j1avbLixjxEevsY2zIiDLqw7wBLCjUDjs4QsHvhSjl9QiiUR
J0HHx58ESONbfwS3KtJqIfDGzcQ0zokSmLrRLWsAweZEN7y5sFIDITEnCxHf3CRi
P1CSbaNEN8uPONjejZNc1QLZGox/lmqyLS1/0f7w2tk=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2992)
zDWC6jlfImoyImCbF1oile7kBIZt6ZFpnmoTPDvVP87r8Lw4KFKygBBA2laoeh6Q
q6S4L+d0cKIqA5orU3kNsjqQYGQjsblzUgKrXRyISLMniU+4vPDaqi7Z3+Fvvr3P
7wbQEoDEK3BcWrKwcUU955HyX4XloAVdjdFgcVRL9NOo/rSKDP+WUlcAQWz+daW9
iPYm+YwJ18PLSz9RQQYP+W6HYX64iNyMBVGiSrqhYEHG/pITMUzibegt4DpQSqWD
DmONz74kO0NDcZYvlOXPXOzHStkohwZS3Br42l4WvDMNbgIrXwiioT9/DWzUUMmR
rHgiigzLETCYUGr901iqFYBm0QQjlW3WfFCDQFuGCs09Ii/mJfjChlDRgFToSDDz
mSpiZLf9RECCBtNh1qtIip0UL3LXSM7CXpU55HONpUilJB/PtYvOsblUiLwO2NTR
K///WsIS4catN9nOfiIisW9KdOffWZrgkSVKD7lavCmlwtVRFzYyNuKnlWuxJwPQ
Yh1V2WPbqbWuCq1cIse47nHewG4dlTdEoyEzuJqo7xcMOzy3Y8mWus5NqWwOv3yb
I5nYKBFteYLQ3OpaN29SvFrF8h6pKOKKWjKebPHAj4v7KsSvHDx5ax/dM3Yf+S7Y
OeDRV0fFVTqvVJrx7oFwumG2M0X5wjx4m94zSV3qYuq2PfcShPmf9uqHdLRQBa9K
pt59HeDOsGHprv+/cw1qg+0QvdxYBSQ0w7DwxxPgHy27ESKSxIfhNzv6/ZecG01X
RLqig5h2ZezVDZ8JN3CI1Q4/OprW9Duth+iKFGOZ/CocLcgZVf4Yy2eAi2lev5Ev
pqXREp+sk3TkiMkc/jj/d3+U5qbIBcqdJGTu0Rb3uLiP8zNUht/mNNecY1vr2ukC
BOAi5+iT4iXlKApJ0t/hKm1ZEuShcJd66SQSzne2Qo+OgcvoAOhhRYdDMfJYZTaQ
3giVrNPcGD81EJiQgfT3ybDzWippLKtPxeEb4YfC2DUQ9vUGuCugKLxCrI49O1ip
jCo/aEXlznH40H4HDJNdVjoz0oLRtpCNmdQpFWXUcTr/3yHYIg+GJByeRaJCnDFZ
SqSQzM7JQV31dWikrQrP5sSrHgsFrVJT0lwLn+Tpp0Fctu68MdiGFx3LI6wTuXwO
qYKCiyBaVAdXK+Wer4FSg49wufAu10yq4FHTPkTyjAb8nJT1aSJ5H01j4KrIiPDS
xtPfGW5by/0GX/gZfme6VEDrc5wRNLhWRbVUNfWQtGjaz0FqDKHfkums0uoqqDon
GFcCNjYV9EQRE+3tH2XC4kotDx3l8gDVeiCTFdICFk3m800CADzgaO7t7gKs4/X5
8Lb0h6hYWfoeN20C2UZLFmPsZqa2wXdlke37sA42VOBFBD7ShAHg7xHfnGnzXAqD
LaxX0toA6MwUqhZyTo63Ez1U9lvF/IdPz+fqNYg85FAZaug6YINoHZ46ryAw/NLl
LBbRYcIIiBRQm+p7FSM4JdHGN+miFH/YvuMkcqa5p18x0FC7S+ubgEb57wxRhrx8
sKn6PCvf9XKXyTXlXQgJby8BjmsVOBNWaXnch99XxR29vhwy6Tj2vaaTkNjGZdmF
EKUKs1BIjUaxLhsVLf3IkuLrqfHAu1B30ld6wnE9DFJYM6Pw6LlSkpIBU/PFOZgr
0qbFzkXGu5asscXNgZw0jVlIrDU/mSdIlhSgZDUB5lD+C4cvzc9lvrhLHi+o/pHo
ZBAk+NLJnx7YZCt4Dx5QEk1EdptKwdCaLbgUZw5J7+/YZr5HGY3Cew+GvC/ARsU5
/TUTzjhnbToP4ge5jdVouXq5byjWaVC3dLyFpKBCfE+zZ890zW5vlfNka/fn23HX
PGHaKQ267APqih6djpI7PW2DrTRKyS4fGVozsZtPOFK7WlfHgGDUiAl+i8vGuSGR
fgSsq6hu9yBtTfTTLX9N3UqE5HB9oWnlHYI4q8bjxAYfvOz3+Y2qiXQ4JXHMtvMH
2Afjl7oVg3EdPJkUTPl/IDOcWr5zaTRGRu/y/15BA14YLiwzchn/2aKClmf3ewV1
PQae1fGIzeCCGOnnvCNaNQW1jmSWBhmbKta7fDVUXJqGbnH4SJnJZDK7/roTc4n3
JhkW4pfXEdA0a/bbkKGAmRcPujDwS3QFlTyKPPxpMeWcmlU1u9E29uOO/R1ucwxI
9BImlUQJEeN5p0vPEg7k8MrVy9gbrIIQRWna6I8N/6FDY4mUDzzMrKPKokS52IQO
HdEi6yNa4bDaKBW11rIuDFJN/5L5FQBXc5ghL6aMWjw2+He8QpZ4rzNPwXFr/0Tu
Iab7/C/z7GrKt3cY5QkzBUknCsUhr9rknXAqT7vQjFLO1RazLXJDjHfyc/ZFUsOK
1S6B61orCKi2cWrY4UI6hqCfdhqgb9Y2/asYrvorQRdyF8AnAD+finsEon6rxIvV
R0LE6JkaM8N/j8NGv6rFKtDIINQmRYy/nIraF6qeR72jY2hCc7KjJy82aLOJRgQ4
tBcFUWWFGXu4ycLGidXbT01PyMNiUe5E/BFC6AVLxKRhm8iKXfkyGWZHaipz+zcq
NDpCWezJcCkWTFXb3RDNkEvLQgLH/D0HgeLiSQw4k295wbDuAAo2jk2Ib4i4UFKv
tiMyUtMda2w+2XRHUE6XQ61+VG6/wzRPyhekSAgYDDNAWBMJjaeO+RCK56mSYF94
Q+hlSBWnP/krk8N4d1ee9yXQYvoyyj4c8J/DoCS1lhXXocQXqo6FhGxrJjjlDMdn
BQ7khCSpNm2j48a1KaIdkz3M9t2OJb6n2RoSxJevEG9kRn66N/eu7YAm/eADb18q
gCrld+54J16vSfyY3FTyL1VJYxdKiCBWGq+pJyiX+Z0ZlQMRknuzd2Md+QusZlCr
wv/c4q+ZsqB+6akEZMVwtCz2gIYWOLm2R+abxUrdlq5CFgi7dWZ2eRRH7aVqI+QF
TWPDTGHsBr6rl+2Bf0Rw7gYDrxZl3zk2T621dChwDLJ2fcwqDMR+nox7iBrz1IG2
1gjakMfQ6mlCcb9Q2V0mGhhNoSueivvtKPHu4Lf41yg0rmHsaYZfJWmtiMumcOWO
Aj1FyWopqaAQur/780LPp6yIF1J6lyPpWQzqkaocTISKiBDCcb4N/2cNdoL0Cr4b
RyYRl0rBgLBua/sDj9ZvW0NWsR5IH4weygwA7bSyBnfo6SqyU1re4ZTVUUPT0xsr
D1qir2qH5PwAqFJ0aidDqekIo5DD+LWpW6dSm2DwyE1BqRYrrbvXbCU2zV+eEVUK
xziH17vbNyGXLJExtzppxuitmH1D62EqIeFEN+SStIqtpR6i7r2dlwkVhAoM/D7i
+0hnDd1twtYlK++N9D9sLyizL5uQNOx5vkbNudJ0S4UqJKxCaLBsuIz3ETkMpXQQ
iZtSUrSR9WY1rL1is+nsyOs5baqqCy2lBAQ9W2VWrcEhm0Uhm2konkcjtq2sMWly
R7acanlx1b803505bWHfmxFqHaBIsMvJCX5jW7uTZHUf48QgbBxq0vjkOTuaNXdc
uK4s4Kp+diuDqS3hiCvvuPyfO3No12KRU6JvJMyyYWLPv0diZCli8wowSVX2KQP4
zWJvcm6raBcMcN5YJuBF78vjQG9/JIzSUJMNF76id0rivPLqt73GUSoYI8nvq/9B
e3jtJ/9P5t9C5Qs6V5/Yu4JcA2K00i9/c2obEdGvYTXfbDnYQvqHS1XS6iztMmR7
d3D7ZNpDj05BCgILwTCo3Qpj8QEUzvFo71xuaXq77QfPd7pKb6HUBQkqVFDOUK4a
YDl6AK0lO/CRMlTR702LOOuL1EPiiA1bW3ZOmiIEu4KYlNRhqtkWE2+NaM+YTyTf
EJfz/RDDQceuXA5VinTyPsbUW1crbVb83zysfql8qA3pn1iBnBteSusBlDYwq6JN
kvpuJXOnw6XhaoyPY6mj1LZgJK76pjtjD7eiEAH34gspllf0x1H3vn8olg36vkYA
RLR+pw1Vwwp88bWU2elOnQ==
`pragma protect end_protected
