// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:51:54 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
DXY4I+5f8ysNuzSq3rwrsjldcTvaIO0A6d5W2+psP4IgMl3RHS+KEjPFsY0fl5+K
Wr6lcgV5a+QdKJGV0h+XA7Ggpc+TsRAqG52/NrhWlCLYR3+fE/QHp+STtk72K3MR
Gqb/o5lMHprRNRXoGByEBTpv2unb/cLFfgZKvOZVKD0=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1632)
rKOUHlcKpMxfsgpcg9iJyc/ZQAlKm3FJPvciOzrAvfMGvWjyXx+HPtIyD6puC10j
ibu1wcIIxCDFTI5rO2FeY50bjdeC7YmPQ/hon7hbMKdDfCB9g5v0iBSkI3VhhVHv
e+P5THSIWcPPq727d0x3PXeCKXD66xFX/glL0WPhZ55Z4v1kyXZ3ZSqAEjLS4Un8
ZfDdPbn1UJeY1CoKLc95R+VKpnmoiFKaWNrNyGadKNM3QsSk2j7O4nTrm9q3z8Lz
uEK+1NqgZBQSXXmGzrgMmQ9P1yIhFjB5AtuhD/wLoNqlvbdfQ8JRt/1ZgT0X6F3c
JYchUHJpZQXb4JXmUSNXvaGnmzWEF1qqwhA2pX6v5KxZNVlaLi9wycpigVx3eF4j
PFRs9LU9sn5LlncI7HuUvz+grP2rJju0BMW+oaqgGquFv31eKTWEGEqvDQsQkfkG
KBXtxC8nz8Q9I/E/5i6PhDr+Uh+8KC8CoF86AwM+tPjM/bdxKT/9Q79m9cPOjAZP
Fr+4QiN9Ub8pAGxDgvpoYPFLjREj+zb5eXu9WcCwfLfGjgDQPPowV/0pLznDE5x/
gvwPS0ZVf7weF7iZekXLNlXj4uUlzDbqmFCxFbvfPTgr3ooEBR/sG0OpWtV6OOZm
UOcn/pXrNloisLAycLMkjvgg+kdixSTBxMarWwgYcGFUoGfHXrZ3yrFhdCD6xxiG
S7eQ4jCcQQFVAg1PKhVHYocTFO5gAHSO5YMyQHYdKFFxEr3Cvhj/l5ecEWPljFSM
pJwVJldSrmq2lZbpWRfE2A82bO7mm4kZ7SaXeIvFe5d0z/L8PpWG6imDAHGOSils
TY+t76uDa8t3vrmcXXtf41NojQpHGyqX7Odj0c4Pm0+SaYQ3LQc3Tr05J6FBDmL/
rhc4nUzs7aR62qKOOLO3LzjNOXco86W+uXikwIaGtrV3P+TKDVTRt/wGUDL+F+6V
DTw5qUc/ufdJAMbmSqkGUCa4GVfHBVJcTypxnz1OJ9ChZPWWJIl065mll2VMZu8b
lH/0o1gpMSzpX07qYLHFjhsVv7sAhLb5LIduddYaYSkUYh4PoBkmSnzv3VJ1boFO
NSLUxN/o+viyjJjDYwV/fAbR7VXMQp4Pr59liCLhc6BmL6YJAnygTA9ZpToNzSsb
nnOZSwwLC8KQkLrdgmpEsQI0Brdl2FnvRrsQfAZAUYu9VpdjEdjW3ZgRWPiMFeTA
nwHgm0/PrtbM95ezxShVX6p/G31llBUaJ6UaiLDY0vc+vfAjUZh1OmtgRPtnpJFR
Y/gnmCJt0Rl3ZyVCGzN2yxoiBPDR+adBFQB3jD0XU+9pfGP3oFLSX8SHiWnPaE3T
bO2+QtMa/0tnlyfmsOMMf2btGUWNj+aOtaQ5haLO0E1Wrr54hZQxLg/IFYJRtPle
IxtbDXAmuZXCDladIRfUGiyq8uxAKf7Ixnl71bAp8ZfLiM/WeGftrraEkK+7LB+1
BRACn4pYxvYk3ZJ5OVMkKwmvZKujLonBn1JjIZoPqNUKqUOujurRXuloRZco473k
1yoURFKokxX0uIL4mvcjLEz71m/jSEoKpclNw0Lb2buRukEaopkwCfTKW3kUCax6
wgbSJOq4k6zhwl+y9M6eHzJ8JAJIM9h74JOkEDGS62TiQnH/o1WeKqwFze+/GJdn
fZ5VogCF9cqnLRR3KDdVvBhuIi/keUgXFgJSOsOiAxP223frE5BmeBX5VxceGoNp
aIicivOiizoVOy4IR4IdAMaXGlp5cJiIRjeh1oddTriUiO/ruxTNs7zt27uW2RJI
UQryX74U/9Lj0jrfoxc2EdEfVnor8IAHwc33oshUsPoWNPjS0S/4dgRehtCDEm16
BZ39hsax1PmWfNol8Wg68YUMVV6YLCm027u1LEA9+PgotMNg8KeLQDqwrejuTVdb
Adb3vk9KBWz1SQ4sdAMokabeJMJFwzLnHFvZDxpJRj6a7rOGLO5EfHAZsLvd8fzY
aYFnk42f6ezyxKkRktkoaoaK58YAzMyZfjR/RxxWAeOxvPXEXNoD5r/cipXiwxyK
vrXv1Z8uzBjeJ+8kHTTwzOFVyidllc7sskpHo1Y/smIo4xJiu2wMhLAHFF68LY3L
6CAmPwQSYdJzhgEE/96oZP8xpdxqGlfN+dt0JQV8fDeRlsWw4Q++0CbTPIAPBR41
`pragma protect end_protected
