// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:51:54 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
S+jeyjCa8h5nsVTR2pxAosgT3kbYpHxu+xaC1wSkNkkZ5wPlkrqsL+A3YBHbTCPk
yJ8dUD99nDMtlCFWnTmRYGSgvKHy105nw8f0Qau3CTrRMCIv2HaEQbRxX8TsQbXL
8dzJdLe4Cpw5wuZQEfXrLSaSAbyNvXPRPguqaTx05bg=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 7216)
MvEjaIKN3hS/LDmq5vX3sb6XrGjfzP/DjMgCdn+zM8DJSfwZP4kX0PVsEC9farOb
UwvSlvHRWaiCP2S1u7mIWj9ItWNtr7SybasUYYkZNGbhIB3VDcp3lfeLzft0XrN7
HMptpktzJTwkxaSaybob65WsNM5EG9+SPWgpHACEdmb9FzSBKcmouEPT3n/i9ypQ
3vFf4hNfCQZmlDLbmL57yVCZhUMVPP5Zr6OzaplS4468tUEiS0AYay+fH9++/3QQ
QnNBjcvv9mkxOJ4mOYePkBwhWVdEUGeM6B2DT7tB7BlmMXieYaIiHXzAdqSGJbwI
IdFc4+f7tDv+/TjzWBX0mtl70dd7sU1Fhq/FTc17wfWQhrDjAP8DRcUKbr4WQYeh
9ch80oC+/uYWXowcbBt3L47q/MsdmcLk6Rr5pudPnMVsY0wV/VYyhdwjmZXopRY4
e+CITRXxHkq/dSDnAXyID7+ata2wGNSodjfrU8ShHkjJsjyte+o0HgCyuzqgmBTe
znSAgbqgAe5Ncp8lZAbcH2+Kuzoy4ZFwJvf1GZcn1JldSuO+Hc5LT+MwGtXXQQCt
B35BfLXDw2OIz3Ei6N18XSnz5kGw6jBX/gwjo9XrbwTPwRPC1sMKFfmJbcxwS3Aw
YlphIwsg1rLN2RUkH/giR6qBnJJvkTmK0I0HSJcuYd760j0ejx5M+TR2eS9yVz12
wve4+S9l75ozHL53wGZnSXq7YZTqbEeDjRHSjt5gT+GQOpRHmcx4UF3svOrDDNbG
1PJKrN/27IDRyrhL0UDzqJVsY2SBI3VrWwU0YfnxA3ZGNLql1OIk9In+VRcUT0LL
6Pl8z7KaCVJHvbYvdBNG/EskM+mz12zxk52mI0goV3anCuJMfpMaxTOoSvQmQxey
9enjFFKhdbvUulK77wNLqtFO/s1ulNlKcb8kH8bKl3EKg9DA0ZthiaikQFXbvaU9
9FnMHBhEvxDgZtfVDDGbaDxx7Y4VqtslvCbmdoJ7skZBRsi9qoXhYb31mvNfW+dK
rMO50kq2Mn3Jaq5TbsuOdPetseVZgSUi5VOdRXKYdtas0CaraG2oii0ovIOrByRJ
6lt0caZVfYSQkJ2CZ1YgVpEVvaWgDDUXZK6z3RnoDc3q1v2ejRYaalRxKx65rrPy
W7b+FtGHWHVN3eW7Fgq+NkK8HmwdC4dHtVa3UsgwTAQnGgZSxtvJ6scSQz98lWeV
Zefnt2+T1ridf6lcTlPSK9i5fLTMM8u6SERylsbF0+GwdT90gqjOtc40OyoVgbB9
ENy9/3lPlUHLPwWcfX15nqaoZZFTtDTnjY2BwaXiTUWaza4oUf0dlgtW2molZ06w
OJKzwVxxqepeuxYd6lnonLMeukCPK3DbBoIL9HUug8cD/6Cg6z8pJ7tLBm4zGDpk
Kx39vslj4LN7378Z53DWg/bzYcroI/XIm3AYgltKi251eRWyQ6AUU+97KTOFjFLV
3V3CwGf63vc3eNIbMGJY9BZir/tXi+SBIaqpyxpHoXFZxKtjJDOPCZPls214XCm2
Ykaf1MKWNZdTsP2BUm63MvJ1wq6+qQfW872md5EaJd4Q6mrW4tthilw/KWqLkeoa
Az643PguYRYSPEySbwtxEM+P60HE7F8vjXjQW7CaXezawcV4cBOJl3Pc6RKezT5S
6cevjVr7iKqWT54AqyPU+ABxrs20OeHwpP/XwQoJDZKAK7IBP1G+rZtAJSWDM7z+
2zSw+BBvDV5VNwtykiT+zDWdso7RzYEXMix3WmqwKKugue9C8y/WP3/XzFz5Hhs3
Kkbi/q31nDh7GC57pj30dLHS+4ta6fWJq6umCZ7VB4H195U61Zruaw2VqHYSussX
bEEX4abyRAizOdtZhvp6mz/7w2oHIQx8YHLRmrf0U2xJNYhuHhKe7eOj0GrR/jQE
s8a8HHUZw14Ap0qV1keGVQnYKIjvK3RoupFM4zwkL/JwS2YAt47I+0qZVLC/zTSx
JigecGewWnyI2eZ3b8CzZZEew9KL4pRR248paot8Cm5oepnqDdjoGG42NedFJ1qo
e0YHctWMoRgIjTkohl7mxWymT8+xcPd9TMqGP01YeRuogiLPiLQYA25l6xOFrxcL
4Ktm+Vp64kkrX2IeRkS2MLRF/vm6ejBYd1SDtSvSKxw28RUJd7OCkZHPRHc8a1Tc
wfRxWAeBJfj3Se4c/HmkGfoPkfyQlnCMxgmRyX8sgBXMXIBV8zhhhbIV66m0f5oA
RHIBqlObPgJjz0T+fm9f3Y8Zj/+FqdHayLnL7xhtPdJo3fYBT7as3r/cxMSaAN48
diQzvKTKfC1w98tgCh8WJRc0HGx6fSft8v0cGFFw0UU+hvpQR+jXIi0svpIZkuPs
l9Vby4CUek/LZiivdRJwCt8Y5opPCWCcRTdmmTIWz+OE5Hx+izouC5n9i2vYLpg7
eawCOeDXCQ3z10bpbDr4Ur4LlfrIeltrkgKUgaEd4tFHGVGB7FG9NxVf6qIEQcHW
KB4f0h9eaSQDzaDOhzC8L8cfB/86q44S9xMwnykLKOr4L6l70k75vN2WkJhO9P9H
9rYdVVKW7J2AZGqgrf8l69ze5qWZMXZBvYc3gXQZTtg9wlNmcIogKRz6renCbryQ
kxjVLob53y7PLq9tEuj/Hlg2EA8el8sgzNc17WUfNXvpjPpAxj3UkCorHrtH2xX9
qM9oXlbQr4XCVAHvqRxizRnVuN8nnlEkSE8ZZ1upmJLcvuogjyfCAKtfObBm8C23
08XucewRFOyT75CdfOAgUEeBL7g+CoxqC2VDjrW5wlTupMIrLo/IQxb+xPWfGBUe
zBuuy1gOILkIbCA5XK3V7OWYE71XXGMi3k3jw/SX/Zc/gfJ4x8BBCk4dw4FajGkS
1zAKaUVYvMZPAVmGYGXtgLnKMa1tbmITcawluu6hPVAxb06fiTdZ9gPq4/8Bug2X
Sg+hvDWF/hntNwjvYsE+5R5zxW2NSyqaQYqljKdplTIUPuNLFUsA9F6GL8U7q7q9
7OWTw2ZvGq49EBqq8/vpWVFSs9CsB3WLjLVLsZpk24+t4ogFzM4JO6DYBPRM42zo
gDmSNB/706mquhtAWCGZbGLsUcenDwR08emnn8rsZQTKIl9eIRL2yuoUvcWOJl/Q
4y2UkwQs7RMDjyXXd/GmT+H9VwrJisAIhw94UL1/kHXvqFqlHcsvbTP3P3J8ecsH
IqGVUSSetb7Yqwso8UlLVUQloLH7KhPbFokMm8EjjWF6eZau08RjajveB3u16yiW
mzmGD+ql9eYHBLwlVPINdkFUdlKokefuyDzIBqmLi+i88nSMIgroU5DewMjzKBIS
Ib5RNoyyijrjXv3gX6AYhxmxJodTkssqJk7uHNDCxf53yVAFuz6CHsfz5ugZ2vPo
QZ57L+Q1Y0YXamwWcmybUUdp6YvcMrx1nEcjVtjgPk1Jans+7cu1ACDJxb2ypLgV
Gh6Ql66ReYyklnh75lVN9Bj24ZYDUQSxVFouWT/x5T56AO5RXfLTAuUbQwweZOcT
pBBZDGpQ+byVX+2rPMbExq73a62cDi22TkcskJ9KojnScbsMN1B5ecYEQXFfq8nO
Jx1m+uHxs+soUAMaKWoNakCvQy/FtfFIj3NguJtfkQkoZvXeTjxUufHiYbP6EKVV
3gLvFaTLg7cOYBzhZuAullh7KLDnK8nKNUJNVLUJAi4cwPY6KfWaeXbybj40Zpig
kpY58RvfdFTsNcuaBx5Q0aBM1k4cJy+stA4ENdzPYpgChDRMDXyZ7TzoF70tfKsx
rtfwE4kzujmk6lbN3nyHvAiVL/ijtIo/L8AsXEy8ORwKuVDKZyILLBFsl4mUaoBb
BPLllSS9fPK9875A4ZaooqZUVpCDZvw8khLe37iY+KUVusqQ6fXDHzQu9318GgDo
KR8DtP//ijcIgDKU6DtrI7iH8mHhZMRO5Ha94lJQ+zFsqM8jVcllgXYY02yAiglJ
Bb+z+B609r3FmiPvG2NX3//NoCI6GIQ/H7h1wcpacZEDTtqkY/fsFVJYmo77Dy0z
ldShiBlmZmWVd0F0Hrs9/EqZOUhKbE4wHIXLNHHQefTNWJOwxcRb4jHmfjerDrBP
fyE3ZpC89YQUVBzx8Ypd5mf+xP08y5Dc8LmvHwhzTTP+tm8VWknb92NjhdfIYvTp
GvqBZ2jcuRiOrzYr76lcpWPdfymd2tGsT4GhfVvcsu0rMKWUYLab5mdZPx1/x++L
hzTJAtVdqqogoQJAz/BMoxCq942qN1jEZDSPZ/GZlHqKIIVJcOHSFmv6v0OxQZJW
r/syYE+SSTwmvUAXGFX5kmu7xSCsdcm8R8+PBvjDJ1bKXZJr4+TQu5Q5KaJXyJDb
Ge6eZWhrvfD+3huE+qIue0jO65QED3O7OqZsuBMDjBGgY1TU3wH7jAKIiH4nTFqc
JFCErz6b5JfoqJN0EI4SNABfWpVtG8ZO/eVQ/JePT5t70JCvs3GDYNMg9uZ5P5IJ
993j9cEoiSOJxWDToMt1mNkhD3NIMM9kvM3hjWz1zopbEQQUCJbwHnLPTvMQ/CLd
qrNFQk+X64VZGNECurfGvHq9EzLdVCnPXme1/Wc/Ip6bIVEMtNZ9rGeIdAxXCX9v
7sX+Exo8dfGRgpGC51ZbFSIu0cNZdP8NITinjuw6T68oqro0rC9xkK4rTzEl2vaT
nL2r5hlQu5gln4+PCIkIsimu4I1YK2aNHPSWUHRpvLY1hd+cpmza9YDrWlNXjpHA
YmHlUTf+o35O0BNI/GOqSneR+xp3NYV2Rbmb/HZ9vNDm/6e3kRnG5Xqexm0hCrN5
nl8A26AgRdryd6R+8TPd/DD5jh1sCTsiSNdH6UG0FXXpRXreeiUIQe0li4hOB6kn
BygBPJRpslgxHdsftk34y6tpv8623H1P8ngq+5ZlQtHPc8r8aqY/+OKwpDjSWcjW
Cvenjs2vWzwFf6tdDuhy4dNGF+y9S60DckI9LVz711n9IgdYuanM1dvKQjADFTtu
yh/Ww1ocg6ROcJUxjJWmzfLSorJVBNXZUxcM9eHKKIuqKjOnnA/cwaFcWyCwcIbS
y0GQIYm7N6SXK6FQ2DVF8VJkvq+qqCKvCv8+8Mzc1HY6cQDuy0/TYl9HKMcVhJqf
09WYxRf/4GWBDfbY/x8VhWXSKAArdjspe4LX7HExPySKZ1gqG+6weCpVL0XDxVo2
04D9jOT7qowNt35p21ZmmF5Q/M4mrjVktArJPNV0H16BdpvDxfe72mVaA6z1LUs0
UKoDIG0kgFUYMF5D3lINlUjEi3Ps754SaUN0pvp+lnFogJnf2u8jwv+Tg0OiYqVe
npva1rIhMXg5z/N4FVJz4GdI9qt4kfiNuRoA6EI6SaA44CUylLBuUXr/ruRcYbIQ
bHbs5dmMjuKW2fRXenc29GSAc27deeISbce+Uj1Ypil7li+ZRUDj5Jn6rlKdEk0p
N03voB8maPYHziOxb2JL8q1huO8dm4HcKtUJg63WEW/4y9CRcOjlg/NyG+b00KOc
E1OUQo4ZTLW3R5W4neIVSTFdffO4LB+H+PkXcXjfmsq60NvJpptmG2l/q+KkztxT
M4tgW5p8aXJaCHR5by17/Kkba6oNOaGzezvlQ4xEDsIQqTPWtD8pDBmtbOFJa2sE
f6JyUkHdvC467rATiCJxotxUQXv39A93zB0/eXtlod4HSUZS3aT1Yp69gd6xPOkD
rPENZczNzU+te7eiwYqAzX1cWT7yqWUrUgpNLJIQ+h59wczJcWEa13DcFskA6zMd
HZsxdnsHfOXWrml+SWFenqFEsccsA23R0VGxHLY0Ni5eHdHiafg2W9xlBU1cTAQ2
xdxgzAdCIvBKh4rG6PcPjFbGcYbU63zhHm+GgBnP3UGTYdj+73uLIgUuIm3ZTzCw
Toh2/nHFAszyVuIZC6H9bYFRMKXWPjC15FBvkXitpIAA6Pa+GtEawpkLsED7UoL2
DujnS5OQ/9gpDloz8PizCA2EEd1XKe3A3VxguJiLIpChAT+hbW8YbBqV99uDDVrl
u1fDC9jott3P8IH2e3eQNqvPKg3H+8nfRz8K/MDj314m230OQNarQQlMq9lI84gi
STv+Kqo4dT/lIF69KJOq34gP6IQR6Y7GIzQxjCNhCb5LdsWkaITnwNBYmtpEQ5g4
oQ2yN3A+DDdoCKa48brhHWDBSZ7JvbfRhtCFhBvUgMone9TYg5XyuBB8QDsFI0ff
fCbcVC9/7+PjA5x+wCCoyVuMEfaCiujCAJbzU38lSSEUcrLqN8QoZDNM7Bm3qw6M
CP3nPLHjlCnxH9UHb4+hXCJPEaArYR6pothemrZnwWH/VG51JNya3zxUgZ26wkTd
wvJjebRwd+vMNAR1d7XgitQuRlQUOkL7InnKjextXeLHxrO9B+h6Q7TJ3Es9jJdw
UZRvSsDWNVZx1jfcr4FDFo9gqzpzSvMBSB5446mOccRNLIBcB9pFGIQw0HaDsPCj
Us8dO/PEIhabPJ+8G70Zs28hYRKnrUZ5NZw+R+9QF9JkZxqijPtwLd/+0KwWOdUt
OfwcjhSLRMagrYb+Wckz8P2zlNOATMZ7bUEp9Fako1iVqPGf3nucqskhzBLda1sV
yQfx5l/vbj795mfW838PSMRLqthiolMBdRIhuCdfNy0RlQXC8t3Y2cFIQJ9tN9yz
xCB5aHkZIOf7BNO+uXYw9vlbpkgGUKmGciQQqzXNoNVhNztx+SsLjhOaro8lPm3b
4c4pVsElGofRQhbOLeZgZg8JQJvxurVBqCYZ05gG3S6mySG1YR9Dd0+roLsemZeP
3LTEwilE+LUReaw5XWmdCFOjKMJa0OkCJQUpsyYv7VIj9xHb2opd1NhZB1pXiPD1
ms4ANCQxTdUqEozYl2UJup7/IP0RWkxCHm/jnqsAb7O9EYsq2jQAOrf1LfsUUQxR
64rf+Smbvo9oksovkxPxQCPgSo1RbsMfpQaS8gQSKiQrZrnqlvrp2F1F3fk7K/9W
MdZHN6DKf8DHRwbMM1q8aTfawfzd+K73DAzo6vkVD1XcHKQ7EvCt0H5kDbJYz+Lm
Wndl3lqvCRde6lCJMf+QlE9IoLZIsDMQc38KwPbx+M5QklAq8z7otaouERXM607M
EqmE64/KdkwfynwubFzkbeAqspZCkYpnN7h41U/mj+5/0dUmm9rRC7pgd9Xlt/e6
5rDgqw8UpJKI25U6d7DAYdo/7CSfWKxdHCX3AkhYH+zj7Piq+cXeEEJ2EBRuJmJ5
kQ1RSBmXvt7JSs7+DLXKc6LSnmzNAmCp9boUMTPIR+9JpdYdVx6qJQn7jPZrOTWT
sN7RJHX02J+I21c9spDQwMHF2ZEo28MYsw1SEnk+LhajCYqtdShiPDttPFHdjTlT
AcRKu4N45xAfE/Df4YK/QGi/soTTj3paA9ixRCwAScUt09AzT7YBf7DlhO8mtKgK
BLeV8tGqLQehTct8mtERyHdGTDVsnIpPjfStqFpVlufidNskuL9cJB/eCb8bapxT
Lkqx95o9Tv1WZ31Z9sq71ZvP7F9IRRqCGNAv4KSlgzT5RlN2UEpRVqESfWAPEQim
44J2uVftwkNkYwgVbpSMTH0z5v7mJxOmfBXGiKe333WOCD4HXFNvHDqshjxHUkXh
BsitPSd0sRvAJahJ4OnDk6k+mwEd7YikvQgnwQ+1PvRmzHvH4fnX9sbKDBVwp4Ml
NNtvXTfxTMo0CIo0DHpNh0eVotSULETI5O3xo7Tr6EjAg6Y8kV8YDDm4wnAz8SgZ
YfMvqLIas64iYoU79zbqV99C8IUY/fB9o+bM/wTEI31W5IuVxMDWfjIIB3/5OqIn
vMhP3HtKk92Vtu5MBefPuscQl9VLA0Huy1Q7MdXu+iPvIrDML/d4b3sAmeFnhvkW
hO49rOy9+CByJa9HrgvG76QTVXI/1JEIzaCRCmarZ5YTsgfLfOB0pId+5vNCwaRw
sryUoQwWldGzE48J3wX0Ww2OY28azo4u3RhAmjJa1RvAGOyrccsohjO24Nv4/5xR
SFO2HsqbpmTrvixCYb56vgch7Qb92UPhjosHKG9nAIxi9VEN/EgBRxILU2LOWwtp
A0xHVsbtQRQ6WHUwkYDhW6paor2OaN7Irm9DyZgoiuGz/2QuA2JNG8packV/XsiJ
HCdYoSFbdMkkt9mxlFlRThcMxISDIzJTXU8nX3Qr3hxDMRfJh5/7jd6lzSAQha+O
TNjvNb7rYnS3HrbNAZgCGs2wljgxPlmPr9pUKNak2nKjdvSdonOxIWMeDfjvHP8d
LU5ALs7KUgi4IX13LwSiog4T8DVVin/Fj06l87L8kc9FvfGTHhy7L5glZQq28sy+
ON0SzUSVsQYEVdjewqPSHa5WVy1GzMNP5n8bnCVXmJeYd6xFNt7DLTk1QneClSUm
/6hpWma3EcxB5wFeJb281CXe7PqSRz2UlK2W1PrZpTxG8udZ7J/1JRFdzItHAP1d
UOxgDRdggUYwk49Hu4WBs3Tmx6JmLAd4dEAQWKUlIPnYe5tKRDcjrjpevd+nSYEe
v/E2ztNg8nlOL10DIcZIacVyWqavp652/7LRJpDHjiyfvlrlq9e9QO1gXXUlckBj
mU3HT9slLmYwa56DUopHmSF3eZkwumj17U1q7x99mo31XewGmPOQeD2+oU24Yv3z
xqlGCA5CLNaNvbrPesttwn0DkhvxxoM3p0lV9Vurl6wWfrDP11egIOcz8KlW7d4B
OBtfMGBWBLHXzuIbSP/oAtEc3QLI0V0F7dcawYMM2oLPwR55PzTn8H+G6GpAwiGL
WY5tj+a0m3eBaslEVtDmargthQn2ESGzqkOEP40wjTygLkaIWACKwa5q0z5O94nW
zOwHn434ok+BRvs8ijI21S89vaSEBlx5lcUwABSF2UqmKB11J6usD/LelVIjJGwD
n66hzGTkfsBM/qU6IrFUJvmAwet7inOvWp++9VqAr8rM3lWtHhsNDZi19mywxDi9
j8jGqJKVTViVYhX3FzZfZxjJweda9GAjj3lN4gnZgO1JN5ceidhZQ/ks4Ai4CB42
F7Ir5rjtVar16vlAHXpMh1LY5GJRVGHs9jJlehoXRisvBsGWd4+vgB0g8D/sSV6e
YSOxUJnKzy574m4f0t8AQikhi1CSJsnf/XEIujHHyUwuf3bJlZ2F7TriLYFqU8JZ
7BXrzxonNl0sk3ortvMssIzVu8HmTxTBTrH04lgvR/63PrQP5PoDqFNhFDhoF0sB
BXj0N3P2Abx91q5T2jHstbsA/7N57t4xSeFTLKZdjuDggLQT4OriwC7K0rEVjYcp
lzMLM8y/ipfI/nrA8hd98okgehpTIpE4U1AdSI2u5G9wLA0oMMUMnw+o7NS2GWSD
Mp2Gv1IF5E4V3nPZuProG5WdVocSA3NrZ0Rzb0x3EmL4g8OJtQIeJYeBrZ9Oxtht
Sw49OTQDrIECiaS5sQSuMDO+qnn45JpOQfuD0Ms8jrslIB+DYqWk0C48uA0TxWUC
/yih5o8t1rORydLaFZuCdHGx1yNHhcW7evGdEYkFWt+QcgXTadpAxvxZqxZ/r3hW
Vt/kzj718C5F5WVW9Wladcm4Ky6Db6c6VaUupt2CrJD1lNeaf0Y1Bn6yqq4X22+R
9cfRQ2Chi26AiNZY6TNlWw==
`pragma protect end_protected
