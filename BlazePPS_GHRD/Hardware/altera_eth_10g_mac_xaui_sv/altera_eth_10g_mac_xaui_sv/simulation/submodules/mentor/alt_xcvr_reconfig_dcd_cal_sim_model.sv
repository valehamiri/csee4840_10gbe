// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:51:54 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
LVQIjOVYgS9fpBDOn+RmlnRia/rWjvfsBgtzYcbEXf+Wr5mkqcKT99V7zw3Km1c2
laqrVhkq3GxySYwsvmMEVoqiRjhbPqiwUGexNATi34e7ZbOtLpPFBdbD/S6WvBwv
LOOJgx5s2G/bgitktSPMz7KO79+dm0H9fX9zFyw0ZlE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2976)
+Ax9YT6YZWwXlu6uA4kBQ2utQHnusB94/yxwJLd0YjZhPuxT7La1XLwsu6aZcNwX
0gWkCVhZEbrVcwpjMh/VS5ATcSccABsHcmT2xs78XP8HlpfyXQixsh+UmmA8gEV7
s1oiTTBNFfkRPgZlMUSCT5vjBpI7RkRNRC8jeBCvIq9khvT+n+qXCoPV5MQvexOE
URrlWmUqQybbo3x+ZQC6/QWBzEaBwRzoGBNTo3GIzkhp4EGKxxDrY/f97eWdXjeN
Wy+7PkpAHSVoWNDAqw/00UOjI7a2JCT4xndvDVcHU+eCzp5j4pKmHMu+VxakAvmp
GikjyCvpYpuDJKOxQ5cihV+R7HzSPHkIF/pDgEhkZCWdBs4hC5VpAoM3ccqmHx98
/tY3yi4KvI9xU5H/gezuKO+4wvDRPoFFHp0eXixXLpfdeQU782aoZGjh2g+m2m/b
4U2nymgBncirVAJ1AOodvPTYhAST25p7YZRc/vztPjeAdShOhogWpI1dvOOBZSxs
Nc5EP7MoaT/U3DDqKLTB685fyoY18AEKTE8i25m8V0CzvLGXi+WfJ5KMXqeNR0Mr
o0ZjPglCQAqE/ichm0sOLRI8PnYecTHVURcKOgS/SRHkjO9fZFunoZ/RgDpPfSpe
3vQthD4BLnq48YEw9caPH7WBP/A4yTVoBdon+q3fe4VUBHpZVQxJZz0CHUFlaMVK
D/5KjNCwcvbr8X6BEPNlJQbh3RHs0pk+An4OX9BSnuftot8cbKbpIDJm1B5o2JmN
I52zQ+fNcRPgIem1Ya5qZxycbQEVzz/cs/wEU21/nNdFbMcWeF8o4Tp5iy5ltPPu
zN947IVzkm15l9s1liJDSIPNWfPNlsDWm3bvoEZWUEWvIjgJk9b+kYv4ruiE2kxZ
aSw/tAwjaY4iIImqps6vPQ74rwDN3xPI5K4fMKLbJZevtM/dgLOQff6URDvzMEWb
uwFDw+WqAr3vLX5XesRhxN6iLIA3Q0qcn6Wlgb8iB0q9bEVEooCqPsFagtMdWPUF
qsTB4Y5vAvHDIZSFcLTiGYa5Z3ZfF3S0H6r/Cfpr3/+jd9Ltsj9ENrVHNbdVS/fY
PxvqGqXM+ul+jlHjc9wIDPAmbueQD8iil6GdGQUFeQfL717BFEI0usbeVS6ljx0G
iIkeJdWMhJGFq0pR7B7dAq8YygCfupMnONMkueP2i7p5xu3/2otANsoGnhGhSnjf
6wbEI6lk+qBpJbMAXN+nOODd/wOU2ZLVZS6cewtmWnrgwRIRf1SXQe2+vkltYXh4
RHU6brCIbTWePl/u9TH8vOmROwnzRr6bfcJCxD6f3VJ1L9xDA/5X1t4j9BxsAw90
bfG422rhyk2ne0dpfBmcs9kfX+LxdhXaNMlo++tWfGIxTwRf78rF5Awh6sFAF9PW
DTGaGBV97h95tMc+P/g+UxsmqvgZl+NRkAqqo8rd/IodMHDH/e3Eoe33cTpYWACy
9qYOhlbGe88zCEO8irf2+oJ0ykBlYIr1oGVTGfl/4Q/BlsdOAdX4Not4yuX4AVNe
XchGrslFkICWX+zhe29IwByFzftU7mgsd20nOxx+mzFw/EbBZpjzMUIANRG8gNLb
1n6+EzgfP/O1bys3EUapH6qZnJra7mVU/RgEB7dxEeRcGQXl9c9xTfgzjWvif6i2
80qkXV8UA/sfAx4ri7nzGUS5BgZSLnalU4kgEvoyqdAIVU0yaVAwpAMNkntATG8D
/O0MqA9nY8h5Wifn5fiUaYDVNxqwcE8z76ucVW23N2ToIiZ5YQzpNE2UJ+sDL32C
5Ijm92x2UPa7NOK2fW0IaX+GTfzvH3ZJvGcZFFbNhtJcHEmGlMFn0p27KWe5wQMY
TSjjV8ftvQlKQtasyQnHR3E7OkdMjCZsZ/7y4OFuIir9raMZYhgELfE8XjLmKO7P
Exyw1BTuAwfxYNSOJTbAUT+RJjj7QZrYsh/E8JmcUHt48PEdmG5twB/CCxD8A4QR
iorfvhxqZPvd8YqwvzFsTRIrCXqYwt/B441NHZ047BFnkiDUvadf4/aIpljIhFeT
3Z4VitLRukRmSbCCPle7ps58IKc8kT6u5JYE+tvM9hZn7AU3zauHbIcMVlS9AFOC
GZggFGnVVPnukpCtUEyrzX7PZmOo2g63LLsf0ctGeTxfMTf/jyxcE2fk6CNfXH4R
F74kELLPyGfUYGH6tA2qeNW8RwUTtPryC5ZifRxUkpnCPGG+TmnZ9ZcK7QrtmsZE
+w/pg9SRANqjmy2/9K7ahaC3iF4XMv6yQPRit6NvNaWzrAJwVxIlhrtDYQlAo0Rw
wowI2fm1MtOySXgCK/qOj9XWKshDYF8tNXAWDb0WJBMCGViEh5YQ0DtN1TOTluU2
n37YfXXIX1yddoNk/PwmgBXKBEnt89qykNJSOrkss4p9GohA2Fz6FtYIDfXyc+i+
JmqI57D8lExMYKlmNs1gxwbbbsWA3BwR/xk9j279Y+xZpY7tkysAwcgbpZvoO5zn
PxeqWfWycqpQMjgOlRnUUrWoFZQXFD/VJAhqskXrmLjGAVGPEGuZQ6Dd47738rVX
mZMHfnhb0+nIHKkE7HGyL5RQsGY4Elw6cKGOSmDyv7i0tGfDknL5NMwyUH3YoWOd
hFcIlmkYT0QQLr1yGbXQWMz+CPNNKctd1WxGT5gnjE4f7UO1kpGBJLnAcCOlpx+h
CRtyKix8r8QsKcUgCU8eJgDgyUyCJTzdfH8APOzcjX7fqrRl6CeSJT4laWuye5jJ
3l0wZtx+/hSg5L7AFB9Fkt7YydLnGMN4kX4UhAmgxG0nwoTTdTILv3ZLr+tdiOY3
o/z1yujViOVkNis3BNHjo//GZPLeHr9ZqHe0IziAHNIcuPUWaSZsFx1keBHTsoKW
evlf296+shKVTh+iOpNkEAsdgV6EmjNP0GYH3hiCn0rSVeAc/BSbW5xB3p9mc2Xs
SkxIfs7O9g1YvwP6Ej7RZcvTfcqUc9M8mq77MWv/LSQV1ngp98rzq+JqiwbO7jFE
+pWlVRmg2UfXx3P8JufB/jXGnQfdXSGJoZS0QI+j/xd2oJfrP89brazX9Fk3AjKC
A1chAJt7gZMkHVwBTVn1Qdmog+xu8bcaQudZ41I0ZNFKmSHStWLsMLEyEM5E9qgN
50LHbhnmS2b34Jeozcv+4L9kKp+dphWFp80NhAcxGILGcRSQM02fFM4Ys7jAO8Ml
uA/HOtZFHDLaFBLdwMSxrGs0PSeoivdFJz+lAb1x9g/a1rN//xE5ItMyRQxK00tw
ofXWs6dOgpx9+X0czoUpZE4RpBg1SylBM5WAX1SW8ULoDUY5XeDMNYM6qBX+L4xP
GpNsgjZb9aMfCb+wi+EnATkOnyLBB5lC0kLPPV56NzjAJVyBjAdv8SMuFrQTm4w4
0ACvdgaiCf7kUZrdoiumSDttrvOxDFxK0J+o9On6ZDkeGZgnnrQ5mcwP9zy8tvPf
grXsBdq54t6gjA6LRCSGCkbbYtq4YLzdxtzTpPXGNpgKK7OsdadFAWffV2Bd+Vmz
bCjD+xpL32Pr8OpM+hDkFto11U+9TmjhMXVkXNIiuztmPsYOMf2R88oJ4UhJGlvo
ZUqvu+O5AbxYPMopY9GTflIECQmbdthvCB2onTOl0XeP0WXCPB0YApozhri0Tomy
Xa+PiqHZJjOthcTXlixK5OXPQhwTcxLIJ/hg3uc3R6ywoM8YscsffUh2elb5/QCG
oNjE99f8ADtRJgrUyP9oZc550CRxf/+9vmq0ExVO02Dun9SgG4knRruvh23Bl0mH
ePFn2Qx9+0H5WUyrC4emqQJmpoWhp/3IdEiiFSY+tuNXTV6Ds/P1V3fPjeNlPYO9
mMcE/TbsRU8NzNywGyZQ2o68OVOvsPstlOQ/UL2X1FWbvOi9bv0Mu7e/hdmIHkRo
nd3r0AAeBzFuym2Q+05LZg3dxJ8oWWJXXsV7Ol7qHG4nv+iL43QdFN/NUFXdTdTu
`pragma protect end_protected
