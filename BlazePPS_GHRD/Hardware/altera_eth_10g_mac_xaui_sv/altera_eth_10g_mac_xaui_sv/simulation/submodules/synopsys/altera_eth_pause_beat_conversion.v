// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H<NQI:YA!*"G0+N4/>D?>S6N]K]KDYYWT(R\TZTW0>?-]Q.''60=2[0  
HL72WLCY2:3:R!33U\B0FMWLR5T^0830N&:,J3.^IO[,#%E?<=X5LM   
HB_ER0]A?YS%-URX#NM)/;-*44TK'$VB;E7I?5!O%;"J!>,[[:.2%-   
H*\2)\R[OUKWU1MVN,Z#FFR9D8ZJ-H6'5F@(%.UJLW1S]6M8F38NEIP  
H('*;1.^73@=4#2U,+=E__[HIU: R Q]N4'SO)U1,@ ;5]];XM<<MB   
`pragma protect encoding=(enctype="uuencode",bytes=6256        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@W=Z% \"5/X#3$9G'\4OJ+U;7H(=;+O.D1N%^U*:6VFD 
@5L__\)=MAF!=.3C5:9=@QIJ'\PP:% D[O$SE[^A(<7@ 
@) Y4PX@+_G_!_X3C1(4"76V>G!ZVX.O_3+_51YX+R:< 
@%$88\[NLD_],_/ZKOB+CY],Z!"%M$X7%$=H*9$IOL+( 
@ZN0IR)G/ 20Y6-18M>.DG8@8@O*W-RFY"PD7JQM7L'< 
@W 7@UC#N>06D6KQ\CHRL''9^2&"^D,=-IG[\O6/"%'D 
@_PMTZV>V2B_5[H?/,DCS0W&;+F^M3^&Q'>Q#-D>K6*H 
@5NO=N _FD3A[P &+:@%2A0Z!ISUDLNBA@FR8-LFW0^H 
@[R%&?$+,_=#3"I8CO\2S%-.]:&O: (17_L"$4-J%?*D 
@ZX%(2"O^.ZH! QCC>!S_FV 94SS\F=/F2]BW[6(7KY  
@O,.:*+LCUJF?7OX'O<DK2<I0?M:P@6*(.6GQ$O?S["  
@G](_3/^C^U2L$DO!)P/8!2=,EZ(!&Q<EMP=?:XMV88$ 
@\-(_@NQAD68VL1X%QO QJ&<QZC=:']C+UT4#MZF&79  
@T^,':Q7DLR6N^WQM@VQFH,G_WY41][0MP;#W57#19.$ 
@ XFULD.F63/--=IZLT_8<Y$V[+3H=28<4G,GG;%*FI( 
@:K!]"]@<[:_5^!?G/3X[&2( $_N?#N;YNF^^+-<0)=, 
@_XPI&,LP#VRT('M:3#C$O47W3HFI@'F8UE"5HGRU_<, 
@"5*M7B?6J3_04MWMBD&7Z6].G97*P7W?Y%2K;TEJ-LP 
@263\V\X4,]NT?$:W7+&5>DI'?Q^\B<V9&DC\NC4DV\P 
@RYHVQ3#2GF/8QA6L.(&="8QG\0U8WO5V+@XPF^-Z+68 
@YWWO;>ILH14.U7WWC2MC)CB2T/O<#+'B[W7S3A.$%)  
@YW\7>"\_4:)A,&792'O@&*QMH-AJB!0<ZQUA8>=):>8 
@.LR ,"972"9O$O-GFJ4,U1:>F0Y"D^7:>1O$'^%:58( 
@# /CCA?I_6Q1&K0?)<$F@'-K60YK9)KZ[$8?YC%;P=H 
@;SN_Z/D;<90,\XC'1!7] 3:8OKHN+\7[O&)I7CY<6<, 
@A\]6$TWF'8G!.76S57#;)>**@R0\:^'"GE-]M]13K[X 
@;681O0ND7%9-Q#%97]S9&R@9ITPDJ<4YY0?F'>VGCE4 
@OAOI=!$38^<\0!:ZQD1<U<35_1&*UP^3F'I%XXT/$84 
@8;/SAD?'QX2%K(Q\D&U0 U%0,WLW*1M>TME11Y_X.;< 
@)]B#+BZ70>TP^U7?W*(V8DIXN%F&'#O5TPZVY8U;G=4 
@5:] +CRMLLXQEM2 Q&OY&B%C6X3-)4HK'!M>. ^M/F0 
@( 5U8= VO5/PFT0> Z'0DJX)OJ@[_?U6N*KOD],<INX 
@E14_0&CPJDK!<73T#D.>-M'FV5(_3X7S&@D72*&I89, 
@=+1@!10_ F+</CIR_MW\ED)L.9:X_@YW=8[X"RA?@;H 
@ >ZP5<8NP)C=6-%36 %OWR6J %=^$'"/RN"Q19=(C"8 
@2/IA6'*K]',W+0)%O4"]^5;9:R.9;-!+<U7()DG[(#@ 
@2/Z0BH5' Q%%KP,M_)\Z6>#R\E)\FOIZ<FBT2F,5?(, 
@-^EO1GW);^V. 46%&A 1&#?S/P$%ZO13*EAH;NL$/-0 
@:):KS,JA^TS(CA3!YLQRWIQ[J/"'*8T0PI['^*P!IVD 
@5@-1H=]B9O*CM.F6^[4)DH2^,#-X4]\!(/-<R_8UC'X 
@$_("[)I-:JS6J8=,\C%F2RF^^^*2@)/D?(4C[X^T_6, 
@ :W2T;%\%@K:&_%S9$_8,2VI5 X;7JEA!Y?<'!NCC?, 
@D;,2,204 RBLS.\ /A?/GMF&QL17K(''9Y+3,-Z.K\$ 
@J_Q*,A.E'*WC=56R'  8(XTV, WH*7$S"J?G'X8[/]8 
@]7LYZQLJ&K\M=M4"0"]DU$6K%HNV>SED[KX(9!I UPD 
@CPGF$>"2+4#)VT"E>.A$1 <H?LFCNYME2'J<FL6@M>$ 
@7-9:C\O@/>"=7; + T/VIQWM3LXFXCOGN?#X"1B!G L 
@/<+9W1;S2%O6(05WNK/K_[J+7&BI9UM<C^E\4H6 P>( 
@2\O73U3YCBC=7]$=-W/!\%E;?4IHD]6D((=9)-H9<]0 
@1+FZ=(%#)MKX?UC-1OJ0 9$8$KD[>R#MDLZ*^7_/6F@ 
@\;F8$?\<ZA&>25%@[Z'2ZBWZ H%9#WVB>_58TC1&Y P 
@!<_J?;K2,''SZ9,UFDAI??R28+N=-^AC)@)$]O_O+", 
@0CYL4T=BUTS&QVTTH!\:%4;1S..NR3M;[_ZQFZ\MOU, 
@6XWM Q\0*9E'#"9T..Q0+LU1WG-U= +ZB%-]+H/A[4P 
@:4>P3O[8,J!)Q&S#N4$>[V6VV@;<# %HAS=Y2&@IH4  
@L,TJP6^FT!W::%-MF3+-^"6';;C @'-B2]*18%!JP$X 
@B,^8MNU]S*7E4ISI:&M\<\U1_I#F+^YL@.BV#)H0RGL 
@V@B(BXFQ%/C6;HG,GR>P[; F3!N\X"Z%T@].I 1^15, 
@NP8XGJ[- 3BK%.GM\$5 [/E,1I<XM60B*[8^!+KI@O$ 
@F;H1-\-,<S[T1 49&U=WW>'*STW3"]8=6N72G%F1S%, 
@E^UW8R]]:<R?VZZ?)><37ML$U2' Z$8\/5Z0%;<XNYD 
@,[TN><P16',#W7HW:%:*9+N/O9B6"-)6QB_WN#Y:::H 
@XVK->G*MIG* 6$M#77'#1?64G@K((P(!1QU!17EC%8H 
@H$4<VXY(GW'\F&T9-D<LOSDB!"[NPQO"2=TG+XAUO;, 
@]-J^./)5)J(&A:(]IY[!;$0^YTZ3;E^*Z:_Z/AIS-@X 
@%*1 Y;B$O$+ZUK^ROCV\IZVE/6Z3XIE<L@IF-0()8P0 
@>=.LE7' ^@274CIF)LR;3C>>$+/8'N88[, !4?/Q/1P 
@YM-#YY"&DR%9.B0J8-6AUD#HXPM Z8)&Z8QUD\!MR#X 
@X++?(]*2O47*F#-@ %3@2\P=#E&$2)K)]VR>N:OZ0$D 
@(!>/*(SF^V6IA6ZO$3\/TM87R#P6205_GB E:/"9E,( 
@-_UH>3N45(9-/0R;%V,A'BW K@;B*ROD5VXY!\:0D;8 
@I8OD/&2Q6[^!^.T0M;V0L;5]P[ <XP\+77^\3OS2:,0 
@N6(<$6ZO5)  X+HZOF:.S/;\1O+ZOL(71F+KL!TH>HH 
@F3-+=L4%[Q%-5+VI0GE $,U& L;&Y#1E1U-S4*LEB!H 
@TBY8)9X?A2%[![V[3,="<Z\KLVM/FE1&;G$[$=QH@EH 
@6,Q;$=:YD#,YVN2&E!W\E0+BD^O?.Y?30 =7>M:'L30 
@^;+JO - 4Y3AU:G5:& !D9J3^/4:\:2KI7&&5>YG4NH 
@=]1TVD7'%2#$1J6QE=7+[8=%L5IE):;ZG;HB*?0BO9H 
@$.7CN\W^8:ZZ4 N'GANQTN#/0+3Q75DUV+.19V_R>?@ 
@ \95MQ!:6\>K&XWMGQ5J/M);+%4F0IC-66MKO5(\,=$ 
@>Z.(S$CPOWDM;N2FQ8(TTMF+X9Z;;=D2]55I;&%61FL 
@WX6P1QC'P*% LID(8*ZH(3VIF=6JJ<]%V0,3H*V>AS< 
@E9<N[ LB+\R.33E@ +1U_#R8%.K;TM+8=FEW?WD03V( 
@]A[U;X++A@ ]S9$6Q4!IS,B4)91B\)&& &H,C![*=JT 
@"!:N0[6=NG)H %J(%;6?P(L0I>'S&EC);3%?<\X-J!( 
@GS#VZ;.#WHERT=U\=9[#5AJ0\8<"M/'">MZ4+T)S&@@ 
@V14,Z1N6B\]!JS?3D^&#HS^:0:)>UQU,1YBX3*0Y'_@ 
@HN^^J\2K&Z,<V>B$OF=TD8"R'EC8)N?UQ/X4E BF2B( 
@G4XXX<]Q.:>;G,"ME+*<Q.97J&N!)(^]_N0$/;)T5(8 
@3]/E("O_:TX4%,I!]($4XO;*Q(Q"JG3L<YXT+BR/])H 
@O&:[;&(+17@3#=%/O<9-$\8-I<)VDV6MKKEB#Y2QOD@ 
@<Q<L"MR4IH4;&R$!4ARR"I/UQ@$H=78IT3\'0= X@)( 
@+C^.I.O9EL56Y@)TX 2(RSM-:0'(1TT66Y4!(UM+A+, 
@CY.<'#I;'9<V"B6BH6YF:I]*_9+A[\<+7.R+H,^,Y<, 
@IO$SOI'+1:!YE] $/B;$W"3!;LG.QZ/6O@'3@+V$J4L 
@TAA3DZ0!+;J8(1"%1^C9SUL&L.)JI1:BR*ZQH]8<4_( 
@SAS^;!*4)._5TF.)@&5>2D:.%!;'"GR-*_#1X92Q?E\ 
@9:I'+5"SP0_5K[YZ![7.&5"V %DCHOS[90IYL'02@C\ 
@:LY,=H7PINE&1=V6Q/OM27;WM"NHJ)(JKR+0TM4>'=D 
@/F-E5V" "^&+UK.,JICV*105:!K<C@@J:^F"J'S8ZHP 
@*Q;77/YYK^SG8## FM 'J#%-NJFZZC*)^HE;^/VW@:< 
@D:TZ9_1J2>'&W2[*ZVN>@D?4'J;"^+AFXE9J&]4"%08 
@0/-W-O0.9X$2GY5_<3B([Z4%>'R:OLZP:LD&),^5^=P 
@HZB5%9+F+S?;3R4&;Z['W@(/JK3ROD Z]Q:PT#B<;E\ 
@P"@?."FP.T6)ZE>;'S?96LE\WN796_J_9AKBK&H#&M< 
@B3*$JEG])(+5T2V8XOOT N$*+F24;WW.*.%IPF@$*%H 
@8,\BK._<Q"0=,0-:PF?>=SX:YGW."5$F*46LYK0!'8T 
@UD[,Y"KM<J7*#^<>\B@ -5:A<G/06<"1$O? 9/-G3@X 
@Z7&N6(FA0 8,  Q15C^YT,%9;^GM(["P(XPX@OPM=,0 
@2NW@FGRK9&C;VFX.ZH&R'4]B1;QD3)L&CYNM^!H0Z(X 
@GRE;"P5XFLK&)PO42C@G2W$Q29HQ/*](UQ*:0*6T!%  
@FC84J=+GPOP,?91"NBM;ZB#QQZ)B89%S49DFP=:#KEP 
@Q8.<^S&Z%;OC#+8.4PU_DDM!SI,'R2NY);S->NS/4Y@ 
@J+ID #_%]W_&?GQ 96O@\J,\QW'HK9AC=3-#X!>+5K$ 
@65TT%W,:@7FF #>$U6GI@1HN#+;NN_^@WK?$/Q5+M?\ 
@YB@>FJA4DTFIYHH%@I[%OC:.(%WEP!7QQ5 )N.PN]^0 
@9>7UN -7FE02$6%D>;")A"TZ"=DO[S8&4'L-]",GNWT 
@PH'NY'P3E'$WXG;RF*X^PMTQ&F9F[D).1B-A+GYQ['\ 
@8R;$ 4NP*SIKP;"6#B3\O5I<+#LH+0Q@!1X'D:S$5P< 
@OP8O?_-$[_EV'1?;3FX/042O@5AZ@@J&W'J]*K-U&E0 
@N[&UZRD39ZN @(X(VJZS9P,?S27@!@/_GX]J[Y?%V:8 
@@#CDIE[L-*CHPA@U= VJ-'/H3( Y@PO+&E=G>XA#K[< 
@S"PTTHV+2"TBK?"&X&2GY95.;\++VD6.A1:0Y(,S8W4 
@^#'-OW=GU=C74$SP="/K%\O,I16ER-XE#$\-L(+DN(P 
@,,&74@<78$_!9]/E-!+7 ^68F2RG%BBY:6+NMG57R90 
@'_Z=M(C\A,B(XI;'?;>3$DHNZMLWY4G\C-*W4,X9A$L 
@SV%3+A;'/N+SE'N=/5>J9HQ0 I8IL9#B;/H0,00ZY&T 
@9 8,>=6P,Q2X?%(X>0 +:>):P))<L$("^TNOJDB3*R8 
@>2.0D_;VS1@)').+9G<-]:5#%7T?5VMG0S\D(< :KMD 
@A*&ZM\0-+#7Y.]V8T6KZLX^1:[)"."B]M-.\\P"-<B, 
@*0C^0]\5-R_1WA!V9KV3%:2_:U<J5M]+V&L=7J#XB6X 
@<<AT' .<\1\@6(K'4AIU>O3:E_='&^]^&AM+7$NC"8X 
@IKD&-TC7@0.9S*#N\6:&W8WTK"/?9':;(X"!'F%N[A< 
@VS(W9ZM-A/B7@0]UMC[39 >0)!6Q')UMBMGX'@8^#:L 
@(=>&O1'IVQ=N008=6"70Q&DF:S/RKY$[8WDZ6*"R"4X 
@6763LE=\W#/>?3^3QNZ_*0@!X6N+9V<NRKKPTN_Y1AX 
@+S8%'@V-MZ;H/*L.NB?.VV/#/ZDEZ:1<4,@\3L5DE/@ 
@'S5P FH\]XU^:C*30E93"35=1)0::/:_%6!QPP^XW=8 
@XZRJ*_2$[I^DSOX'PC9&7813396JGV3W%(=-3%;Z010 
@J*F-JGM]VT6H&A"'VK:*M(-=YA5OBFBCEF1FPUVV?7\ 
@^]L1"XK^\Q'Y&!="$>D'A<GZV[/@1DHP1+*!9C^.@W< 
@*:DLBH$5TP3+J-LN&2*+Q^)QCQRA%L)*F^B&T!-=,5T 
@ED,'.=^S<_<%QD3WLURX "ZC%*.^_E.>R@7DG$8A3$  
@">: I-' ?FF:+ZD.EUV#UH KJ /5%^CZH]*TY23VK&  
@&ZF/&#+X<E40\LV6%#^-N0,)0@&#8=&_"Y>>L^%?A:4 
@/XXK'1IGA@AL4O ?2U2:M=U"KXKEW@E:Z\-Z(27#0WH 
@'7/ <-6<B;YTGCZ&&7>F3TX]6N=XLN-EC40NX@]*HC8 
@?\F=V 7TS[RFL?.S[Q]WH%W<%J(E:1L?]&_2Q3[_=]0 
@V+9.1OUDT\+-&)+BF$:LON#9G>)YRJI=]-W/^QC7OR8 
@Q7SR^TM=.<3 IQ9A-O2R*M0=.Q;J]SE]_B4YERL%?M( 
@6081O/\%$<&+%9,#+'IP[77>Q^C;6+(:-<)*M%,%K!X 
@W\%0"P&[*OY7+NO;N*#SPL(5&_/0=(H;-\@B6&'RIWH 
@/@/JREWF@I=:\'PS#:>M;57%B[<&5R%T($*9IE>YA7H 
@H0K@?K(;X75H@.=FL3RB5%[R9N==UGF;8TS6G.*GN^  
@Z@OL) K)5.W;C.VL-SB%/?+5,6-ABR>[^Z=\)WY)TYT 
@@MMG LR&L&_\QV"2YN+NZG+X#K4MJ#OT(]CH4_?K.2P 
@ )?IX0WJ0B.QPCV6NA,TFDVS *FQHOK\,6<!1$][X4X 
@_@HL^NX.EU@EI,J5$/"F&#3FBP]4[PCWY;R%EO1R@54 
@5#RXS?7@@E[F@NY4'^M4]"735&^7XHNYZ!>,'U2%>ZH 
@JHCJ^31!R%+Q'Y;B-HF57=UF!"K*%@88E9,H9(;2 8  
@U(Y.YULF*0M)"QXMKW7E40JXB(:>;,KA<EIC"=JWL_( 
@--'+P *>'8>/D_4*;]#*[U2@T[,"Q?:I:O>Q;NRN*S0 
@ F(E9,*H?-X2:H>H\*CU&V5-8UPN/!/S9.HH'XWEOCD 
@^TNX?+;A@(^]/'*1"0%.6FW]U@""1S/_:Y^R<W;(&[P 
@N"H5T7&X4F6L-2RC>68Z*&B&P-XYN>O&OC1:KLL:TV0 
@$@E!Y/TDMP,'6L5$9#W-]O,;8K]M3MRRFIPQ&XFPB#L 
@J'?T'/Y9APM+P%05V2+G6Z$1BXX_J.D6J-RU)DBJ+^, 
@KO6H ;U7'K[H;^]"Z84F6)(9>NADT>7ATZRL;DVRXPT 
@8<B7)$N/RIQ6)"FY?]/&=!%B"00<&6H84A>ZV2SHS>< 
@#D<T#X&=S:<4W>UK]HS8!A<2;$R9+;5$"C#*8(N6_<H 
@BHUM3Q*#1+Z9S/5\L" 8#?/%I=QQ#4*,<<0"PO#J &\ 
@I9QN$!<O*+#TCK9/**\OKM@=#?$2S'.HQ)DE[H+*#"L 
@L)P2!OQATJ(S$J@5M^%@%AXYK1AF/K?7YH&-UX11<]\ 
@6M-8#NB.(HE+0/2!9@^< 9M*)1,YBUZ4+20^+6D>S54 
@6Q"T&(+>B'8&U/=+8"U6K#77H>Y6.>.BK.6<6JY;:Q4 
@$FH[^P6)Z?Z7U+FM&8 IZ+E]^4ZLO2SGI7#H>! E<VX 
@9=7GMXWGX/=YW,B"1(QB\8EC<FLA&GL>TE( I3K3<'0 
@/QZ:1O6\0?P[/!%"&\DM\%./3  Q?N(7" ND(C'=)7( 
@ (+@J]4(SW7G?1=5I=>O?P\/">(OD!W-8UHF[Q!!=+( 
@WM@) L$[FF)D<OQADS4EH=9/XA-X.#;M6.W/:$C,X0X 
@1A>  .>B]9#W69D._.Z99#]H8K.6<8M=-%'MB@9'H4( 
@]YJZLD$YUNPB[S:-T$WE'XU3@4!2NV),/Q9@]&(+\8D 
@T(-\(?GJT\P#^OMX9AB8Z1Q8>O;@=^T$ )_2WD5YV<@ 
@6/%V!RAM=GFH^12S<<KC.Z%L?E!DU*]2O618R]7MBGH 
@C-/0G5@6SU QA(_9PP_1NQ'YQ_TAX',=M$)$]-<E-^D 
@:B-"=^.XOGZ=E&44B?9_<+"R5.W(VY/:\KLG-Y\>IB8 
@[^KP.":+D& Y?NN[E4&3R['(-KR2C"&FU99;2+\98T4 
@M[2(R)-@,:UE9/OVT6;C@%$ N&-$<(GU49+V[.7^R.8 
@#0S=VUS8!S?WJ[K<Q%%RP@NI%<8LW2/IM>7=([E,4L$ 
@>.)7^^@@/:-S366C3B/0'X!').ZU:3_C_S^$"<_0>_8 
@&>3Q/T[>G053U!P]=6,@C3ZT0C=M^Z-=2(O9/2!DUYT 
@J+/2G:QLC87\>$R8<W&S/;?\/;82B@X6OT='^9;Z-NP 
@852 I ^VA^@&=A\#[-G;_C/EO>SD03[B+>/!.I47 "D 
@/BW._JTZ*^?ZY=-3S312QF-H\ MO[VU'%LY6V\JN=W( 
@)-% E@;;1BTO O',T2]@%,<1<%VUDCR@XI'S6N74%T\ 
0_!G.6/7G@S,A76=Y#,I(ZP  
`pragma protect end_protected
