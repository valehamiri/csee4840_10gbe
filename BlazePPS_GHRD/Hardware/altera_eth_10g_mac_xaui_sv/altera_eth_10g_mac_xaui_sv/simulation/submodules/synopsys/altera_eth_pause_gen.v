// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H1"Q_)Q:OSTRQJU"+4D&&CKS#*E3LYOLSC^V+C+;J50C=R38XM;YW&0  
HENP3A8K0Y/]M[47O0"1DS  6I8NQZJA4K+D-J;ZBA9<'B(7HZY5(R   
H[]+@L8V5!D^BBIE&V!G6EZ! +RPUJRD_FLU*3X #W=;I+(YH4A(X<P  
H6)N.!X,R+@C]W>BT^EQ!Q(P$RFA.X AZ[>Y8O?Z8J.^L/_8LBQ/S%P  
H\"ZG 2@VS8Q"7<-?\5R85JQ%)(S\^+GL@>A YEH2<]4O*MUV7MP3'P  
`pragma protect encoding=(enctype="uuencode",bytes=4752        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@MO2M\]:.>")QT;G&AGY^B)2X:8 _QB'E'.!G_MH(<P< 
@5KR<#H2=&P]'&Y.90.DOI&:@M_X(3&;?$*Z9J-CH(E\ 
@C)_W%STDQRZ)CO:]SYA/"Y>]J2F3CUP0TG:=*__0=O4 
@M=3=C_JF%JB\:]QOGN<"UP1SM+:W:N+/R+.M33< F6  
@>$[PQQ)V!2,A$8YC.^V2!X=$GJ,6VQF6P9FCQC+_304 
@JG#TG$&YC7EX!"Q7LY)[N,@69(/+&<0KF],"@Y7UCRX 
@N$?7F?Q0EAQ+'GK^K$V Y^*^/20@X=?0](CUT8#1N?X 
@O6CJW@]=,;A6EV'5L<*AH/EA/U,#10)ZO@ALQKZ/2/L 
@-+B\E:)M=#.M[=1X#Q>U( +R_R!!+XJT3O;13IZ5Q68 
@N)']K65G^@DU(%O@#!W#I_Y_(5) >/$$RE<8J2ZD1F, 
@"Y=!V^*!4$I-7@?4^RR+GI;Q/^4><%ZOMV&$\:4H9.X 
@ZI;3QDGG"9/VK4K9.P13GYQW(62RB;ML&A![#Q0D$O$ 
@PZ[*4RF>7F$S5G$2$);%=4F)?,:@)&E3W?TP<F&SO=X 
@@.?KH&FSFINT E@[1:8#X1FSN>>+T8ZYY9SP/K4(BTL 
@1A;"ZFVHK2+/[1LD=2T"-VN=12*"Y,1ULOE!7+:DE_D 
@.]EX$3M"(]Z8&/]0(9QK':BM=;S3V30P=<\^T:FC9_, 
@ZF5M)5N B33U\!SV+AB&M5E00G+3[F]>R12Q"N$]( ( 
@\QBWHVA,"NT8W9E6D9P_"AK$VUT]"MU5&>[CV1?V*J, 
@:M+EVZ4R[LHL$455#(Y:AT;/B.TLM6,?;3L&GQKOG[H 
@+ZX2B.NMT\8RR=;A/G&GU>>[@HO3&!?(_/(>[+F]'", 
@&-:7^@LQOMT1 U+0"^4%:JA*Q)CN[^.5D6C@NX[DD'0 
@R-SG__XYBMWU0493:SG1"75YAEI/+6C43H6(6'*\EXD 
@EXCP?C-7E!GZ\@&_7;'<..4\4P#*AY_LK*_)BT_@+ZX 
@12S!0_.HFT*5<?AP_R76BX,1+=-6J*Z*2T)FN_=QQH$ 
@54 ^*A'*'N.A4%NEH P:+-5T1M0(V\3I[W^]I)P[GHD 
@R*14Q/2Z\%&6B=UP^75@\ZN1ZYQ_[RQ()E/:RT&_6_8 
@!T3?M>=^=23/1YY^?0@+S*(%*:KZI9T;,ZZ-&DPP/#L 
@42>!>W%<.R6KR?\JG:"-'AA0<?Q054,>=24E]()O)XX 
@NB+131^.J6Q](R?5)!4-K+;LFJ0+DFJ.Y%M-LMDM.?$ 
@8YCA='AL+0;#IW_1:N;GC(VFE&X$\[@J& [@:4DB1IP 
@5WF)&+TRV+9F;2N):X_T>[RRU.'45WO)E.D5H.UO*S8 
@\70KY$\S?_.+W%!01*P)K3$2-.I>K'U*)YL[7_HT.O$ 
@Q"6NS&7),OXG+]9[$R^=4;\\C*V--[$:6L.>RT;M+.P 
@BHVKV=DFBX5PGS=+2$NL^-K%ZJ!6NK+IR9OXJT3;F$H 
@[,AUR9Q_&Z#WE%LMTLB6$:?/]C[3CB)$8P2*LH#8TU  
@5]';BXKZM?<HKW>3Q1VV':( *LOIXP]]2T!P+Q1SOX$ 
@ <%P"RNY7G>>V1HC8+0&2?9E$HARYM&HDCZ] G)=@!8 
@"PT,$ 3'V!*.R ]@X0.5MXO/MA9NK/OT95%=P/!94;4 
@\(OXCFGIH[QJRNL(E@ND]>'CPTGBO38UO(51;P=D!W  
@ Z$$YG1:)5$<5X8_S:D')/Q?(<R$M"T2CYQ:,1_.DL0 
@<Z7X*E'Q',W2(0.1PWC'$L'QT&&9*8MA3O_$L(RXAEH 
@N"N2+9C,CTV[QQSU)/44H8NM+PE9>RV45L1X X]88F\ 
@?&%\ SM=UUAK-:A(NR(P8Q+*OXDLXJUJ@'G,$9ZE:#( 
@.]4Z]JLC!86;<R"LEN]ZT4DDY:17BU;OK%,1!2,Q\PH 
@17O3=9R'>ESJVC\RLEG]N8"FN!@0_\LF?%'9(.T1T%L 
@<#<LBY&_+2#YLW:<YVM0ZV678I]RUR ! #48E2!LNG, 
@S)%ZQ*NK,W8GIE8*QZ(1NO@3/MD8.Y(W4=D +OII[;4 
@&+<QGM;-KCXY'MM9&GR-V[#[ ZY1,\N'I@DU<3PTWO$ 
@'7_?$M*>41RUG)R2%34Q/DBXO6S_^L-1+Z<^27@:_6H 
@F2R&S-S.H)W0@7#3=E_1+O8MD]3[;D0S%\EB54_0Y>, 
@%[DX]Y0&\9VZ:8A<YBC5 F\X4L.3)! ;L?:\1"D4*\0 
@QY[1Z8ZF'>4:CM A_EUZXVHPW'H$R+^%YS?U-IV%8L< 
@2UHA3NT&M^(ZK!0/%,.R;)[>=NG"[68\^;;2D^&@I7$ 
@9+P=[/.G?<+X'K]@41IJT6R=7DMW="<V67(AX90N0W< 
@_E1U4"J5S)N!9N&&',+@%A#>$:]Y?+!>=,-2ZJ>,/'\ 
@0>CIHK8F9FG)^-B7MZT<^_3QHGJ*)N-OAX^V?OTO_C4 
@$ED'7:".C.N<,*,3?W&/KUCYO.;X6VU2,#:9B:LA^ $ 
@4&T;:Q:ATHL8(&6U\,D[KJF.@F&#7;JF?69';J@0@RT 
@<3@K^A'1L!A5R]@ <YGT(7)&SR>RYLFCID(CI,<8\@L 
@E&&.+.T'L_DF?4S8\PRT6)")=OX5_C(C<?JSZ?X2/!T 
@3B?T)_\$A;D;WBJ.2^#BD-OO;Z;[$<N3N_8G ^<":Q  
@O96D"LF*ER%"=T0HUB)TV0V=I$I-(9^4\&U[BE@%+_P 
@D^1?HN_>8K<[X$XG-RMP,G1QJ1$O8Z'&[J;I7[&PK;8 
@BBH<F^@)S!X,>?::">C4E?%HS)<VMV$*7:G ; I.9DH 
@\0>")+@^"WOK:01<P%-N< @R5&W182JA]H7>?HQ2^,8 
@9LQ(S^O-&#AA/5LX<YV*L/##@'";8SCP.PCR=C^^!+4 
@.J2E#6-+-4R2#"'2*KK/SD 3S7DJ1BX$5!$64;:U6?\ 
@/)1RBJ+VG.XUQH!7?IBAMG6X=V;?P2%U86L7=98#9BX 
@%N$CR:E?WH]:[LO(+HD^^NY:T["Z(AL\%730V:?*P+@ 
@WB\=935) <+9U\9$'<!EE8P :(RJFO.$7TFU=PV2@E, 
@.RTST9R:#7B PNLI9A"I<27LUQE%(]9%/2\N+&-H7YD 
@/%+576#OBG_V1/-G@^&%9J]RE<I1>*8#.>K6Y$*@3OP 
@_26[4&ULKA@G2':X_!(=17]2FJ-&Q[<5X.>'PTKTN*8 
@*W"/;.J].2L3CW)0<U':7^HCJ-)(N))RFC-@" =Q[AT 
@1,6O/_?>Z<%M84SS/.\DUW.ECW^'6>VK5P6K,U?./4$ 
@[=8-4Y7J0>>4E!W'2O $":%I_B(=,W,0MZB(/>ER@2< 
@$_6*-W[T>1*24[9A!M"D_TC_!.GE($M(L#,Z12+J0LH 
@?#HXDBI")C/WTY?O!NQ1)?4&40NGUF?"K"%935]PG5X 
@_DLHJSF,A9-(,LI )0_F9T;L+1;24<]E>*]N"#8YXBP 
@/.F8@-7<I=SX_2YW]);)%*M+?ZF!( &=; AK /6U4FL 
@F88,EPA8XN\G"-LFD 95[ +"QK_V\'%\##X<DB8^3[D 
@[Y[RODK4X;"R0PZY+=%KR8 ]P$4HQ!CX>],PL2*JQ., 
@L[.?5,=;;#4<*:J (W/295XV5[P..9AW;#3JUW]W*/8 
@$YI/C<[XF;KC7\MZ<@3K&>FPTN,:36LLFL=<%[F:F4, 
@HUJUI#8MNXW/-$AF>4O2Y'#^1D2%>F;[*+/J=B\Y6<< 
@R/ARUN]<FS6EXIY%W'&L%6S?O!8'B_E!@HH#4>@B=Y0 
@"S"&SCLZ#H9' 6Q/\-X<^NF__G].!^,[1LAN-S2NP'X 
@M$[05^1T4E#)VXG%A_4PYD13YVK)R\;^3*_74U 5:F< 
@XFYXIO)Y8?+N*63MNI%((O@CW!OHCP\T)$(2Z"$Z%S< 
@R59]6;)@#H4E@-'T,N_Z<AG.=</6[TS?I41!H! M$L\ 
@LQNV=7.$UX8+\4V<H*4ES/F?*Z,X1)Y3MZ1YT"=@6., 
@HN8('#<>YD[=>^-+B#15JQN$(JMPK76"*0:'*BY?3'X 
@UG?++:)W5W\U2F?L*WI<[%J"3(MMY09X1+Z$5\YJH9@ 
@I16G-6" W9PPG5=N 2AGFX=::H!+*U!'0-V K'1M+#, 
@_)% :8U,2=*,?GA"9%DX%[)ALZW.<1K#>WQ7 0(EV!, 
@\P-XO(55EN9I/R>"$2ZI)O/?Q^R8X8W$]&&3K.?$5&T 
@P0,@Q[44YD@/>SCCAH")>9()A.&"$ Y^*@,%SC@=UIX 
@KMH],E2%"\3^D0G G])0QX;$?0)4C0WR.RW5]#TF%Z  
@'V)W88^E;[=-33W-=T0\.@][XD]C):D4Y&#4N(QE.*X 
@D2/8TL9Z['60Y6U%C3=1<'8><L$D ;=Q@ A->SGB(6@ 
@$^E-!Y.2)8]^#*:-FWOH .Z1UP#??)>DW3^5G.K>; ( 
@L*M&^%09R<..8@KG9<;C5EZ!::L2M7 DZU1!FB&\6<P 
@<>W/_?Y7K^ -,:1*RKCO1JF!BZ5].^/2!LFD,T2DF,X 
@92&ZUAUD_9DFMK4?QVWN<U31F SU!!JG]:0$HOXSB(< 
@HJIQ"KI*U&G/I@YKTVCB!IA<&XHPE7FQ\A3$^5$VY]\ 
@@<+_ B$_[RLP0D.1BI65$A@\PE/<+@RH*<(R]3)G?LH 
@X7X4)F,O4>UA?VSE99CB[X8SYP1FX>.[3/=+4>?S(;X 
@Y].]KT BUZ\[URZNHWC=*^UFDKXN).F**4R,!3C&)X0 
@8R'MU'6(7!!QYXNFXO'#VOT_Q&TJ<R,TN.F3:P-PBF$ 
@\$H_\PL05AF*=[I-#@L9;.K^F_&7@O_6VO>3604:W(0 
@RS5CSP2C-^0_.@>V^[P3)>DD9M*Q2&)W.[OF67$:2@P 
@B-9Q007./7 #FH6.U;.G"NTTABT1Y^ZLO4#^[!>Q5YX 
@L-Q;^#RHTH>+D^Y&A=H\]$D9CP;$!N]0\2YSNA)=]AP 
@9#=%J.-*9NM\U1-OUY6O-=K)^@_L(?*(!.)&I"\6]#  
@KD54U B^7\;Z#R=4^2G<WXA>U0>)MW*?F8R 7B_WO>H 
@\WJ;2"QA*>D],V3S9UR[QG#3;[&NF.Y;2C+>Y3&!\^< 
@%BVA!6[A6).+*3X[FX#)Z$ ) CO>(1&H#<W.(?=M\", 
@3Q7?#SG.OVT\"GVO5!<33*,_U$ )-K8UE/-FL(,T;K$ 
@T_<ZGAW/,4XP 9G7O>EFS.4.*!N5R7@<SGB1:ZDKG)  
@R-,M19)BT]XW;J05)<6I-&EG@GDG,<,LWD'JA,R3S/X 
@*CX Y.NV[6 %W] ,-\",43+,KSRWI<T^TX8*!O*(GN4 
@U5<DD8C?2!>B]D;O%Y5BJ/G=OQ]> XJ.R1ONTO,$$=T 
@:<=:(0:Y-AXFP/:K<*2/E)":O.8^'!M[ 81OSH\]/!0 
@G(P);"S[F:*QNX8%,]:RG-"9_3V,-O]#?E2"8?.5/)8 
@)U?4GIW2TP,H3Q_*N32@Y\F]=TJ[S@RPAN?D^3),9=, 
@';%Y%O@9-.PP4 E1L?>P'*K4*$W^9+RFE^#6I)MN+YP 
@%-]O[1UC1K-]5[[)2+@4;Y &HO,53!^X[&W:$Z:U]O( 
@K:?3 [XZ#0VES2']\NW16T-TK2)M(0=N.#4)Q;?E6/0 
@(&(BT@%4Z5RUPP"<\*6FCO2+S3KBL\V2Y<J*L?7)96L 
@-Z\)MWA*)3_L:WR9Q;8):K44"Q-TPLG>(@L&Z(+W'/H 
@C<+U&5*T<UN/: 6<2<MP@>V)=QR$UY)EXBS3:=L5<2$ 
@6]-B8)C,OV6 NB!K3OY*95:;6BXO)6)8T$&&$4U>\T4 
@_QZOT/K*@8^C"+/V,'JEQFSUS*;KDG(^ACRI[G4\]K\ 
@IGRQ2>:P[L4'*^V?CN8V(VMO.!5LGK0D=XUJ[@V63/0 
@0^WS:.:;8K,L58X*I;AS- P5T5!YPZJWN"6*7S6,[/X 
@JS]+P'(((U>T+PZ_USS8Z):53H^#(GS\5L,:IGMVRLL 
@GZ3& TNV&HW"R-O+B442N^P1Q.MW0]ZRJ,99X-8Q3'X 
@* B\A0<B<GP>5GWDOW<D'=)N+@LLX4%UN0G?IDH>]&  
@C\+/WSKS$?_K])FA7:6\G6A%ME78@)/^1XE^L<UQOS@ 
@WC$.:A:08QCWGVE&9X2W4E(68XDM%$!ZB3\T$W)4VM, 
@DPF+M[?[E^$!.M=>7?<^IOI39U!DOOY%(\B!;7P*.3@ 
@?GJ=B1UP%O>7P)N8P1N2]9\"?+4:QF+T/XK^LE%-]!0 
@D:XI0H-6U[3]=$.?F.K=C13!U;*Y1UG-?/S(#DZZ)?8 
@VC;&2SJF;7L<@BPD:=\!F8O46KCZW%M.>SA3!RDYZ5H 
@QQI;-5B>$FQT"S@0%>_T]P_6V"5D_%RZ&FB=&/I/IW\ 
@$P]'?:$\W/@>.EIAN9_D'PQ#7-&XG;TI\?G/7$"$V>8 
@04I([E6[#D+G2GF;<=7O_92PV1.7\?:'LB"B 0*E#,0 
@^;P9>F#B-,5U7VL/,J&&R,JZ[7(!XZ&IRTNW( D"#Z, 
0U031H35FLYDRJ[P2N2@:/@  
`pragma protect end_protected
