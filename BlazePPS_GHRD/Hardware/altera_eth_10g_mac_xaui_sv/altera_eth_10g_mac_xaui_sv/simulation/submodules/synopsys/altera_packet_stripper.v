// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HK^*#:U9!A5G1)>(4M\5VY:2#:2RFT2SN9Q;FC_8_P9-'0;LI,Y6=*@  
H[P0TS'.3,%RE'G_@@TF*9J=X1#W[]FGA=;8>)39HF:P^Y(K5<U:+PP  
HN20%;'*FK^$C(M&Y9.T&'Q0M)&^<UY:ZROU%/)/4TNK::-1KI43^QP  
H3.95FN/J]Q!/E^FCPV(-B4MF#M96!'\;"%^#1I;^0G]D6P]*-\AR8   
H#7C_*X"-5='V8B1;Q;*F>$$ZY]2VIYD^I:LH>,2^'"D^(ZWW=RM3;0  
`pragma protect encoding=(enctype="uuencode",bytes=6384        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@4&PBVWAE;\AX#&:R]9%F%?.YX-JW=K((1XS[;0/ 77P 
@6O'-S3R:2!F&@6 FEM\C*(,*]2,O)%MVZQ=^[7A1F<\ 
@9,.8=V8F]2HKC)V<$CGP%Z826C6O^-77#8/T9"-=Z\4 
@8W,R]U6AH1&3G#?-#;L?1BGK()/,DI%VD37Y"4[L;ZH 
@7)2EJWVLK1I%0P':R&OAMU=:)@#AI"4B3!!\'3;F_:< 
@T$9>%6(S3(,(%]2[S(._RS@RH;,L1$/VR"<_I2$_&+$ 
@QO#U[B5%$"'AU\HB+RV6!G/(L)(.-\4*6TM^5O=ES6D 
@T]I[O39@I<L'!8D(N>I%I0/K'I%<@+[%86O48V$6J&8 
@85P7(^YY((B*E$YF3<H(?OMOK%AZY=]/1/PPH6LPWS@ 
@@&MJ.GKTWM_$0X@9.+!05I]F2$XN?PZ7?*A;V12 QJ\ 
@R?-0[<"O[147#(BZ?OM3'L!1Z$BCTZU9[6*/7@3Y2+4 
@DR=(3Y@AEO"P@=/%,>W57A>#J5ZB+OW($"00M7X*_Z4 
@^Y-$7RA&EY6+ R@KP2_BW_]$^O]HTG7!GF5O8GEF91, 
@==D97BO\W#<_NUD[-<\F!L^C3B2I'\SAT%/0QZALA<, 
@W* &N'.#D.95N)83I<_%W$9MT,5?)Z"Q(?C_>R/X)68 
@RBA?+.>1$?R'MM"#[?A=S*HOEGX)C:'42U:WPJ7N?P8 
@9%U9D@/WC1?@$41,UIU=?:++S/ 3K@^\@H(OG'S#4B, 
@C1>]0 K9YQ:KC$R39YR^!'&3W0Y;'Z)DRAZY@S'.P\X 
@=-8>K,-OMJ&-%K=Q83/_9>L@[AY)/.I/EOZ/8].?&(D 
@/:(?\LHOPDS7"!8"F>>[&L8T2!U\/06WF,XS-GRJ(>X 
@28WX2!MZQDR*'2^R/KQ$C)VYZG87H\0OG'_7H6\&4E< 
@%YL0^9?25P.F]_+^?17GZV,ET(66F:/464S$PMFSK(\ 
@]_OQ!K<C3@O7;O/"E[W@ 5X="^6/K%8^@"A/K(O@ZV( 
@AUCQ$@, X^'_/EEL1CBP/G[E @,^!"9#&4:1>H=$%[< 
@D#@J!Q_ &R6_U'_&Z"9H;GV1?<]:O^BXX*/^T./MR2( 
@UFK>SE6DAF()\,^@0=DR(R.<V ?Y;"O3438ULT8J=ZP 
@>(R].V@C7X\D"][:R>A.3$5GS*P-.5R:4+O894*Y?WT 
@06TSP0[T,<$-XAGK5[AYUL\5C; O[<[Q9#]$%\\Y_C( 
@NLXJ)];-08K=+_=5GXU"AD0A8[^^<K,"^_ZK-45'WX  
@]LK3Q-!/3G[AIMI8R;_;S':B9&6\\.37=I40CE?[PC$ 
@9["(+VQO[A_=5FER7WS@_ACP/A]*5H%*H^P#F]94/20 
@U+[$S=GU1O K3JL=**O.FKY@[1B#U?![/UN]MT][@$, 
@?PC:,RBGE(_>Z_7YBY&LM9<.T+*6/WBD8U@!UU\%/@< 
@*[+_<<PGZROA D1&OPI5D>-6=KK"WU]BX*D#!R$6P6H 
@?[;C.^_)J UEB#WO8%>V[;TG]:H'7SN#KVK,5.6',1$ 
@KCC]\I8C:BW';8(MO'SBY>_0MT4YRC%C\7@98B!^?-0 
@/RK$;/Z:D_(?C]TNWJ6/MF;-UD:A\;&%JDMSS+WYGIP 
@;[6*'?Z0S5Z'S0Z11X6W8'5_V <SR(*^)O KA$^W?E8 
@R"R%*,S"> 7D!\PVYQ_VN=Q-I1J@K<MR/AMNUW.EZ+\ 
@WXN[X@&0"8GRR45N:WW4VNONSWY"\06Y,8F:QE+@9MX 
@#)T?<ZQP*HEA=_8Q)![V2E"P2!F%71S+)ST7VUTYOK( 
@G:E%C,NU>>&"LN-&@2+"KD9Z5P?\FSC>5JKP+D"T"Y\ 
@>WIMJE<9S&XYX3E:UZ;"/;;WX8]RH,&+T7E.?N:C[S( 
@0(P,1C'L\B7;Q7EGSX\@0/.&*P]$$*UJI3;@8VV#V.( 
@ 7H6!X<=B:HM9C:85&Q]S0)_C\70%1H0N_]: ;%<MZ$ 
@\Q\[^2,A!NL"\8U;6/*5"I4/YV#QC(5(:+M_KK82]$T 
@[>^]*BY5YJ0%/_6WFY'(<R=C!\U0 8\IPRX+1@=_17( 
@\HG;'H*TPREPG8 :\>C?Y6XUY'<A;/*<,57R#E0_03, 
@20J'Y^$\0^U9HU\6:1!Q>Z"TNP+!CB.<@VHTX3&-9K\ 
@5JA[8/I/(HCTM83@IU50N)(.!&EZIKU[P??^J:\1L9D 
@LP2(1[FO(HS9Y0% YKIKT,@N@_AJ#= >ZAVIUR/_P4L 
@64'^1)G<VRCLA)4VR-599"=!N%RA#@=O>(A29*^//#  
@;;5Q;-*Y_+;F5->2+[QT%N%F?XU_($,917\@_I[K%)T 
@ESML:V%N]Q1V.:XT9-C$?P1G237R]#W0F.?6C!]K:9L 
@A;];0L!D/\VYOT?0;5T3Y3OZ]WJ0?8-@LR%?H&;ZV=T 
@(J=>P_EI]Y=WR_Y 1;O\&T?UZ1R(2\(PKYS3<%>M9Y8 
@MGJ8<!F( ^+J= ;4_A5E+J%G\X5\136L@N>F)==4&(< 
@4:OY;78<'TGHQEQ97+;_<D!%RL3?,TF6BJ.1-[HX&"< 
@O 6B2#RME8'.><TE%R,^Y'6,E>]\D@" >34K&(SYKI, 
@3$W-9C0^)%G>&N-;Y>QP_V/SN)M[]:&HKY*Z!=SJL(X 
@$/+<MDU,/1'D."S]&('MH"[BWAFE_AUT-FU6U5O'OM< 
@NW!ZA>.+#Y8?QWQS*$A(.4!JY(M:B?\]1!AG@N9$6^H 
@*Z%H:&]PUA:&Y?KN/D(:I"$4;1;''^(=.L*WBJROHWH 
@Z0&O[=[@H*Y[8J2+VY,62MD7@(N*)?F>&T6U5/B-&SH 
@AY)[XL373IW2XD+O#ICW_?2Q-0#HFKN]:DMAR''<A'H 
@1C2%Q/[ZIRO I,1,)<[4"ZZU/PM8.*[T&77E\+57\S\ 
@%N#1*FT2^ZMJ\2?4NQ:2 0V1# <W8W'U*!1:QR*T/?( 
@BS\LL7(2B:;8X_V?6!]M$5.%H3&M ON$9 ;/O4Z[W?< 
@49(AH]AZ.WB;LEPSJ"EDM:M%.W1[NDSCL#^>,B)4I;\ 
@;'R-A-SC5=4M#ZQB,;QL(5*]1F)Q62"$A8U%H N6;WH 
@.[XXX8M /MM3Q^#LQJN[NE01^Q>BZYY!TX8V<$G"7NX 
@JI101/*YP@*A2XL34($GX26FG/E/#KL:5GZRZZ-*/WH 
@0!H.^,13;;'S&KY#@.=M;3SIEY3AJ=W7@'AMF(K\WK$ 
@(V7W@%=P@SA6AEC-_(;&%A(E-Z)<X_Z&52/.DM8<.\H 
@SE3,A5O%ZDQ528?Y+2 &<K%70Y^Y-<ZMA+P\_?[?%L@ 
@'DJ)=8 *3ES<>&Z'[TC']-?[)ZO&UY_,#O&W0: $*R8 
@>5^O7NZ9/I^'0Q5YASH*X\6H=/XVN'\!R4X,.GP+*IH 
@2W%)6.#2K>3GN79P?T3G:A][#PB.@%L1MASSSWHLRW8 
@L ;!NI-& EP*W^3@TW0.5JFSP!V69U6UP%$B%,AD>KD 
@NIPSSHF?888[DL?-J 3GOM^]I=.K1AY-J/\Y[3S: )4 
@X2WHAUUT"PT*:<MQEEJ*Y6.VEEG:YH$A+?(P>'_D3JH 
@-.MGP:@L [N_8/11'8/:M_0Q@1'##+HQ0[XZ" ]C%4( 
@78>+L+GGP[F0GE#<#O,;%M;.W=\+5',TM$M]1II^6F4 
@*P&:;I-[V\,_!)SI&MRHK]6J];RD)_XAZ8WN1:#7T-D 
@ZW^ZW8V,YE,-(!T+7/]TE-/L2H$?F.MMX1S60-5<K+D 
@ 06;_+/2?USN;IRS:L?IC!M\I\[73D)Q$.(XW8,!A2$ 
@VV87G]/T23JM,*>7ZQ(R!Z]L:(5J @1&@4K3K_B+(3H 
@Y5-:MS5B4LCL#%1#@3T-R4PZ%[\LL@"P7),C!.2B&2@ 
@Z.$,EH.SP$'2\F*@SUP6.6R0%&":RLYMTH[*GU](1N8 
@:,\DDR_(,Q[GDV>)=G@XKQZ:\K7>@$)8"?>/3*4:2V@ 
@K <45$R3(2#E'+L/76F9$]-[]ZUU10\_:QCP+&NDK40 
@X&RFTF0Z!RH\FMI@U>('N-#+219ET!T T?369,6^PH$ 
@Y\[_7<%(AI/+D!.>SU)MD@ O)B%YR-#UI4NL\Z6)C!  
@](">LFF30C$VP@NU@%AEJG'9[8E'5[G<7^Q]RNF,>IT 
@5+E13:U>6%C'<MOIU*A2C]26T]<@.*V@<@(4H[I0>ND 
@$\;O\X.U?/5_@@A(M!13[8O_5)-X*Z(J3AG=93!T(;X 
@0]\$97KJ?M_"X* *L$V<[6EU/C;R]-+<B^+!9['_ I\ 
@EX-3$B.G9.F9<JI&1[9O9GFK^&2J4T%JP/D]:"W#>%T 
@,3I6/1F^V<6[?W+5[6M[=TD.7J I'Y% *QG?JQ);\!$ 
@C+?%#\G-*779?0W_6#':\'<FT!??RW4N9JMZ_V\_1!< 
@!Q>72*U4[C0SWE6*< \1>-P[X&PG!^PPCO:R%K\8",4 
@$8(V^VE[A0H$UK]&,YO(P4#\P!WUM"1,Y@_F)3)+/=0 
@'[Q%H?"MB3:Y12>UL1H/U. ,;922?#.2/BQD.^58:MT 
@/F"R:8<_S4P6-DSPWNE @O/"BJ:.3G/6$[H'RM7L&PH 
@*J,OCTTT\T.ZI#VV**F"S; 42OCO^;:/6&%SR_JR0), 
@;%YO0:K0<.2$D[O C-\\&+;9H!MGXXRFGG4<P5N",>0 
@]4!\YJ'(NG8^Y-F,[7PDR+7SWMBN3SM:\EF;)RLV^UH 
@,O#W2%SR#91^M! 8>K9G E[TI7FP,>=J?<KABN42\OL 
@(A967O_2)X.-.D42/UUO6.H^,VMP-6% ]TRZP:,]3$D 
@!JS"W)+'P%#]&)9NV7/19C3.SL&+HX2O83^!Z3.H>4( 
@5K#NX"]=\;!BFVP2(A\^;;Y:[F6<'/!4#J52Q.5KMNH 
@8LU:I&HC>\DNV/A!LD_8__1I-EGWR?DQ7]7KE \:[0X 
@6!AD[%O %I [TL+CM^?4H*/W$SF;6%O91J+2P,-4"<$ 
@BJ=NMA!79"XAT$3I>Z8YOV?I,^9?JME*DM]!['^YNU@ 
@)+HQ\_T55.@G?HG.9;H;KMRG,@Q7ZR+I1NTCK>*JAGL 
@PZ+L%^@$#]:H!'O)[R1_VKG&?WD@[/@SRV>"SL:6XB\ 
@3SR"B20@M6,BII&9]I;,&;5B,9'$K\0IJ[ ]B$P7?OP 
@W@G,DJZ=H)6&_9M;(6-0&A!(ME$LYS4IL"[?4A<T"U0 
@Y&-3<3QF"BO.@:/F^/VQHF^U7U@O+E#)$<^>GHM;M+< 
@9Q1H;I54&/TX9C?++%3\)GLH@#XF.#AE9439V ">IY4 
@P=9V*>I>OQNX/Z!WDR0$--F Z3>A+4%L&YXSP/]GSL< 
@;U).GQ]16OD.*S2.!N&SQ2.G/D1]1JT4J$JP%AQP&1@ 
@8,8C(A##2QD=\[#-F;((Q> &("[33[IQDA:(.WB:/KH 
@B3]R#CK#O?/;0R(1JHO5HE11J,I#,_!P<C]K\:H7F6@ 
@$#!Q94TZ:>^P%^V#NZ>#:*!I23J/?%%%,9'=",?-&#, 
@E><GT.*Q#DXK5=@#@U \X%Z\[T:Y-!PO"A',[3 ;MCX 
@;]:#6KH>J5@9EN?2YV,U&LETOBW5 J$-!(#9Y<_YH \ 
@&]Z?(Q:4'?"S2ZV?!Z!4/V6PZ;D=U%YKP;&!/-(V?;0 
@^:ZW_&K.E%''/?Y'B$*1QUWT2"3W*06=H-VC$\4,&NP 
@J?R<8M\(.!X!J'9L)"2#0DK"^_LCLPX=,.[5QK\E+FT 
@1N3S'WIR_.U>I#'-:NPT4_7Q3EX9"Z+6<G:^"V+FRVX 
@1_WS@ <S]K!5]ELV!F[T/D")!6/3(EWK[M0:LM^QWQT 
@/2,+(E@PXE0LQ]XPQV"T$-O'1L /4%TU,)ONKKR!QJP 
@_C&R^MR2LO6V%"F<Y W9!W(>4)T'#AYZC%P*3,486_P 
@E"05WLU!KP<2(]19!=A-(_((G DAEHLL/E>0X5#E@MH 
@3NK*%[6%1%<MM?C9'07OS;@>DLLB2BI %?2B;$!$@WP 
@$:?317P*&.\H)@->.O'*?0,D%(I][_A,TI0I3-40CLT 
@?3&,,6(*CO)/[18ZEEW#VJ#B+;:!'N*WV5P*UZTV#FD 
@::;0W1^JQ.O300O&F-=3+0 FIS&;'6/LIQ-CV:Z#F>  
@L([*^I;YJ3I^"H56]O0P@1\_!G!V]W%99-+=[@$QT7L 
@A=V(0%Y^WI]OS)42"+G/I#A9)$F <J*GTQ,RAUM46G4 
@%D0]N?IQ0R[[,?>Z_XLB\@*KZ1M-34)[VFEC?,F<-4, 
@.(9&0[>14K"\!E]/OVR.=W7U!E.*N- IZ0U)WD_A[^L 
@ W\. -LH$WC=4CI/"!'\+W/0ASH3>AIF8#T43[OQL2\ 
@/!0+VW<D)&038D#"/^BYVQS0@BTZ-OM#ZX&YL:XE=6( 
@ $8C@P\OG0* 28\PW)K\L88*F=MKYSHZ(F4.N$>4O%\ 
@S(F54>8\%MW.)B7W8:&3F4MR"^NB<\D +TTRTRN# ^  
@*QS\VRS]-^+-MY8[^9[# 2]=K*'+2H>UC%47<'PBGW( 
@@I2V)>XTT% ]VC7<C(AHQN6:]%E\^N2I3(G_V"1,;R, 
@6W FN$5LS8HS4_3IG?)H'8L="/\LB:-;>G5+0DT0;1D 
@2R<O-&17QXVC\,-94)M#[F7C-0<M<P=-(PAQ['"R2K< 
@!@%,FC+RD!TM ,K=DB"A-U<,YNFF0\(A/T]"BX=10A\ 
@5K0T+I%HQN <4=+QNJHV_:+(SYFZL/TH6"#62/0M+%  
@T,FNF_ &+90TN!34$WH"&M%VD$^%-;F<$T+YD33J=C0 
@_J6_/"E>.YPTIT@E* &K+VL3=^URIGX?-[0JF)HI?8P 
@$)8,#K6K&."OK/2=)+U!$70\;L>3R  /'$GV(NIE=/P 
@@SXG+R[?4)C([<0+.[<)G*F98A(<@E7649"_I(;@( L 
@XUA8=F[.C>07_DA5'[/WOL@EXSZ!(V--J(XLE=!;)B4 
@+0:,W;+=,AQ=?':8*)6.?]!+QP+1O80A(JQC88W)]]0 
@5Q/NG33VH$R\O@@$E]QOB&XX4?7M@RXLR9>$*K%=RF( 
@<M!K5>0D)?O$MM&R.[]S!/^L*DJAD5)*?IY6O=_&C-L 
@*?-J*M[=IO1N*'R!$T7I /$D8F@H)AU#4Q !]-ZO3[X 
@U1.*&F4_3RYSJ[3X"[/+D#LB-F1TEI<AM1R$)KZ-1_( 
@L_.\P[#_97RK"?S'=VD=7</(# B*EUGZ98M-KF<+O/T 
@6(L0YBW<8?4UZI_ 73CKK&Q.H6HOPOQ+54VCJPDS9U8 
@Y%>33X:AMPMG)I0N% L(19=]^RW )R%3K%/EPD7I!^, 
@7P89-]'(+(5YORPZ%#.$& ,5*35;7)DY%*H=!(U>%>  
@6U&:>8A\H3-W%>BHVK!JR<1&_4F>A#4]+>(NS9.6"#L 
@L9%(4%F6IL[!UOP+1+^9KF*IL#\&V4GBC:(>V,KM"_X 
@8CXNWW"\4=QPE.*QSXCA=R3V_!94<W*;--16MRHZN]X 
@NYD,+1UGC?RI3W>ZDBG_<6C[.@ND[9=Y4-[;>525M3H 
@>C-I?^JD'BG+^;K.?JJZ=W:#&_#@RE!G[(U^^[#SN\, 
@;5%WX-7S-E;:/?(T7UK?^ ]:M4Z,B%P*$J'$+L)3-NL 
@2<PG+,EYM;X:5RY\1^L9<+5C"V\X>(QA/Q3_\YY^EGH 
@6-H77<3+L<;X'F30MF8&BK4]2_Z1QTN&L:]^(G38@YD 
@<J$&0*:B_'0QHX@"AO?3>O2<SM%=</QH3^[>[5PK_8  
@ZH\#$-"/?X9#&EH6R3>*5.CRXC_Y;W##4=%3;%[)G@< 
@K@%*(O+'B//WDZ&7_BGC'8(*(&*&)-',J@O3B.N]/(P 
@[FI(HZGEAZ\CUB-N X38[O3(BWZFR"N=-S:UM,SMD9P 
@)\D $^A[EG+I-[Y#[#=995<!3W^K6X[(! @'+;)OS,L 
@P1^V#:%*"#9JQK3H$'K0&$P1,D5!,1-C<K47(O4/'A4 
@#B_S&:](.AWE:^9.U5X<?.[P"P<7V$3$"KDLLINYL,H 
@M&-V.7BWH5ZZSU=Y3JPJ$&U^ >3</V*,TVL#@:RC=/, 
@S#-ET>N]N101I9<ITHW7C)8TXT!#&X1C\D'-?OIXPH, 
@?KEM0BY(I^=V!5DW5KT6' ;*><Y0 2.#:)"+RDK1^ID 
@NKHNRO-?^]D& ?>ER!7X3F3'LI9CF*[G !L),S<,?%8 
@X>KQP%AHL*KR.54DF<0-9A$>=+< (;R%J]J_I!9N9QH 
@A'NF%4WWT>BSK%3!(8AW/EBOLC)/=5NLDN'/"*B-RZ( 
@[GG"F$_>>TGJ2GK3%!J@-]NB.I5WYZ.(THP1S@LRXI  
@46?CX:@]W71*CX5=/,;\@V$LQB!53)/0JGX'_ZBSJ^P 
@>P/E@DSGFUOID1,DC 0>IO%SBJOU+CQ?J2&3-UV,7(8 
@=M?I_=+P#H'YC$)H)0+8N[6'7)XZ<#C^=$> WF(>OF( 
@5$DDCL#H^A?AX3+4C@97ZJLJ6Y<N7FEC$3Q;"EYCP\@ 
@!,,Q0!+"1V]\-07\J 7P@=,8]A(X,7+YL&(JF5*?RP$ 
@@?WJ<0!R8%.8[?F\Y -GSR*\:BJ'5O7&#D)T+MY.66P 
@VLF)I"\(%I !<IBFF?C,RMD@]@TJ_,LJ@:+OE8T=4=, 
@.B(3=#%R,'1WUA+]I$"LS_\\?\SKVLZ&&BR15_?=JSL 
@%@^\XXB<5Q03P2SY 4J3L,YMA)Q<]!\[_% *.!KUI+P 
@HQFGC)RYANI[W^]V3+R%="&W@=?6YSMH[J.9)QG2&H  
0J3PFZ__E ^!Q07+P2.\CJ@  
`pragma protect end_protected
