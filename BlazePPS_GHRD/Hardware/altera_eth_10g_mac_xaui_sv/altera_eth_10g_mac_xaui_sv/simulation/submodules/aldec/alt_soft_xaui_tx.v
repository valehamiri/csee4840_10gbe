// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent= "Aldec protectip, Riviera-PRO 2011.10.82"
`pragma protect key_keyowner= "Aldec", key_keyname= "ALDEC08_001", key_method= "rsa"
`pragma protect key_block encoding= (enctype="base64")
CevwaEyb7mPn/PEhio+f561bCXEw2amnxtlTSgQ1ckS5KzbE3CryOUJRSO1Wz7oLLF6L1wMeszRx
BV4kaMgSPwyr2Cb+aqx6rn2i+1rGcoAxMxo/xb+Sc8wiA1fczQrtv8DyHsnmmKTJ/JI8TmtZYph2
hdV/hws7bMTKO/G4+l0yX1b7iegjRX1YnqmRNyIHKbFnHaxtRvLYniSJtgfeUqBim+pR+bq+S3lY
HNJrkTiklTHwLrccLyafe6Oxu7aWEzKcXBy/mAsQ2aJTl07ZXwjBZwY1gbiM5leTIhZNwSvXm/23
+FqZky0f/n+nAq3oX5HetwziPggs7Usjl32J+Q==
`pragma protect data_keyowner= "altera", data_keyname= "altera"
`pragma protect data_method= "aes128-cbc"
`pragma protect data_block encoding= (enctype="base64")
fhOxN95TMgj9mAog7pDgjQb3Nmo8YCwYIbjd4KaJhv8rd+E9BUjX4iVtaWzTh+dV5gA0Qz0hxo6Y
1Xq6sCXfe6JAY2z/YjFsCUDv/gXti8z6nh2DDY2Er5EL7udYjTU852Uegru9770hnTCOxOeMM0JR
z5mYrimGVDINe3mXwv+KznzR18Pku+bDE9bWKkblXI0qDMlWGv/vvywOjrZt1LNn77dGOJZcuRL5
fsBcoqLMW8+nVb/6cVFlGPMpX7Qh9GVTcj1mlY/HWYG8eNXyTDw23+Ad+e2wK5Syue0cDEosRmGV
BZY6BkKKPtepsZ29HfUiQpGlBMblRjkWR6x1TLdBc7F2kfBQM7ewzANCHtu8kquvxTEQXxt5+7tC
pDbjqacx1GvgUYUySo623EAOyTxjQxh+rj4yb9o4JHWYmY2I38eYhC+WeF8fi9od3OvA+e8pGJV7
jqtukej9/lvPKkr9ZzOnA7lxDw3sNiHkMh0e9seSw53Z03ANJoZWH27AUnqYUF6IyIua9T+MLUot
tNV0eo0UDq+1C8zwMHGpxyt5UaSq+JYVssV5xagAla6f/zLs5lIRnGcV7tG3esihkrA8qYBtecra
XFCBJwu0ERi7LeisVWrkjnKSSLBFuL/oTstNXpawNxWDEjc+67A/Ass2CK9FwTyXAYit8HPPkTLa
GPaYwzYLhm8v3yxXUQrcbqzqDSAH+vzXXUrpZQHcmWVTOh8FfTPIopd3FQbCNp0qrjM55p14kn/g
ZrgBBEDKA5fAkbUXOUIjS4sWmasKYrDhxmblmUgc/WC5MXCNtHE+khJFFbeq9g9B2LjCswqNYgAZ
qDaSmlSAHtkE5n9UeQQQIWC0UvQJ5dwyGCAFOl9tBc3SyOi0etw7pxeCVXXydhw+V64uqzCZ9nwq
bUubZNPDRU2flVBYLc7d+5XDzeKmDYbS+SfOM+qHZrjGFiO29Kn1IdSHuAXkQAWPH1TLRJPcayhh
BfWFDR8jQJI4i0F/3Q02OWlG9VN5vOeq/Vqr3ocP5COXELdyQ5OZQI4PEoNOhtmmcxvN8QvpsTaV
4gCBcD8JGSBHRfZ5lrzFORiXslPfrKzLzjNojU2LWQyrMoG7BvIcMb+uzVjGZO3BmHnswaITroPy
Cpw+Q0mL6Tx5DHnp0erU/RprA0rqdNLEDc8imw4MXGulRBV6sYzGGUApoKuUsNyTVqlOjjRn60SF
qVmsSU5MANWMhD5Bpfe4QXVTbG5j9WDe5jb8I0h6DZI9/GMeKUrB4IEgTqQ3FDHok3QtthTMGKQW
zuT2c+/VreA8qDA0ptbGE/l56xNPZB3PDx/5/sr93R/hLsqrYCLjp4gyUvyqWb2RybD28UVwp+VO
6RrDyxHaGmX3Zcj1mqAG37YQr5YhqMbkC3yWyJWDuSOPUKD/GYxos+mFlXc9Eq+/clvAccTGouvk
b2c46rZ0JyE9Jv5ZnV2cRxXb7QavQrmyYA9OEUGu5o4RZCBJ9OgWiBZky/BoefyJ7nNKWY7LVZp2
lvKZdIC3fxeWl+0cDqE8oQC0QF7trkQgR9sdsGBlfcllZeWbEh6H18gi5bCFqXnnWpKLrPI0LOND
W1Z8kDdt8tWiQ3kJazAvatfmLKvUgtB1i5HH775dl0QQ+hSvwvpxTK+S0sv3ioK7IffhuvHk4N3K
Lr86/3ZKlYq9Q1674Epw6rejoGFwuc8HBQdKqvNj59QyFwwNGLXiVLhnhlLKwteU4CMjbvDKlp5c
ivBDTfGJsKl0VF8+SwwPKBGBeQbY1ANCLenm/odUgyS3t2VTQwFVI+40L4NrF3gT+MXsRZmYRnWF
+v8VITpRwxLrX0j/y57dmn+kRaICZ5rh31+A7t/vr31rPWqYDlCtbFEuwZm6Ep/ygUFaPD205HN9
cLX2ISXHPP0oPUxfHsFoJOBB2Qb0v9anqPrA86A91M7D4Kvo2nq9hvqa6OzwCaUSfEY1dzM+w2Gn
fq2vUHz7VYENfd4WyRkGyJWbPN0rkpeBqNOVCtNtPi1pOIf581Eaveuysq9UgDRhpcVWp5hKidP0
+vUQRM+XcYjLoptl2lHhg7nx/9khIp6kJpqPMIzloF3+4P8rICSVvpK/8VRndsiSnL6b1Pf9uHsU
tHTN4gWKCRavryuJsx4DKuJI6bPS7Sr4wzD+fXikLMFfYV+bvXDu3Lpmb9LgpbOGilJhhDvFOBip
vT88D0sTqtXZButAr1l1YC/KdkW4RrKQFu65BoSpwHuQoxB+ruY9+jhq05PL9sRaUFO/EnbDX/QK
0AXq0+QmEn0dFaHfyIIE0zVGWyKKOVYIw93SH7pzliE25Md4u7+cH7gcK8OBK0e9Rw+cf0xgFAQ4
7T+iaQm3qfId6Bz33uYmYwd0DgIf1UEi6SFxOWYnpTQBTu1QoUNpcK6CmZvencTXuOH+IIy3nHOQ
K7/XdHV8xoFeOyaY64/0bKkz72GElOEy5gMimXdCOuDNN4ZoocHjZjMPX3CBAU/s79IwtYCf39M1
t/zI52MUXg8h6ZkOdYKR4aZPfeFzVH1bWOCtSsJEtqaNDZH+OqCNMz5nq5yJkds5HoZdgUi2PVqp
xmPKjPqMLwCKsIRhy7Se9XKdXS6ICpTac0sk+xrFkAd5NJeud2Y3z0JkEAve+CTw+hkc+FNnrOvu
pNyN5XNw1Ur6991Fyc+GHqYp23WFOtT3mNhri7/wZvUReLIYtaq/L+TB3DzO+3WVNlTI5H2CoUvM
zF6Ib6J+prdRP43BMmZUZpsAsqff+FcWDiZ9rS9jbRNY1YidiOmG33qMZt/WpWEq6bQHuUSXS9Ly
EWiJJAUXAZ5IOFxuv0qzrauDp2cRXLDQydHsDKu6/0pe3YvPwhVbXHEjQblYERuRA69+hUFqCF/m
MlTf8dU9tVag7QF11sdpuL3hgCgnkvrcctKQWtPIA+bNOJ3UQQrNK4rKbXfsGJwPtaOFeh4lSrp1
X9I5LwWRMiRwRIsdksM/QYniTq+7tmWEKOpd6ZRvizMgi1M0y6IS2kB+7C2o43ApZ32J5e50j4si
bGuAhcyjxYjLiexcPj5th4kj3WcavwkKG9HgQJfCa6q2v7dimKKTyi6j5zMPeSTI6pr5NQpq5Y4P
jsbuk2/m7uYu5JwAnrnkAtrwjx8sNsiMCizOMFOipdhdetdb/E5Yii9pVxcHJBLgAJdGIcNVUGha
susEXmTjVNhTTGKqBixDYn6wUNYNVEiqO11JmmNzXjDXsKGz/pLF9V3pNjhz6EEod7yA7Bd6TtVr
HL+U44rdhd1XLlxDKS7xJW4tgZDiW2Ij1tozxGI+J1bYIDFbWT4SlEJNJXx01ZRhM/syEgkHF3mK
MU7GweAfv5x3Z6bHU+TpVFledHpTXsEBVKWsdocBgO4+2qYbCwfzeNWpGatGVfpKLLZgRgkXhNTq
0lp6sc4FRfkI+CnVW9YBGwPu0m2G+XZU5jANmsweLQK9YZfjkfgmwhNZOJLDiTOM85QHigl208Mp
inpIk+FX7xRzadUpTos+zZPpDGnBilJP00f9uUHlSM3I8P0RjHz+sh43Wa3x8WN9FgLpnq2kO8fC
9RlQT6pbkfW4ZTubx3e8/Pok2lAh0hhL7LdT7c6kL3ToRRFxCnQtZcwXJmGDsEXP4nL6gvnbuVjr
6VcDQWngNcs1UsY9NIo36mYMdfwUjnHCg06R3Y3KhGUe3VO8y4tUSRvVoSswqtyOFDP4xL9vxskf
1Otuy8OzH9YxF452PCBjLef4h07LBds+gd1h2kGkDuQRpdW6WVFMw+Me3OYHJ8hOtc4ncC9oxEF2
7M4gimz6+rOz0I+NZragOlYIiV8hDFEYT0nSGqjRQGV4O6dqQSyGcqD+SZ0H1MpFjcdtjvrzpeft
/ug1lHDanEl3FWcJRBNL0pWlWekKKf4dGYSVjTA7rm1Rpjufn+ZPqVqa72+jXWfqAwRnkFBWdRpd
f3XeSnfGNIaZw7HJ+d2wOWrTCLS79lz2KURL/oRKcLDWbq4YpMicDbXLlI8eFqGYrdXPnZXBPTcR
wt56wPsuDtFBBNFBAIyrmZ2TANVdP7UB7whMJxTkUjT6jrB8jt1iJkEGD2ShB4HjP1MDg4eMIWVz
13lRGV7gsoel2/LFEzMHYqvO/xmYn6c8AhXfsj1NmlxM609NBcWJEhdgl/NYYWH5ErQ0GzJyfgv1
L3gkE1T4HNAq5WGEmXX45S3GR0lmqAxX+g+h6zuJsK/mxMLSkZVUY88Oc1APPTRQ44dHCE17ONkF
cMRxx0wj80oXYndHWszJoMrp3c2jD42Zi6XO9axmtqRSY13SswXdjHM30zvI9XndEhCvpEUMThOg
sizQmd4/fajZkgRv3aOXN2EqrLnrNjJyX4YHgXmxqq/8TsjkNlBtgd2VJH0QG7QJEWW07PnQ07/p
5+E+sV4GT1h/KUo8dFrctD2UZEj9I/xXbYR3usp/aVaqgJk5TP4P8OqPfDRoFu65IKtINDQ82QXx
1fOJeTZpJFmoNsW+K5QROHLSsOjKEd2Hl/vNoNJACgQ9yUiZnTa7aKFaaPe09v7ZWLjhFch9g0Tu
knFDKlkcxOD1GUVoNaqoXdyoXnk6l5Qvtm2qchnWZIlNmo1qpxlrAg7CR7/a9deEtRxYzFWMAoU5
oLj/QuKiGIoFTfTrJ3mBEKcXgOLiZys+1u36N/r0xhXNkePilKsUadbJgEDNxtkXMxvCLs/K7HT/
0QKuIFcWLnEXXCS4bSHDHNI0j4WnK7AAf9CDwbJvOdn0nRkiVQ/MB3HmAT7j7Ho+mh1PQ3Y4bdSv
l+w7GuYrNlFW64cnfMjKAH9Qvs6szidhW+Nh6rBicrwopifpNzf+9QKvuhZcwen4rDJz9K72Seoh
uPFv/k0UFgUHk6ZTTrnpSrGg5uDbMW17jPmNhnL8B32n9+FwM+mXELwE6dTVsxgovvn7lSxlP8hT
KeEzJssqPoJvtuEukFEjvXkPiewX3r/mbb1CsB1JyBX3Xh1/zoc1Mhdr3ktQ7SF8HzylcB+IFX2Q
wHrHNevuBHZVV41yGy/7tOccNyNSqLwrd8HqiX4qbaEiuURfyva9gHQXxQHhxBR0wUNDY421FdyS
YKPPrziQ3PWVvLm2AkgxrrQWmmEbwQ4EmI40rtuF+TphwGF2mTx6CpAKbhh7wj7inmItWyeemj7S
pBNBeYmiaHYC9xhiNcMWZ2cQdmrsJMIa0Nc30Xx1OB3ewMGbdGOh6+eMQ9tBIq+bXxdeQ4bkPJMc
mff+0l0FyI6liu9B3p98q5Dvbvzm64Jb4jAfUDVKiOg4H+nn1GLJ5d4ndqDVrirbYec63Qav79hb
cha5v/YMowCN87+UJfmMBhLA1SZe75mrq0IeAEQKREVQQWqGqDrGV7gw3xgaRknKxr6lypdcb6lI
/AEl2UtdakakBkS4VIOiadKVz5j3S5znzFCvvafPgUhhGlWIZkJVQh38rEq7aXr2ibODnr3YXGzF
KwZ4NFY+O+ZLeYyEoslH0Wh+bQDLbEeDsLwDHjmZk3C2T7EkW6d/QM7mV6raiNMQ2UeMw2Bhm0JO
thhAUD5aHmu6egDIlOm7po9RrAABKF4hpc4vJijFSvkTsukuWWsRLKxzjpoAEsnnjPI7YAIuTBXg
KEl9qUIHGlSjGpxKt4o3Nl7fgo4jIqvwHUO7rc26Y3WlwtYEAQ3jam6yaRPxLRZ0cybaoapU56DT
C2NAoDI++Sn17dynYV7V+P0LEs/Vp+hLKA5HSWFaLcFW3ytaqAzVYqBY22WG1/SwRRA0LovuqvkO
OchAecIqUoAxh/oUZiKrua2AsS2HfgHC0d8VWTYeBu3kp8DLWVdW7S9OgqDxxNrImc0rUv0ISRUn
UAjQhsYxTN7iFTxR2OuFGYgi9nxsmKcbHkJWHMP+SOrteiN1vP9bDUfJL3GWK21RPUm/0fttV35j
lKj5+b2o0b3A/N7yC86X2ivc8BcQYMvFCley/h2u0BwBYxIIU6RCO2dprxWVfuM4ucIIuJ8FEWDQ
9WD7FcoKZNuY9Qgb0Dv+tyicWTflfV2Qzys787+nEihM+DWpc/43/OI1QOkb5cgNWY12ygfB5arc
w3BEGnNusPlzFCVhBOv7KzP5zVdJ3904JJzaMsm399uoFyiUt3moif2/VwxuG0cO0uhM1yhtrx2l
PrnrEkgUOhjfGVIVmfzZ3YiT93H8ojEqBYg5feRRhB5UdlT9bOMo8vjMs6+eM6e1FlizChAmTcY0
Q57B9G+3fi3Iq3lRvGdVmR7NBw+XUZY6lR3KPZcnJ0n37YR2R21GGcHhAd7XvaLZsSa2MWiCAB+B
Km+Acv0Pjhh/+JYqzu3EkminvhaCTGhE/8jdDbASzQOMULpejU+fvIrKO+hT0BZaOv/kkPLnYjIk
H5vznHie7GiLBlMuN4XFfVxe8oKE9psNCbUBJ3wJkoHG6y4g5HB1Ec+SnQN+bwkScpLqPzO6EVGJ
qM8UaILSISKh67gTXCSfDwQuqDqoA1hMOS8AaaGY9JNYZhsUmY0ElBHUI0islPtjL54bVpRPziDj
XoPrxtrenXFhO9Gk0a/kBIEZ5INXQg0h5evHtJITMSCBnZD3uPN3w1D1gPHQp3z8sOhgJHwyOS5c
nThNdmtAJdwwsBjL7a/4v4Ze7JGKb0yyqBDLcGqEhQEsgVtVudeQqF8JEtNnaB2VFiS1ZPPJs9/a
vtr7xpxwo0beN2J2O7OcqxaNjscBInXU268YFlOsrCRnxvcDcTsrGqpZg4y+1d5McmnuR7aUrrk5
k5fY83Xau3AjH/PnfKOWu9TzJDBCrlhykbFejttXdlWAM6VBpuby3+9Srprdrntcg8KInvLJdzTF
gP08N4zI9TUxi8YBhWjaHzgSNCQ0nonVl6zA7xOmpyLp7xcxAvwtG7UArVjnC9lxBEuK2saKJ8TK
TiXbmmbWGDDGEgETTAEVkc+2WmcnVgm7TOSEi19YWKm7gx+qxbJt7Tj9Yj9t74m4KU4suBR1R+8C
V7PacKr412ciI7mpmK6oZQKtvTfAQjNvGx4nHxtykwCKWeeTQEXBUpZ8YucdFhTRrrgm2z29M8sh
+/10yHjx13YEDmb8hJssbAVosciiahYdn8dz7aqDoAGCFoD1yi8tw8j5D7C0Vuv/oc60E5E/5jls
ecVODmhN8OrrbDRiPwSxQty+sZcnu9c651zp1fnU0Feh65PEPQgyEFLwiC49vcBu3ruZYKa6zgeR
5lYeMX9NuKc3zCmAByEo5nb5Q5jP1+upaLh2j3ws6TXCAHFX78a6dVUFobKbkng4LTagw3RlUIYb
Y1n1CYZ+5mLkdWyc+a4nU/iHnZqh/A2doMxhiLCz8MJeDv3QPhroYyvlRm47vZrcg+U7jJI5o9QU
mjkIMbW3hRvvuadPg42futvwdzmbii+2ZBsY/WuvhjDL6PHgVUYtIPVDLHOfPNS0WoqD3neI/njK
hq4FFyuP8xjm/L0PAKh7HOVgcxVxH1N+QQRFVYN2atm8iDZRCmzbpf+HPjSSg8jwoQp3wUa/wkDq
eat/I5dfnKtd74HdMAze9TPr4mfBLQLNTOlUmLZWj5xwf6T7g7rpux0EzEWVF6G1kddQ0L8Tp7+8
ImLjox5XZRufmb5qHlhZotS8FTGgy+I6fiB9FR8DcnvgxxVAY2WaAEcssERsvmpcYvQuu/guTV7I
/Gow7Y9NmtXT52QT+2ooFIX1mXoEcbFbI/JajKAryrE7Bz2K5R7tJYrvjvatHupT9gTyjcDOabSJ
jfYVdT51ECP6RnGRi+72RmKD9CvpGxRFUUU+5g5Cimg3PYqikCo02CCqpT+LWoqTar4/ImHTStNz
hK1bgy0fv6AoZn8skbEZS9ujIWB6TuG2xFFP4p7A5nyF5xgsKp2v4rQSeV10sTAe2Q9qLGIqIS6J
5lSckAWw3HR6L1jfweG7XdDq/hiUOfBZYkDFrUSmD6n6t7tqYMzvp73r+Ta1Rs6YWAMxdQjYKr5D
ZTg03j566H241GLvyAKJIM7wBc83VxedJtaMnk2nsus72+Y1NIYLMmRReJ4XT11zjauHPggv9fJb
n+xkCeKP
`pragma protect end_protected
