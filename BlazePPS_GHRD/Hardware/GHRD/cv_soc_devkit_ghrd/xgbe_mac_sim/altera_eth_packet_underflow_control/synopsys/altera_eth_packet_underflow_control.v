// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HFCAKYO^D&<.44=.>3A6^$FL[G>-/I-ZDSHU"W?6.N1JW1D+:';/"/P  
H,0DM6H,'=$8%WR]Y1YOR(\ /IC=G[# -$R!Q%';&)*7"S/%#V67:@   
H3C)[R(*MK"HT.='"^'\F'@PMRU3[3N95!K='*9H6^4!F(SW=8WQ[EP  
H\" 7U*"[)P:X3=_;W97^Y&KFX?@FSK+<\OX7P092I&E.#J$(QEE66P  
H $/3+4P\=TS003DGUM^R-F!J7MWL6P<$@-?\-*-?+#)1@IG->+]V2   
`pragma protect encoding=(enctype="uuencode",bytes=3840        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@/&/;&L/:BM0";)@&:+.N'%\DS#M #.5*/ZL2'&(?5 H 
@O:CH@)KZ_+5- 19=%J7%7-S^ 7V?<G8F+G+F?SH;#7, 
@9@F&)(H4DY'J-%ZS7'Y391WG[].DO[IF9-B1!C?:*8P 
@UG;7#:6W/LO[:O;E<\0=T<RIDK/PPB6:? 8M&X5G&V\ 
@I58%>3[4J5G==HN\S50CM-"A8)A%.#0F_RB9O>Y^'XT 
@:R;*F1QLX=<?)4ZQ'*;]^NW!HJN M,URHT8TEM=CI-D 
@'^S51=JZQ8]NV8'>$L,[N947B8PAJ2?>M\M%FO!OOJ\ 
@&W$.:9#<(Q:M/>U81IMR$56V %Y%]SN=;+A(XT:0E>$ 
@H>'+4'\;O8#:+6MLLKAL(P58>S5R+<+D[Z99N[!WFW( 
@[)UG $.F#!D+&AK.!=UGFU,:>'J'$U3!YY=_Z6)Y3;0 
@9[Z2)!ZS(7KK9OY!W$\^4_ S]>4>\=Y=G"O-J]^HD1\ 
@Z8/$EVF,@=0,:JN"W$P20S]IME#M#7.[C(DMSX;H:;$ 
@.*OX6L'^G[A#8P.7%E\\'GY9@I:RR(RZ8M8=P#X1!90 
@$VI-UH[-,24Q%^:Q#0%(  0D\G!Q5H0(-D=JQTH*,,4 
@*<<64<DTY DE5X=/PLZ7DCA  ),K1DZOLP".8%F?X<L 
@WMD=.CNQ]_'235:X;Q^Q:V94#1T2-N^5G7LQ:D..S4H 
@")-Z54:5'?P%O!F,RQ'&JQH[RU)D,V7"(-_C<OQO#$X 
@\EB]=66>Q[FSK"N9QXE:FL2G9;P\96CR_JCK2^5A&PL 
@VL5[^*N*@YR.(EM?A>J:Y@PZ6ZJ%CGH3I:ZB-#A4W0T 
@)!&,.(VCT-M+><Q<B)&:>Q^Z3&<Z='L!N@KXWQXV*_T 
@@**/0& O-=;&R2]J$EZ"7OD_Z8/#_7HFGESS00[P &\ 
@*$0?HKYJYK_,& 6;B?^P'DGV.3-?R5#)U;$TM]!P[ZD 
@<"@)H+_,[Q0+X](2A9PR\ZVG$E3O,BM<XR,A%F#Q$C8 
@QNG$BW/"8=0\ U*=C;/*K*#IB*!W$G"[/8S.@60%=-< 
@/RY6%VK4 L0B^CW[GF[2@HV6(M[=M7A?E((TN"!V-'4 
@L(V"%T_BXK1H_@5LK<QS"?>3E<R%8F)+<PEDP])R=C$ 
@V8I&?;53<B4K%^*[#6:&1=\;+SK\XS+(A+(M#9/7B@  
@'E)S4X?SC8-C%)D)CN[9:DUH&=[B44LS294N26>OLWH 
@)E8-PX@^#E8((C#WMF>I^6VIM@,&,GB_P,L'YI:D7MT 
@@/O9R%:MSD\UJLOUF9-*#[F;E2REVQH@)*\-1C$)TU8 
@+W*OQF*8(O9=6F,1\Z+F\I/ESX9="[:Y]IXMBE33^IX 
@I(=CJ[?N3S[/IB#C#+1"*Q_V-YTXNO^%Y-U+QRTZ]:, 
@\=_.%SAJZM= H$;D.+.LW4=%^/6F14 ]P!8Q5R6FZ20 
@JFO6=H/PI><O JM\\>N4_U8OGK#8].4T$N6S'4A6K&  
@OMO;R6;9A/N_R4:8SKZ%TCJE*OGC^L,O^+AFW,C=TM( 
@M-!YP1#?ENUCY1++7Q^&3,.VMVKW0T794?IID3.9E_  
@H\A]* JZD/$;Q].GF?*F:Y1;WZQ$,&)G_G=U+3&G"=, 
@34<EF5BW'0%*@N"F7D^XW#A3YLE3?4\#$+]=:TP-5$8 
@9V+)$R=7 H-/=CC^I*#N4T.688 %:2JBK'3N:?D,\F\ 
@NSW21BK?SZY30 )8&)_P,FO%YU:Y <5B'$J DZ?AY$L 
@-7/D%DRYASLL3 J3RP9+-UB?J=>.+6GHA%L/1=^/%:( 
@, ;O@N!PF]V5XS5G6BF0 OFD8<1U4W)A@P\1=4-V4?X 
@?I>\(G+N@XPFHYD<R5LWN8&;8Y?<D41_*P2NQZ="K X 
@[O3G.VK?G$723!<](7MQ_1$[;FPL_([@INHF4R^Y53$ 
@A,O*4,.;S9Z'S;G%[O!E*>>+/Y9<ZA/CDE)-[ETLHGT 
@*"F58^E&$7I&^U]/(A$S4=+.,-:^" MX4=:=VW*I\1( 
@=[/4I55I02@@&D^DZ/4OHV_@WI#,,I#8@M0K@#^:B-$ 
@5[[S^A$30@);D +HMFEJF%@=IO?/TX[94H 77&"M38\ 
@#A?R[7H[T$3]":S8?'KQWUMZ[]J]B#SCG_2N 4#]7#$ 
@U5?A$-LP<4%>UY%BB6BV1=\96%DQF"TLIW$PNG4=2AD 
@;0+1P?!O= _ZXJ7EQV)KR:,J'B>S[&!0)/5Y8"+*'+< 
@]$"[CT)8I2*S=UN_5ZQ%0R*<.A(UJ(T@@M)(<CA89/8 
@"*Y)9JC+^/)QONVT&!886"HUIND_I&6"FM^MAE1]7=0 
@@OE^<:X4&>B$0"H5Q>%&M2C.*"LJ7Y8M*"FU<*T++P4 
@DQ(4='2/IF]=ZE!(>W T% W;NDQ_P#D-05T>:*\Q%N8 
@(*J_&>90,&P$MI*W89X4$I:_R;$%]M_#Z^C76K0_=^4 
@EM2%4"'Z:>_F>2$';\JM]A/@( _G*@[U[AESYHS. F( 
@O0=4)FS<%O<E,%@.YUSRHN$US?,Z]NL[IK %_29I-&D 
@/;FZU:=&'9*&<SMT85%N80X<&H&P6D[BE=@GK>ZZ_)P 
@5H1^WQD)U17+H4:9%A Y!4.S91;(_4]140<DHR?O\%  
@R (?* CI):G*3:34:AU<2OE1,LVW^Z'CAYE_+SPD40@ 
@3L"ZQ C[2:ZP>Y7?)(5W_W<_Q=Z+C\25E"2R.>N$:[4 
@+0>.$+G^/Q3E5"#-+_Y+AVHX]?%.(B$IGA/L=)=;8D  
@@[@X1Q]AVZTMP\ZXN$-D;6OJYO3"=G:]+ANP0O(_%7X 
@;&@(C'_ @(*82<.NP7=5=5Q]$)CW^@#Z9F*Q7,[R L  
@ LP@>)"F]\("[+M]&JIY]]O,FDVL=W=ADR<^B+DNQ=4 
@FBN%)JBU<=/Y_IQ"3//,(?_F6J37@NX,T)MX&<^Q9]0 
@Q6[PE4H3(LENIAL(ZZL=Z>9TFJSL#<&U+H8B/%7^-AX 
@',8V%V8XTW.J]W%7/ \(0DF7ZL]G5 6&$\Q:06K!1UT 
@UA9!>D>T6VD8P$SF!JE4?N;+5U*<\S0.; W<U@I:F"$ 
@!?G^+'0=0%$["7@3\+%99;1I\-*XL>0'X'J5<1T,^QP 
@4@;CD-.)/!3&QA,5P:1GPXX4(QTZENA:O?HA8[52TC\ 
@A@4YN-0E(1E0>VX /8P'0;B+C6N*_V1"3'*LT=W[FTH 
@R[,V@J&5('W]D%U%MH7(9J^)XN.#H5$SD("23E6P\"D 
@\/F"V2:DN?@Y:[R]/>^KU;NI(HPWN@XOA-'/T!?9;ID 
@].V2OBOW32@Y]\+3-<]^D$DIX-<6?(P)GY_$];7:(5X 
@.-8FH(P*5^KV+0?V?FVRY4A0,?M/VF,M)>T5\*(=+8$ 
@)QNH<OG(G%VSV@^7$1B@V!,D=4,$O9L*G]2&L[ WNFT 
@;?2NCOL.!&?!$=<VG<O4Q+ZX_/8WP ^]!O&PWDNPSDT 
@&14E^+>V=M0Q!8#@Q0:^"3UT]9!I>I1'Z6K1<&]\\)8 
@\B?"WNRW%EXX'B-I/F(/&?#X&(XYEUYWCI$&0L,O>R0 
@&N\J(G4MC=)0?J]F7:P5*_%HT%W/C^SFJQGQ2\II[HP 
@BDV'G4]GB(J5_M^+.[#\_ING>R]5']B+V#0^ISVH_U( 
@Y)9B%&69R91]2Y4V(GHS_O4ID+SG=7.H 'TCPP5C#;H 
@QN"F5^&.;4H*,4 D<.0OLY,Z<5:D8LWK&GR+\6<51A8 
@:)K7]0S00@9RU['W6SY+O7[/(XB$M5:C6]/*F)JL9TH 
@OR95! "]GA5+Q[$Y4UV(UG=CB"6L.<>(2EU5%83FED@ 
@-J],A7>CGU NF.>H*8$8L)E%Q$)GQ1U9_%4N5"6*O]T 
@QQC:NK'JO#2N\L5J>F?H66S/:OM:L/I"[98V6^ANV\P 
@&,NG(;BTP7V %RF$$D^!H]X0;SG0=#OTT.BHJ^N![:, 
@3MKJ]?52I6U9WECBP+HY9 BIM+?NGE%O5J+#HLSJ-/\ 
@/MITJQ3WG6#VA7CC;>\C8/K7R9'QR$K[3T"7BT"AT5@ 
@SO$E#8)!E-#5$3 ?+-1J(LL-4)JJXI:R2TD>GK>ZT*T 
@-PRU5(86C=X%.Z2"ENAY!?J(.)";/(3Q@\"'\E%9B.D 
@RW?4%(XS]9NBHPMU,P_HO,'W]TGF,UD;"_Z2FH%;B$( 
@H*<+B?4XT#K0,7!R^S'WU^>5%J..Z(_Z5K&?<?<&X&D 
@'9=9[4WI/;X2 <NV9Z]NS5HNNP5^R/7J#KAINP-7>NX 
@5F\ 6.5H.WDO->Z87;<HI=2I03CAO#78#Z">S'5"'5, 
@S-C@N?OS9?H(EM_CB."'!JY;G+E#CVS*]2MIRSK67I, 
@4@?$&@OS])?[=GQ*APJX9CUZ\MKG/8&OG^A/8M7\\F$ 
@?AFJE<Z *,?#+0A:O74B833[2W$'M@QW+E[\R'+:T1( 
@+ PU0$T$1:W(2VH7 X%^N9$9L-^$P3"/26CV:Z+F7]8 
@:D<:DG%V:"*Y7*.YXWN_%J-X9VL^@O0I7VHEJ-^T6#X 
@CWIAK U7M0_POBQY=?WBO;HX)AT,519/LC+WD5)RTL\ 
@780P'SR+2!]$'KP1H";E45/_P4&YT.E,*DPZJ8MZE5H 
@X-=JGX-"%6;*3_#7B24L*T<(""B;GNQ-LZHM+7-YZ?$ 
@PZ12N1[!/)NP%Y\@RDQVQZ:6?(27QQ"\W2@@UI&[,M  
@S_'6(]NHE1=%_KW7E<8FOW##XK#!YG!*%XMU9<^KU*( 
@P7YA'?PL\/_ \NRQF3<!PK!MY=[0.K##U3N2$ J_:YX 
@B#A))^O)E7<^9C%Y=O6+?@ZO[X))[7O1ZMK!M<"NWCP 
@<8X-D($1J[V:HGB1G)X7.8HDB\D8KX(]%"O&VW4GG?( 
@*;IL\ID1#W!_/KET M)XBQ:/,KW_)JB(%]Z-PAT6IH@ 
@03$/4O-M8WDP37:Z-E]]4$I9G;IF!+PNX$0RPR%)^98 
@R*1\KWD/#$5/B/HM"A% ,9&69]P-3X=91HG=IH@N(K$ 
@8+FQSB@CX<]3T]B](DH6J/;ES@,P_+30PVC4_!5#Y=  
@M *T&*H+Q-^9 RQ'W'/=0J3.K>$U7?#<%*D*@K+7@TX 
@TP-1'*RB47[E,KF-0 EGO>+U%9]DCT;<%J[[EYZ>2:T 
@&UL#FD,?OAT@/L^8W7I@D5@U"ODL_"K\>RUY]OIZ6YL 
@!RK>'IS!/$Q"V[U\V2+ JP6)<!JY<H(9/AS$4VH / @ 
0RR;!3Z_M(851AU\1FOP6GP  
0AT4G1!@)*TC9FNW.3O0)F   
`pragma protect end_protected
