// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H#)N+/T2P^:6N%Q5X&Z[DXT*.#?ZF_6AD<YGPHFSVFCNF,#,&1U.S9@  
H)ZQ,QYD3RR+R2A#;2Z(A%US,_0', 0$7JIJR!7I2*=6%]VV-YZXDC@  
H-$5UF$T:SZ2VW@L:! E,6@GT7_%T!EP];C/$-;M6H\@+<NKBA<ES.P  
HSK)O;?L16"(IUP/BJS2;OF"9IHW!?];'2YT,I%,*Y$,O'!?$9(?95@  
HGM XO$I?:/Y^03+ >P\KLS^_;P]9.7'4_]JYH(9SS#HV0$LHQ,&VP@  
`pragma protect encoding=(enctype="uuencode",bytes=5712        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@LC=I,'Z00FU%%23%4<K'7)?[-F >O^+W?-;)]2]<2LH 
@[:Y;$PW/VYNW4UP2BG4M(/A/,DQ(FO_AV[#F[)W7:R( 
@"V2;3B'9HKRDXF;Y_#FOGMYCENGR.F-$TQ5?Z('30"0 
@$F1ZM6^L,DR+9'_-!=^$XA$C\)2^O8Q^MYP\D$!]9 , 
@5&CC>#N"IR'N8>]5Z?@AH>1P3K"E3-CM_M7P7<ZHG.( 
@U]U1?L$QGN ?8K^=XZ>X;NQGAIB#54 'NS@SP$@FZRL 
@K(*.'$=O&N>@_OPR(G,-NGWUP2)J"$LNV3H[;2M$-GX 
@91=:@UKQ?LQ:I [C2\G'=4QQDK%[7_BQ84TOY01=@T4 
@!S16]>B&2Y2KQMY9%Y&BN9$5!:A'$:P2N[T7H=@<'?H 
@NC44O$B ;,;;4"RRR.B3U@#VE.::)[T-!SP^-8DIV2T 
@)<#RNS543QRK09ZJ]C107B!A:GQ/J]]Z3Z;G=,[?3+4 
@_7,O'DTT%FC[^\YA)/)R-%K;/7V=V&-NBZ,U &1]10L 
@_9O)%FNWK-XXK2%Z]G)?H<')U-X$U#!?9MSH*2N\S-( 
@OF<"SN%R/N7781P?DV&#=_2?&[H!2]!.5"**0".CK/( 
@&JDCGO0L$ H68 -"-D_KA"Y!^:]+83V*0]E:;E1.QQX 
@890]J0%\L%NHN3_S>_:S(1=D0A'",?54IEE6Q)T7]M4 
@7[4T*!N@[(!1]J]=_CHM>;%$F]@2!,$-L4&L_UIS.24 
@ME+%?*<0->Z><P96Z=(\#F':OW?IRRN^\2FY8DB6Z0P 
@&?<_."VPZJ$>4090 ]*9("=.ACV(HMK*6WY@D9VEL9( 
@U_P)5''\MH$;_"W3<\%G]]NTI)KI!M4\,WE1!LPXB'  
@%0+O4X_)UGO!D"%N6F'70QR<KBI,A$![%72F,#RH(&, 
@ N(A%G5/DYS.YM[W_AZ,\W_LQ6W">AYJS/H!-/=W<LT 
@V^*YE9%G@EY49Q"S$]&U%M1DY7//$?PA1G0U\<) EN\ 
@4ONW)/-PZOHRF>J?)68O<;4#KE?:%'<BG7T_VJF3JX  
@ R3<%2Z3?L$(+M\TB63Y?!1$8IF[6;X(%J"*HB3/EQ0 
@"RB/%1="FC4!WV"QPGQXC7G 8$(41A=FHB\IZF4B,Z, 
@>@YNZ/D.U)/9PSWIJD7_*4;5J =5/D[G F? DUJMX;0 
@_6&RH(WGQ5J6NMI$R85BZMCN/>-@0CC>)M"L:'XI2C, 
@</>^EK0!]=/OS&.NTX8O*2QWRS_6IO'/6YH4/TA+<$T 
@C(=(=K?[:5C9>3FB_2-#1XX!4/(?0FN-;\_%45F#)>4 
@Y0YBQ<39,ZQ'ZXHL&6P]EV4(T+KI077Y* !%#8,$FO$ 
@[#FSTGH7"W^?KE^@9TB.]NYR7WHK^D3;0X[3[M*5QS$ 
@W= #JZ8[LR!MDN?"Z43UM 5PV:)(UUB$V38_VR6FM<$ 
@B8#V4-/P1*51X]0&I(#--WA_S>?V[L;CK][NC]7,@18 
@OUQ-6;*R]%<$LFCNGRV?O3)0@KA=( !HRV&:T!>$\!( 
@9,%JHNE[FZFPQC4A6;?YMB2!2^9_@]\%"!]GR]8L-(  
@#J2C$Z^1"MQHRFYB/P$VW-5'R/Q/; C]^.+W>[*9+DP 
@U#T%O2;?/EPS"YYUNDN*88V=<2<\S:Z=&QR,3:Y<Z_\ 
@L6.W)1D)0##,#FSA^8%IS8&-F=TL>*9C]Q^>."[ M%D 
@SJT]#;=8MK2HS&*0=U,A6A?'Z[!DBN<([VK\N7)W+0@ 
@*W")CH: 9*M Q,29XB8MS84M9Z( ?H'R^^/*=DG6+%D 
@&PN"X&^T6[.*5% Z*Z#;%2F-/^B;Z![XU'H+L-8\9VD 
@"%$$K0QBR)RRR)!%>W[P)_YI'F!%<8,&D4PRK$B9!^0 
@=&B*7+TKQ#"VJ=^._'^P"-@M%UCF &QPY]*[8JXUH\  
@$"3,[C4?8XX]F0<N;L2$=T+RH%]Z7',2>([5WZ&-"UH 
@K8I[&O8LBVI@R/@S#' @4T+?M]E34W^$/P:V4H6ZU.4 
@HT.OPN]_H>BKCV#+FK6*(=31>=4+$@B3.Y(O):8&Q9H 
@1@L2R2 ,VZ"/,>RY>(HTWU23)##XR]=VG2KBH\)/56\ 
@/'T;AU4J< .R>;!,5UP/"\JEB.][A*O9B[9_W3JXWQP 
@^/]>0%)[$D'N$RV]26&,-/_16+Z!*(7TJ<R+)^PS C$ 
@^\3ZS4LGPM+K]6&T8KC ]O%J<76DL8YXQ?6-<,ZDK8< 
@02V $:_>P)0[5IF5-&$L.K02* -)0TGD=U!Y5NH46%4 
@U#%0LP,W*%(%5FJ*F*011I]X&) ;E4)\9[HVG"(H,S\ 
@X2#9H-@&S0WI;<XQ6*XZF1I4_/C=)KA(;1LQO8:#UT8 
@;GJ!.Q-T!=S2V(]2S?-CP^1&4JX8:(-F9V.>AQDH'-$ 
@5$' LUE$EBQD3$=S4-YZ -W+%R=B1^J4S6#M#TNL4]8 
@( <!9R[GJ&. AM*=<M3KS WNN\<&.V="A24I3D)MO88 
@WI$!6O8*I]%2;:TB.X.= !;OLT/?A O5)71T!:N+4WT 
@UO%,X"QL!%1A-)+UE#()A;J<?:$JY1A?12$#5!1M8Q$ 
@RH/+/ 33F+81,W%'"_O(GL*7]R%BGSQG&J7R]'GH?), 
@EIFO,WM9&EW0J$QPD_>>/<N!H1F_+T+$.'6,*/=)A:D 
@5OXP@K-G8L<H(S^JLX_F-_^ONEQ%H-Q-2Z=PHW];<C4 
@; @*R_\S9K[QA6MZ</Z;8]IC9^(3L8_@8GX_?I;.EJ  
@$3!RID8U0C&-+  \&Q?JD,F!3"2ZQK0\_$WDU(.1.)$ 
@JM< >A+:8WX0ZI2! YJ5/Y*.[8NI&Q#_Y/]ZO]7R1\$ 
@:22<=<?796L],UQ[AZNV7D*%^8U&_Y01P&QAB+]GA2L 
@B:>J"5#)>\J\!8T*K:SP1-9-Z:?X>EFW25Z(_,K@?X@ 
@$-WZY%FH;]M LS+#C$*TGC@*.!I%SAMV+<*@&?Z(X6P 
@9!68/M'6:;IJLK-ZA//BY([HAQ==(G</<3F#/KKRW5< 
@&9E0VY2WP,\9:Q8&OG'2TR9S[*&6$4A/=.!D'E\TAJ< 
@@:$54V!3*"N6Y6.P 7@)0>BTEKO)*8GA0GD!RSY'IB8 
@D&=L/-Q5$H+?K!E*PB?^J?9-#H]UYN^GI'XM<X^FF_8 
@ LRH%4 .EC">]:(M;G,4<R'9GJX6X$E(@827'D#81T( 
@G0CV["\^F/< A9- 08*%@8\7TM#X NN:\5?Q4&D8!E< 
@>JBV UAX9\*>K=( 3;$;.N;A4ELZR_PF&Z]><TA4KUP 
@#P?_%<$7$/_-?U5\*\+;0Q,@\9KOQS0.96495">-E>X 
@8@4/RO<&2&H^3ZV3=AXMC>4!NC*5>O[U5.$="W*3HW, 
@")HM%FWIO7.< <"3^ IYTE(=@("U=H9*Y[H5HSL_-IH 
@P@QM[E6DNR5"49>!\;JYMU ,T"YJEZS+:%@2TKUFJ4L 
@5Q,C:.)B1HN2EQUSV.@+;%SV#D5[.0PM[END>%#U>I, 
@:+O4J_3/7(PUL(;1"R\5A481O??=WSZ7W%W=10X2^3  
@?D$[M :2'60^$\1Z*72D/3\RI11RMPTIFA!M /C-7E@ 
@=1L%&&/I/"\Z_BI&Y5GUX&[!<H>)U2^;"!$KC$J]MZ< 
@ZJ%6GW=/&JAZ&T$CD_D:DS)5.AA+>J4L'58? SDD6V@ 
@5_V=RJS$I.5'L:,4+B#!CE/5 Z:,5(!^N.,6X"7,4[( 
@ET], [YF$ X"G%N*=D4WA1]RND >*<^3N*+U/TF.'[$ 
@N:S_!GD-AKW%#S2_"0=^#?8G)F39QV*XAQ)6'U[UG.4 
@6L,WL@UCC7*W)T75<JF<PZ_TBOBHJXU ],-Q8VB.D", 
@U 6GFD_B7T4=@OB3SSY4$UA WE!L97KA-NT.6.]\[%( 
@2!:QJ*/+RFX-@Q =E)]@0-;6?>C;.^YUQ<GJ>F/3D9  
@[X3F=*E1[1Y1Z2C6/A^T5K%\$BC<V@>"..XSX-B+EB$ 
@<TJ=MY"C<$3M.,2?M8TGRN6M/]BJ)21!6!_!5A!@_#@ 
@FY5=L@^CX/AKNA=G0$T[S)C3,'30D@2ZJAMG34GM=)P 
@ S&AL2I4%(=5'8&"L%GK61GZ#VT*)#3XH>?L1$<=K?8 
@H!6,Z^PM=R[26.>1(G>6TVH8NK$E=+>M@_:.\X\6X%D 
@(NBY/W:!&2]4+@*%&'B.L% 6&PA#QBARMH:@]5*+U-  
@'.CU% ZY"C>^+_+O:A*UU#(-2/F!):$A*"&Y=ZOBAM$ 
@QJK&E(-1_$FYM.QZZF</ZYV.T&3F1"]6"QL:"!U\F-( 
@G1$%6S\[:7G>YK RN8PF^==?5K[!0*>^-!!Y?SG=YD, 
@OFR$@/L<P:GU@L&>WD"PB5DP!;2J:ZO(Y?H*=)ZJAS@ 
@*"#]=\W4N4E7_D0*!"8FA9XP,O)'SJR[G]BQN!3IN>L 
@_VSEX.B %^2@R%!Q;7 C56(4\Z?J11_Y#+&S:5%^4:D 
@,2S%@HG:62 2Y82-WT]HC?X?YKJM/Z;<>'V90H?"46H 
@#2JV) M7<NZP5.":QU3+S">"C\'7JR@Y*1#'5/'>/GH 
@\'JQG<TRFYJ_]; ]!!&#G-:27)011(]W&$E(R]F1])T 
@ST46:UWG&SAO/@&_IRT4Z].UGGNS_O66W8HBLV-*]*H 
@H]<%6:;5/X?J"%ED&V!VN=R59!MB?R95BU!7'\ _3P@ 
@#3SA0N'=VCW#,==MW<K](9EI0?;R&R "-V<X5P>"W;H 
@</V/I)*^>G/SP[(+;N#DAD?H-&=%:"UCLH1L::F<6AD 
@G!VH,!_J.$A0(O+A7LRL^P333]F;Y9<6VK:%X*??$34 
@7-_CX2LS51\H/LL;:KO7>O,K+4#'VWQ8KO M0Q'V:!\ 
@^Y4Y%!XHWV3.W):= )8:*9C4_[5.^>C0\I.UW&Z\-,L 
@/G&?U6LOBDX&6W^AT<4,75KA_:D;G.%35/YZ$V+(*!( 
@<*D:$F!1UMXK1(ZKV#37T'>(*$BHM@W=RSYBMJ4F?"D 
@NC8*N'N]L-4/T 7F5K8B,I+US@5ERGB>,8S II@#6JH 
@U750XAQ>%9T5(V&1>1Z:/'!0[;)S%SF 1GM*Y698W<, 
@M)_KYBFK%O3_K4#(*?>L>80H@4PG;F-"%**%^_L@C\$ 
@&3=A".U=^6Z*%"#B30!2T#[V4U&RUH,=9BFGK*J2_;P 
@+=P]9\=!.VFWEF<;S@MOB6'E8=$BFDL9"+N3;&C"LCD 
@PH[+;U?KJP,#589&CA9/XWX\/GCR#K?H4EE#F$#%>3( 
@KG,U1L\'\& X\G0D8.34L$JQ<A3RHREX.Q:[->=(M!@ 
@62,$K!%2PE<F\-<&35/<.V,I-QE$44R#+<JJ+D8AI*0 
@O2NZ/$07.(!'[;L)/G%,5$MHOVQ,GS=\EI"&$(A=:+L 
@&^/KY\HAW_NB$5=T!D='#7U' H2NY&[BV/KQ^?*Z#DX 
@A,HPD,],5!-=,3S9!!D#="!6VMYKHG6<04LK>M;[P5L 
@2."%P9/C+XO.<BUR6FLB;'9VM(PKIH7>N?JDAS>5^+( 
@;/0=Y.'H8">&2B'4F8[1P=R%@@2\U/QT2.!TW#0==P$ 
@=W;>=;GA'R(3] 8+#]0I*[.(V8Q,2F7L/0#MD3O[NPT 
@C'29\E"<0*(-J>4C1.S#\]NG8H1J.I54IL]FE?7\\ML 
@_VE?E?+IKO;)4+MOC3H7CMV"O?.OLG5H)Z[A2R=3RJ\ 
@2 XD1T>V<YP&"&],,R[QQ/2$B>Z)4AAS!2>82TU@4XH 
@M$C@VW 4T2E7*VF6&<)HO]KKR%*+]%A*,WT3+R/50T, 
@E>^09;)8\YYP!T^TL286"D3XXLX:$=_O$I7)L0J'1)$ 
@&MG#%J<$S( &3==AMS)C<20444\T/?CCV+]/55>)G"4 
@CO%3(84$&S4D!.5SV3!2-< #SZSY/=]E8.;(FLRK^A  
@9(D_R=>@.Q N9A!:.:762I1BXS[D#\B9(T=%I(4\60$ 
@!P?-[-/X#H"V*NM:HTK),3#(4X6%\OWL;55P.TU*VT0 
@ =1#/&2M:'Q&"&8H (1(H&9\&/$GQVLS@#W^X8%P!\L 
@$#&7J5L%NNP+I]SIB.N:,7A4CO"%>VK;QO#18@$02>( 
@TJ,DSK>2]A:Y36PR F ,N2.W$AO[#D.*=9"^?&X9.:< 
@7?Y/@#$*AI,620U>%<BNO:AK.5ZOJH/D)D;EF<W)ID, 
@FP.R+'M'UIK8NS1ZRX?(?<K,[^2T4O$+8IQ .0^0KZ( 
@DQSV*@FX)#RG+!8)U0?1FZ%/]MQ71IQ2Y:?;=W^.27T 
@+E.U)7,2KVZ(FR-0EGQH)V?V<ENUZ+"=B%$(>2P'0!< 
@"D@@Q@BXR9QAT[VD9R5;V1F4:<#ONY'=#'U>C9Y0[0@ 
@< @@LNR#%(4:5*KR:1PQ+U2C>N@6TZ[L!2Y+\BOJ/C8 
@PMR3QSW@IR+_K51HWQ,#(5= "@9SBQH[5PR$3%)Y5R( 
@:[KC6-M]9SQ0C1$X>C,\O%9K:0KBB^I!L.!<G2Q+M+@ 
@$O7-3M[K,3@[14ZJK*4:^EJ\#A@^':V"H'R(]9D<P*H 
@!1*4I./]1?&97?4Z?7B$K8-P_7Z[3G&\^8GY\*2#JX  
@S>]>9;P< *F352H"W&=R225 (J-4S<8W)&GQ*^&/0<X 
@NY%B/=%S\=_<-DIHP>0*QMP0<6RZ:H+$!)#"8],8]7( 
@03Y13K1TQ$^WLPDW,<RI4WZ[C7BG9,>L/#^?G\E;<WT 
@UQI4LBGO8B-$(:LZ)^&LL5U!\RKGZ 55U^90=6V"X"L 
@7$&X$Y[%2&<T<4LE>U[2:=#_*$?(BC0I7-56H/$]F\8 
@?$"RD)M!LQ%+K^3W]H;IOGD6]H;KJFB9KP$AR6&#A4, 
@-\6='$8!CT?YPA-_?YPCX-)K(XGP2[[PKB862+>H2I4 
@4>&UQU$8'.N]/]<-J2$9K1_S@6X]D2G9P>=)YY6QO:\ 
@JCC!>["24?Q#]3976;NMIYIW'^_/=:][FS-$IG@@*@T 
@G)HRWV$;GQL\Z)HR[=D+-#?,L/"^$U&W]X[BV47_EEH 
@D?:-_H9,&E,T%".LY/GFS8%-P,N%O4GL=^+ 4=Y-(-T 
@/LLR[R+'4]^J/=X9). NU/-OQF_D>R0I8B,#\J/0=W8 
@E)JL<Q>ZQZH)I0&@\1,V>*5,W+ ]@A15!*0\T$T,] \ 
@2[<#+ LW >U;I2C#XR;ZVZLX]$F@P"TH;;#D8BIKX(< 
@U\(&$T.?+2U8<B>OBZ2GS;\[*N7RPUU.@ BH*"F:#=@ 
@TM0SR[XV?RHZ:>1$/7!>FJEF\P>$N43BK:WY%(LSNZT 
@[FE:#__]QCAFKU.ZR&_JRN'*H*UVGTZW,(M<,/Y1_'@ 
@'BYC@G1^MVD;5S_ W.W=!:;Z+]RCFO3U,S8W)\MN$*T 
@":RE40X>TT6@VP@%-R_!KUJB<-$9NCDV?+82#0"C$F\ 
@TDG<I_/UI%/7^.\</W;1!.S8&9N&-%L]0B(UYE2>Y'$ 
@27&Q$S@@T_R/[S:S\W"T%4R/'MR#FBGB6^I.X;)!CN  
@&Z,\0X./FE@&W\S\F.,#.IQA]]Q*71"GXA.K2:ZF?<H 
@6IM\W(7HZXPB, GO^9<L&&Z>CD=@G**2,,N#^J\>35@ 
@(,69CE?NF)\9%@R#DJ_C=KV@BTM% +?OQ:4!3ZMGE(@ 
@:I!]'16BF?GQA<$S),SB6,'BC!CRNHO(W!&_6Q??"_P 
@< /:'C^\!&%$*:+@6D,;\-D.VG*H,P6M]V-TLW',T>L 
@$<9'" +_)9OD^AWU=;6Y;@8K\6S#\A]6#<SV]:<7EH@ 
@SUD',0%#<MS\(O1$DP [\,\??4NZEYB)^IM->8(XCE\ 
0HP@6DML&$D48A!(V_9W6 0  
`pragma protect end_protected
