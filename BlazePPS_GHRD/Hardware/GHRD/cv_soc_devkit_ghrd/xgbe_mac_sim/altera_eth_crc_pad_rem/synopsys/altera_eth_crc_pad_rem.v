// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HG^,XFX]E5K<SV9%SB*VQ0S@,"BGG$L#/\#$*C"']NJ2!GM^L7;G9<@  
HHCI/ML:MA1\U8\*=I&0)V/!ZF2J%(LQD.FK6@#>C="H_G4F&SGN/1P  
H ZY!0'"9Q')V8?*Q_70.<XGS4/ TA=S5G^Q65^>4'?:U,$2Q E:.=P  
HV/&BF1A !&G,"O ,^SO)58"<H<="'(DY#7#*8@:I7&M1._3]J)X6-@  
HF?:6#:M<VXUSJ#.>->;_0L&3P]#,0H?YK1@B8]CB3AZ2UOO.<:9%8P  
`pragma protect encoding=(enctype="uuencode",bytes=6448        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@=$E0[SO6)*ZYX4<Z%3U*$O\"-L4NB'5<DH'#DY>@C38 
@V<#8CWR_$GP!M7+IZ;:)^\(BI.J;'V ?^&\=YB98*9\ 
@[C54 M^=:9FC9P&<+A!0@#UZP@4;K WK:)1YG_S75-, 
@ 6>?U#P;/A#4CV]BJUZTD,<!]Q#O)AU7NF@ZU<@YCAT 
@!L:5'&2.H\(;.]:N(6N8S!$V>.B_09X58*G;Y"KKZ14 
@.UGO*R*L=,D(Y&066/AF5ZLBB 7XM@C$=3UI\FCTPE@ 
@4> +PIYHCZP_CK=\U1<#"/$PMEU"L:4[@/%S<^9X:2, 
@O^Q(+^O3-I6,<SI+\EEN&2ZFJI7[/T.M9;WYMV 1MH< 
@WN.B6LTQQ[<8Q#ENX<W3AXFO_J65"Q;-<.ZZ4X$L'F8 
@[4G7)6P30[[H!AM\$@,'YL<"@._5JPXZC^3LK5S6L*( 
@0G))4'.)_;< L6/<6E<1_M<E6,8?M;0/)1#WWM#<J$( 
@I5*]MSNB9%\0?5F>.0+$5LX6?8\D4SZ)VWW[;DD36E0 
@B/8X<MV?T&LGQHQJ<\FV3/^(2UZ0AI(/OAS/5N5NL:T 
@M*] 7G20U*NJE#CY+Q277>=WT;GJ:M(+J>0/:[K<>20 
@I64"4/MZ1,,&J'RNW'VE<S].[&HV1BY\;!FMG9!QCZH 
@$0U"Q*QK2/VU[;&  &;[M1!W#U8"B4 \WJ\7,EN<9_  
@DD2(\I=EK?938$\B.Y+[@U:YONR".U2\"=6E,H,K!ZH 
@DA49A&OL/.>UZS]DE=&ZR-]"\D>VRQ_V;_'5(<BFWG$ 
@U.V?MR^-$X>/FOJ9>H! ZO#(9C29XA-S6TI4"X0V>)< 
@\JO YBC4/.=:]LUQ=9-,N(J/ 6\?CM9,@&@Q;BROC>D 
@QE%54$U96KH90DO<YEQ6?";S5M&;Q?J=O(W"U(8[+B$ 
@0,&]$0+I=$K#$<288?)JU%[<T!#XBWX-=^GLU,7VY2P 
@(6)<J%M8\]HQ:W2A_.-TUXY0_7N0.'37[]A6_R@<[KH 
@FZJ5*VDAF):B[U];->)QCT-*0(W5G1"B'/H<3;?"^#$ 
@C[&Y9YFQ38B%>X1/G A(;_&J'V,#>[\R6/TMU"8+.V\ 
@W#< #.E ,KZY2X6=9Y'9R)?NC<L@Q]?!?1_73'*'5DX 
@Y\A5FIW*Z>B9,-<U@$#AQ%"W+=9'OQ!>$F,(+89/QB0 
@_-0*H<.I=(8JO<\GB2*EC+X7JQ&#Q$U1V@>3=\&/!L@ 
@*=F8+7-[245J[Q-B#<%Y<-)YY)][F%Z53WT8:7AJJ D 
@(K$N>$B2K%CNFY^%,CUKO=ZM_?L\Q#@^0VF.=<'F-B\ 
@Q.!3GIG;Y==Q7T,[&('4$*V/-UQ6F%-'P$RHJ?6,3=H 
@*R,MGU">T/(&*V\01ZUWZ/IW).;KC('3/Q)[IPFF:>T 
@Z5OV_AW<2R7>#@\RXC#'FBVKMN__Y%<'C?;$HEJ"S)P 
@->YRC2*6-!>]%+\54U$65<2$@1JQ0%J*K]/-[;<T3,\ 
@)">NV8'8N^LZ8*<5I(.@:?G2W2\LAEE26T@#K"&-__D 
@"W86M]SZ:/ZI_[;U1U8B-7?+59;W/&,Q'WK<Q6@0P!( 
@"(:-!'@;VOW4I^/93UKPX%A.:A+>>%/7EGU=N)\1.$< 
@E-M204O000#)X!K<ZS;_J>+KTTKL*3=5N''0S@'I9JP 
@#4,,>>O\\=N:?P(W5"^(HK8"RXA/&_UH,?24%Q[K?U@ 
@]G/[EII87LP) T1&6%V2Z?DVE9S_S&0;W"CE6G@F%F0 
@C[&M+;<.=#-GSXU4J///=OGE7,S)+CH]Q'&!DIMH&M$ 
@:]ONL^BM>L+O'.;28/N?X1$L;ZUB&VB!^EG#'/+ ?(( 
@G7H)E>FCSY6@KGL5.@Q@.4>68*OJ2)-0152T""Y!+;0 
@,R@'?MWO$##[S_$K$L-8^B'$MGPO$ZN>T#V6E$Q-;7L 
@+GF[0M%J]/",D$;!?+@.:FJ;4L3E/.'7PIRD]$QJJ;$ 
@OR#S,)_/#H=A=>LIW]B]QYQ9N"9JQCPKRWS>L5P7!MT 
@/ !FTRSP+YM<(S+M*-)XT._I5;[3J0=:88F P\):VC\ 
@(P$.#\BZ#F T;UB9#\C4\G@2JB-.@UD+00>F#<]4(H( 
@WE8;/\C[4YBT-!@7YH4K<X[Y^E?>K1J3UK\4%0Q-%H4 
@-:4I(V_WQ*8RSRH7.BNKF#-)%R>$AWQG6;7S.C7IURD 
@_,Q,LU_/Y.$F4O\U3W#>PXQ<GF/H$V@%*YFD+C@FR;( 
@Y7&Y?HC8$P6*@ZV1JNL>9YZEW1>T-VC\F=G5_7C687L 
@F*>75-(0<IN$";=J?[6X"C7ST!5[D.<XFMM78<(@3&H 
@$B&>S18+V^6U_E_QZY*"RR7E'?N&H)(09R#Y,&Z7O,L 
@$4*UKOC9A&,J+46MI_A3FJ>!;I1'CVQ#? NV#VPD0/P 
@UII\3__MNAN>^\S8J3$_M^%NU\M3MH)K^!BV.>?(^(4 
@YNQ811EP4A2_D9HB]QQ^.[3+-OTMO3D73\.X\2=1U04 
@HZ[+#K;']?1UABG?NTYZ8J)?'X0//),U&U5U H-6[L$ 
@(W]<@8?MY"K[;#H%##[9E8=M;I>;F!.3\(HODX 4[%@ 
@6:BVG O, ,?G<"50TS:E7:YJ4?HD3"RU!$CW&">0[<\ 
@+.N.ZC5AL?S;>,O8Z3E1)B2/&R\P?]X*ZX#OUK4E&Z< 
@INR4@J%8[A!Y_.>G3,!^6L1)MY?AX";-VI6+:?EJ1%, 
@ RX';ZWK/9#FTZ%8+T.)7ZIA5 U[[Z)'3/(9X+M(!*4 
@<982B1!NG-ADT\--(&4!QW3$K+C9!KY+S8#$*OCA+5< 
@QG[BZA'9WAECRSS*T!P<6FGGL]F,7:/!AUL"L'WFWK( 
@@"@I;^P^;KE9<##,;?PD+;;@.S 'G]J61DP.:P<"Z"< 
@@"COLHD^$F8;:0_- N# UBB:<QSLV2B+P!43KL\PQE@ 
@=O4.6ZU/32Z#@JV;Q L *W0RQLO P[$=" +8Q#\"&A( 
@B-4*L0-V+V">O2BY]&,.C&AX_USC46#3-LOKZ]E$$[( 
@^ #PUIA9;J%.[156"7A=QQ*_X?<K&'L)=3E6,&=$NM( 
@:J!?SZYQ]QK,[@RU%VH:.E 12E!I/ZMQ8HJ]%9DZS 0 
@&?$(*O-V#TN.0B;B+"$3(M$)"-0I9SDQ^GCX=LA:>;, 
@7\4YLZ,M!$82.ZZ$9W+?@"V.E8R,_F!:4%7QXHE9%_8 
@A&<&.A2:@?Z$# KTPZT%.Z<O7[9Z(X./"*"[^AU6/'< 
@]U@FC\,AH2UZ5!\'/:ANMKZ=7A8D%QLIP;HEA$U2M\< 
@MZH:>FO6VNJG2<KC5%/OC@4I@J^1 CM9_IFA#82/]M4 
@UNGS\V.>S!,[L+^L4^$/)X7X=$-^*Y+U8/M!PO7#!/L 
@E!/M1O!IOV,83*1EJ@>RR(0V:4#KLK4R7(0N?W&]JAH 
@;&[A_4O+#%APXF">*1PW_F7UL,N;BH]>PO2CEH-D+/0 
@1])S=:J \MI"F[1$:=:,85;L!RXA9<$T,0;XT4,(+HH 
@7<2N'*Q&[+&TE)9-78C\L1:5-0EI>@]-&MS\U($]T>, 
@C9<C73RTHEI8-^XYRSXYE[Y_$EY$G1);B)=^2"1LKG0 
@:>5>Q'J5,0LU +BE1)AR6A=^@[K5[OXSD6@+ O^,Q,T 
@Z9<'."; 7]0]:,V,0S29?V.8:VTN$1!+)8"<XT),V@H 
@47]TZA;%#=V9HML)FNBX>-/G4*EZ&#;$8_8GQ9$#/2D 
@]N W</I>LA'+W>!OHJ>'EQZLLL,TS,R6L@)=@P"0G_0 
@1\QIQWPW)N==GG]42<J+=B1>%CTZOK#\/'J[_"#2\_X 
@WG%65VV03)T'$TQ#W95=AHQ\WO+13/B;*.RHU\XKO%T 
@Y(NQ#Y7^L@5]O?8ROLIB]7X6HG1UX7<X$L-G:&?()FL 
@GQ#4PW;^EXS(Z5S9>5UP"/&5]31RL_NW@DW.&:OL-PP 
@/D[U-S&L[/]GUM!NSF_Z$0*>3 "/V>]F>O.,$:M]YI\ 
@?7:4DONB=(9540#D+ :K<?"W Q $J@I.S\C](EW+2HX 
@T-^$CRYF."Q3 [E%<NHKY.VTT<5EMME'WHC]Z_:3CC$ 
@O#<K!Y10I67 K[163E1+N>S?KAV&$E[E ]^1W3*2.8H 
@_^'FM[ ^&$J3C;\.QIEOCBU&_^.#O(LI-J5[:\< B/8 
@*V XDZM#0EK^^&'I+LN),S\(T@A@JR;F*^%P-J9WL5\ 
@^(.HUX]O2N<,I>HFHV2 4.0IO?4>$7X=(Y@PL^YT$Y0 
@:G$IO(M8\+_ .*3[ZT4(>&"+TJN,5+;V\.8K-0KU+Y  
@.AO#FFWLB:L0KS/. 1<3Z(A>7%^X63/CK =4V_&QW'4 
@Y-T(UQUHSO!/,UD?LDQZ9V;>;2RGMJEO3>PED:=UH1P 
@\CN&_U=B?XE=YMH2;%."-@P?I9!,VNT37GM76%CY-S\ 
@^G--_&_RYXV/3Z]3X_$P$)U[#5\&),%V,0XVE89# B, 
@,:.*=%&LY?)"V38(!'%=TA>YF9^LDD<4&4[11&P:U#< 
@&^[<L>_V[=R)4OB ;%QSF?/56$_-P+V$Z<O0M<V%-)T 
@1J@E/O#,,0+SO(O+V9DA,RSQQ=-6):0N*]LO+IU/QIH 
@$[L(.P>-MS!GYR!_V;K>/]I,&:5'Q^)B5I27/R4XT1( 
@H=;SD]Q1M_9:=)+(F/3BQ<-G @P(ZT/L*C2K"!QQ>KT 
@_A]%BGLIN/DH#8CPRWY9V#8>,5KGW<P5.FHQ6MRKQPX 
@N6ZXWODJFON_L&$2PI G5^I5\Z"W>+0-=^@.;)6\X0P 
@6X6 ^7?,:"(0"\ VT8GB3%(TKZMS?A<C$D(\2M0C\WH 
@X&V1D]^TW*G+S$KI>+AH_FX$^B%J@P\R'(G0R"EY>S, 
@<[M#Z0_S(=; (PU\1@0EXA24=FL"1<<A1VXHRB.[T-( 
@90.1[&@=EVD6_C%LK5G#N;+VA.Z4.$2?-WW<&4\2"A$ 
@L-I?KA2'7T4M[#?" 7^=TN3[,Z?.#+IGV$Y"YFJ@R'H 
@LSPC;%3W;@PI"J^=2#OAOL:]J9%*.T_J'GK?97:4%FP 
@+\[)5Q$J:R:\I'2MBJJ;C<2##[R6\0&QCBZ>0*&7-90 
@T)W[5[QIUXR+IW9P[%89B-6XIR17)X8NAON-?8L966@ 
@F$/=5XQ[.I ><GQA4BI^4IB/.!7 : '9 N@M"O:UJ9, 
@N?4@?/W-61.0P<;)S)5B'6@.&GE0>R6+$"96@$0+N]L 
@-+[B2O11[4FAP;;=U[>Y(@4&=?:S#X/[TYF5[I 6P,P 
@71@4QQ  2V[CDVM89P-!?D8&M%5&"C_@RW-L$'K&K[X 
@</*$F>*@K893Z.EX,8)^ *W+1G.S7U,)WH"M L>X38\ 
@GUJ';['4L$_#M.>%$MXX#[TD?FB0O(-#Q_MFF"QPQ*0 
@5(,4SK'"21N>N(1+'6\7K2!=SU$D_U>!<14UY+!LPU  
@I5P,F3%L1SCN)\0V)S9&[9)M=Q8/:7FP\>7CEV$QCEX 
@$^&F4UC0*MS=3^U_ULT2+@YMH8,CRQ_9;I82[?SJ+C4 
@#[;-R"%4M-WC=.5*E')6+V3S+UUY2-7,X%Q;3K1/7<( 
@H-=(\J_\%@J6!?0_K'1.OMJGXXH1@U,5A>'#ZNE-<YX 
@E5"K9XZ)A[G03W;%75T]UC2PB/ 13SJ./973?*EQQ)@ 
@4[&EMHOL14JE ^U?XR;V$&:V.Y)X_-<%IK9'-V0?0T( 
@F70B7C<2_P?L$M<P2ZVO<9RD#4(851NF1:1A>JZRV(H 
@>I]/QN(PI5.FQKAJU$UC[(8\(<S[H5>^D"1>Q5\"=@  
@8G$/7?;K=5AF8\[%R$!EG2S0%>Z X;#7 D-?,_E41A\ 
@[QD%2J %8/M[&D7_N&:C *@*[5V'U\G;5?-#'('8N!< 
@2>!7VAV3O7EJ$BM(0YEF;H\&;"O27?I;TEZP>*1:2UP 
@V*&WOON;Z$L1W+S]%4=PSWOC)^([5"A\X%3XN;F'4\< 
@HS N/OGN8I&-,.UO=N9!G:R[:SW%P6Q.9)K5WHV:X#\ 
@&_5)IA5&[G Z/:9P1[RIAPU[O;):ARQ>OX+URR4T56@ 
@YXB<7EY !V$^\<>.P"YD-8_A(#'.>G^J\)WPD)TY-<< 
@X-+N$O+3-L4BJO1=1SW=^[Z>DYLPIHQ!1Y:CP?G<VB( 
@.35EA?[]P;QD+6#B&I8L>K?NC5F(^DKTE3('X<;AQ04 
@MFZ6H<;3PHA@25B'9R'M52Q[<K;J?0F#:=G<B=TJ#&P 
@;N,<)J=B*_V^?HI(-BNQL3"/[%'-[-3VA&MMN8L\0T0 
@\ 7$CP)G4G%6]<"MF?&D5>H#O*?K'P2%>]M.N5IMKY( 
@//\&LMU$6)R!TIP02IZ_IU=X(5GBW[)%ZXEQL -^_BP 
@^F/OEU1(=5]S2XE5!N JEE(L5NEZ.\3D#FE<^H\ %E$ 
@!TF.MJD7"KE,Y+8Q3^O'78RI3D,&.2)0=E&VPD(M$7D 
@N:PL5T'&&8#+S<B+/^R*XB8A>]/K%_"-/FY%%Z*K,,\ 
@NK3"_:(6OA12GY%=_\-K-2HKZ:4H'FR X!,Z;E>M&:( 
@(;4=)#0@YX*O??)4_26I/H2J+A1QXPVC/J^K+<B ;@X 
@K;%18F BA#-)=#>A_5P19$*Z8]#* @ZKS&VP(2Y_-9  
@IJ<]IB%V;2#FC7"E%SYWBP$8PXGLX95L<X,T;03'>WL 
@>@C7_?SW6[5G+5T+3<=@HQR:]:3.YVZG\GG3*4HE,JP 
@Z0L6=*2KSVR$6,]W7_&'Z0<K7H\^[J1COEUV409I>=T 
@Q$/4LDDHY9%9PXW>F>MA6!(+I;<IAJA2(T6ZOM_O%I  
@0$VHN/>I4=\7,I:WMQEQ0N9%SK;W%@"'3R],3;,)LTT 
@)\<91^'M4TJ$$ZY52ZD <'XVV!BR\YU]_PQX[GOFY0L 
@KIH3I%I"#8K/-=>^Y:=PW1')#L$5<-9.2?C3TSLE02( 
@OJL,3(/;I8["L><A7-BL46-BP3L0V%H4")W;AXC\FTX 
@C\D<S>VW;G0R0,B):QY@P<HQAXC9:7/,NB=8M1;,-$@ 
@WQRC5XFF9"7C!?? ^6SM87:=(8@&TG]A9/W'3\,?A#8 
@ F-#X^9:_G OOT(M5IRRG4ZE1%M(LF[Y7C''64GE2.( 
@*DZ^U:5(H<1A@;>C.<29P^Y$K.TIA]D.YJ,'Q3NUAKP 
@UOXY4TA[[VWX_.A+K2^FH3;&K#F0,VRO>>V(T>_..[, 
@90S((Z\-@ H2L;OZR[>#W+8,Z:X3?N#G73[ZC0;U*6\ 
@+#D!T5Q'1PO5_ED9$1^TA'1"L#,:%1"E5>L?+?LI&_8 
@7@J_<'P9HU[KK<@3TC1$&?GOL(&"4"\_]-&])/TL[B  
@AKNC0/ E2GB!GO%_)H], =E5,4JG^OTQ0EAG8$P?K5< 
@^9$7"_XY!3*#1CUGUR>LYO^1P]Q.?C\R\3-_G697/CP 
@,$O&0%@->R0CDJ*^3 /"-#'+MTI+Y,/+6#6C=W"0]$0 
@A?\MRK^O _X%9._311E6@J0S6*O]3O[R\9^!K[U*[>L 
@CNI(HR_M(6[H(QB\P&.3F5BDF '2U)_HAY(!P%5\+8< 
@A.*(3."2$OEL-T3PZ!Z#BXJ46ZS==WS,(^0\OL@)PQX 
@:TWEM^(_,AL!N)@4%616R_\4"AM_7&905.I$[YVXA>X 
@A9_71>S][ZC$OSB(%'$/R 1WA :/O"LZYH[S?YNQG<T 
@EF!\QRI('4/D33O>3EC&27VW>P.]!T1=(:N$$A.^@$( 
@PORVCQ,YOZJV4NGIUQ(?J ,2;!R.@\&9AE>% *:6<TD 
@29KR0AFR1CSNA($@ B>W%FD6R"R/OG>7[Z$*6+ QC\@ 
@D<TQSPB ]Q+_P3*3;PL)F(+7,4&QQ[KRI9B6\S.T4NT 
@9Z9R"#57JET#_QA>W"_S/YZ^P2<Y.8.BZ+#4)L4"I7$ 
@&D_[K"G)%S/?LC/Y4><-P&PLV(3 28*# %5UVE9< :H 
@],'"G4_11\8PH9=<V%$H9)L28A(X8/!9%5D4NYX5M)P 
@C<74$5]&HPX).9L5_.[^B7L<.-RAA]Z\?*\^9Y ;<<0 
@["[N?Z'0# _K=L"D@L,/$4"N&*8 +ZJFJO(4#T%5'[D 
@F65</'":+WYC>V54#FY,3['S5; GXY2M*IATH%XM?RL 
@1+*B;;66MC:L0;(S_I6]Q+Z7D:\A$U!QNC;&-'[BN D 
@61U!95-=,V_)*NG)(-H*4N)*I]O!E$^S?T$O<><-U\T 
@X!;KGG[T69C5 $>8\8Z(PKB#>JT3$X4L<C)[&!(:S?P 
@]L7@/L-D[-OC\[L$JD=V?.\/403.GC1[YB5G66E<[E, 
@ZJ/YHP'K$4[$^]>1@V%9%5L<PX4V*+K(<Z;^!_MG,?, 
@$>QI$C_4F3N^@T\B-D+ /[B<0'5PVJ*>"U&=[LZ8.5L 
@CK$=KHVS'XJL$A/HVE0E%9+:1RZ:):I@'5_EWI7!5E4 
@+U68:GT' 8PP1!?OB$<=1LW'1+#^4].[W7/97U/.E_X 
@TF\G+-=#+15G-8?9N%M(;D!"G_IJEN\I>>=38OW,#0< 
@!>)""6L0#/-@W5FZJRRNAO= Q_SPHEDA'?T,10TN13L 
@H&UE#JM]R#77R ;1>D/9LQ%(Y@DRKR?021H,NWCQPI, 
@MM*6"NU43_\QM29V%Y_<J3N)A!3EQZAQM+I9,(ZU=%H 
@^O%A?L0A5!?HHN=2(DWX.T2J;G5W^I-?S.4O)EO)5#@ 
@&IZG()/\LA;Y19/XFS+]<$[5T @ K#[T>@-B<>=U).4 
@[H/>=^(?=JAAT5KR[[=M&OQ1!'8D1;5EJIA4$Z+!VN\ 
@TS5R/KQ6N,TO.B8NK<*DZ#&BUNJ:-4@9M.!V_M/#3@< 
012F[$^@<4; NX[AB5X\!<   
`pragma protect end_protected
