// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HL^UY5QO:UT,-3C!SLZ3'^/6F<VP?" =[?&>H.LPUUKW3LD32S0=-0@  
HD_^-/JNP]*?1+9389;_9K\09P_BU9;*DO.S1G^&HIT"AII3$-E,^)   
H#53?Q[,W[[V]Z"\V"KM1R[*KMAM!Q%JXL+Q'3S/C/J[EM.6$=-HFL0  
H$K>Z2LH&3]IR;R":SP&T#C#2>%.W7)PPE+$&R)93/9Z[&)R,A9]>QP  
HP%A>SC@^>]!=GZOM,E% &5Y=#B$2PT<45(ZFF;!Z3G5?)3T2RVY&J   
`pragma protect encoding=(enctype="uuencode",bytes=3584        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@.=2*MI!/[IPH*%(\;2VXUN%T9!G=C>=M\]=ZK^)_A>X 
@YUV5OB0=O73?0&1',3(.<G8EF. ON23[E'N]AZW[:80 
@AL\*ZT64(TR&6VM$S=BGN<:X(0C8O1!_'2;3Z#IL1A( 
@2B30#:=!C6[/:-22@EU@T[BKC0TZQM=(^@7)Q ;ZFXX 
@8W[66>^&>1VOAJYL>M!@$K+%*EWAZY_>SFUZBFM^S.\ 
@[V+1@^1K@X]0I6)0]MFC]-)I,'EQ\O>LFXF$R/A40YT 
@/+/V[D]1F#HTC*GR)BM2:(=,>;+L);"VG0?@6-SY$^\ 
@QAU2M=G1^PF+:R]=3BL\_829S;EL4>Q]V4\"V=I]V H 
@2(VH')HT\+;T&*5N27JQ60C6YPO?^V)C[7CT]B?1;&T 
@&4;&57X@*:^V<*(:^Z$4WLF/F]K@^YDH T24Z8\^"T, 
@[3%Q:2_9-5D/MGZ *[\V^S9?<%%DQ[')J,<.@0K\ 7\ 
@T<)G\.@%;_!Q_ =*#;[98(&%"! ^^6X4W/)M9#U*-%0 
@=R6:&^%,F<QS(X@G5Y@\.ED(,SPTABF?\A.@ NWWN]H 
@K<A*^1VAV!HI4SL(F.(0<W59:)W6E_LYN/$NM$4=XR$ 
@\TE+(H"X];$MV4ID5@)=LGD4_<I *5$1QS7@7K5I?;< 
@,"YFPV_-0IN]Z^1B!'<-/Z5$;J"?-G4,[(RJWT[ME%X 
@HB:!GD7]9KVT14V;^LI;8[FUROAK.6A*,=&7-Y!0JH, 
@1VM"J]MS/_1.OS0,TL7.S8(S#&]%&WU]QYC_Z(2-.NX 
@7ZOHPX?M]1RDW!Y)N[2')1DN87(_EF?!RDYH3K)%!WH 
@P1ZV^K.?\Z- ](T^$\9-D!M)6O*4A;/%T%N0F;VHC_P 
@)%?/"=.1[>CR8BRXNL*0Q:&IK\<!L__&&@_Y'%?1FZ\ 
@[OF>V%QH$/Q%^(];FHA<F#O8!+%:[U)@W]IX8J&Z;A@ 
@P[:'"W_L':6[4]HK$_FJT9[9TNTFUB'-=H]M"43'OI  
@J0C$1Y3%TWD3GHD?!!#KM@B&9SFXG;2^9@B^@E^>$L( 
@ T-)A7T:#E:NE%6<_QESYD;AD8H^I*9]IY*U,7='B@< 
@I Y<9C^##?P.C^WFL&Z/4GF^/&;.-9&T=E[&"%QOEKH 
@WQ).GK@C!.BK@!0]*O66?*X*+3%)U1VIP#P+M*+8,;P 
@$@?G^$#[>:P# WX\^Q9P>%RJU 5PK\P. 9U<8>Y%_S\ 
@(;UZ" S?"F(.)0&-3J<[[-O+AI<F<[+Q @,>5>J F(H 
@-,"4L(NO9A99X?W?&S%]*[:O+E#\347(U3T'_(VG55P 
@YD7RF"-H8MEBP968#124AQL-VC81?6"7F%(MT"DM454 
@&>/%N-1?>#]TMQ]PKEVQ'>/"^C(%P#VZN8E7+'A?).  
@Y#N9=@X=:L?)2T)C8.@>M#R&*.+WSG\.OG[/+@ ;H)0 
@NE(S41N#1O_&BD_@8P-[!#%'.*%'#<1%8HQX<2YK^(8 
@9(O90RM4/ML,/_H:6G7_C=48J*&@Z5)Z D8U(O:MY<, 
@?EY6K02%]VVCEAT8RW#+FYWD!17C&LE-E6^1Q?#I9%T 
@M_V2]P1D0?E%=6IF":I/Y)W,4;#,=E.NU1\7W/I&AE< 
@,LUUW7*N--J <D+KU"R)?56]-7C3JIM/-N:)8! 7C[8 
@A93I2('#&Z*!75.D7IU Y!&4_'*^HO'WEIDMOJ@!S-( 
@@4P(1)+4<XG_'.TCU'#P;$AQ]$X'1[E1"BK"CL>$-;X 
@>2:7(\U +*<*J#Y.&QBEPMCXZ!R2\QZ*PN%Y3!P!+-\ 
@JEP&X!K\VC(F E*&<D._:^X_NO0!9V[W/L6?VJ]0L;0 
@9/J186&Z-*QFC&FK=SZG!W0=NRJ)J)S?/OMFCJ=R9D@ 
@DHL!8C6#7R[.FCO+G8U"$7OYV2@S8;/"Y3850\*8S,$ 
@>+TG0:H%N9K1>F(2US:O2SJCFA#FAV,+.9_E> +I"K< 
@S&Q$6I;,4AT+W:?Y#[QZ#8RT$0<&?).*:^X%A/ZZY70 
@A!"S+QB4#,+!\551+?$Y)CM\=.H1XYG.8?083J5:N=@ 
@_+V;"F5#7;TLQ]F4T.KQ5.$T3'1M2D48SW4C^@2F<F, 
@.C\@SWK W(*?C/2B.$945EH%-M_A@\H-$-N-TFIF!/  
@+-$ NXD+! C9:Y73.27_!PZZMU1]NYDD*KM=54T'A54 
@]%%9X'G,H(XV[#UJHO9]EU)QI<OV_2:^^Q; 2'8* I8 
@PVL*\.(-$WZXC$E2Y7&'T@1X?C;E_D4T(Q>$FB1G(+L 
@(0QX[[JG,:B ZPFN;S'=%JH5@*)YMK>%)D[J/(E2?1X 
@6?U%OZR#@4YJ>_=&$3<$+_&<6!7=ID4>7454_3Q,-9< 
@<FB85BL00_@;H]9]X4 #:0\XMNL/XAHEQHF";RXAI-H 
@,=2:39#:AL4WA;N@J;7MBBAA,&WBN-OK+0& _T^W'MD 
@YMK.:*A*.CE%?IU_?(>2^0>&HZT"?TBZX43EZM)R4!  
@PN$CU@DI3SKK:]#^"3]/GLC(V[@4RKI3Z[^&;;0ZPT4 
@(#V0,3&Y^1TF*99V37OFD) BH9)O5 ?7+8?SD2L))A4 
@C&'J1,$TJ41LAY#Q0Y*2D<48E+#!0I5SV1@1],BKLYL 
@ T#<R%H-?+C%45.YG8U =?Q<)=(OF'[4BRAUZT1ELU8 
@[I>YG:L(;2TVJHPIF.^X/%V%TO(@)FTBMU4P&+VL=BL 
@Q>WK_Y"@MX?\ F0FT\>@) RDC3</W&40%:C,AFP4+P8 
@J*&W *DJ66I8EEDMK .X=\U^L!/&NSHF]Q?N4B0M=WH 
@[P+]4AYEYDJZPY;@7DQX9[L#Z;2Y#D8N"FU?U.L:(,$ 
@))*D9+_! 61P*LL3%TL,<J9#WBD8IENJ0R"6_H1*=., 
@A1]_.RS>,(0C_BZUZJ?4[XI?'VTPJ,*',Q=EQK+$5<4 
@=>7BNTR:09]WF0E>,L0H^( /0U52P7"P2=N.IFR,Y,D 
@L&V^X @LOC5+)3X]IJZ$#OMRK86B"15;;%% ; [Z=R  
@+CQ0>I8E%W.Z?#&0D+0\K(@..2UL"Y**L=P7'H]Q:C$ 
@6\ DYL2<FG*N90N1CXQ-&7/4?P^*NV&4ZO^4V9)W788 
@''H-.XCN.[G5"72Q("K+]ZE+LQ;3(2#+>F4$X6_":P0 
@,Z/[9$=B+^,9#I]=MX)PK]?K>V%[F,52M^X%%_R]5X@ 
@\!&^9%G]$SX>T !%71,9ZTH 8,_'J0X;U17BW%7H 0D 
@M]]25,40Y0<Y?1B1]7L=Z(]._/6MG7YB&(FA#6X<=U  
@XMUQ2CWXH64(/H0M]AFV*$M_('L-HMY!:LG@XN'6"#D 
@B59=2,"$2DI/P1QP@"U.E]O+]O4D=FHA4G]64#K;X%D 
@?#J0J\&\Z0W'HW\C6W%2O^.E8NBI2R$I',XRO,(%5L< 
@4BO^Q%D?6B"1%G[^A]_ L.[#)6([3D2Q^*;>;<D0SK0 
@?F5EFF5UC8.;H?EI7MM,^MC2YEE(IYQP"-:G,#J:MF4 
@#R/,#,"?_NADMD$TK9L>X/D84[LMC:PBB!C*Y?'DOD( 
@+!U<_;B)41-R!-R4[#_0W[D"8<=VS(H6"(L%GY_[-(H 
@#*BN]-U;VNW"[01GF+&\I38N=)A5)V]"PPV?1[>45PH 
@IM9$$!^_&(7)I@:X,KJ-<-D6H'S8'A2:JU1XH=T+%K@ 
@N,B.\E9:7OJ0VX&4Y#H!I;I AM+^HS&=2^)/^JV5*F@ 
@\F*-T7KOJ9*YB[HD-X3(V)PJDE+#(1K".AG]601)'CD 
@MP(/E0*C8'Y>8FS9.P:[#.H%E0;[NR!KTO\.LC#@;LL 
@&IJ/WZ*;/WEP3,JFRC40Q<'4?GFP)&\=Z0=K'A65,*P 
@"A8XQVQN#I70&,Z_?31N(.^"YU"]*5]-X0378%9Z:E@ 
@[&]HYD1>B)CV%8-6ZT\HE93OW9NJ*,/JJ91</ONL_28 
@?& "JU*EW#,:_337+;5A1"A@,#J Q\]W^_9M9'5PX'P 
@=U-,M_0G._-[OW\0ZXV;LX-"H$X^]1B\CI'&CTF&5"T 
@.28MW*<9%H*V6X0\^4H L :'PQ38 2-37V4/MLAMX(  
@<G#[3.8SFT:/ .K7+>"8[5VHAVYL="$\^YLM")U_:U, 
@GS,?H+A8(*H+,)[N<33 X"+%MAZ-#H>NU(5!%S1M(S0 
@ MZ#N*/T9GA:P#2C,;-_+NW!;AU)1(\9H+@/4<B]7 @ 
@$#'[&D41M8>6P.#] ;#)8**+=Q-YKF<&*U5H<H$7$O@ 
@<C$P1-AW 4UUCH(7\N,&U>E!BT(\DIV$YTM KD^^<M( 
@QF!W?$&\(JO9J($!$IYFKD:P+A]$V^#?W[$_/%Y)BZ$ 
@\R;GH>9\2VE]3N/9:5[<9JA&$5CA, -_+RNB,O$R^CT 
@]$)19+N^86/P/S]J(@T:H"L(_(6&K$GHL7Z1Q KPIL0 
@L $C*G[R&3A]-3_&>J@1MU[A:B =,+G,T?O/<+6L+7@ 
@Q')U#C^!;>V?N7NY-FT#6Z(^J?'S'D22N4F24]!7S1( 
@?#^Z:9_T]I7RHEA+J*@83XI"*ESL>38-]9:7HSZ!$/D 
@#TFN(M9W$<VL0I\%:R'0DB]=3!+EGSN]KXH40[4-:[$ 
@&YV<=(0;17K19JYYZV#GOIP)>SDG&N1-UG?ZQBIXG.< 
@?)UB\2UD<<&6&F[.7 =#?3LC&9+KPJ-?;@FS5!_<V14 
@DIG!8IIUG"+1L)<.#"!,7U!'W]K;!_VO0!TEK,[\]0  
@9I%&*.\?1%*X=<&0)=<-*G_L(OX_@U:Z/2U@LZ@3US0 
@XN(HJ;ELOM!FMSSH Z80=C%(1]#D(-XC@^%;VL-)R:P 
@!BG>9JQ;B1$;H)5$RP9-;'G1+8@[873ZK2N"FZNI-=( 
0P110(N+_(HX^NW%&GI++50  
0@^+?CSXGF+5B:,5[O0W&:@  
`pragma protect end_protected
