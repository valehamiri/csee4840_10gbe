
# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 13.1 162 linux 2015.04.19.20:35:37

# ----------------------------------------
# vcs - auto-generated simulation script

# ----------------------------------------
# initialize variables
TOP_LEVEL_NAME="xgbe_mac"
QSYS_SIMDIR="./../../"
QUARTUS_INSTALL_DIR="/opt/altera/13.1/quartus/"
SKIP_FILE_COPY=0
SKIP_ELAB=0
SKIP_SIM=0
USER_DEFINED_ELAB_OPTIONS=""
USER_DEFINED_SIM_OPTIONS="+vcs+finish+100"
# ----------------------------------------
# overwrite variables - DO NOT MODIFY!
# This block evaluates each command line argument, typically used for 
# overwriting variables. An example usage:
#   sh <simulator>_setup.sh SKIP_ELAB=1 SKIP_SIM=1
for expression in "$@"; do
  eval $expression
  if [ $? -ne 0 ]; then
    echo "Error: This command line argument, \"$expression\", is/has an invalid expression." >&2
    exit $?
  fi
done

# ----------------------------------------
# initialize simulation properties - DO NOT MODIFY!
ELAB_OPTIONS=""
SIM_OPTIONS=""
if [[ `vcs -platform` != *"amd64"* ]]; then
  :
else
  :
fi

# ----------------------------------------
# copy RAM/ROM files to simulation directory

vcs -lca -timescale=1ps/1ps -sverilog +verilog2001ext+.v -ntb_opts dtm $ELAB_OPTIONS $USER_DEFINED_ELAB_OPTIONS \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v \
  $QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim.sv \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/cyclonev_atoms_ncrypt.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/cyclonev_hmi_atoms_ncrypt.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_atoms.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/cyclonev_hssi_atoms_ncrypt.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_hssi_atoms.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/synopsys/cyclonev_pcie_hip_atoms_ncrypt.v \
  -v $QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_pcie_hip_atoms.v \
  $QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_handshake_clock_crosser.v \
  $QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_clock_crosser.v \
  $QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_pipeline_base.v \
  $QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_arbitrator.sv \
  $QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0002.sv \
  $QSYS_SIMDIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0002.sv \
  $QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0001.sv \
  $QSYS_SIMDIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0001.sv \
  $QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_merlin_traffic_limiter.sv \
  $QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_merlin_reorder_memory.sv \
  $QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_avalon_sc_fifo.v \
  $QSYS_SIMDIR/altera_merlin_router/altera_merlin_router_0002.sv \
  $QSYS_SIMDIR/altera_merlin_router/altera_merlin_router_0001.sv \
  $QSYS_SIMDIR/altera_merlin_slave_agent/altera_merlin_slave_agent.sv \
  $QSYS_SIMDIR/altera_merlin_slave_agent/altera_merlin_burst_uncompressor.sv \
  $QSYS_SIMDIR/altera_merlin_master_agent/altera_merlin_master_agent.sv \
  $QSYS_SIMDIR/altera_merlin_slave_translator/altera_merlin_slave_translator.sv \
  $QSYS_SIMDIR/altera_reset_controller/altera_reset_controller.v \
  $QSYS_SIMDIR/altera_reset_controller/altera_reset_synchronizer.v \
  $QSYS_SIMDIR/altera_merlin_interconnect_wrapper/altera_merlin_interconnect_wrapper_0001.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0011.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0010.v \
  $QSYS_SIMDIR/altera_avalon_dc_fifo/altera_avalon_dc_fifo.v \
  $QSYS_SIMDIR/altera_avalon_dc_fifo/altera_dcfifo_synchronizer_bundle.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0009.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0008.v \
  $QSYS_SIMDIR/error_adapter/error_adapter_0003.v \
  $QSYS_SIMDIR/altera_eth_packet_overflow_control/synopsys/altera_eth_packet_overflow_control.v \
  $QSYS_SIMDIR/altera_eth_crc_pad_rem/synopsys/altera_eth_crc_pad_rem.v \
  $QSYS_SIMDIR/altera_eth_crc_pad_rem/synopsys/altera_eth_crc_rem.v \
  $QSYS_SIMDIR/altera_eth_crc_pad_rem/synopsys/altera_packet_stripper.v \
  $QSYS_SIMDIR/altera_eth_crc_pad_rem/altera_avalon_st_pipeline_stage.sv \
  $QSYS_SIMDIR/altera_eth_frame_status_merger/synopsys/altera_eth_frame_status_merger.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0007.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0006.v \
  $QSYS_SIMDIR/altera_eth_lane_decoder/synopsys/altera_eth_lane_decoder.v \
  $QSYS_SIMDIR/altera_eth_link_fault_detection/synopsys/altera_eth_link_fault_detection.v \
  $QSYS_SIMDIR/altera_eth_10g_rx_register_map/synopsys/altera_eth_10g_rx_register_map.v \
  $QSYS_SIMDIR/altera_eth_link_fault_generation/synopsys/altera_eth_link_fault_generation.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0005.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0004.v \
  $QSYS_SIMDIR/altera_eth_xgmii_termination/synopsys/altera_eth_xgmii_termination.v \
  $QSYS_SIMDIR/altera_eth_packet_formatter/synopsys/altera_eth_packet_formatter.v \
  $QSYS_SIMDIR/altera_avalon_st_delay/altera_avalon_st_delay.sv \
  $QSYS_SIMDIR/altera_eth_10gmem_statistics_collector/synopsys/altera_eth_10gmem_statistics_collector.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0003.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0002.v \
  $QSYS_SIMDIR/error_adapter/error_adapter_0002.v \
  $QSYS_SIMDIR/altera_eth_frame_decoder/synopsys/altera_eth_frame_decoder.v \
  $QSYS_SIMDIR/timing_adapter/timing_adapter_0001.v \
  $QSYS_SIMDIR/altera_avalon_st_splitter/altera_avalon_st_splitter.sv \
  $QSYS_SIMDIR/altera_eth_crc/synopsys/altera_eth_crc.v \
  $QSYS_SIMDIR/altera_eth_crc/synopsys/crc32.v \
  $QSYS_SIMDIR/altera_eth_crc/synopsys/gf_mult32_kc.v \
  $QSYS_SIMDIR/altera_eth_address_inserter/synopsys/altera_eth_address_inserter.v \
  $QSYS_SIMDIR/multiplexer/multiplexer_0001.v \
  $QSYS_SIMDIR/error_adapter/error_adapter_0001.v \
  $QSYS_SIMDIR/altera_eth_pause_ctrl_gen/synopsys/altera_eth_pause_ctrl_gen.v \
  $QSYS_SIMDIR/altera_eth_pause_ctrl_gen/synopsys/altera_eth_pause_controller.v \
  $QSYS_SIMDIR/altera_eth_pause_ctrl_gen/synopsys/altera_eth_pause_gen.v \
  $QSYS_SIMDIR/altera_eth_pause_beat_conversion/synopsys/altera_eth_pause_beat_conversion.v \
  $QSYS_SIMDIR/altera_eth_pkt_backpressure_control/synopsys/altera_eth_pkt_backpressure_control.v \
  $QSYS_SIMDIR/altera_eth_pad_inserter/synopsys/altera_eth_pad_inserter.v \
  $QSYS_SIMDIR/altera_eth_packet_underflow_control/synopsys/altera_eth_packet_underflow_control.v \
  $QSYS_SIMDIR/altera_eth_10g_tx_register_map/synopsys/altera_eth_10g_tx_register_map.v \
  $QSYS_SIMDIR/altera_merlin_master_translator/altera_merlin_master_translator.sv \
  $QSYS_SIMDIR/xgbe_mac.v \
  -top $TOP_LEVEL_NAME
# ----------------------------------------
# simulate
if [ $SKIP_SIM -eq 0 ]; then
  ./simv $SIM_OPTIONS $USER_DEFINED_SIM_OPTIONS
fi
