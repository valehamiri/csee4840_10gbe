
# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 13.1 162 linux 2015.04.19.20:35:36

# ----------------------------------------
# Auto-generated simulation script

# ----------------------------------------
# Initialize variables
if ![info exists SYSTEM_INSTANCE_NAME] { 
  set SYSTEM_INSTANCE_NAME ""
} elseif { ![ string match "" $SYSTEM_INSTANCE_NAME ] } { 
  set SYSTEM_INSTANCE_NAME "/$SYSTEM_INSTANCE_NAME"
}

if ![info exists TOP_LEVEL_NAME] { 
  set TOP_LEVEL_NAME "xgbe_mac"
}

if ![info exists QSYS_SIMDIR] { 
  set QSYS_SIMDIR "./../"
}

if ![info exists QUARTUS_INSTALL_DIR] { 
  set QUARTUS_INSTALL_DIR "/opt/altera/13.1/quartus/"
}

# ----------------------------------------
# Initialize simulation properties - DO NOT MODIFY!
set ELAB_OPTIONS ""
set SIM_OPTIONS ""
if ![ string match "*-64 vsim*" [ vsim -version ] ] {
} else {
}

# ----------------------------------------
# Copy ROM/RAM files to simulation directory
alias file_copy {
  echo "\[exec\] file_copy"
}

# ----------------------------------------
# Create compilation libraries
proc ensure_lib { lib } { if ![file isdirectory $lib] { vlib $lib } }
ensure_lib          ./libraries/     
ensure_lib          ./libraries/work/
vmap       work     ./libraries/work/
vmap       work_lib ./libraries/work/
if ![ string match "*ModelSim ALTERA*" [ vsim -version ] ] {
  ensure_lib                       ./libraries/altera_ver/           
  vmap       altera_ver            ./libraries/altera_ver/           
  ensure_lib                       ./libraries/lpm_ver/              
  vmap       lpm_ver               ./libraries/lpm_ver/              
  ensure_lib                       ./libraries/sgate_ver/            
  vmap       sgate_ver             ./libraries/sgate_ver/            
  ensure_lib                       ./libraries/altera_mf_ver/        
  vmap       altera_mf_ver         ./libraries/altera_mf_ver/        
  ensure_lib                       ./libraries/altera_lnsim_ver/     
  vmap       altera_lnsim_ver      ./libraries/altera_lnsim_ver/     
  ensure_lib                       ./libraries/cyclonev_ver/         
  vmap       cyclonev_ver          ./libraries/cyclonev_ver/         
  ensure_lib                       ./libraries/cyclonev_hssi_ver/    
  vmap       cyclonev_hssi_ver     ./libraries/cyclonev_hssi_ver/    
  ensure_lib                       ./libraries/cyclonev_pcie_hip_ver/
  vmap       cyclonev_pcie_hip_ver ./libraries/cyclonev_pcie_hip_ver/
}
ensure_lib                                                                                               ./libraries/crosser/                                                                                      
vmap       crosser                                                                                       ./libraries/crosser/                                                                                      
ensure_lib                                                                                               ./libraries/rsp_xbar_mux/                                                                                 
vmap       rsp_xbar_mux                                                                                  ./libraries/rsp_xbar_mux/                                                                                 
ensure_lib                                                                                               ./libraries/rsp_xbar_demux/                                                                               
vmap       rsp_xbar_demux                                                                                ./libraries/rsp_xbar_demux/                                                                               
ensure_lib                                                                                               ./libraries/cmd_xbar_mux/                                                                                 
vmap       cmd_xbar_mux                                                                                  ./libraries/cmd_xbar_mux/                                                                                 
ensure_lib                                                                                               ./libraries/cmd_xbar_demux/                                                                               
vmap       cmd_xbar_demux                                                                                ./libraries/cmd_xbar_demux/                                                                               
ensure_lib                                                                                               ./libraries/limiter/                                                                                      
vmap       limiter                                                                                       ./libraries/limiter/                                                                                      
ensure_lib                                                                                               ./libraries/id_router/                                                                                    
vmap       id_router                                                                                     ./libraries/id_router/                                                                                    
ensure_lib                                                                                               ./libraries/addr_router/                                                                                  
vmap       addr_router                                                                                   ./libraries/addr_router/                                                                                  
ensure_lib                                                                                               ./libraries/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo/       
vmap       tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo        ./libraries/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo/       
ensure_lib                                                                                               ./libraries/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent/                
vmap       tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent                 ./libraries/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent/                
ensure_lib                                                                                               ./libraries/merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent/
vmap       merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent ./libraries/merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent/
ensure_lib                                                                                               ./libraries/tx_eth_pkt_backpressure_control_csr_translator/                                               
vmap       tx_eth_pkt_backpressure_control_csr_translator                                                ./libraries/tx_eth_pkt_backpressure_control_csr_translator/                                               
ensure_lib                                                                                               ./libraries/rst_controller/                                                                               
vmap       rst_controller                                                                                ./libraries/rst_controller/                                                                               
ensure_lib                                                                                               ./libraries/mm_interconnect_0/                                                                            
vmap       mm_interconnect_0                                                                             ./libraries/mm_interconnect_0/                                                                            
ensure_lib                                                                                               ./libraries/rxtx_timing_adapter_pauselen_tx/                                                              
vmap       rxtx_timing_adapter_pauselen_tx                                                               ./libraries/rxtx_timing_adapter_pauselen_tx/                                                              
ensure_lib                                                                                               ./libraries/rxtx_timing_adapter_pauselen_rx/                                                              
vmap       rxtx_timing_adapter_pauselen_rx                                                               ./libraries/rxtx_timing_adapter_pauselen_rx/                                                              
ensure_lib                                                                                               ./libraries/rxtx_dc_fifo_link_fault_status/                                                               
vmap       rxtx_dc_fifo_link_fault_status                                                                ./libraries/rxtx_dc_fifo_link_fault_status/                                                               
ensure_lib                                                                                               ./libraries/txrx_timing_adapter_link_fault_status_export/                                                 
vmap       txrx_timing_adapter_link_fault_status_export                                                  ./libraries/txrx_timing_adapter_link_fault_status_export/                                                 
ensure_lib                                                                                               ./libraries/txrx_timing_adapter_link_fault_status_rx/                                                     
vmap       txrx_timing_adapter_link_fault_status_rx                                                      ./libraries/txrx_timing_adapter_link_fault_status_rx/                                                     
ensure_lib                                                                                               ./libraries/rx_st_error_adapter_stat/                                                                     
vmap       rx_st_error_adapter_stat                                                                      ./libraries/rx_st_error_adapter_stat/                                                                     
ensure_lib                                                                                               ./libraries/rx_eth_packet_overflow_control/                                                               
vmap       rx_eth_packet_overflow_control                                                                ./libraries/rx_eth_packet_overflow_control/                                                               
ensure_lib                                                                                               ./libraries/rx_eth_crc_pad_rem/                                                                           
vmap       rx_eth_crc_pad_rem                                                                            ./libraries/rx_eth_crc_pad_rem/                                                                           
ensure_lib                                                                                               ./libraries/rx_eth_frame_status_merger/                                                                   
vmap       rx_eth_frame_status_merger                                                                    ./libraries/rx_eth_frame_status_merger/                                                                   
ensure_lib                                                                                               ./libraries/rx_timing_adapter_frame_status_out_frame_decoder/                                             
vmap       rx_timing_adapter_frame_status_out_frame_decoder                                              ./libraries/rx_timing_adapter_frame_status_out_frame_decoder/                                             
ensure_lib                                                                                               ./libraries/rx_st_timing_adapter_frame_status_in/                                                         
vmap       rx_st_timing_adapter_frame_status_in                                                          ./libraries/rx_st_timing_adapter_frame_status_in/                                                         
ensure_lib                                                                                               ./libraries/rx_eth_lane_decoder/                                                                          
vmap       rx_eth_lane_decoder                                                                           ./libraries/rx_eth_lane_decoder/                                                                          
ensure_lib                                                                                               ./libraries/rx_eth_link_fault_detection/                                                                  
vmap       rx_eth_link_fault_detection                                                                   ./libraries/rx_eth_link_fault_detection/                                                                  
ensure_lib                                                                                               ./libraries/rx_register_map/                                                                              
vmap       rx_register_map                                                                               ./libraries/rx_register_map/                                                                              
ensure_lib                                                                                               ./libraries/tx_eth_link_fault_generation/                                                                 
vmap       tx_eth_link_fault_generation                                                                  ./libraries/tx_eth_link_fault_generation/                                                                 
ensure_lib                                                                                               ./libraries/tx_st_timing_adapter_splitter_out_0/                                                          
vmap       tx_st_timing_adapter_splitter_out_0                                                           ./libraries/tx_st_timing_adapter_splitter_out_0/                                                          
ensure_lib                                                                                               ./libraries/tx_st_timing_adapter_splitter_in/                                                             
vmap       tx_st_timing_adapter_splitter_in                                                              ./libraries/tx_st_timing_adapter_splitter_in/                                                             
ensure_lib                                                                                               ./libraries/tx_eth_xgmii_termination/                                                                     
vmap       tx_eth_xgmii_termination                                                                      ./libraries/tx_eth_xgmii_termination/                                                                     
ensure_lib                                                                                               ./libraries/tx_eth_packet_formatter/                                                                      
vmap       tx_eth_packet_formatter                                                                       ./libraries/tx_eth_packet_formatter/                                                                      
ensure_lib                                                                                               ./libraries/tx_st_status_output_delay_to_statistic/                                                       
vmap       tx_st_status_output_delay_to_statistic                                                        ./libraries/tx_st_status_output_delay_to_statistic/                                                       
ensure_lib                                                                                               ./libraries/tx_eth_statistics_collector/                                                                  
vmap       tx_eth_statistics_collector                                                                   ./libraries/tx_eth_statistics_collector/                                                                  
ensure_lib                                                                                               ./libraries/tx_st_timing_adapter_splitter_status_output/                                                  
vmap       tx_st_timing_adapter_splitter_status_output                                                   ./libraries/tx_st_timing_adapter_splitter_status_output/                                                  
ensure_lib                                                                                               ./libraries/tx_st_timing_adapter_splitter_status_in/                                                      
vmap       tx_st_timing_adapter_splitter_status_in                                                       ./libraries/tx_st_timing_adapter_splitter_status_in/                                                      
ensure_lib                                                                                               ./libraries/tx_st_error_adapter_stat/                                                                     
vmap       tx_st_error_adapter_stat                                                                      ./libraries/tx_st_error_adapter_stat/                                                                     
ensure_lib                                                                                               ./libraries/tx_eth_frame_decoder/                                                                         
vmap       tx_eth_frame_decoder                                                                          ./libraries/tx_eth_frame_decoder/                                                                         
ensure_lib                                                                                               ./libraries/tx_st_timing_adapter_frame_decoder/                                                           
vmap       tx_st_timing_adapter_frame_decoder                                                            ./libraries/tx_st_timing_adapter_frame_decoder/                                                           
ensure_lib                                                                                               ./libraries/tx_st_splitter_1/                                                                             
vmap       tx_st_splitter_1                                                                              ./libraries/tx_st_splitter_1/                                                                             
ensure_lib                                                                                               ./libraries/tx_eth_crc_inserter/                                                                          
vmap       tx_eth_crc_inserter                                                                           ./libraries/tx_eth_crc_inserter/                                                                          
ensure_lib                                                                                               ./libraries/tx_eth_address_inserter/                                                                      
vmap       tx_eth_address_inserter                                                                       ./libraries/tx_eth_address_inserter/                                                                      
ensure_lib                                                                                               ./libraries/tx_st_mux_flow_control_user_frame/                                                            
vmap       tx_st_mux_flow_control_user_frame                                                             ./libraries/tx_st_mux_flow_control_user_frame/                                                            
ensure_lib                                                                                               ./libraries/tx_st_pause_ctrl_error_adapter/                                                               
vmap       tx_st_pause_ctrl_error_adapter                                                                ./libraries/tx_st_pause_ctrl_error_adapter/                                                               
ensure_lib                                                                                               ./libraries/tx_eth_pause_ctrl_gen/                                                                        
vmap       tx_eth_pause_ctrl_gen                                                                         ./libraries/tx_eth_pause_ctrl_gen/                                                                        
ensure_lib                                                                                               ./libraries/tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control/                                   
vmap       tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control                                    ./libraries/tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control/                                   
ensure_lib                                                                                               ./libraries/tx_eth_pause_beat_conversion/                                                                 
vmap       tx_eth_pause_beat_conversion                                                                  ./libraries/tx_eth_pause_beat_conversion/                                                                 
ensure_lib                                                                                               ./libraries/tx_eth_pkt_backpressure_control/                                                              
vmap       tx_eth_pkt_backpressure_control                                                               ./libraries/tx_eth_pkt_backpressure_control/                                                              
ensure_lib                                                                                               ./libraries/tx_eth_pad_inserter/                                                                          
vmap       tx_eth_pad_inserter                                                                           ./libraries/tx_eth_pad_inserter/                                                                          
ensure_lib                                                                                               ./libraries/tx_eth_packet_underflow_control/                                                              
vmap       tx_eth_packet_underflow_control                                                               ./libraries/tx_eth_packet_underflow_control/                                                              
ensure_lib                                                                                               ./libraries/tx_register_map/                                                                              
vmap       tx_register_map                                                                               ./libraries/tx_register_map/                                                                              
ensure_lib                                                                                               ./libraries/merlin_master_translator/                                                                     
vmap       merlin_master_translator                                                                      ./libraries/merlin_master_translator/                                                                     

# ----------------------------------------
# Compile device library files
alias dev_com {
  echo "\[exec\] dev_com"
  if ![ string match "*ModelSim ALTERA*" [ vsim -version ] ] {
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v"                     -work altera_ver           
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v"                              -work lpm_ver              
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v"                                 -work sgate_ver            
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v"                             -work altera_mf_ver        
    vlog -sv "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim.sv"                         -work altera_lnsim_ver     
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_atoms_ncrypt.v"          -work cyclonev_ver         
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_hmi_atoms_ncrypt.v"      -work cyclonev_ver         
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_atoms.v"                        -work cyclonev_ver         
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_hssi_atoms_ncrypt.v"     -work cyclonev_hssi_ver    
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_hssi_atoms.v"                   -work cyclonev_hssi_ver    
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_pcie_hip_atoms_ncrypt.v" -work cyclonev_pcie_hip_ver
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_pcie_hip_atoms.v"               -work cyclonev_pcie_hip_ver
  }
}

# ----------------------------------------
# Compile the design files in correct order
alias com {
  echo "\[exec\] com"
  vlog -sv "$QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_handshake_clock_crosser.v"    -work crosser                                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_clock_crosser.v"              -work crosser                                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_pipeline_base.v"              -work crosser                                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_arbitrator.sv"                                  -work rsp_xbar_mux                                                                                 
  vlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0002.sv"                            -work rsp_xbar_mux                                                                                 
  vlog -sv "$QSYS_SIMDIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0002.sv"                        -work rsp_xbar_demux                                                                               
  vlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_arbitrator.sv"                                  -work cmd_xbar_mux                                                                                 
  vlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0001.sv"                            -work cmd_xbar_mux                                                                                 
  vlog -sv "$QSYS_SIMDIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0001.sv"                        -work cmd_xbar_demux                                                                               
  vlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_merlin_traffic_limiter.sv"                         -work limiter                                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_merlin_reorder_memory.sv"                          -work limiter                                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_avalon_sc_fifo.v"                                  -work limiter                                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_avalon_st_pipeline_base.v"                         -work limiter                                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_merlin_router/altera_merlin_router_0002.sv"                                      -work id_router                                                                                    
  vlog -sv "$QSYS_SIMDIR/altera_merlin_router/altera_merlin_router_0001.sv"                                      -work addr_router                                                                                  
  vlog     "$QSYS_SIMDIR/altera_avalon_sc_fifo/altera_avalon_sc_fifo.v"                                          -work tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo       
  vlog -sv "$QSYS_SIMDIR/altera_merlin_slave_agent/altera_merlin_slave_agent.sv"                                 -work tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent                
  vlog -sv "$QSYS_SIMDIR/altera_merlin_slave_agent/altera_merlin_burst_uncompressor.sv"                          -work tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent                
  vlog -sv "$QSYS_SIMDIR/altera_merlin_master_agent/altera_merlin_master_agent.sv"                               -work merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent
  vlog -sv "$QSYS_SIMDIR/altera_merlin_slave_translator/altera_merlin_slave_translator.sv"                       -work tx_eth_pkt_backpressure_control_csr_translator                                               
  vlog     "$QSYS_SIMDIR/altera_reset_controller/altera_reset_controller.v"                                      -work rst_controller                                                                               
  vlog     "$QSYS_SIMDIR/altera_reset_controller/altera_reset_synchronizer.v"                                    -work rst_controller                                                                               
  vlog     "$QSYS_SIMDIR/altera_merlin_interconnect_wrapper/altera_merlin_interconnect_wrapper_0001.v"           -work mm_interconnect_0                                                                            
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0011.v"                                                   -work rxtx_timing_adapter_pauselen_tx                                                              
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0010.v"                                                   -work rxtx_timing_adapter_pauselen_rx                                                              
  vlog     "$QSYS_SIMDIR/altera_avalon_dc_fifo/altera_avalon_dc_fifo.v"                                          -work rxtx_dc_fifo_link_fault_status                                                               
  vlog     "$QSYS_SIMDIR/altera_avalon_dc_fifo/altera_dcfifo_synchronizer_bundle.v"                              -work rxtx_dc_fifo_link_fault_status                                                               
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0009.v"                                                   -work txrx_timing_adapter_link_fault_status_export                                                 
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0008.v"                                                   -work txrx_timing_adapter_link_fault_status_rx                                                     
  vlog     "$QSYS_SIMDIR/error_adapter/error_adapter_0003.v"                                                     -work rx_st_error_adapter_stat                                                                     
  vlog     "$QSYS_SIMDIR/altera_eth_packet_overflow_control/mentor/altera_eth_packet_overflow_control.v"         -work rx_eth_packet_overflow_control                                                               
  vlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/mentor/altera_eth_crc_pad_rem.v"                                 -work rx_eth_crc_pad_rem                                                                           
  vlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/mentor/altera_eth_crc_rem.v"                                     -work rx_eth_crc_pad_rem                                                                           
  vlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/mentor/altera_packet_stripper.v"                                 -work rx_eth_crc_pad_rem                                                                           
  vlog -sv "$QSYS_SIMDIR/altera_eth_crc_pad_rem/altera_avalon_st_pipeline_stage.sv"                              -work rx_eth_crc_pad_rem                                                                           
  vlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/altera_avalon_st_pipeline_base.v"                                -work rx_eth_crc_pad_rem                                                                           
  vlog     "$QSYS_SIMDIR/altera_eth_frame_status_merger/mentor/altera_eth_frame_status_merger.v"                 -work rx_eth_frame_status_merger                                                                   
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0007.v"                                                   -work rx_timing_adapter_frame_status_out_frame_decoder                                             
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0006.v"                                                   -work rx_st_timing_adapter_frame_status_in                                                         
  vlog     "$QSYS_SIMDIR/altera_eth_lane_decoder/mentor/altera_eth_lane_decoder.v"                               -work rx_eth_lane_decoder                                                                          
  vlog     "$QSYS_SIMDIR/altera_eth_link_fault_detection/mentor/altera_eth_link_fault_detection.v"               -work rx_eth_link_fault_detection                                                                  
  vlog     "$QSYS_SIMDIR/altera_eth_10g_rx_register_map/mentor/altera_eth_10g_rx_register_map.v"                 -work rx_register_map                                                                              
  vlog     "$QSYS_SIMDIR/altera_eth_10g_rx_register_map/altera_avalon_st_clock_crosser.v"                        -work rx_register_map                                                                              
  vlog     "$QSYS_SIMDIR/altera_eth_link_fault_generation/mentor/altera_eth_link_fault_generation.v"             -work tx_eth_link_fault_generation                                                                 
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0005.v"                                                   -work tx_st_timing_adapter_splitter_out_0                                                          
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0004.v"                                                   -work tx_st_timing_adapter_splitter_in                                                             
  vlog     "$QSYS_SIMDIR/altera_eth_xgmii_termination/mentor/altera_eth_xgmii_termination.v"                     -work tx_eth_xgmii_termination                                                                     
  vlog     "$QSYS_SIMDIR/altera_eth_packet_formatter/mentor/altera_eth_packet_formatter.v"                       -work tx_eth_packet_formatter                                                                      
  vlog -sv "$QSYS_SIMDIR/altera_avalon_st_delay/altera_avalon_st_delay.sv"                                       -work tx_st_status_output_delay_to_statistic                                                       
  vlog     "$QSYS_SIMDIR/altera_eth_10gmem_statistics_collector/mentor/altera_eth_10gmem_statistics_collector.v" -work tx_eth_statistics_collector                                                                  
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0003.v"                                                   -work tx_st_timing_adapter_splitter_status_output                                                  
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0002.v"                                                   -work tx_st_timing_adapter_splitter_status_in                                                      
  vlog     "$QSYS_SIMDIR/error_adapter/error_adapter_0002.v"                                                     -work tx_st_error_adapter_stat                                                                     
  vlog     "$QSYS_SIMDIR/altera_eth_frame_decoder/mentor/altera_eth_frame_decoder.v"                             -work tx_eth_frame_decoder                                                                         
  vlog -sv "$QSYS_SIMDIR/altera_eth_frame_decoder/altera_avalon_st_pipeline_stage.sv"                            -work tx_eth_frame_decoder                                                                         
  vlog     "$QSYS_SIMDIR/altera_eth_frame_decoder/altera_avalon_st_pipeline_base.v"                              -work tx_eth_frame_decoder                                                                         
  vlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0001.v"                                                   -work tx_st_timing_adapter_frame_decoder                                                           
  vlog -sv "$QSYS_SIMDIR/altera_avalon_st_splitter/altera_avalon_st_splitter.sv"                                 -work tx_st_splitter_1                                                                             
  vlog     "$QSYS_SIMDIR/altera_eth_crc/mentor/altera_eth_crc.v"                                                 -work tx_eth_crc_inserter                                                                          
  vlog     "$QSYS_SIMDIR/altera_eth_crc/mentor/crc32.v"                                                          -work tx_eth_crc_inserter                                                                          
  vlog     "$QSYS_SIMDIR/altera_eth_crc/mentor/gf_mult32_kc.v"                                                   -work tx_eth_crc_inserter                                                                          
  vlog     "$QSYS_SIMDIR/altera_eth_address_inserter/mentor/altera_eth_address_inserter.v"                       -work tx_eth_address_inserter                                                                      
  vlog     "$QSYS_SIMDIR/multiplexer/multiplexer_0001.v"                                                         -work tx_st_mux_flow_control_user_frame                                                            
  vlog     "$QSYS_SIMDIR/error_adapter/error_adapter_0001.v"                                                     -work tx_st_pause_ctrl_error_adapter                                                               
  vlog     "$QSYS_SIMDIR/altera_eth_pause_ctrl_gen/mentor/altera_eth_pause_ctrl_gen.v"                           -work tx_eth_pause_ctrl_gen                                                                        
  vlog     "$QSYS_SIMDIR/altera_eth_pause_ctrl_gen/mentor/altera_eth_pause_controller.v"                         -work tx_eth_pause_ctrl_gen                                                                        
  vlog     "$QSYS_SIMDIR/altera_eth_pause_ctrl_gen/mentor/altera_eth_pause_gen.v"                                -work tx_eth_pause_ctrl_gen                                                                        
  vlog -sv "$QSYS_SIMDIR/altera_avalon_st_pipeline_stage/altera_avalon_st_pipeline_stage.sv"                     -work tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control                                   
  vlog -sv "$QSYS_SIMDIR/altera_avalon_st_pipeline_stage/altera_avalon_st_pipeline_base.v"                       -work tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control                                   
  vlog     "$QSYS_SIMDIR/altera_eth_pause_beat_conversion/mentor/altera_eth_pause_beat_conversion.v"             -work tx_eth_pause_beat_conversion                                                                 
  vlog     "$QSYS_SIMDIR/altera_eth_pkt_backpressure_control/mentor/altera_eth_pkt_backpressure_control.v"       -work tx_eth_pkt_backpressure_control                                                              
  vlog     "$QSYS_SIMDIR/altera_eth_pad_inserter/mentor/altera_eth_pad_inserter.v"                               -work tx_eth_pad_inserter                                                                          
  vlog     "$QSYS_SIMDIR/altera_eth_packet_underflow_control/mentor/altera_eth_packet_underflow_control.v"       -work tx_eth_packet_underflow_control                                                              
  vlog     "$QSYS_SIMDIR/altera_eth_10g_tx_register_map/mentor/altera_eth_10g_tx_register_map.v"                 -work tx_register_map                                                                              
  vlog     "$QSYS_SIMDIR/altera_eth_10g_tx_register_map/altera_avalon_st_clock_crosser.v"                        -work tx_register_map                                                                              
  vlog -sv "$QSYS_SIMDIR/altera_merlin_master_translator/altera_merlin_master_translator.sv"                     -work merlin_master_translator                                                                     
  vlog     "$QSYS_SIMDIR/xgbe_mac.v"                                                                                                                                                                                
}

# ----------------------------------------
# Elaborate top level design
alias elab {
  echo "\[exec\] elab"
  eval vsim -t ps $ELAB_OPTIONS -L work -L work_lib -L crosser -L rsp_xbar_mux -L rsp_xbar_demux -L cmd_xbar_mux -L cmd_xbar_demux -L limiter -L id_router -L addr_router -L tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo -L tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent -L merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent -L tx_eth_pkt_backpressure_control_csr_translator -L rst_controller -L mm_interconnect_0 -L rxtx_timing_adapter_pauselen_tx -L rxtx_timing_adapter_pauselen_rx -L rxtx_dc_fifo_link_fault_status -L txrx_timing_adapter_link_fault_status_export -L txrx_timing_adapter_link_fault_status_rx -L rx_st_error_adapter_stat -L rx_eth_packet_overflow_control -L rx_eth_crc_pad_rem -L rx_eth_frame_status_merger -L rx_timing_adapter_frame_status_out_frame_decoder -L rx_st_timing_adapter_frame_status_in -L rx_eth_lane_decoder -L rx_eth_link_fault_detection -L rx_register_map -L tx_eth_link_fault_generation -L tx_st_timing_adapter_splitter_out_0 -L tx_st_timing_adapter_splitter_in -L tx_eth_xgmii_termination -L tx_eth_packet_formatter -L tx_st_status_output_delay_to_statistic -L tx_eth_statistics_collector -L tx_st_timing_adapter_splitter_status_output -L tx_st_timing_adapter_splitter_status_in -L tx_st_error_adapter_stat -L tx_eth_frame_decoder -L tx_st_timing_adapter_frame_decoder -L tx_st_splitter_1 -L tx_eth_crc_inserter -L tx_eth_address_inserter -L tx_st_mux_flow_control_user_frame -L tx_st_pause_ctrl_error_adapter -L tx_eth_pause_ctrl_gen -L tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control -L tx_eth_pause_beat_conversion -L tx_eth_pkt_backpressure_control -L tx_eth_pad_inserter -L tx_eth_packet_underflow_control -L tx_register_map -L merlin_master_translator -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver $TOP_LEVEL_NAME
}

# ----------------------------------------
# Elaborate the top level design with novopt option
alias elab_debug {
  echo "\[exec\] elab_debug"
  eval vsim -novopt -t ps $ELAB_OPTIONS -L work -L work_lib -L crosser -L rsp_xbar_mux -L rsp_xbar_demux -L cmd_xbar_mux -L cmd_xbar_demux -L limiter -L id_router -L addr_router -L tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo -L tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent -L merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent -L tx_eth_pkt_backpressure_control_csr_translator -L rst_controller -L mm_interconnect_0 -L rxtx_timing_adapter_pauselen_tx -L rxtx_timing_adapter_pauselen_rx -L rxtx_dc_fifo_link_fault_status -L txrx_timing_adapter_link_fault_status_export -L txrx_timing_adapter_link_fault_status_rx -L rx_st_error_adapter_stat -L rx_eth_packet_overflow_control -L rx_eth_crc_pad_rem -L rx_eth_frame_status_merger -L rx_timing_adapter_frame_status_out_frame_decoder -L rx_st_timing_adapter_frame_status_in -L rx_eth_lane_decoder -L rx_eth_link_fault_detection -L rx_register_map -L tx_eth_link_fault_generation -L tx_st_timing_adapter_splitter_out_0 -L tx_st_timing_adapter_splitter_in -L tx_eth_xgmii_termination -L tx_eth_packet_formatter -L tx_st_status_output_delay_to_statistic -L tx_eth_statistics_collector -L tx_st_timing_adapter_splitter_status_output -L tx_st_timing_adapter_splitter_status_in -L tx_st_error_adapter_stat -L tx_eth_frame_decoder -L tx_st_timing_adapter_frame_decoder -L tx_st_splitter_1 -L tx_eth_crc_inserter -L tx_eth_address_inserter -L tx_st_mux_flow_control_user_frame -L tx_st_pause_ctrl_error_adapter -L tx_eth_pause_ctrl_gen -L tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control -L tx_eth_pause_beat_conversion -L tx_eth_pkt_backpressure_control -L tx_eth_pad_inserter -L tx_eth_packet_underflow_control -L tx_register_map -L merlin_master_translator -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver $TOP_LEVEL_NAME
}

# ----------------------------------------
# Compile all the design files and elaborate the top level design
alias ld "
  dev_com
  com
  elab
"

# ----------------------------------------
# Compile all the design files and elaborate the top level design with -novopt
alias ld_debug "
  dev_com
  com
  elab_debug
"

# ----------------------------------------
# Print out user commmand line aliases
alias h {
  echo "List Of Command Line Aliases"
  echo
  echo "file_copy                     -- Copy ROM/RAM files to simulation directory"
  echo
  echo "dev_com                       -- Compile device library files"
  echo
  echo "com                           -- Compile the design files in correct order"
  echo
  echo "elab                          -- Elaborate top level design"
  echo
  echo "elab_debug                    -- Elaborate the top level design with novopt option"
  echo
  echo "ld                            -- Compile all the design files and elaborate the top level design"
  echo
  echo "ld_debug                      -- Compile all the design files and elaborate the top level design with -novopt"
  echo
  echo 
  echo
  echo "List Of Variables"
  echo
  echo "TOP_LEVEL_NAME                -- Top level module name."
  echo
  echo "SYSTEM_INSTANCE_NAME          -- Instantiated system module name inside top level module."
  echo
  echo "QSYS_SIMDIR                   -- Qsys base simulation directory."
  echo
  echo "QUARTUS_INSTALL_DIR           -- Quartus installation directory."
}
file_copy
h
