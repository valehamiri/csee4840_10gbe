
# (C) 2001-2015 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 13.1 162 linux 2015.04.19.20:35:37

# ----------------------------------------
# ncsim - auto-generated simulation script

# ----------------------------------------
# initialize variables
TOP_LEVEL_NAME="xgbe_mac"
QSYS_SIMDIR="./../"
QUARTUS_INSTALL_DIR="/opt/altera/13.1/quartus/"
SKIP_FILE_COPY=0
SKIP_DEV_COM=0
SKIP_COM=0
SKIP_ELAB=0
SKIP_SIM=0
USER_DEFINED_ELAB_OPTIONS=""
USER_DEFINED_SIM_OPTIONS="-input \"@run 100; exit\""

# ----------------------------------------
# overwrite variables - DO NOT MODIFY!
# This block evaluates each command line argument, typically used for 
# overwriting variables. An example usage:
#   sh <simulator>_setup.sh SKIP_ELAB=1 SKIP_SIM=1
for expression in "$@"; do
  eval $expression
  if [ $? -ne 0 ]; then
    echo "Error: This command line argument, \"$expression\", is/has an invalid expression." >&2
    exit $?
  fi
done

# ----------------------------------------
# initialize simulation properties - DO NOT MODIFY!
ELAB_OPTIONS=""
SIM_OPTIONS=""
if [[ `ncsim -version` != *"ncsim(64)"* ]]; then
  :
else
  :
fi

# ----------------------------------------
# create compilation libraries
mkdir -p ./libraries/work/
mkdir -p ./libraries/crosser/
mkdir -p ./libraries/rsp_xbar_mux/
mkdir -p ./libraries/rsp_xbar_demux/
mkdir -p ./libraries/cmd_xbar_mux/
mkdir -p ./libraries/cmd_xbar_demux/
mkdir -p ./libraries/limiter/
mkdir -p ./libraries/id_router/
mkdir -p ./libraries/addr_router/
mkdir -p ./libraries/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo/
mkdir -p ./libraries/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent/
mkdir -p ./libraries/merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent/
mkdir -p ./libraries/tx_eth_pkt_backpressure_control_csr_translator/
mkdir -p ./libraries/rst_controller/
mkdir -p ./libraries/mm_interconnect_0/
mkdir -p ./libraries/rxtx_timing_adapter_pauselen_tx/
mkdir -p ./libraries/rxtx_timing_adapter_pauselen_rx/
mkdir -p ./libraries/rxtx_dc_fifo_link_fault_status/
mkdir -p ./libraries/txrx_timing_adapter_link_fault_status_export/
mkdir -p ./libraries/txrx_timing_adapter_link_fault_status_rx/
mkdir -p ./libraries/rx_st_error_adapter_stat/
mkdir -p ./libraries/rx_eth_packet_overflow_control/
mkdir -p ./libraries/rx_eth_crc_pad_rem/
mkdir -p ./libraries/rx_eth_frame_status_merger/
mkdir -p ./libraries/rx_timing_adapter_frame_status_out_frame_decoder/
mkdir -p ./libraries/rx_st_timing_adapter_frame_status_in/
mkdir -p ./libraries/rx_eth_lane_decoder/
mkdir -p ./libraries/rx_eth_link_fault_detection/
mkdir -p ./libraries/rx_register_map/
mkdir -p ./libraries/tx_eth_link_fault_generation/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_out_0/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_in/
mkdir -p ./libraries/tx_eth_xgmii_termination/
mkdir -p ./libraries/tx_eth_packet_formatter/
mkdir -p ./libraries/tx_st_status_output_delay_to_statistic/
mkdir -p ./libraries/tx_eth_statistics_collector/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_status_output/
mkdir -p ./libraries/tx_st_timing_adapter_splitter_status_in/
mkdir -p ./libraries/tx_st_error_adapter_stat/
mkdir -p ./libraries/tx_eth_frame_decoder/
mkdir -p ./libraries/tx_st_timing_adapter_frame_decoder/
mkdir -p ./libraries/tx_st_splitter_1/
mkdir -p ./libraries/tx_eth_crc_inserter/
mkdir -p ./libraries/tx_eth_address_inserter/
mkdir -p ./libraries/tx_st_mux_flow_control_user_frame/
mkdir -p ./libraries/tx_st_pause_ctrl_error_adapter/
mkdir -p ./libraries/tx_eth_pause_ctrl_gen/
mkdir -p ./libraries/tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control/
mkdir -p ./libraries/tx_eth_pause_beat_conversion/
mkdir -p ./libraries/tx_eth_pkt_backpressure_control/
mkdir -p ./libraries/tx_eth_pad_inserter/
mkdir -p ./libraries/tx_eth_packet_underflow_control/
mkdir -p ./libraries/tx_register_map/
mkdir -p ./libraries/merlin_master_translator/
mkdir -p ./libraries/altera_ver/
mkdir -p ./libraries/lpm_ver/
mkdir -p ./libraries/sgate_ver/
mkdir -p ./libraries/altera_mf_ver/
mkdir -p ./libraries/altera_lnsim_ver/
mkdir -p ./libraries/cyclonev_ver/
mkdir -p ./libraries/cyclonev_hssi_ver/
mkdir -p ./libraries/cyclonev_pcie_hip_ver/

# ----------------------------------------
# copy RAM/ROM files to simulation directory

# ----------------------------------------
# compile device library files
if [ $SKIP_DEV_COM -eq 0 ]; then
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v"                      -work altera_ver           
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v"                               -work lpm_ver              
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v"                                  -work sgate_ver            
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v"                              -work altera_mf_ver        
  ncvlog -sv "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim.sv"                          -work altera_lnsim_ver     
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cadence/cyclonev_atoms_ncrypt.v"          -work cyclonev_ver         
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cadence/cyclonev_hmi_atoms_ncrypt.v"      -work cyclonev_ver         
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_atoms.v"                         -work cyclonev_ver         
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cadence/cyclonev_hssi_atoms_ncrypt.v"     -work cyclonev_hssi_ver    
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_hssi_atoms.v"                    -work cyclonev_hssi_ver    
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cadence/cyclonev_pcie_hip_atoms_ncrypt.v" -work cyclonev_pcie_hip_ver
  ncvlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_pcie_hip_atoms.v"                -work cyclonev_pcie_hip_ver
fi

# ----------------------------------------
# compile design files in correct order
if [ $SKIP_COM -eq 0 ]; then
  ncvlog -sv "$QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_handshake_clock_crosser.v"     -work crosser                                                                                       -cdslib ./cds_libs/crosser.cds.lib                                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_clock_crosser.v"               -work crosser                                                                                       -cdslib ./cds_libs/crosser.cds.lib                                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_avalon_st_handshake_clock_crosser/altera_avalon_st_pipeline_base.v"               -work crosser                                                                                       -cdslib ./cds_libs/crosser.cds.lib                                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_arbitrator.sv"                                   -work rsp_xbar_mux                                                                                  -cdslib ./cds_libs/rsp_xbar_mux.cds.lib                                                                                 
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0002.sv"                             -work rsp_xbar_mux                                                                                  -cdslib ./cds_libs/rsp_xbar_mux.cds.lib                                                                                 
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0002.sv"                         -work rsp_xbar_demux                                                                                -cdslib ./cds_libs/rsp_xbar_demux.cds.lib                                                                               
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_arbitrator.sv"                                   -work cmd_xbar_mux                                                                                  -cdslib ./cds_libs/cmd_xbar_mux.cds.lib                                                                                 
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_multiplexer/altera_merlin_multiplexer_0001.sv"                             -work cmd_xbar_mux                                                                                  -cdslib ./cds_libs/cmd_xbar_mux.cds.lib                                                                                 
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_demultiplexer/altera_merlin_demultiplexer_0001.sv"                         -work cmd_xbar_demux                                                                                -cdslib ./cds_libs/cmd_xbar_demux.cds.lib                                                                               
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_merlin_traffic_limiter.sv"                          -work limiter                                                                                       -cdslib ./cds_libs/limiter.cds.lib                                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_merlin_reorder_memory.sv"                           -work limiter                                                                                       -cdslib ./cds_libs/limiter.cds.lib                                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_avalon_sc_fifo.v"                                   -work limiter                                                                                       -cdslib ./cds_libs/limiter.cds.lib                                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_traffic_limiter/altera_avalon_st_pipeline_base.v"                          -work limiter                                                                                       -cdslib ./cds_libs/limiter.cds.lib                                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_router/altera_merlin_router_0002.sv"                                       -work id_router                                                                                     -cdslib ./cds_libs/id_router.cds.lib                                                                                    
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_router/altera_merlin_router_0001.sv"                                       -work addr_router                                                                                   -cdslib ./cds_libs/addr_router.cds.lib                                                                                  
  ncvlog     "$QSYS_SIMDIR/altera_avalon_sc_fifo/altera_avalon_sc_fifo.v"                                           -work tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo        -cdslib ./cds_libs/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent_rsp_fifo.cds.lib       
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_slave_agent/altera_merlin_slave_agent.sv"                                  -work tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent                 -cdslib ./cds_libs/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent.cds.lib                
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_slave_agent/altera_merlin_burst_uncompressor.sv"                           -work tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent                 -cdslib ./cds_libs/tx_eth_pkt_backpressure_control_csr_translator_avalon_universal_slave_0_agent.cds.lib                
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_master_agent/altera_merlin_master_agent.sv"                                -work merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent -cdslib ./cds_libs/merlin_master_translator_avalon_universal_master_0_translator_avalon_universal_master_0_agent.cds.lib
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_slave_translator/altera_merlin_slave_translator.sv"                        -work tx_eth_pkt_backpressure_control_csr_translator                                                -cdslib ./cds_libs/tx_eth_pkt_backpressure_control_csr_translator.cds.lib                                               
  ncvlog     "$QSYS_SIMDIR/altera_reset_controller/altera_reset_controller.v"                                       -work rst_controller                                                                                -cdslib ./cds_libs/rst_controller.cds.lib                                                                               
  ncvlog     "$QSYS_SIMDIR/altera_reset_controller/altera_reset_synchronizer.v"                                     -work rst_controller                                                                                -cdslib ./cds_libs/rst_controller.cds.lib                                                                               
  ncvlog     "$QSYS_SIMDIR/altera_merlin_interconnect_wrapper/altera_merlin_interconnect_wrapper_0001.v"            -work mm_interconnect_0                                                                             -cdslib ./cds_libs/mm_interconnect_0.cds.lib                                                                            
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0011.v"                                                    -work rxtx_timing_adapter_pauselen_tx                                                               -cdslib ./cds_libs/rxtx_timing_adapter_pauselen_tx.cds.lib                                                              
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0010.v"                                                    -work rxtx_timing_adapter_pauselen_rx                                                               -cdslib ./cds_libs/rxtx_timing_adapter_pauselen_rx.cds.lib                                                              
  ncvlog     "$QSYS_SIMDIR/altera_avalon_dc_fifo/altera_avalon_dc_fifo.v"                                           -work rxtx_dc_fifo_link_fault_status                                                                -cdslib ./cds_libs/rxtx_dc_fifo_link_fault_status.cds.lib                                                               
  ncvlog     "$QSYS_SIMDIR/altera_avalon_dc_fifo/altera_dcfifo_synchronizer_bundle.v"                               -work rxtx_dc_fifo_link_fault_status                                                                -cdslib ./cds_libs/rxtx_dc_fifo_link_fault_status.cds.lib                                                               
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0009.v"                                                    -work txrx_timing_adapter_link_fault_status_export                                                  -cdslib ./cds_libs/txrx_timing_adapter_link_fault_status_export.cds.lib                                                 
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0008.v"                                                    -work txrx_timing_adapter_link_fault_status_rx                                                      -cdslib ./cds_libs/txrx_timing_adapter_link_fault_status_rx.cds.lib                                                     
  ncvlog     "$QSYS_SIMDIR/error_adapter/error_adapter_0003.v"                                                      -work rx_st_error_adapter_stat                                                                      -cdslib ./cds_libs/rx_st_error_adapter_stat.cds.lib                                                                     
  ncvlog     "$QSYS_SIMDIR/altera_eth_packet_overflow_control/cadence/altera_eth_packet_overflow_control.v"         -work rx_eth_packet_overflow_control                                                                -cdslib ./cds_libs/rx_eth_packet_overflow_control.cds.lib                                                               
  ncvlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/cadence/altera_eth_crc_pad_rem.v"                                 -work rx_eth_crc_pad_rem                                                                            -cdslib ./cds_libs/rx_eth_crc_pad_rem.cds.lib                                                                           
  ncvlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/cadence/altera_eth_crc_rem.v"                                     -work rx_eth_crc_pad_rem                                                                            -cdslib ./cds_libs/rx_eth_crc_pad_rem.cds.lib                                                                           
  ncvlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/cadence/altera_packet_stripper.v"                                 -work rx_eth_crc_pad_rem                                                                            -cdslib ./cds_libs/rx_eth_crc_pad_rem.cds.lib                                                                           
  ncvlog -sv "$QSYS_SIMDIR/altera_eth_crc_pad_rem/altera_avalon_st_pipeline_stage.sv"                               -work rx_eth_crc_pad_rem                                                                            -cdslib ./cds_libs/rx_eth_crc_pad_rem.cds.lib                                                                           
  ncvlog     "$QSYS_SIMDIR/altera_eth_crc_pad_rem/altera_avalon_st_pipeline_base.v"                                 -work rx_eth_crc_pad_rem                                                                            -cdslib ./cds_libs/rx_eth_crc_pad_rem.cds.lib                                                                           
  ncvlog     "$QSYS_SIMDIR/altera_eth_frame_status_merger/cadence/altera_eth_frame_status_merger.v"                 -work rx_eth_frame_status_merger                                                                    -cdslib ./cds_libs/rx_eth_frame_status_merger.cds.lib                                                                   
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0007.v"                                                    -work rx_timing_adapter_frame_status_out_frame_decoder                                              -cdslib ./cds_libs/rx_timing_adapter_frame_status_out_frame_decoder.cds.lib                                             
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0006.v"                                                    -work rx_st_timing_adapter_frame_status_in                                                          -cdslib ./cds_libs/rx_st_timing_adapter_frame_status_in.cds.lib                                                         
  ncvlog     "$QSYS_SIMDIR/altera_eth_lane_decoder/cadence/altera_eth_lane_decoder.v"                               -work rx_eth_lane_decoder                                                                           -cdslib ./cds_libs/rx_eth_lane_decoder.cds.lib                                                                          
  ncvlog     "$QSYS_SIMDIR/altera_eth_link_fault_detection/cadence/altera_eth_link_fault_detection.v"               -work rx_eth_link_fault_detection                                                                   -cdslib ./cds_libs/rx_eth_link_fault_detection.cds.lib                                                                  
  ncvlog     "$QSYS_SIMDIR/altera_eth_10g_rx_register_map/cadence/altera_eth_10g_rx_register_map.v"                 -work rx_register_map                                                                               -cdslib ./cds_libs/rx_register_map.cds.lib                                                                              
  ncvlog     "$QSYS_SIMDIR/altera_eth_10g_rx_register_map/altera_avalon_st_clock_crosser.v"                         -work rx_register_map                                                                               -cdslib ./cds_libs/rx_register_map.cds.lib                                                                              
  ncvlog     "$QSYS_SIMDIR/altera_eth_link_fault_generation/cadence/altera_eth_link_fault_generation.v"             -work tx_eth_link_fault_generation                                                                  -cdslib ./cds_libs/tx_eth_link_fault_generation.cds.lib                                                                 
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0005.v"                                                    -work tx_st_timing_adapter_splitter_out_0                                                           -cdslib ./cds_libs/tx_st_timing_adapter_splitter_out_0.cds.lib                                                          
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0004.v"                                                    -work tx_st_timing_adapter_splitter_in                                                              -cdslib ./cds_libs/tx_st_timing_adapter_splitter_in.cds.lib                                                             
  ncvlog     "$QSYS_SIMDIR/altera_eth_xgmii_termination/cadence/altera_eth_xgmii_termination.v"                     -work tx_eth_xgmii_termination                                                                      -cdslib ./cds_libs/tx_eth_xgmii_termination.cds.lib                                                                     
  ncvlog     "$QSYS_SIMDIR/altera_eth_packet_formatter/cadence/altera_eth_packet_formatter.v"                       -work tx_eth_packet_formatter                                                                       -cdslib ./cds_libs/tx_eth_packet_formatter.cds.lib                                                                      
  ncvlog -sv "$QSYS_SIMDIR/altera_avalon_st_delay/altera_avalon_st_delay.sv"                                        -work tx_st_status_output_delay_to_statistic                                                        -cdslib ./cds_libs/tx_st_status_output_delay_to_statistic.cds.lib                                                       
  ncvlog     "$QSYS_SIMDIR/altera_eth_10gmem_statistics_collector/cadence/altera_eth_10gmem_statistics_collector.v" -work tx_eth_statistics_collector                                                                   -cdslib ./cds_libs/tx_eth_statistics_collector.cds.lib                                                                  
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0003.v"                                                    -work tx_st_timing_adapter_splitter_status_output                                                   -cdslib ./cds_libs/tx_st_timing_adapter_splitter_status_output.cds.lib                                                  
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0002.v"                                                    -work tx_st_timing_adapter_splitter_status_in                                                       -cdslib ./cds_libs/tx_st_timing_adapter_splitter_status_in.cds.lib                                                      
  ncvlog     "$QSYS_SIMDIR/error_adapter/error_adapter_0002.v"                                                      -work tx_st_error_adapter_stat                                                                      -cdslib ./cds_libs/tx_st_error_adapter_stat.cds.lib                                                                     
  ncvlog     "$QSYS_SIMDIR/altera_eth_frame_decoder/cadence/altera_eth_frame_decoder.v"                             -work tx_eth_frame_decoder                                                                          -cdslib ./cds_libs/tx_eth_frame_decoder.cds.lib                                                                         
  ncvlog -sv "$QSYS_SIMDIR/altera_eth_frame_decoder/altera_avalon_st_pipeline_stage.sv"                             -work tx_eth_frame_decoder                                                                          -cdslib ./cds_libs/tx_eth_frame_decoder.cds.lib                                                                         
  ncvlog     "$QSYS_SIMDIR/altera_eth_frame_decoder/altera_avalon_st_pipeline_base.v"                               -work tx_eth_frame_decoder                                                                          -cdslib ./cds_libs/tx_eth_frame_decoder.cds.lib                                                                         
  ncvlog     "$QSYS_SIMDIR/timing_adapter/timing_adapter_0001.v"                                                    -work tx_st_timing_adapter_frame_decoder                                                            -cdslib ./cds_libs/tx_st_timing_adapter_frame_decoder.cds.lib                                                           
  ncvlog -sv "$QSYS_SIMDIR/altera_avalon_st_splitter/altera_avalon_st_splitter.sv"                                  -work tx_st_splitter_1                                                                              -cdslib ./cds_libs/tx_st_splitter_1.cds.lib                                                                             
  ncvlog     "$QSYS_SIMDIR/altera_eth_crc/cadence/altera_eth_crc.v"                                                 -work tx_eth_crc_inserter                                                                           -cdslib ./cds_libs/tx_eth_crc_inserter.cds.lib                                                                          
  ncvlog     "$QSYS_SIMDIR/altera_eth_crc/cadence/crc32.v"                                                          -work tx_eth_crc_inserter                                                                           -cdslib ./cds_libs/tx_eth_crc_inserter.cds.lib                                                                          
  ncvlog     "$QSYS_SIMDIR/altera_eth_crc/cadence/gf_mult32_kc.v"                                                   -work tx_eth_crc_inserter                                                                           -cdslib ./cds_libs/tx_eth_crc_inserter.cds.lib                                                                          
  ncvlog     "$QSYS_SIMDIR/altera_eth_address_inserter/cadence/altera_eth_address_inserter.v"                       -work tx_eth_address_inserter                                                                       -cdslib ./cds_libs/tx_eth_address_inserter.cds.lib                                                                      
  ncvlog     "$QSYS_SIMDIR/multiplexer/multiplexer_0001.v"                                                          -work tx_st_mux_flow_control_user_frame                                                             -cdslib ./cds_libs/tx_st_mux_flow_control_user_frame.cds.lib                                                            
  ncvlog     "$QSYS_SIMDIR/error_adapter/error_adapter_0001.v"                                                      -work tx_st_pause_ctrl_error_adapter                                                                -cdslib ./cds_libs/tx_st_pause_ctrl_error_adapter.cds.lib                                                               
  ncvlog     "$QSYS_SIMDIR/altera_eth_pause_ctrl_gen/cadence/altera_eth_pause_ctrl_gen.v"                           -work tx_eth_pause_ctrl_gen                                                                         -cdslib ./cds_libs/tx_eth_pause_ctrl_gen.cds.lib                                                                        
  ncvlog     "$QSYS_SIMDIR/altera_eth_pause_ctrl_gen/cadence/altera_eth_pause_controller.v"                         -work tx_eth_pause_ctrl_gen                                                                         -cdslib ./cds_libs/tx_eth_pause_ctrl_gen.cds.lib                                                                        
  ncvlog     "$QSYS_SIMDIR/altera_eth_pause_ctrl_gen/cadence/altera_eth_pause_gen.v"                                -work tx_eth_pause_ctrl_gen                                                                         -cdslib ./cds_libs/tx_eth_pause_ctrl_gen.cds.lib                                                                        
  ncvlog -sv "$QSYS_SIMDIR/altera_avalon_st_pipeline_stage/altera_avalon_st_pipeline_stage.sv"                      -work tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control                                    -cdslib ./cds_libs/tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control.cds.lib                                   
  ncvlog -sv "$QSYS_SIMDIR/altera_avalon_st_pipeline_stage/altera_avalon_st_pipeline_base.v"                        -work tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control                                    -cdslib ./cds_libs/tx_st_pipeline_stage_pad_inserter_pkt_backpressure_control.cds.lib                                   
  ncvlog     "$QSYS_SIMDIR/altera_eth_pause_beat_conversion/cadence/altera_eth_pause_beat_conversion.v"             -work tx_eth_pause_beat_conversion                                                                  -cdslib ./cds_libs/tx_eth_pause_beat_conversion.cds.lib                                                                 
  ncvlog     "$QSYS_SIMDIR/altera_eth_pkt_backpressure_control/cadence/altera_eth_pkt_backpressure_control.v"       -work tx_eth_pkt_backpressure_control                                                               -cdslib ./cds_libs/tx_eth_pkt_backpressure_control.cds.lib                                                              
  ncvlog     "$QSYS_SIMDIR/altera_eth_pad_inserter/cadence/altera_eth_pad_inserter.v"                               -work tx_eth_pad_inserter                                                                           -cdslib ./cds_libs/tx_eth_pad_inserter.cds.lib                                                                          
  ncvlog     "$QSYS_SIMDIR/altera_eth_packet_underflow_control/cadence/altera_eth_packet_underflow_control.v"       -work tx_eth_packet_underflow_control                                                               -cdslib ./cds_libs/tx_eth_packet_underflow_control.cds.lib                                                              
  ncvlog     "$QSYS_SIMDIR/altera_eth_10g_tx_register_map/cadence/altera_eth_10g_tx_register_map.v"                 -work tx_register_map                                                                               -cdslib ./cds_libs/tx_register_map.cds.lib                                                                              
  ncvlog     "$QSYS_SIMDIR/altera_eth_10g_tx_register_map/altera_avalon_st_clock_crosser.v"                         -work tx_register_map                                                                               -cdslib ./cds_libs/tx_register_map.cds.lib                                                                              
  ncvlog -sv "$QSYS_SIMDIR/altera_merlin_master_translator/altera_merlin_master_translator.sv"                      -work merlin_master_translator                                                                      -cdslib ./cds_libs/merlin_master_translator.cds.lib                                                                     
  ncvlog     "$QSYS_SIMDIR/xgbe_mac.v"                                                                                                                                                                                                                                                                                                          
fi

# ----------------------------------------
# elaborate top level design
if [ $SKIP_ELAB -eq 0 ]; then
  ncelab -access +w+r+c -namemap_mixgen $ELAB_OPTIONS $USER_DEFINED_ELAB_OPTIONS $TOP_LEVEL_NAME
fi

# ----------------------------------------
# simulate
if [ $SKIP_SIM -eq 0 ]; then
  eval ncsim -licqueue $SIM_OPTIONS $USER_DEFINED_SIM_OPTIONS $TOP_LEVEL_NAME
fi
