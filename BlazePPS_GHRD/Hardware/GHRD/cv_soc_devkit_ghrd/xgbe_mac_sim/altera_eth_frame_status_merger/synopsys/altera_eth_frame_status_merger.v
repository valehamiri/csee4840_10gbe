// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HK1*A]=<RB< *)D5 %8X^3A^_NK-)N9) R4M<@U&X1$Y F23.0V67(   
H@)=N9ZD1W:N/".#]&^LK#]NG3=+UNDF]&(XLDK58[HR?'@XSJZL_%0  
H13/0BF_93ZYD5P)E(W5 !(,^,ST?>NR]W&-2'FGE+P0-IX@^)VXS"P  
H['  V@Q(J,IR\=)QQ'4"!^8&G7M.YE. DY$[X+7$96*TL;94"NH6S@  
HS?/EIO%I7L[=QH/T/Q=1:ID3OL@]O^*#J 7<9&JH5FB!9F"6F6$0]0  
`pragma protect encoding=(enctype="uuencode",bytes=6656        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@-ST03[-W7T77\WO!,#]F.YBW<#@ BU+SL[K(HM=S8%4 
@8GE'4N#SVE55MI #+&@#&#4(<FZ?DZ-ET6E<*]^MW>4 
@$RJ[77:X$*KYQ1,CGS5(4C=J?WOQDR,*WI0-FC,*AH\ 
@,>FNB]J_(E]B^UMX_L\7O\6O[Y=K4LB"BH+[.7.LIS0 
@H&W&"1QB[)<DT5S6+J;"%LUM2>AJYF#/LF2=Y7M 6B  
@VR+\P"9PL]#R6AS"Q;;O./^Z&'US"4,USM.PIS,XPU( 
@W9>&=?=.YN(;$I OALBCI]U6SF1> 0<&J,MF<"Z>%_8 
@)\,RSG,PQQ%0$18^U@XMF:>7PFBV(CY,-+9R=]'W3&\ 
@(6LY#]A\=9"N9H >=A%6H#N_ '*6[ 9<)S0T."U>ZH( 
@F@$[=)'XGI CD30:)4X/P'[P%4J+^+)=O!%5" T3^ T 
@E!+P>_#:6S=1 !&#+MGMYC_]V)1Q@IE8>&/?XW2;6T4 
@!T&<XE!I!DX$;(AR=K\:_.86PE:DKZ1B]B@O'$K**/X 
@&P5<81-D;<+<ZXP6LIA,=:P,+?L4X7$5#2H3"?=$N=T 
@QH:@7.N_>PH%T2.1 G3[>.UDRC@I(;=6$6PE::E=G&D 
@FUNM]MCD9$5B^^/.YEQ/C\=KQJ DMU"4\QD3);SZ>L  
@V6I#K<7!+I6=AY?,>[K+H,CH1N])%Z9/<BG2$"%_Z2  
@99!,*K+:97^C0P6_0#D65U9\Q+-P5=C &Y35\M0?_'$ 
@IM/GE3!*\#U9Q^^[1"X^?435UY!IZQ:R2,YWM&343N( 
@.M8Z#?>:8FE$MPC>X3CO\6O"O</S.<FLZ4<F 9F4Z9T 
@/:4\YEXM?).T&"M2I__&WF=W:%[2/TJ@$P;Q_WQ;^3\ 
@*UU/PVT?VZX;S\Z0M2J6^'L&(;U@KPF0E.I3]M2I<OD 
@B\HK$ U.2OPSO5_'Y8V1==BS'];^JKV3B4J.VO[CA*, 
@;[._+2YZ]H%8(,'D&5#>@ZR[C4URYD4%*Y5_45'D5.H 
@_8H8A]-7*-MM:38J?%6N JKF44E3N.$6_=.[BZ+O8!, 
@"@MZ=T7@9&D8^#[E$L:J%XT6CU#"$K<2WC<9(D!O,U  
@:+XA7J&2LT.I'3H+0)K9 D[XL;-/W(O^)?3M#R+F=L@ 
@*Q_&ZW'3"51CA^:$Z;\9?%:XN^F#>ZU%0(K_<>SVO60 
@9P//BUF<JIJ1O-U-V/=CWQ-JFGE/(6!F?&P'F*]#69  
@1CB>QF=]77+7L&?B^U[G>XXH)>A#$-EEZ)7T%6.NB<\ 
@5_Y?G:1-._\PL 3TV61+>:-H@ N!E-3&Z5F?!I&&VM  
@XOK8GVIG1B:'@R\:2K^(I:.6\A;?+J<,(.LJ0QB[;.X 
@IQB(@.0\X2F<E"^J<L@E\$5SST("4 FV.[%0H,/3 M\ 
@Y+F63A'V2.,-L-546]DLRA$Z=AW6"$0^X!)8[5#JJ'D 
@\XR83/UR!@;2^/]L,[AY"-8V+E>>%D@]WJ3=E#AH[5P 
@4?X&H9CX"YK7'#'S60"F+U*-")$4_D6!R_;=H/#(_4T 
@U(JTC*RSV3V=/NH9R-G7"AU'1XYU%7R[C)51BT4U5V$ 
@XE][]#C@TL 6<<D0=5%9(E?'A%Q<)%QSJ#._V+)#93H 
@2)\RA16C2)&<\ZQ7!>^0343YX$P)OEZ1K7L$2CX17\@ 
@36C<W%&$ @^:V1^IB!F9-N>NU<\43RGQ=::T>YZIH)4 
@ZN,A)_;P4O,:KB2!*)NW^!L:B4P$Z[='*QB_.;_/ $P 
@H-NYE^7 . IM%Q<L+LB@3*?I4*39U)43241\ 7I."Z4 
@8OC5]^B0_Z"9WB!4)H@7R.06[2C.!&WP7 $N&QYWB)0 
@ ^Q(S)9,A(@+2VZRHOVS+@H-O])EIG8\N]2$BD\^!;, 
@)B!1FW(I$&VAX5Z.<8*TTB(GSE*FYEHX3-N!0RWK^L\ 
@%C#+9=K70MD5!:-G.#F&^A_Z71-B\-D[$Y\D(X94?_@ 
@1L?#M:V17 IB5PD=TD2JZ!ORVAI688U^>1=53Y4?MU@ 
@-RLV?M"R0'$[5*6XWK0UVVY"$");496R/Z+VC!@XL[  
@ <[3)7H<D4<?"B.;&)2*CT;U%4*2/W-.BVP.8GG'DUX 
@QD4^"I.O<?-'\HUS=)G=QL=(F*M-P37)](J>$F'LNC, 
@B'_+B#>0Q?@0&:B$5A>=_20Y5H_A<YJE8]-G#M]CM[@ 
@@CTS-+;T#.A"Q\>?N.?UV5SI!HH*2?^<+[]',9?!J]D 
@VU)NLAXH?CH".\F-EJ[E -*D$0PPMB5.HD AN+)W H  
@<^TTW+5K]-I4]Z;M3-BZ="=T4M44!-PPMN/3Q.B<U4\ 
@Z4S0CJ,IB!N$B_IW]4V-C$P* I8K?2,?@B41LM Z5B8 
@D?"^ J1[LS>Z.&(L78)[QB)_%3(5T6!N6_'FNUDG\18 
@B6%B09&V=1H>Q^?\DX7P"7CN?NMEQ2"JT&0A@"X.P]L 
@GU+V'R+#YJ=(7XW)Z!""B  W="8G7SX/E^CT?=*GD^X 
@N5LOJ-SG!AD!17-3M(-3G-K!HRH.?]9$]Z4L"A('>MH 
@%#DF/LTGP6MY5#Q C*A.8Q4Q^7X!54 /(0+:.'L99Q8 
@721NYOQ&!*.G.=A>V?(> .TQ #6QLH%.3T[Y'(5'+[@ 
@AB+H=-^N)@!FF8-L65\X!Z]="T6PK1_+$1!M1Y/YP^T 
@/=X,(PJ6U]:'S$<J/!JR!R=@RT3$CM%7C[.P2*J0NCD 
@DC_Q7O^%.!EI1+QNY]5/_*TSMD<;M110BJLP7:O*@G( 
@$Y08A*P7_,2>=#Y @ 5>EW<*W78^9+#'68TZZ9"/*%$ 
@]TZCP4!3V05)![J:UI/ "S++^JV__WDDOQRWZ[A.\K  
@(L'U/3*%.^'@93GAJ"[T]^)>3#3GJ6VEZ0BQF!M)4[$ 
@D-3)*:XF*$\3M!!F#@L(#323+,]\$SV__'#Y@J[E_Q\ 
@#R;!BA=FHMO>'/T/^3=>0+W!UQMD\JU'N%WBDD)VU!0 
@9Z,F&@-H-E%/_1N&<@G[61.6R.S*R(A=?U5I&.J_^'@ 
@_E<GYZ2"!AD^.X/5(/*5\0[$431G54:V4!O#9/RM8@D 
@6A"&H[LO=E,FW?7S-RIQP1S:9/F_VIXGI0HUU"]T%ML 
@':98RX1W+)),*;03P^PJ1SB%DC D+;G.8)BZAUN6'FX 
@W.U\8;)P'OB!B5TD"L13_\L:[\,SO+Q1/Y3^+&B8I!( 
@Q8>A1D4R0?G$.S=C!_1CY+[$JQJ!()S;O>[W#D%'Q'@ 
@7H*FIZ9CT7/20M>2V_SA\> E2C 15A&;=_I#[&]/>N  
@6_]!@]K$Z0XPB4_P16Q8@TK_Z^3LKB?:2W!3Z(_NU5, 
@";"PBCE^X[OU>66T^>AGMIC>Q:".V^'SA]-U2"6Y=@( 
@-/AG&]L;S^,%C1D\>XY*#9G;#;5S3J/132A W=1OPPP 
@>!FT[5157AC3"-:5<WU9+?LN9X[OV]UP(,.KUQ'WD+P 
@8/7_[\&#$!4@O=UBF-(JI%6RN?%<#"T :/,'+>?$YJ@ 
@:B^VK#'K)3*Q[\F@?9R/5J[E,GZKM^9Q<?YT*'A\BM, 
@%*UZ0VY'X$M#I[&V!AP8'-Z?&Q@OU0IP"A),-57'8J8 
@/Y?=Z@KNLT[Z@_O,) #9=_3[OC73HPKY8[70;STQ'D4 
@(!W5@0>RI-3&NA-.PXPDLGS>);5ULPB%,'*!@6U%>N, 
@/QP5D:CY;:WH2\@I.0Q)J&[ _*@:>M^?H^@3?>%X!IT 
@(J-&;_\!&)[1 7B>-H(PIGS8_!#F#;_D'@ELNT#]U,P 
@F"="YW S$IUGLWQ'_*^]@K<DL>W[CA,;T>]%:'W/@#H 
@,)]T\KOH""4J[AZS!%<DCK*3*N7PR76G >470!G\.3X 
@W:]??E]<?!4< U'<O$1*6(!B-YGR<LLG\_S,6'+L<[H 
@ 'D<.>J&W#BE?!#&VUTG$C;,QLVZ=,0?=/"K,T5ZD"( 
@+W3($+O'PRRAAHBG51=J%\W:CKL_JG>$$DWXMHJ+Z=T 
@CLY-U7/P(FXWI.(=#8)B>RMT3%*(@ 3_ROF<N_9G*78 
@5Z#N8Y1-1RUKANZ2_E9,>KJ-^_LXRMXH/OYHJC)E".0 
@1(UT_L!.'M\DB(AA ADGJ;DWI/96##>D]->)M4OR\X@ 
@ZM$SU9WE+PPW;H!$?NF@*%:;.KV&T,Z/8N#G84A!27, 
@+MVDN'WD0PV% 2NW6X6LY]$5R@?N\;$[Q#F(><Z4Q@D 
@"CA>6!P\1Y*DQ+KW7V@PZJ+I[\I(N3,$8I_R @;X>^P 
@ZP(K/M=9#=ZF39]N+@N8/J/"_[T^&;>[*2I_%-O8CST 
@&_NP*,$P3-;BV(DJORK P^/3M/J$ 9K/Q?C4$=(_[N( 
@9_DCOD;'Z)F*Z1Y"+*8BF6CJ3OQ<,JB0"M5D?E(K?88 
@/8KA_G2.$0\%@!3:9S".B^.2-0?IZ1QT0'Q_P C 7#  
@2 B%4D>0J&FB;"IX1+#%45Q=P5X$W[+KKD#@;A'([$, 
@&(Q;G"S7I!SYY]K<-MVDFXG'+M\]1 H=]F/QEBHODD8 
@1Z7")YA%0]R%ZDV+PB36;L4^JQ8E']*R8FLXF+X3R)T 
@9AKQ"(^0J\]%=@0VH.LF%DL)YIH\TY76O<S<7ZG:"+4 
@,O7^.!P3XM,ZK(IDHROK2 ]-RG\!YEAOVI>IQ2[))<\ 
@IQ'=^R$5$8A6_JE:62H1^ )GG>A>MPW,RGR-#L49CO< 
@UUX,L$PU$8&D&=9NM ,SR]E7#J898H$ D+PG$699O/( 
@U!USPY='0F_C'OC\B_SAE*(*P)LRE_ 5;.02*>I!&WD 
@LO5^H%+<1JQ+[<,5T-2Y]S<@3+B6NJI1;W<\TD+WNH\ 
@W7(HZ$A!4*!2@Q I%YU:!1>H$>L/'4! A76:&7$,/6\ 
@L=0IE].GS9C;G,MRMZL]DCU0*$$Z=K<9A^T]WT+&=>X 
@\4WB<@RQKIN5WDF[W5BVU. LG[;XGWS2^4-+B_9G]RD 
@J,V10["F];!@]C;D5>)=D%(,N>.=%,_IXE8@6AP3+98 
@8%;LY6<9-BAOBM4E!?0\INE13V; (?_Y_1Q><$\<_0< 
@*6]]P@TB'SUC9<C:LO+M;A\"-2AEF &2&^Z:(59-'VD 
@QN0'NFC[-0Z*VY-HO+-$P\/_;WR%$2Y&R)PKQ!:@S^< 
@*\O^U]Z)W@5D?S?JK*<L5,T/1=AK#$!:+@F:WH!QN0T 
@6PH2;UEP@IJK7FQI01>*">GU<*@*&BG^.5O:=*1OO_@ 
@,1$GH)>QO17;3 X9JT4%#!;5EX:M7U_BU%U45=#I76T 
@9,].8$,@)UL]( WZ*2]?:]4*HD  RGWU3PD!$RZKZMT 
@G,VZ6T!W!1PFM+^FB^Q9SWV>4VWO:?]%!YN;$&N@))D 
@ZE;WDRI^(/<\G/^C]5?O1 SXL_NNP;<K!]T)!GHTRQT 
@0W2G83QPUO0_X44?C8527G/]1B5E1^O3G]3\675<")P 
@/CVO:/G.(& QUQP(:$ BR909;NI.E@?RKU8D[@;:"?$ 
@_9Y8JQ?C46.#^T&)V0V)EFGZ8%;:DY SWMCKUV&8(58 
@K%;Q?-_(3EE<M59,_(_3'O6C+$3\U"<']NVU8NU$EC$ 
@;R\Q50-AQ&7(1&"&/8R8U \BND"2.$8CKFS/6S=IP1  
@, [O1]^_Z\@;J_*>+FPZ0RN?^L6-;:ZT$*D](T^Q%Y0 
@*@]: 0_)&!4Z.C0-9K7/:6J+R-[-%*F[\60A8^"&U84 
@^S9>CCA64!+(42F?,:.R?G%W/L$G;L?VI.U>*E6WPPL 
@<( $BUR=3W@T1+VNI(GU]%X47+5$!CZ-Q"<WN_9O#$  
@3MNYF4SVY86\3L'GP.%^70/P)=89>3H=_95IO.#*2<< 
@#5C26#UR/AH9D]F5@BB%P)7B1U%ZQY3<E38CRRJ!*34 
@GSQ4SPO=-)4 3H/:?67].';[JV4K&N4N\GUI)1$DB-$ 
@O$T6'E$7:3N3O$K YLBRD\/KJ;7J:D7?3#,LEU^W_@4 
@N#_E'8*>&)?*T"+L*4XF-O_7[X0^B@E$G1( 0<<?A9$ 
@YKE$$)4R8LG#1TV1H^(YNZ854B@^_;[V.06P4:2?R!$ 
@RB0T3R9NRX +.'38=U.U' B+R,U;#E6K*9"I-SDJ,/, 
@J6UY^"LA9"H>N)%RK?1QXJF :LA9^A6\CCMU['SVY6< 
@=%+G4<%EBL'9$!,20WDH>&5*N]==DY9OU5RPO_DK>SP 
@QCA5L$'6BB[DI:3_:"VZ\@"MU(YH=#!U3"EG B(UM(P 
@O<\K3CI-G:;H:?W S\VZENN\>HGDK0@3E',-I&SN9B@ 
@&<T@!PQS_>UA-_QUNVR\YXE:(Q<QV/E5'XD)N'R(IC, 
@K8F8A+Q:%T+3R,@^CDXW<9IGLO$_Z)AW=O$F3N0J>X$ 
@JK;OE]C1?[L^5-DJO=1BP=6SS2E2,@Z6&Q:_C<2[F=L 
@?81$3*W!S$XVEO#OXA,V0:<U4T&?,S%2F6:+:ITJ\*X 
@*F009.<@I9\IS;,!:EP=9K::15CZ@V X0!*,VZZ/8'4 
@MO])7XNV6\9GXX,NH53!):4F9?=_9X0R2D0.S9<# 6L 
@X<B"OM.W8E0Y,/OYQW!BU4[[0RLW/MM! _Y>'*J>5(4 
@^QY>G,X.PU? <P,2!;^YZ2*!42VGUGJH:B)+5>P-K$, 
@6Q&33L^/Y 3X$=:R=.J/5=VZ1^).!R=( HD2="4U"]@ 
@TJD6LM][IS(H5U954OL='8;2_ISG;\#]CQ6YYV<LWX4 
@4;MG7HS=K,$8NN10Z 4@[#+,3+[@K5NZ&]E>TTC5,NX 
@7)W@-;^<$K>(*X0$4HAW +,.4JF_A[17C)%^D7"(^?L 
@*[7J5+IVRT0Y'&?W8_.B.[M]?3$Q_9SK7K*/@6U<KKH 
@L*(F[2D]4T"?41]B"T*41Q8-:[H^Q.]SQ#!JXS(C!FD 
@?MD]G@Q(F;^S)9=R8<*NOG\VD$PH08!JY..R<_LD($( 
@#^@E<DM[*!Q#?$[B)5))&55UL^PLGXB_,OA9?8OB5-4 
@3$N XM4.)"^HH75Z]VK]'05')"PX&.!>AI%VGP<32;, 
@@AS1 "R"+E+D"-T4S3Y"2+R<6"4'8+_PL>+"%(>8C)T 
@:2'H=24?0$"Y8K8K&(@6>!]O)9&^DFFDUM!P0[/6@W4 
@!'&A#^)*";O^Q2Y:6]_N/\4QQU12D?K!USAAP-&H>8< 
@6XX(6XU;(!N NN>\A6'0PW"67N'8+92)4[>4.8*J8DP 
@9!*.$\$@8LR'M7.MOSUST5E,+HJ@O]W5LROK,:4AN>  
@*@ILZYSY3&)W?G+J_G'>_VB7[[&^?Q\)5]_Y-P%%^<H 
@JA[<^XX/\"8,2@?P8.O]B'&]Y_]5]R^Z-0_>V70!:;P 
@;DKMVE5!+X/>$<1):E&@7-)_&38:>F-G3;4,^26F[;T 
@1$H/TLY=E@=3-J?=V1P)XTW@Y!ZZL+P2(WMZF1N$9+4 
@TB7P! #TGI/OS6H/X4>_BKFXH"U<8#8[6=784U_QBP, 
@M5 @(U_"ZB$8R.0<DY5\G[(>^P3JX.D!1\?<P9/(!,( 
@M[1=)Z0IZ+RFOO-Z3S-67C653Y4%U=W@Y#<WU>%!%2$ 
@..(99P&4?9 $C5%1;H*\S7)T(>I?EQ?4C[MM>I]>9_\ 
@%;5ZBNI@(60?SE.P11M\U'C8_O-G!AZDJ<> J KJ8!@ 
@Z0Z-Z7%J:!Y4C;KV[S)9*-0:CD_6^[?:CJ1Q!)#+8#  
@14]#<'6A B(HEWI_\)_A^12P16TVHG_MRWH L7_'C"D 
@M:S(9QH&3244,E2I[7EV58+QWD&O[77WSY786W)"DR, 
@'WFE!Z\L<S\ S?8W*<2\VD]FTE&<F/M[<A2-0Y(BP,T 
@?+1[&F6#YQ,DQ3S-)J"@H5J\T;;X:O%@42.Y<CID?UH 
@AL[N5G"L6! F,SJ,RDXQ=XN_<CT9W8-IBOH\=[L'D+@ 
@C*:T4,8",TDZ=2=(9,#!3*QZG'*'UQ;"/GJICM&EC6@ 
@^SF,R1A*. 0:S@#Z5#$U)\<2,6UR*$#RP*?UTJ-I%3L 
@\_=GFNZO/D62RK3,)'OWDI\FQ/ 6$Z<7[8D>CCE!I[@ 
@[;=X:(%7*0T3M\NWD'[@D=O$2%DU&C;DEV+]0*XR\<L 
@K2(R&1;+RSFM9"!D,8ZH1YW@ 1BS#"H/>M:9^UE6PB\ 
@C,/7@LFT,-QGZ%[VPECPD1J(L37@O@K?US2;HB$:RW8 
@ $(:0V7;+'AQM9[ (F6Z$\Z8[CIXC^,3/2P9U0/LN_8 
@):FLJ -<!,_S829VG2W%&4 WST_Z#@;K20.URUQK0HT 
@7II8Z!7R6(H.261%O3EV<]&&LB\;4L(V2LP5DS 3\\0 
@6X#,KC0G:\[V#*!#O-C-87OWRB D#--WQM\-W8M),)0 
@4:L;+TIIM65_+/&;A(Z=B&NN0%5V>5'?YU+%4IANE0D 
@F>2 .499TVH[0Y.F$%JK3ZL-)+H^X*KD,ER/.54FY)L 
@G''&R8GOI?2R/^6QD%D3-L(0;5:V+GIG6%5M(=T+YRH 
@5(&.RN!X/W9$FW< %!TL#$7'V 8[XZAF!O5?1 :@148 
@\*WQV:LA8D>*XP@2)6*23/7=I;ZE#.'SF6-8).;%86H 
@7H6,/(K;YMS@DM&>%6#5G90H"[T0>H:-<."MN8F E H 
@-3W_XD ]:GDW'26]%OZ@ZX Y!V._<3>EL_'G3)CV?C, 
@5"[8X]XK4DQ25S5PN;>H^?HTD% S%>[G6FS=Z\AVL_8 
@NCF\[[9]:SUK8L,DIWZB=H0B5&V"]25*D7D>)=E3'F, 
@0HRD;*NL:0ZX]92P9)^OEO?H*=(W]8/"6J' =;4@6Y( 
@K%Y^[W(^/_.),!Z8$SUQ\;!)5SS\%]>O)&-LA!7%6(8 
@L=7_H:P1+2I0VS#1[7TW.'7=II])B&V6EED XGK_?_, 
@X4'MS<=(LV@7'&,;,PH]^-6PVQ.\8BX+B7 I+Y])K(P 
@$2 ^1Q]!J$;RBJ:>_^ZV@YI4/WJ&:&N4 S!N4]_U650 
@U9;X:[D?Y*U9LKE7M 55XX*<R#>JI4[<=H>)9N02VU  
@@N6KDP;I)A4S/;X_6S8*R6I;4XIALQ-X5Q61C#;6G_< 
@!TW1I-2=IB-!\3_<F;RZ\-_(=AOO8,IM*6M=L3Q%LG8 
0PDDZG0='LDV+%/UGJ!W/[   
0IA*(MEYP_0>>%A0I899KKP  
`pragma protect end_protected
