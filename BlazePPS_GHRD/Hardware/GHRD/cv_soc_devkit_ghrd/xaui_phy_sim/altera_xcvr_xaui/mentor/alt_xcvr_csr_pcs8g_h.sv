// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:52:02 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
sescQP0Q+OYBfwPyYxHhCipEuC0a3uoClAJjDe9I6KmR2wH9bfqYEx+zdMMzjRzE
yMXQPlwBBO8WU1GcqT2ftHFiQXzLfZQqLSO1NHYbwhpY2uc8comMJnnmlZlkmNL5
QApr5dD046olHFt1WMf0jSsGZS0y2Hhpw54GFEaFY4A=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1920)
P6lVtJTdqG/Ls45kxU5S9B6Ch5p67S+80YZz9p4afocD90U97qOdt0eXaHjPyAdL
lMUr2+ybqVJur7JyOI0AI2icy+Mcw2u5ydsHO7qPayhslPdjNMOzF1E2QEDIDwZO
HiQczRVs7095ixJedpR92c9M3Q2sfgVzslMrznXGal24dHO08Xrhp41JIo5k/isW
Yz54urpxJb6UeEscaFMuCxTnSe26BIkrKRT9TPhUXIXIqHwIrGNlj8Ry/3bSc1kq
qXMtqXGtO1aSnj/ZUPVO9qcHvpHV2Eg4i0q91Mxa7gYxB2PWeGea8LMVYnNIaLI8
3RHT/RyKYjrgFaY/mNCZttMr2W88JtYSwwhKMrFVUtcR9v768aYoaVVxd7PzvXI4
LZlJB2/thTLEboB+2c8SDXI4TBktF+bv463+fZpoBsaUW7/QAmQIn9vg+bfgTwF/
+Wq7lnoDBhuNX4lowwde1Qo3cavDvSSdIX0NcXsx30MN3p8UyKo5lcZL/BCwS6tH
ZpsLTd9H+F0klfvrTsjCRHiBwfQvnezn/CBtt5LxwG6uC/h1D+vvH116iNllkMeI
QqTN50ykDOrUnwiiVZLDdHDULRNC1uOc8Ov/jtJzCHE/5/54o9yRDWlUTBrkQV8W
++99aodz8ygcQ+kks3WIRyscUA6p3STGIPznUxBQG29ntV2GAq28B22rjIoLLeie
p6hs5lVpEyw93oxeregb6wFNb1zTUEVr75vo4vjtzW3p2pRQ6nJgAZ8mrcbIPdVr
efMZmEi6gTOaW+V5xmnANuH7ebSJUGFXncsmwi14YNIRzleLsoplxIaWeFHJUhpu
B2ffdfI/YbWalk3+cgVNIsnNT/IvSTMBHVlxzETLACZjQlnrrl63+Bvoz6j/DJQs
/AWvBeUlSaKv5LqY+OsbCJ98eoSDNr6/QFWvjUHofAG9g7wyCHUsywbuYBcMwUUM
uRy10pJsJuWcU7yBMNfUg/fskctx9jI4k60Y6Yp5DxLZDY8SpwloIzqtsfISQDGo
yKJddhRoC0SV+H8FeF9ERGXn4IE/62GcXBuAGvLJ2oyeQs0AARPxtt8fLWVLlfAJ
uQrXCUKkUuUulr0KEc3bMv+BZ93vTpNKzGkHfgBhadsgn01fTLfSh+Xe1jRKOkqW
0WW8kUt4M9+uovaMA5a7FKSTDW/ydaC+NRnLLR+EJ0ZFbhHUyWF9HW/De53pPQOI
e/+yv9qeXYh41RNpGe7m8FLkc1HGIbjBoahq2O0pzqyacJcjKaZNkD+AbiGxYC+Z
Rw0N5t3ybEoXPbFwVWoM1r+Fr/ycpcGYVSi/+A1dFWHZRQMaIIGu0id3Wj65so66
gXjt99Junzpz6xISiMz2Aw8gxobBDsCU0WMCPL5Df0cv0LHgKqFCGsXzxqgxK/zi
SNO6HM29ZqaPQ6yDNZv9j/NiSK8zxZv1rjFITQtr7xbo3UkGpVt2mbIiCHj3QnET
Wh/C9ALCR0gYZs9z64XddSrn7s3tfvGGf/1tVFPbu7q2sgg0+VsXpEmj5mVxOPGu
uUAt4lL+qSYiBGdOFgVg0d8PDcyOGvGbdOsJWTWjZ7f+8jvuqv1ZPZSHi5G8Firs
Z7EKpLOQUA5r/FK3j9W2VNRybtI5H9x8q//6xwOiugwk8oz0jpG5AoNmgSazXdKU
0mSE2jWpH4zffr/pws6v8HXaZIDseR9IRsJ9jp2pAx5s0gk+RIK6H7t75kDMfQ5H
a71AiP6KK4RmUg5SDhjNPgkjcI1o0a8HCCXnp2hOa8sW70raWVP9AudLZzl7+D/E
DYvrCN22b1sD1g6pjOpI1HSGsUFJ2j4cON8JgLIXU1sEETLI5x9lm+ravpTSXuy6
5Mq5OKfEQkqxrLFHWTQp0q0dSBj37KVtqqdwIjFbI7KlO1nlVIefl2hqRIDTRaxV
ijq5hPzZ0pguIkb/oHX/9ejbMvHe9S5isFEdcRUuCnutImCrnA3IjDmguBtW+YsV
RQvNLFrF0vcUqN/haDZzVMU6OHHRVWSfm/zJaJmEo9PN3djrB9kdi2jEP6BOO6Jd
YuWDlnbsi7rjPkCUGOjb+lc2n5LpnE7sW3lhluQILatljtI3gTrf8pnkRd8R9wfp
NKsJ0dNQEZMy+AKKtkfGz9f2i3/ltxXn22sS4bxnSg+knuKpiOEvrVYzq1fCRN1Y
g2l8IUOhz9yADOB1Gb4nANG3u+BKweJFOtt02pmd9Tv/ebqYvE8E25Bgk1GkOeHc
GKgUEPOF5jSigpusfGAnyWbp+OhbrGk+fnXEJgUuth4uADGt81OtjuSmu/yBkXgK
uIX8V4EOFG9B/rAzTWpggnHPtH9uxG8VSZs3NhLvZJU9oTMoWnyFpwXxqCTAq05X
rbIBsxdpY3XEys+7ghx9ENmdt0/uYMTdWCVbDzqO2FqDX2O+jx5Bxkk48x7tKHLO
j2/24PibHkBQaU/p7kDRAffmjLj6Axq23rsdfwcWbJvw71sTVZUE18cQaKj0H+lc
NNrMd4lXQsmeL26TKdLdwwF79nj7YR9Chq2Kf0ptu6K+b+9uXNPVp9q10eAStImb
`pragma protect end_protected
