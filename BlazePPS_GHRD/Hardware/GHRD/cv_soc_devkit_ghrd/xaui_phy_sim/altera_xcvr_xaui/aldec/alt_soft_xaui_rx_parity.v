// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent= "Aldec protectip, Riviera-PRO 2011.10.82"
`pragma protect key_keyowner= "Aldec", key_keyname= "ALDEC08_001", key_method= "rsa"
`pragma protect key_block encoding= (enctype="base64")
EbNOwmziXv86z1aeviEoc2mamd29Nr1+qp6ExGyyLcJvy9Wxgxr0GvxI7pW8wIg4pnW6jdC9HqaF
5mhINVA/W6+EMjej/5215O6a5uBvokMFh4gCEPkEi/0/w5iIDaO/+PzRABhgdx1iTtm5rLhiURvN
/UQBPcsk/SuSM9bYYP09TfzkwOOWNkfXmnmZTaZjbCeYx93keOkkbIQaD90iw+X+gYlf8/pEpXom
ek1SQ1OutbrVwqsHptlVXMz+99wMnLbnJMUYB+3n+9RYa+4kd+1p4yyAOALUFutlUr0c2RmTunnY
nA66tp7ax6pAJ/obzXjPm3gh0Et4PPvTLbJCew==
`pragma protect data_keyowner= "altera", data_keyname= "altera"
`pragma protect data_method= "aes128-cbc"
`pragma protect data_block encoding= (enctype="base64")
8e6Qka15eU8Y1DU/5QrZ54Q/L3jIAD51ZBD6w+kJiymZM5dx8fIg1XwVDLh0aXmV59EPJ3HWZPUX
MJ1sia7bj3p9uiUf6FG2xPBauLSsqaNsm/DfBQalXTtKNyi23clOBUMW7IORIKMpRPVwZrO4QeMa
6laiXSlGC3RcCHrqi7XLP5ch8sKbkLvZ8QX1bFdnW3vTQyGBXripxw8SeSDVRaaCV4iNkrsJCORj
RHdf7+d3flgQN8grCAcE9BYzPdKC38HtqYEVuSTMmiQs3YngzQet1pPGXG0qSHSKeGozmjph4B/L
d6+WBL0Zi6a7BLEMM5BlW7DJowremtXrH7jtkKUQ22psVyARWJJg4LYlZRCkyBZJszxKyL+9zDXq
K/0O/oz7/Gp8XV2ZzPVfDkSwyGeTcmaOtnkDIQuZVxaQ/9kQLho3NvaV5/55hN75PK1EDzI9qqeY
mVr18Fa53bbBVG6u1ydqnwqw53zGHbIssXZ4xEKGm3wwuwV3I/58yQgb0Zl5+dRUUEq21NzUNbSI
2YNMxMINlOPP8+velUkxWMjk69QCIPMUs7k+m372kolFWcwjLhSfMyg3DL4JIqq34e5iWyDWHFZN
C4R2mWw61fYZ77Q6faHZ1lUU1K43FVoSuFcsp+8b0R+d2LrGNpQCw1igN3Wz8pKNaCSQbFBwkXW3
BzbIBY6pMwXOMso7PL4+S1m/yI2Q4rZ+BIEmDDcOk0pyoKPrjlVEL73XqpUGpKtEMEe6nz+Q+P2K
HAnk9PSMOtFDrAfp14BGnbjieH2irWrXpHhW8RgWY0EMQHgk4/1c3lxVZiqg3JZuDfyS/Oa6Nj/F
AzPE3lfWJtcBFNVULp4h49Pdw3J+zca+6W2mfB4G7E+jiASxsB3y65utGbQtgHjQOo3Aj7sbKFso
UeAFWT6K4rxbUkz2bdaXs4JqMZTipFikBITnS8hQvuFpNcvw1WGEbCLIomj27b5/N1aXTqYhzUGZ
hWOD5gm7+hP6wDKIjAl3d3ieCzF6qHacBVVaxxP6ai11VMUV0UHAYNnQWTHhgjh9cJ/wC/OuTEMM
ToVD5k+MFLlASOON93lQWUsFNh5G/+BKfvpeSYP5oTlu6ywMgdb+n7VMpweh62bhlZnMczJjjFqw
hYwdOp5wqV2soMzWDCKoTiusPJwmn2Q4lNJHexzKN5REtH5CjzkbPxmKNTibSqoog2SeujULnOFr
KpulWYTIFGcRwqgcg/2OGzVcxJ68plCHyV0L+bYRaEjYeuXJC8Hdix4pjR5j/pWiT+viTCzpvK5y
3rAomhdpAwwaal3KwWqa2AaXZ9JhCp4mhCEbkSkOgg8F5TTs+p6MQwHGETXSyoNTZG9T105gbcwW
iza7rp9aykHKskwm9Njr2r5LBSx5DsMr/cgOAAryCNiyaucr3keR3jPTr+9UIRjEfaRCDFssSaV6
0GhiCngFLdCa9/joRLTs5bCiZNPlbkXLWHoYHlZrKDVc/RCBSdXt/hkRdT0u1R+LWyLRV1Vefhex
WHinnDo0EKumXgaHfSrhBgUu3SzgUsfoDVrEVX+sR8EYwDk1KW/p1tIN+i1eu+UIhJkSHW9cuIzz
5upCJwP2BaxLPUZbVlJJJNaZvIYqI6Dh4uWp6ahm8Uw0nsPGMS4bocKmFEqK4ibePJWpm8y9EbBY
v1LgDY4FUNFV5x/1h2Z5DZVTkS4eYC6e/HBRN1jPiMsSWtvgzGNXpPQzrkytsUGjT4SA+ce99n9y
wHA4jB9Kkr/HXTOJJ8XPx2RAraEDlskmGbY52AJpKhsvS9p69/2PI/7MDYul1MuRTeyAxMJdVygB
SkyJvdbakdK8GcClJTwgfU8P42xulz8QGVpLFy8UA3hzkCbh0qBuPOkziBoWHpGbhvqOL0XGghxp
Y6dUTUst8bvBUEHQQw4JomfMoyKOdVTuV6Uq0HRy4twlXEyGWCh1PYHvjugf6I8WOYqBCZz1VhHN
BsbznmnQoWP4ge9OBtNZclAky15gbse3LSSsIy28TcjRY7OFuQXfIbhux/AJvMknEzLT0TiUQo6T
jHCOwz42w9YkL6/+l42SD7jyBjoNUuJQInwfnVUlDqgIG2xXCHctb8JCkSr3k0F/gsoBDzF+HygU
xi0grG/ofkp4w3NRF/BLq457IwgYMoU1lXWN2VM3hSEw14+ocF4Gi7vknkiizH/iYvQNHSRUKoDw
oewisK0IgO10lb+PtaDtTffV7NkJc+inoJwYpFU5+UJFW6HRJQb294TWzFLFBtshChzfzK9T47no
Wt7wX+hKtuuqMkevfNhjkQeu9kbdJhp+xn3/gtyXaFqoGlO19NGZvgxb/OOBQRJVpwbNDWfkxYxR
gEgRW7jBb7yLCNvFGXisRcNPTJeuv+4Mf1HsiAEV8DtKjiMNfjxOFaYGr+PJniWN1W442S3j9zt3
wztMDxI9qWQATK8IwUtVHl6te9y2/8d8+GkXRGl7jiO6GStkOf4OuxhkDWAyhGUulydbvV6uo3Fe
HgCkiMrtyrBWCpme7aAjSKpw0+edxXZt2RboevF3n27Z8SXnmHSW+Am9cb+UfNP2xcjOyKeChjU2
cBKZ/fp6+U2C+MN/VprETDh0865G8bGOTXPlSWdatKhPOC7BJfElmWz4hkwQVtR2tT5SI7k9Oe8e
RHeXTMHxYYmjH6zy4U3iLLM1SDzYnMeO6cBcdvkJz9fWvqlFcRwv0GQtl216oiBDUVpKJSYAge+I
4HjKJqp8iJ6ptoklh/a8m4cDaOmepwMvnynTv9JxbBgh1iWnc4wU1SB4q66C38+O010fUvFjTZP1
ngH5zVEPCntxbLc+pIg3b21srqMuKd2YluVkoJBcv0WQIcWwfbSlVkJ1EQHfEJyuwKI5PPbZmHb4
fHwKqyIkxLDaeNuT4CfHpDXsUru07Dn3c56lRXs21Ctqa2XK0SVNvuUK3DIK+8QIObALbYaNhpm5
XmZ38AoL6uCU3XP+PjxD4/oo1YvmjjOnAp4hpOvvcPhB1qQz+nqPnis1rrxq2mIjRCw35IY2e5Ly
h9SpeuKuo35POdajJSegBkjLwQZ5cBWOoc2DSey7f7inAZ2WHGEv/1xB0SQp/iVwkq0ZU/hMyupd
kfvbzXRSP2mKu16ZmxOEA7gHF9MFUWD3AgHVVCzydIBs9oQcNJREbzkm3k+iYGELhq3/OGzK/2Np
6hN9madJl74PSVuLWOKDBrC7dr7xxaYmgpPJj3E6CRZm8Czqan1O313Yh4LeyMWmzlpQQEsN30xL
KCG3iuZXTDBoYvpBU8vOrj0e+DGZmdEQUR/w3Fk6++/BCroCO/3vbgNIeu9eL9q5moGT11V4zio1
pH+bJzBSwWKRwvtyWLxfVuG28wbdAFIiDU0OYDK/wDNrcTVPxUw9u19zBLS+IHy9qpXs5j17J5XO
mehyekm+C6OhqX3b99/fg+U0MZkHXISga2eaIHa+4BYSvGYnIfYqKZXytdZ3NR0BhD3EL+DjfLPo
Ow1laJ3cDQ7MODefBYBrTScfOWnOlVWWHzjrT8tugpPwSvikSF/nTElifpai5Ko1SY6W6hVOagqY
napJcJl047vxKizAz2OOc7doxJN0MpmITNijGsuB1Iz/9meOr0IHpvmZa4M9cr4BpnyM/bQHGMhF
JJEs9MOCUELeuKrAhoZBYhld8dX3ZIvG/MgPPLl7pWlfNAFnL2RmFXZaxOmEMMHt3hTC2Xc/HnEh
9jEtvKDsTEt6oSZ8HB4iVtrmPNUeM246mJU5S9gIweJ1i3ET/k0k0WBofdQ5WeNaggUhPSKgRWhq
SpNCv+waQPxJmnJCnZNZhvULEBL19xTMzkXhpcuNVAfbigf5zhYShH8Jgdr4uy+KDHZUqRxAVfkN
L8UOPjtUC8zmaE2YmhPZ3623M7R08DiMgi72r4ehHAdSNIL3dSnissyi2Wzs2L9UXRR1QN+sRDgp
o+B9SPuF/aXGzLM+5PvQh+dgGVdTWNp2FnsuT4ulV8YyoHLC0s4OxLR0OE6yIwIdy2AF/4tb6O8G
OaHW4xobf08JmM8hnLTdlkoEqo7dJZj8WPHxLSqtQNN2I/YoImxxjoN0SdmI3nntq5sITSqUyWjG
egCZRm7hWs13gVT+XQr67uxaTlWSRizIyJ1dqexiamBIHXPvEEKkgWsi667k4kqQ/FrI9E/+PLV0
KJC6r4m6OE1gpbKhDTFBFQq1yjKeL3D1jt1we/PtaPSbySMeSLp7Uy9hWdty4nb7N05xQqIO2mZV
zPNi96LeO+70xYG0uUnsiaYet5eoO78Rr9z0/etG3TYxS3yeccYdxrPjejzIeGLV8+IlkM01cNfu
tw1ykBm0QpPePOiAViyTn8Fb4MxUVsfVB3jyMMixJHBHZJCKWHxkuVce7BNB19LS7OIaJ/Ep62n3
piXzX+cbWQf+ziBHmEjYobmFyUkZBO6zGPanq0wJ01VAHTNW0VE1YOZ1rBw02THm54k6ZgEgCyQ1
G7rD6BtluTklrrLgzpEdPc3er3D5ezymX4xt9nNiMPn6g+suQSgX7Z38LAm9CNrUjl3ojBj+UEZx
GghyZCGKF4G2ulgWNNyC4I6YtGQZlAS8y8VA1tcT+0SStg5czxlYTpTgeln3MNgWkO1oy4U+wtqs
1FuDBOdPDaQVD0KNhbgpk5MkWYUavm2fENwhEBbx7vlkUImtmD+Z+6J/ysL2HlsotyDmSPqc45Wx
x0W8MtdZ35uhdMIIp9M6OathHE8G807okRwRhaqo7HuXoNITjnYAgp4FHOcRRjrqV18W3tYoMLwJ
Ot5QsId2B9+RN8i4tRuC7wp7WcI4Z+5rAPaEd+IKNb72l0JXLI8CpQBtRFWv4IypE/+JE/1kpG/m
NcGERGZdfoirBdYdHAc40yrxE73qVEHvvB7xeMLuKu8vWGvdY6FGbhVXomhfvQpcPF8NaihkGwsb
Vs805kHqpi2ZExRccLeM03jgWTVFTFX3CHqXwtSMg9I3HFiFdAVh5HCfEwSr3qGM7W/bO0X2YT4z
dQYMECSH/ij0tHZwyxYU39hAp2eF79wdH20VCV4uKLjpoewuSErIyfvDK3IdI61uyPF1XqMrMb5g
K2E91MyUL2/zGSWND5SGXUDwfrbkDWxtXdVFRNg58FqHmeYvORn4LXVzuCxBKQnj0yz1xEdmXMSu
1rKVuTfEVIKcf/B4NqMD7BQF/ekp7/6KVZeMVG80gHNfB/pROHfF7LhQL1kZtsDEgzJ6L/sWZOAn
UFoREMgH1h+rajVLjn77zTyK0Q66FmmOYFStG3/Bm97ju7aU0m0nkO/Exn3uNGbD9xEgfPpEHy1q
lYWiZq1uDHxfYhjQrcjOBRKH7UsZ0MnOzdRgq/ACnq6skooJXgcJDb0tvp8XnoMBt030nIlbT6QS
A8WlcVSAfWFOxtAPe7u+dA8aEr5o04cVyvbtNSR1jxK+ERzlWJV28b/CEObc/F/l7xjfGQ0/e4io
wPBzJDk33I7HuwsCw95pOBrl3u5MUGc1j8Q3Eam9QHobpvK/bW0gMWOerfxxOdhEHm4E1lI6gCV1
I+c3KEf9HEk0OfdFURqri2UBhxPMSveQoEbDqmjRiq1t6kCAmvGizaoXpSMhriU+N4ZS+4ORzytW
+ZeZYFel6r2SuqkunVb58+NR/h2rIpbzgUSlcyYw9Z+1lKkx/smiEgOxDOB9G4bgnx0J7jLINbap
VU3H4gBbmXmpSju93FHbYP7rZCiXetz+BsQZirPF4nSIPqqIEZS4OrLsM7aEfF0Crt+euxRHFrff
0wCbgkWpgLiuC9c/A4aVGW4UtE39ArEVPWa0AbvZ91dH20p5cjGIpsaNkchyk/XlKpzxorGb0L5/
F/S3DK5aR1ihYUnBDOHWJ1QnfIRA7TFkPsZUtEzAWRVnJnLMIsRayA7KQ+L+HM76x3CChm7U4JEW
kv5HvWt3Y54DHWimoswUnHqy8TBzJMqhGleRwqi1fZMeLsicF1t4hZotTsmKRiBDiKQIYK9iWY5J
4wSvUfEDGtkvwlHW+ErL4KEC4MOlMMKn2K02098vYjFcQhDaKG8MT1SY8u1zTUJUip+vFhZfvlmF
H/qYDO6H3kX8n1PRitcen5G/qKieRZ+9mZlY/IlioBK8aS8YXwNKYnaRuY7mgvmJIrMwMHGT2OQU
6NP6BS2tCir6SZwWh0Io8dfJdGMPmm9Bt4bxlRFqUKvRbndncQdEOLFQby3QYC0ya5soIgR10Cij
oTOaHbmEkMK0M/YWkGhOJwDTm+z5WzzMGvsmiAXaOGL6sWEJp72KmOSCtxaI4bDP8Joz0/hsDSx6
/KaTQTgyi++83EdRLu/sGlfbUlfqWmsgSUk5IEOc3eA21RhqtSvAj8GXHkrXxMmvO08daFAsD6Zp
U84n/l38vY0XIYTA81iQnkAERqQBm6VUtYqZ++UCbCaGWg2vqn8CMuwtrExuciRpwZ60Fq5scS6t
7SgV
`pragma protect end_protected
