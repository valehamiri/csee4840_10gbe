// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H?I-V: >JLI'PO6^LPX_8K6\AM>OI?<0A]^9\&'XQ=!!"-($2J9L\W   
H[))+B[F3EQOD>;X5V\G*XC==R[V)W2F+*OI747#X"[?_$M,+)%57Q0  
HR][MF?K#EZTVW87"5Z7/,&M&X1I'Y1A/T>..&F AO/M&)#/)KK_HY   
H14?G<!UY&8)**ID.RJ/$^@))OQ:\M\,3UC-W&[988-.VZ[GZ'<)@JP  
H,7(72NE&"2##FZDAD6Z9==B%ZCB2W=(CYNV;G=C.9:%5G>IJ07<SA   
`pragma protect encoding=(enctype="uuencode",bytes=5312        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@KFZ_Q"]_< Z(>S*]H0,AEBL6039"!'J"%'*()0!(>H( 
@_FWD(%[++SFP#^#-V9I'X8S+G@1RQV&B(WI_:@5SO:X 
@P[Z'I\Z9SO[841<'JM=7S1W@12OJX8BQ$XX3TB&9)*  
@3_H=33;.49\.9?1\-IP%'1<Z)ASBEM G:]:5Q&2>EVT 
@BY.AGQM.$A"U?CV\!R*7"L%VC[T]RZ_U:9:P[I0V[D\ 
@ A=?/%#.\R%&+=F.5W9EL2N]_ILT:E]TT"PSG#2#)#  
@1.'Q66@:R%F1$':75"8@&=:L_)+,!,&(8#R # MY#L@ 
@DEA393^!*MJ-9R"N1)<L+.IW3^" C<D[J9 6$GVE46( 
@%.@9GEC"&"\DP*NIGD8'GY%S-*"Z6O(,B_V.#LR ?,L 
@8=B]W2*WL ,DHMBZB6-C:64@V"3/. *S^F5TV%@F.KP 
@V43WE\O2B+ZGY(M6@^.FM0VUB7$V3D,RTAQEJOL*$.0 
@X5E%2KQBJ09='+OP>/"EZG)PNS$ <O=<]I*.+M^51AX 
@33244\3A4(&9$T']PQ0X^*Z'&^*D_P[XGE;1@<<.)'0 
@*F$W$+4%Y(3=>31FVO#D?"O1N>J9JG=EL6?H?NKNQ#\ 
@SPDZY#JS%3=F\0]+(8V*G3$GUBO?AK9TJ;+JW0:=OZ@ 
@#,G#F#2C4]@XRR;'GB47IVNFC+LZ#+WS$)-[-,E)<UX 
@?" BBRS8_+]+XNHO$X(W!G:"JW0>N_TUHO*CJL,T-WL 
@!>3HG(B[O$!Z: Z2\U7BS:Y*03;C*"<QX;TU;_=A;Z( 
@C(R0#3<1K( *0,'H84261 S:$NC7 0)ZJ'& )2>UN-P 
@&-Z.?L->[ /!]0V1A/FC^I=7>U9"1OXS$*.?K$:+V#@ 
@+U#TRF+Z**S,H]7 ^D1-3':=5"/U9(+Y#R^6JY8J8+  
@+TO7RS>(*DK8L?3Q/"M(RG=(Z'F%32<E7#UY;4<,/BT 
@#DFA'=CC9'O2QC"7IBR&"D;3+:7-XYL2[8,=YD&W6W4 
@^9P;]4/VQBG6Q,EZ[DE&-BF\Y(>0B'EU03B)\S="6EH 
@W?C-&9N81]RBRLT5XR,1;P8@\F&@YC%-[/XD<">>:BP 
@D8)J_G)LW6\05JM ,Y A=JHU-W]MG OTFUQZS4O1OFH 
@;,V?'LWZJI8?'U)6N"W4XC437CV>Z/\(**QPC"4R2%  
@K\*^M-!5]Y]VJ'CL3JA/&,G6K=.SEWUS0["\.B3H7+T 
@>?#MG8<8?'G4FL5M8%M8SD/I^X)M#)N)%^TNAT<]%D@ 
@W37]^T*)66Y=6XN%/;?: %*H'S-)"/;MY6<V:M06T+D 
@@YGVAK0\\+I9?I9J2&3^;W?PPFZIN1;6H\;N@<A_,W( 
@^6>APC"07HV11\Y"]2^PQ.5;)Q).=^%=0)DNK-;  _X 
@"L?A_1;/M[[_"2"B,SD*8DG;W>E!/3"A&KX!*IM^FT0 
@R;.C"+.BC3)[!P!VKCH- #0,XW0,CGN1NQX.QW<7-3P 
@=PWM^5:L,DODLE!OSOV&R =1H4N32C%HG1@EYRVE>V@ 
@9,GIDCW%:"!HUUJ;5Y]%8HSR?9_G9_Y%V W"LB[E;WT 
@:O $'J6_ I\)H5#R,Z1'O)<SSBLYY];.5EW3/$8,NEL 
@'1L4'_K@Y0(+^MYW2C>\Y?L/4\M*8%=!'GLA:\;9!4( 
@0)%/@:+@CG?7,Y[)CNX=?"?@6??%/7L"HTX,+T#MON, 
@:'#U(8RS&"6*8!8+%3MT7>>P# IWTH760:G&[%V"9'D 
@?,IV8,7GL/,!+\&UX>H>RS& ,@HVCY"A0G?_Y]L\EJD 
@'UY4.9?&C^,ZX/\P/OVS]K!H+*.F=5?UA6<S:520-,0 
@'L$SX'=P9R$SC.'QHF)SFQ5Z_U1Y*Z%Z75FHZ6L,TC0 
@(9 K=Z-%5L7(%"T0N2P4:S97(C/@?>D%Q<(!C (>U&0 
@L+[QU*&:EH"LO\"Z9$,Y4YRA\\4U3R5X([%LL_QIP5( 
@8+<3G!@@P6H,^O^AK+3Z:88S!<*:JI/KT] ]"NQ4]^8 
@<R_B%3C^U=:S'RE";.,Z=W^#N#/6PG]HZ,);T<0Y@ZD 
@4%>46.\DC&/GRC!S>T",!J^"RNN+V9UPO_U-G!> #M0 
@]L[/&4F^TJ=_6?\5UK $#]23\E);(Q,@[=W?U2)5NFX 
@TM.2EG=YG?@)TV\?&YQXCO#)E:]@XK#9UP07O*\SB:L 
@C;(-NA9BB_RUA"%&E!":$[?*0B+>3:(=210;3[@3.8L 
@FEEXS)13G]06C+K63-^3F<>H[I=QS(A;*@L!DMP^@Y( 
@CDF[15Q^,T^Z+!Y/YYOU9Z7T3;QJ)9VT-T K&GH=D:@ 
@GI+NLHZ%'.$M!9VZ1+%<=,I&SA V)CPBUT3+HXI8AO@ 
@NN'X88#@^K_DWH;[]_DRCW4>Y2=SS/P@O7PBDR4<PN( 
@;KVEYUF2C&G$MRFRHZ_THN5)0J31XZ7@UO<"[!33'S$ 
@#'A:.&@V3#8.Q5.LA2Z#4)V6(W_C1"K@#M8 Q7WEACP 
@JBD=SGN844-9<62]EML:OO5DNV57>ELEJ$9!9+&X"4$ 
@?K07;S[K.!&HL-]?_JKE1F3QA"9\Z'T$P:)1U$%[03$ 
@J$#S%:T@UZAA!XU9QPW=^@OW+$CN!DG^9J$E12D@H X 
@9Q"\1>/;[7[YFO S9-V/EZ=&4ZP:I3<38T'J,+4].:X 
@<&$!;O^NGT. JA5-_[7]BO5U0A5Q" F3P.-& 8 M7*< 
@[TRA\@.KDOZIOH^!\D[#7"V=<$(,FN#/!2#R1K\13]0 
@)9K]Y4(%OV.'B8XNC"][KNSEXK4);==4F%&,TH4FRXX 
@*?@0^U:3L,W1M)0F#O;$4]DO@"3UUAE/#Z2Z&-\3;:\ 
@\Z>UGJ *-U!A5#(^K]W<Y$H*]3[(8J+?30U*C0A5<+L 
@N!9XU+F5.(AS<A8RIM*%WN0V*83NBVQV+\PD129#G4H 
@M:+A@I/5&B;.]K%U%0_L.ME2(.QG. -]TXAP-$$>9\T 
@%:#]*;O7TAL4WL:^=-:!KE8U[FIJ^BO U9+0C;OVUU, 
@@0Q80$4(Q):<TK,N?'C]9# ?RV5)B,O>_YET8"$W /X 
@;8X2YE8-=1(_$PKA"6Q&FJ82:'JY3MT'.M DL0K4<1L 
@"5OQ.^@57OC\O&:Z8TX>:$'B&'V2P)<1/F%J]N_% 4, 
@$GS ;3;&CT1;OZ[B(ON> 89Z6 +$=&ZP8[.&L'T_K?, 
@['[,@-IMYK.U+\%QEL\:#&97(IE Y,ST3/60T&V?5EX 
@RJ$(29=HB8.$C?GN1'=MR5^N]?*.A?AS2BK3D-VZJP$ 
@%<VC<.D-9G:--Q&>8(3J&%R8M_AY::]WDR:@$@5)G60 
@QJO,)+<A=3P3.%H53#EX7](8H,BJ#P'I]@]#=B8G<U@ 
@#N"G&66V\20O@$+<15_E0K8D_"NXUXK@4&_ND_)CC?< 
@U3C:R0J(N^O_D^YYXC8=;-3IZ>#3 .>(:S;=.#GK.#L 
@WR1+[+)AUT< 5*#9X\]'*S5>F9M3T.5CHW0UQ P/K<L 
@50 O#-K.KG.M@!Y8P4GE?$$HFV8BS)A'>@WPIT02R!P 
@%H#NH7C/,T JT$ 7#(?WB)-=#.%02NPD*GQY@$)U"I  
@@?C.B);!T(=/EYU]CV%46 7MUC1"60S#O[JE4O59F20 
@%'-C9 96*T/54< E7P_$)ON*<@L'$!3@@WXCQY!:)$\ 
@HYVHL;2]&2:SS@&O A"R>E>R(WLH!)P[;NH";"32T5$ 
@]GV';G^@(UOQ?NP)*27/<8(9[H&J1VV\?JI@,TC0B$( 
@B4)-P;(A*LQ6-@GO;MR/W$&_]K[WQ[@YG6L??]-#:J4 
@F'J\4WMWAUET0E;:*E7,L+-'OVO;M3+(#(:5AG-]C)@ 
@8+O4]3W:V_4V/@2 F1*;0$V6O):(I EB)1$A&-PTF\  
@':(E&6#8[#;/1?N$ )!K26DRR&&Q8*IQ4O<'K9BNWU@ 
@K2E'E7'Y:L<RB@GK%0!EK7D<#'Y3RYROQ?CY.> P,)L 
@H]BGZ'VI*7SQ5=CA\JL:1T3DIMR2/=VW-@9'>@9?[ @ 
@/;1/2A3L["NC-QV\+H;$!KWX' *Z.@ 6$><W&["=K,D 
@6>[W7I__KL",4-)YFW'X: EG%2=]>.TG186%@)/TW3< 
@^[3(G,CJL>BL[PN[SN/Z_KL8$#)KB!YB_F;KVT?W'B4 
@NL:=<[P,/.&97T=61"\/D=2Y7+=1V=HYX)BBFLN%>J< 
@Z O O9,E.4SBL.Y]$P&&X!^Y>*,WJ=7GSCTAA'U4#9  
@TP&###0\$+:12M>5LF$<K."@ZV.4G#R[')5@:+G)==4 
@"=4*#U3RUT3141(ZBV $4%$TS!D N)14J137*JN%H50 
@%\$83Y!<U#S:\$TW;P;A^7Y?@[(6+H^:1^D:B5(0<#@ 
@OS7,GQ'S>P)#-'8V)=+/61\-4Y>]S=H1J(?^BQ#$\BX 
@16&LTC:+!J]Y>-JU= [^/2"7#?T.3Y,0B)PVB=&BGE4 
@H (6K".[Q6.U0."RXH..SUYDWF'(V ,5>#<D[-*?U"X 
@,US',(/TX:Z+>P+>YT.8=W[--\#HR+X@!NDD&K@H&Z, 
@U>RIAG/?3Z"S$""@Q NPD:W"R4E;@LTJ61Q*GU=3.5< 
@(@C#O9/1P$RPZ)(#YIZ;GA>4P918P1C !6_P5Z='Q1H 
@EP<.CQ4M\2##AT2!S=-OY ^RJ/D;'?[3JRX?F+UA(+$ 
@90;CK<DX/7(GP;9/\&W*-ZQYJK;HQ5F^.FFDO!+L+#H 
@7AX3OPHC+%*=@Y\+.#1?,BC@>6,_$;/;"-9<NB6K@\H 
@H)V$ ^Y4[NN?"@:>M\UB"3ET>>N34%^![#0&W59X=J8 
@,] -*VBQBF+A<2EXRJ *YY33$M3.R<+P)?/G:8/PWUP 
@0W3G3,3-K@442M>XVTUWD3+0$,LW*_^7WO>]A9AM(=< 
@;6<.5^T.'G([-IOR$0#7V/^/LUR6IBQ/310GZ%?DW<L 
@RB;WV@;&'09.+>I"[,?7I(I"Q= 6)\BRKLC/H&2J @  
@36 !9/BI%/WE*,+L/PM:;6_RA2[;"[30\-XV3T_:@JH 
@J1GIQTO9#^<,:.LGT1#DH5TKPC7!M03W6N1XM2W];@P 
@Q8/-^KO9?2^XOTMA2ND+],?[OHN-+6W)>33W?KL;P_X 
@/[%_M'YEMG=W(I/Q8ESA_LI=H,DY9$*ZW%43.> P_C, 
@%)0GOJ$VS'JQ21J)N/(&;N&S1B)+YB7/F*BHLX88TT, 
@^TB=9O4=)&C%+:W5&NJ=AV9#(@"M]BBZAR/ ]I7[J3@ 
@[\3P*O<]6S-D(0<^B*0N$?&U2[N@CZI#Q9\;"7C\D0X 
@<>Q0J9D*IHI0]$(0A>V,Y  ]8'RX;:.!0#CL9R?G0IT 
@?5/P1-O 0:B5'10O(*3.H73X!%J/G>P&,N/R/6HN5&< 
@4E; 3KQ=?U\[T@GI0[!-5$3Q;#UX;=QY="FO&N2JC*\ 
@ D;N,1\+!(IV0US6RF_^^ @E2L&.I^O[:6\C*<4;?1D 
@H%#M#_-8W:M>I0VB CJ&)V9ELKC-?OHM%GCR@$/SE1D 
@O&,5@3O7["0<*W;80_"F>QDO-PL,I74GXPU!9]8:%6$ 
@U2PS?!&?1Q&*]T?>K1"06":,"]YI\ :7=9\4G2MV8?H 
@V!5'QVC?8A]M+.Y:KH'OYZ&44 ''[068V-+ZIEI;.\T 
@L;JQR7J):?L),LCR/L=2P[\>;[FE$#-!5B'V(:>L]Q8 
@Z6  YR#L7F40[AYC 31?N<CTQ0_:WR^A2P8@-"A\-P4 
@&Z2\=H->Z]/<EWOXOM&_]L?3W:1_33SRXNTN3 ]6@IX 
@N>M,@AE/JA'@8=W=4GS K5=N^'B4)#M*5 ,WNF<G3@4 
@UFG8$0\Q/XEW:+C/6CQIV,R?#Q7SN"%4VV58%:Q'>-H 
@#<\W17D+AN9M;"8R*HF'T^/A\0,0$I+,J94>^[@6ZV$ 
@T X" ,_\ZE,?LQE,78PQ4'G&[T')!WM]?Q1O\L$Y%:T 
@L=M?;H9.0BY5;2EV+H+J)XKM*1G.E9JR'/I7=O OC:4 
@F[Y?_2PU2C\)R.1,Q9XBN9IR&2_E >0V(3N488D]$2T 
@0K V:I_+@&)BRIDY,LZ^CB[PS$LOC&,^QH(IL-S1Z]P 
@5*F7=\6]F)V]JM#ZT@]9:>[UWW30@7Z!T0NR"G-^S5T 
@0]EVQ0_66@8'H5?4)^2G@8L-+<?HQG#"<3P34-UED-H 
@6P:CW1$5-YQ>EB&'GGE;.AH:VZY85\20HB"2]&J=35\ 
@2+:/6((4RX'W/-0'-"M-6_N<5)5(1'$1)'+L2+?#*M\ 
@U(IKVP4&[;F^(_#.?CA.)+5/"6)]59L3K<NC6=HBRG0 
@OS,/7LIDRH7# =HIZ;G"W65KJPT-%TJJT?(D6):,O6( 
@=;UD=,!+MVDE5(+V;G9 T29#!(F91.T0W34%J[-YI&L 
@F&#NK+8'?TK^!135 ;BJ\T?A6M\QF)>O5^;/'(123U0 
@:5?N;"@_%F&NP?L2XMLC!<O=R?/3JTUPBO04'D-,_6@ 
@$(<VMBH*1[V']^N]9"6"2%JJKC*,K9B-Z5#0I%K)*4D 
@A %9J=*0,;O?7;XHU51QD?D@T!*G>(12X)(E2[/ 65( 
@'E3#$ZG]?(Y3Y@GB\H6-]4&M#IM_N[Y;Y>JN@ XBR7  
@Z(U[KP6#\3R/J5VMBEMH2CL&/4K^)YH67CHVF!LW8Z$ 
@5N(])I++-58\2X3PZ]!=2#,EO>]LA30YP8'+=C7J'QL 
@9,9H;2%8SGE=#1S<;I!K+/++^0YYB,QRZA)+=/Y\.K8 
@;*&9XOR_F%\6:&]2L:\'<]T?1&[8DN,GS"K\7((26OP 
@+\ #A\.)\*KI@-D8!+YV32 /)LWOUV!TGM=]+*QZ".@ 
@O'K /E&^%=[8KB:,Z@Q?<%M5>N=GB9ECN22*"6C+*BH 
@+-FK4H8TK?@8Z=8.X?7_ZS2_UUB#QW7L!_,K)],Q[4P 
@'ROPGNRF!SJ@J\XHT0R6E2@^]O#T_ZD_4CPB"CN/Y&P 
@)58![-VX5AHBMYT]?D-F81L52+V'WH.E6_;9;^+Y']( 
@A<HK?BIZ[)'H>TM 46XUJG* \X3#$Y[.X6TXFF[Y]\0 
@+'?IZW0R8!LUJ .P=.F>^4=2E2<'8-TF$PIQNVE?>7\ 
@+O@O J>C Z&]@I*>>8$K2-AC_.@;00K+"ZS9#Z;/0Q0 
@ <_&#7X1\$<:H^052@%E>G RUWE36[(Q321B2/,M??L 
@K7^B !Y1 EFM$8(W$S&GS'ZA2UX'OIH$QTX&@4WZCPT 
013F5<*XL^G-SI3'^E*?$-0  
09Z):]]22=%44 4\FY/Z+:0  
`pragma protect end_protected
