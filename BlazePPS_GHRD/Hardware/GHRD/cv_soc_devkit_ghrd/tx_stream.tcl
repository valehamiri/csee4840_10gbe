variable infile 0
variable s_path ""
set s_path [lindex [get_service_paths master] 0]
open_service master $s_path

set src_base 0x28
set sink_base 0x20
#set ready_base 0x20
set src_base_control 0x2C

# Set register "ready_reg" in Avalon ST simulator to 0, buffer data in the fifo "sc_fifo_in"
#master_write_8 $s_path $ready_base 0x0

# Send packet data in packet_data.txt
set wait 20
set precursor "0x"

# Send special characters: 7a7a7a7a indicate "startofpacket"
puts "sop"

#master_write_32 $s_path $src_base 0x7a7a7a7a
master_write_32 $s_path $src_base_control 0x00000001
master_write_32 $s_path $src_base 0x01005e00
master_write_32 $s_path $src_base 0x00010017
master_write_32 $s_path $src_base 0xf293c2d5
master_write_32 $s_path $src_base 0x08004500
master_write_32 $s_path $src_base 0x0046417c
master_write_32 $s_path $src_base 0x00000111
master_write_32 $s_path $src_base 0x032d803b
master_write_32 $s_path $src_base 0x14c2e000
master_write_32 $s_path $src_base 0x00010272 
master_write_32 $s_path $src_base 0x02720032 
master_write_32 $s_path $src_base 0x2fd6534e 
master_write_32 $s_path $src_base 0x51554552 
master_write_32 $s_path $src_base 0x593a6468 
master_write_32 $s_path $src_base 0x63703437 
master_write_32 $s_path $src_base 0x2e63732e
master_write_32 $s_path $src_base 0x636f6c75 
master_write_32 $s_path $src_base 0x6d626961 
master_write_32 $s_path $src_base 0x2e656475 
master_write_32 $s_path $src_base 0x3a725a77 
master_write_32 $s_path $src_base 0x6472533a
master_write_32 $s_path $src_base 0x78737672
#master_write_32 $s_path $src_base 0x7b7b7b7b
#master_write_32 $s_path $src_base 0xffffffff
# bits 3-0 indicate empty (bits 3-2) followed by EOP (bit
# 1) and SOP (bit 0). Here empty is 0 because all of the 
# symbols (here bytes) in the final beat are valid
master_write_32 $s_path $src_base_off 0x00000002
master_write_32 $s_path $src_base 0xA916C57B
#master_write_8 $s_path $ready_base 0x1
#for {set i 0} {$i < [expr $wait]} {incr i} {
#   puts "[master_read_32 $s_path $sink_base 1]"
#}

#master_write_8 $s_path $ready_base 0x0
