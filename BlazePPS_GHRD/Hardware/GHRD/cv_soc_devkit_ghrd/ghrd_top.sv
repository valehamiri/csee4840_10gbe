// 10GbE Subsystem implemented ontop of the GHRD
//
// Version:       0.0.1
//
// Authors:       Christopher Campbell
//                Valeh Valiollahpour Amiri
//                Sheng Qian
//
// Last Modified: April 13th, 2015

`define ENABLE_MDIO

module ghrd_top (
		 // FPGA peripherals ports
		 output wire [3:0]  LED,
		 input wire [3:0]   KEY,
         // PIO ports
         input  wire [3:0]  fpga_dipsw_pio,
         output [1:0]       pio_led,  
		 // HPS memory controller ports
		 output wire [14:0] hps_memory_mem_a,                           
		 output wire [2:0]  hps_memory_mem_ba,                          
		 output wire        hps_memory_mem_ck,                          
		 output wire        hps_memory_mem_ck_n,                        
		 output wire        hps_memory_mem_cke,                         
		 output wire        hps_memory_mem_cs_n,                        
		 output wire        hps_memory_mem_ras_n,                       
		 output wire        hps_memory_mem_cas_n,                       
		 output wire        hps_memory_mem_we_n,                        
		 output wire        hps_memory_mem_reset_n,                     
		 inout  wire [39:0] hps_memory_mem_dq,                          
		 inout  wire [4:0]  hps_memory_mem_dqs,                         
		 inout  wire [4:0]  hps_memory_mem_dqs_n,                       
		 output wire        hps_memory_mem_odt,                         
		 output wire [4:0]  hps_memory_mem_dm,                          
		 input  wire        hps_memory_oct_rzqin,                       
		 // HPS peripherals
		 output wire        hps_emac1_TX_CLK,   
		 output wire        hps_emac1_TXD0,     
		 output wire        hps_emac1_TXD1,     
		 output wire        hps_emac1_TXD2,     
		 output wire        hps_emac1_TXD3,     
		 input  wire        hps_emac1_RXD0,     
		 inout  wire        hps_emac1_MDIO,     
		 output wire        hps_emac1_MDC,      
		 input  wire        hps_emac1_RX_CTL,   
		 output wire        hps_emac1_TX_CTL,   
		 input  wire        hps_emac1_RX_CLK,   
		 input  wire        hps_emac1_RXD1,     
		 input  wire        hps_emac1_RXD2,     
		 input  wire        hps_emac1_RXD3,     
		 inout  wire        hps_qspi_IO0,       
		 inout  wire        hps_qspi_IO1,       
		 inout  wire        hps_qspi_IO2,       
		 inout  wire        hps_qspi_IO3,       
		 output wire        hps_qspi_SS0,       
		 output wire        hps_qspi_CLK,       
		 inout  wire        hps_sdio_CMD,       
		 inout  wire        hps_sdio_D0,        
		 inout  wire        hps_sdio_D1,        
		 output wire        hps_sdio_CLK,       
		 inout  wire        hps_sdio_D2,        
		 inout  wire        hps_sdio_D3,        
		 inout  wire        hps_usb1_D0,        
		 inout  wire        hps_usb1_D1,        
		 inout  wire        hps_usb1_D2,        
		 inout  wire        hps_usb1_D3,        
		 inout  wire        hps_usb1_D4,        
		 inout  wire        hps_usb1_D5,        
		 inout  wire        hps_usb1_D6,        
		 inout  wire        hps_usb1_D7,        
		 input  wire        hps_usb1_CLK,       
		 output wire        hps_usb1_STP,       
		 input  wire        hps_usb1_DIR,       
		 input  wire        hps_usb1_NXT,       
		 output wire        hps_spim0_CLK,      
		 output wire        hps_spim0_MOSI,     
		 input  wire        hps_spim0_MISO,     
		 output wire        hps_spim0_SS0,      
		 input  wire        hps_uart0_RX,       
		 output wire        hps_uart0_TX,       
		 inout  wire        hps_i2c0_SDA,       
		 inout  wire        hps_i2c0_SCL,       
		 input  wire        hps_can0_RX,        
		 output wire        hps_can0_TX,        
		 output wire        hps_trace_CLK,      
		 output wire        hps_trace_D0,       
		 output wire        hps_trace_D1,       
		 output wire        hps_trace_D2,       
		 output wire        hps_trace_D3,       
		 output wire        hps_trace_D4,       
		 output wire        hps_trace_D5,       
		 output wire        hps_trace_D6,       
		 output wire        hps_trace_D7,       
		 inout  wire        hps_gpio_GPIO09,    
		 inout  wire        hps_gpio_GPIO35,    
		 inout  wire        hps_gpio_GPIO41,    
		 inout  wire        hps_gpio_GPIO42,    
		 inout  wire        hps_gpio_GPIO43,    
		 inout  wire        hps_gpio_GPIO44,    
		 // FPGA clock and reset
		 input  wire        fpga_clk_50,
		 input  wire        OSC_50_B4A,
		 // HSMC
		 input  logic       HSMC_CLKIN_p,
		 input  logic [3:0] HSMC_XAUI_RX_p0,
		 output logic [3:0] HSMC_XAUI_TX_p0,
		 output logic       MDC1,
		 output logic       MDC2,
		 inout  logic       MDIO1,
		 inout  logic       MDIO2,
		 output logic       PRTAD01,
		 output logic       PRTAD02,
		 output logic       PRTAD1,
		 output logic       PRTAD2,
		 output logic       PRTAD3,
		 output logic       PRTAD4,
		 output logic       TXONOFF1,
		 output logic       TXONOFF2,
		 output logic       OPINLVL,
		 output logic       OPOUTLVL,
		 output logic       PHYRESET,
		 output logic [7:0] USER_LED_G,
		 output logic [7:0] USER_LED_R,
		 output logic       CONFIG0_1,
		 output logic       CONFIG1_1,
		 output logic       CONFIG0_2,
		 output logic       CONFIG1_2,
		 output logic       SI5338_CLKIN,
		 //output logic       SS338_CLKIN,							 
		 inout  logic       GPIO0_1,	
		 inout  logic       GPIO1_1,
		 inout  logic       GPIO0_2,
		 inout  logic       GPIO1_2,
		 output logic       SER_BOOT,
		 output logic       SMBSPDSEL1,
		 output logic       SMBSPDSEL2,				
		 inout  logic       SMBWEN,						 
		 inout  logic       NVMA1SEL,
		 inout  logic       NVMPROT,
		 //input  logic       OPRXLOS1,	
		 input  logic       OPRXLOS2,
		 input  logic       OPTXFLT2,
		 //input  logic       SFP_TXDIS1,
		 input  logic       SFP_TXDIS2,
		 //input  logic 	   SFP_TXRS10,
		 input  logic 	   SFP_TXRS20,
		 //input  logic       LASI1,
		 input  logic       LASI2
		 );

   // internal wires and registers declaration
   reg [19:0] 			    hps_reset_counter = 20'h0;
   wire 			    hps_fpga_reset_n;
   wire [2:0] 			    hps_reset_req;
   wire 			    hps_cold_reset;
   wire 			    hps_warm_reset;
   wire 			    hps_debug_reset;
   wire [27:0] 			    stm_hw_events;
   wire 			    MDIN1;
   wire 			    MDOEN1;
   wire 			    MDO1;
   logic 			    RESET_N;    // S5 active low
   wire 			    clk_buff0;
   wire 			    clk_buff1;
   wire 			    clk_buff2;
   wire [71:0] 			    link;
   wire 			    link_clk;
   logic [71:0] 		    xgmii_buff ;
   logic [71:0] 		    xgmii_rx_dc;
   logic [71:0] 		    xgmii_tx_dc;
   logic [71:0] 		    xgmiialigned;
   logic [31:0] 		    crclink;
   logic 			    eop;
   logic 			    sop;
   logic 			    valid;
   logic 			    ready;
   logic 			    checksum_err;
   logic [2:0] 			    empty;
   logic [63:0] 		    data;
   logic [1:0] 			    state;
   logic 			    eoptx;
   logic 			    soptx;
   logic 			    readytx;
   logic [2:0] 			    emptytx;
   logic [63:0] 		    stdata;
   logic 			    validtx;
   logic [31:0] 		    crclinktx;
   logic [31:0] 		    cctx;
   logic [63:0] 		    xgmiirevtx;
   logic [71:0] 		    xgmiitx;
   logic [71:0] 		    tx_buff;
   logic [63:0] 		    databuff;
   logic 			    validbuff;
   logic 			    readybuff; 
   logic 			    sopbuff;    
   logic 			    eopbuff;    
   logic [2:0] 			    emptybuff;
   logic 			    checkStreamSinkData;
   parameter idle = 0, trig = 1, lock = 2;
   
   // connection of internal logics
   assign stm_hw_events    = {{18{1'b0}}, fpga_dipsw_pio};
   
   // LEDs
   //assign LED[0] = 0;
   //assign LED[1] = 1;
   
   // Uses for PHYRESET
   //logic [1:0] 			    setOrClearRes;

   // Unknown
   assign checkStreamSinkData = (data != 32'h7e7e7e7e) ? 1'b1 : 1'b0;
   
   // BCM8727
   assign CONFIG0_1 = 1'b1;
   assign CONFIG1_1 = 1'b0;
   assign CONFIG0_2 = 1'b1;
   assign CONFIG1_2 = 1'b0;
   assign SI5338_CLKIN	= 1'b0;
   //assign SS338_CLKIN	= 1'b0;

   // Allow spi-rom to be removed 
   // 1 = Boot microcode from spi proms
   assign SER_BOOT  = 1'b0;	
   assign SMBSPDSEL1 = 1'b0; 
   assign SMBSPDSEL2 = 1'b0; 
   assign SMBWEN    = 1'b1;
   assign GPIO0_1   = 1'b0;
   assign GPIO1_1   = 1'b0;
   assign GPIO0_2   = 1'b0;
   assign GPIO1_2   = 1'b0;
   
   // 1: EEPROM Slave Addr 52, 0: 50 addr: 
   // During deassertion of BCM8727 reset, 
   // latched into bit 10 of register 1.8002h 
   assign NVMA1SEL = 1'b1;

   // When high protect non volatile memory 
   assign NVMPROT  = 1'b0;
   
   // MDIO ports connection
`ifdef ENABLE_MDIO
   assign MDIO1 = !MDOEN1? MDO1 : 1'bz;
   assign MDIN1 = MDIO1;
`else
   assign MDC1	 = 1'bz;
   assign MDIO1 = 1'bz;
`endif
   assign MDC2	 = 1'bz;
   assign MDIO2 = 1'bz;
   
   // Reset
   //assign PHYRESET =  (KEY[1] & setOrClearRes[1]); // active low
	assign PHYRESET =  KEY[1]; // active low
   
   // Physical address
   assign {PRTAD4,PRTAD3,PRTAD2,PRTAD1,PRTAD01} = 5'b00000;
   assign  PRTAD02 = 1'b0;
   
   // Transmit drivers
   assign TXONOFF1 = 1'b1;
   assign TXONOFF2 = 1'b1;
   
   // Optical control
   assign OPOUTLVL = 1'b0;				// 0 for active low OPTXENB/OPTXRST
   assign OPINLVL  = 1'b1;				// 1 for active high OPRXLOS/TXONOFF
   
   // LEDs
   assign USER_LED_R = 8'b00001111;
   assign USER_LED_G = 8'b11110000;
   
   /*always @(posedge OSC_50_B4A)
   begin
   if (hps_reset_counter == 20'h ffffff) hps_fpga_reset_n <= 1;
   hps_reset_counter <= hps_reset_counter + 1;
   end*/      

   always @(state)
     begin
        case (state)
          idle:             
            RESET_N <= 1;
          trig:		
            RESET_N <= 0;
          lock:
            RESET_N <= 1;
        endcase
     end

   always @(posedge OSC_50_B4A)
	//always @(posedge fpga_clk_50)
     begin
        case(state)
          idle:
            //if (KEY[0]==0 && setOrClearRes[0])
				if (KEY[0]==0)
              state <= trig;
            else
              state <= idle;
          trig:
            //if (KEY[0]==0 || setOrClearRes[0])
				if (KEY[0]==0)
              state <= lock;
            else
              state <= idle;
          lock:
            //if (KEY[0]==0 || setOrClearRes[0])
				if (KEY[0]==0)
              state <= lock;
            else
              state <= idle;
        endcase
     end

   // SoC sub-system module
   soc_system soc_inst (
			.memory_mem_a                         (hps_memory_mem_a),                               
			.memory_mem_ba                        (hps_memory_mem_ba),                         
			.memory_mem_ck                        (hps_memory_mem_ck),                         
			.memory_mem_ck_n                      (hps_memory_mem_ck_n),                       
			.memory_mem_cke                       (hps_memory_mem_cke),                        
			.memory_mem_cs_n                      (hps_memory_mem_cs_n),                       
			.memory_mem_ras_n                     (hps_memory_mem_ras_n),                      
			.memory_mem_cas_n                     (hps_memory_mem_cas_n),                      
			.memory_mem_we_n                      (hps_memory_mem_we_n),                       
			.memory_mem_reset_n                   (hps_memory_mem_reset_n),                    
			.memory_mem_dq                        (hps_memory_mem_dq),                         
			.memory_mem_dqs                       (hps_memory_mem_dqs),                        
			.memory_mem_dqs_n                     (hps_memory_mem_dqs_n),                      
			.memory_mem_odt                       (hps_memory_mem_odt),                        
			.memory_mem_dm                        (hps_memory_mem_dm),                         
			.memory_oct_rzqin                     (hps_memory_oct_rzqin),                      
			.led_pio_0_external_connection_export (pio_led),                
			.hps_0_hps_io_hps_io_emac1_inst_TX_CLK(hps_emac1_TX_CLK), 
			.hps_0_hps_io_hps_io_emac1_inst_TXD0  (hps_emac1_TXD0),   
			.hps_0_hps_io_hps_io_emac1_inst_TXD1  (hps_emac1_TXD1),   
			.hps_0_hps_io_hps_io_emac1_inst_TXD2  (hps_emac1_TXD2),   
			.hps_0_hps_io_hps_io_emac1_inst_TXD3  (hps_emac1_TXD3),   
			.hps_0_hps_io_hps_io_emac1_inst_RXD0  (hps_emac1_RXD0),   
			.hps_0_hps_io_hps_io_emac1_inst_MDIO  (hps_emac1_MDIO),   
			.hps_0_hps_io_hps_io_emac1_inst_MDC   (hps_emac1_MDC),    
			.hps_0_hps_io_hps_io_emac1_inst_RX_CTL(hps_emac1_RX_CTL), 
			.hps_0_hps_io_hps_io_emac1_inst_TX_CTL(hps_emac1_TX_CTL), 
			.hps_0_hps_io_hps_io_emac1_inst_RX_CLK(hps_emac1_RX_CLK), 
			.hps_0_hps_io_hps_io_emac1_inst_RXD1  (hps_emac1_RXD1),   
			.hps_0_hps_io_hps_io_emac1_inst_RXD2  (hps_emac1_RXD2),   
			.hps_0_hps_io_hps_io_emac1_inst_RXD3  (hps_emac1_RXD3),   
			.hps_0_hps_io_hps_io_qspi_inst_IO0    (hps_qspi_IO0),     
			.hps_0_hps_io_hps_io_qspi_inst_IO1    (hps_qspi_IO1),     
			.hps_0_hps_io_hps_io_qspi_inst_IO2    (hps_qspi_IO2),     
			.hps_0_hps_io_hps_io_qspi_inst_IO3    (hps_qspi_IO3),     
			.hps_0_hps_io_hps_io_qspi_inst_SS0    (hps_qspi_SS0),     
			.hps_0_hps_io_hps_io_qspi_inst_CLK    (hps_qspi_CLK),     
			.hps_0_hps_io_hps_io_sdio_inst_CMD    (hps_sdio_CMD),     
			.hps_0_hps_io_hps_io_sdio_inst_D0     (hps_sdio_D0),      
			.hps_0_hps_io_hps_io_sdio_inst_D1     (hps_sdio_D1),      
			.hps_0_hps_io_hps_io_sdio_inst_CLK    (hps_sdio_CLK),     
			.hps_0_hps_io_hps_io_sdio_inst_D2     (hps_sdio_D2),      
			.hps_0_hps_io_hps_io_sdio_inst_D3     (hps_sdio_D3),      
			.hps_0_hps_io_hps_io_usb1_inst_D0     (hps_usb1_D0),      
			.hps_0_hps_io_hps_io_usb1_inst_D1     (hps_usb1_D1),      
			.hps_0_hps_io_hps_io_usb1_inst_D2     (hps_usb1_D2),      
			.hps_0_hps_io_hps_io_usb1_inst_D3     (hps_usb1_D3),      
			.hps_0_hps_io_hps_io_usb1_inst_D4     (hps_usb1_D4),      
			.hps_0_hps_io_hps_io_usb1_inst_D5     (hps_usb1_D5),      
			.hps_0_hps_io_hps_io_usb1_inst_D6     (hps_usb1_D6),      
			.hps_0_hps_io_hps_io_usb1_inst_D7     (hps_usb1_D7),      
			.hps_0_hps_io_hps_io_usb1_inst_CLK    (hps_usb1_CLK),     
			.hps_0_hps_io_hps_io_usb1_inst_STP    (hps_usb1_STP),     
			.hps_0_hps_io_hps_io_usb1_inst_DIR    (hps_usb1_DIR),     
			.hps_0_hps_io_hps_io_usb1_inst_NXT    (hps_usb1_NXT),     
			.hps_0_hps_io_hps_io_spim0_inst_CLK   (hps_spim0_CLK),    
			.hps_0_hps_io_hps_io_spim0_inst_MOSI  (hps_spim0_MOSI),   
			.hps_0_hps_io_hps_io_spim0_inst_MISO  (hps_spim0_MISO),   
			.hps_0_hps_io_hps_io_spim0_inst_SS0   (hps_spim0_SS0),    
			.hps_0_hps_io_hps_io_uart0_inst_RX    (hps_uart0_RX),     
			.hps_0_hps_io_hps_io_uart0_inst_TX    (hps_uart0_TX),     
			.hps_0_hps_io_hps_io_i2c0_inst_SDA    (hps_i2c0_SDA),     
			.hps_0_hps_io_hps_io_i2c0_inst_SCL    (hps_i2c0_SCL),     
			.hps_0_hps_io_hps_io_can0_inst_RX     (hps_can0_RX),      
			.hps_0_hps_io_hps_io_can0_inst_TX     (hps_can0_TX),      
			.hps_0_hps_io_hps_io_trace_inst_CLK   (hps_trace_CLK),    
			.hps_0_hps_io_hps_io_trace_inst_D0    (hps_trace_D0),     
			.hps_0_hps_io_hps_io_trace_inst_D1    (hps_trace_D1),     
			.hps_0_hps_io_hps_io_trace_inst_D2    (hps_trace_D2),     
			.hps_0_hps_io_hps_io_trace_inst_D3    (hps_trace_D3),     
			.hps_0_hps_io_hps_io_trace_inst_D4    (hps_trace_D4),     
			.hps_0_hps_io_hps_io_trace_inst_D5    (hps_trace_D5),     
			.hps_0_hps_io_hps_io_trace_inst_D6    (hps_trace_D6),     
			.hps_0_hps_io_hps_io_trace_inst_D7    (hps_trace_D7),     
			.hps_0_hps_io_hps_io_gpio_inst_GPIO09 (hps_gpio_GPIO09),  
			.hps_0_hps_io_hps_io_gpio_inst_GPIO35 (hps_gpio_GPIO35),  
			.hps_0_hps_io_hps_io_gpio_inst_GPIO41 (hps_gpio_GPIO41),  
			.hps_0_hps_io_hps_io_gpio_inst_GPIO42 (hps_gpio_GPIO42),  
			.hps_0_hps_io_hps_io_gpio_inst_GPIO43 (hps_gpio_GPIO43),  
			.hps_0_hps_io_hps_io_gpio_inst_GPIO44 (hps_gpio_GPIO44),
			.hps_0_f2h_stm_hw_events_stm_hwevents (stm_hw_events),  
			.clk_clk                              (fpga_clk_50),
			.hps_0_h2f_reset_reset_n              (hps_fpga_reset_n),
			.reset_reset_n                        (hps_fpga_reset_n),
			.hps_0_f2h_cold_reset_req_reset_n     (~hps_cold_reset),
			.hps_0_f2h_warm_reset_req_reset_n     (~hps_warm_reset),
			.hps_0_f2h_debug_reset_req_reset_n    (~hps_debug_reset),
			.clk_xaui_clk                    (link_clk),            //  xaui_clk.clk
			.reset_xaui_reset_n              (RESET_N),             //  xaui_reset.reset_n
			.stream_src_data                 (stdata),              //  stream_src.data
			.stream_src_valid                (validtx),             //  .valid
			.stream_src_ready                (readytx),             //  .ready
			.stream_src_startofpacket        (soptx),               //  .startofpacket
			.stream_src_endofpacket          (eoptx),               //  .endofpacket
			.stream_src_empty                (emptytx),             //  .empty
			.stream_sink_data                (data),                //  stream_sink.data
			.stream_sink_valid               (valid),               //  .valid
			.stream_sink_ready               (ready),               //  .ready
			.stream_sink_startofpacket       (sop),                 //  .startofpacket
			.stream_sink_endofpacket         (eop),                 //  .endofpacket
			.stream_sink_empty               (empty)
			);  
   
   // Source/Probe megawizard instance
   hps_reset hps_reset_inst (
			     .source_clk (fpga_clk_50),
			     .source     (hps_reset_req)
			     );

   altera_edge_detector pulse_cold_reset (
					  .clk       (fpga_clk_50),
					  .rst_n     (hps_fpga_reset_n),
					  .signal_in (hps_reset_req[0]),
					  .pulse_out (hps_cold_reset)
					  );
   defparam pulse_cold_reset.PULSE_EXT = 6;
   defparam pulse_cold_reset.EDGE_TYPE = 1;
   defparam pulse_cold_reset.IGNORE_RST_WHILE_BUSY = 1;

   altera_edge_detector pulse_warm_reset (
					  .clk       (fpga_clk_50),
					  .rst_n     (hps_fpga_reset_n),
					  .signal_in (hps_reset_req[1]),
					  .pulse_out (hps_warm_reset)
					  );
   defparam pulse_warm_reset.PULSE_EXT = 2;
   defparam pulse_warm_reset.EDGE_TYPE = 1;
   defparam pulse_warm_reset.IGNORE_RST_WHILE_BUSY = 1;
   
   altera_edge_detector pulse_debug_reset (
					   .clk       (fpga_clk_50),
					   .rst_n     (hps_fpga_reset_n),
					   .signal_in (hps_reset_req[2]),
					   .pulse_out (hps_debug_reset)
					   );
   defparam pulse_debug_reset.PULSE_EXT = 32;
   defparam pulse_debug_reset.EDGE_TYPE = 1;
   defparam pulse_debug_reset.IGNORE_RST_WHILE_BUSY = 1;
   
   
   // PLL Module
   //pll p1(.refclk(fpga_clk_50), .rst(0), .outclk_0(clk_buff0), .locked());
	pll p1(.refclk(OSC_50_B4A), .rst(0), .outclk_0(clk_buff0), .locked());

   // XAUI Module
   xaui_phy test(
		 .pll_ref_clk         (clk_buff0),         // pll_ref_clk.clk
		 .xgmii_tx_clk		  (link_clk),           // xgmii_tx_clk.clk
		 .xgmii_rx_clk		  (link_clk),           // xgmii_rx_clk.clk
		 .xgmii_rx_dc			  (xgmii_rx_dc),    // xgmii_rx_dc.data
		 .xgmii_tx_dc			  (tx_buff),        // xgmii_tx_dc.data
		 .xaui_rx_serial_data (HSMC_XAUI_RX_p0),   // xaui_rx_serial_data.export
		 .xaui_tx_serial_data (HSMC_XAUI_TX_p0),   // xaui_tx_serial_data.export
		 .rx_ready				  (LED[2]),         // rx_ready.export
		 .tx_ready				  (LED[3]),         // tx_ready.export
		 .phy_mgmt_clk		  (OSC_50_B4A),         // phy_mgmt_clk.clk
		 //.phy_mgmt_clk		  (fpga_clk_50),         // phy_mgmt_clk.clk
		 .phy_mgmt_clk_reset  (!RESET_N),          // phy_mgmt_clk_reset.reset
		 .phy_mgmt_address    (3'h000),            // phy_mgmt.address
		 .phy_mgmt_read       (0),                 // .read
		 .phy_mgmt_readdata   (8'hFFFFFFFF),       // .readdata
		 .phy_mgmt_write      (0),                 // .write
		 .phy_mgmt_writedata  (8'h00000000),       // .writedata
		 .phy_mgmt_waitrequest(0),                 // .waitrequest
		 .reconfig_from_xcvr  (),                  // reconfig_from_xcvr.data
		 .reconfig_to_xcvr    ()                   // reconfig_to_xcvr.data
		 );
         
 // SWAP LANES
	 xgmii swap (
								.clk(link_clk), 
								.xgmiidata(xgmii_rx_dc),// input data
								.reset(!RESET_N),	
								.flag(),
								.xgmii(xgmiialigned)//output data
					   	);
         
// 10Gbps RECEIVER  MAC
	mac_rx rx (
							.clk(link_clk), 
							.xgmiidata(xgmiialigned),// input data
							.reset(!RESET_N),
							.c(crclink),
							
							.data(data),//output data
							.empty(empty),
							.sop(sop),
							.eop(eop),
							.valid(valid),
							.ready(ready),
							.newcrc(crclink),
							.checksum_err(checksum_err)
						);
 /// TX Buffer                       
	txbuffer buffer(
							.clk(link_clk),
							.reset(!RESET_N),
							.stdata(stdata),                 //  stream_src.data
							.valid(validtx),                //            .valid
							.sopin(soptx),        //            .startofpacket
							.eopin(eoptx),          //            .endofpacket
							.emptyin(emptytx),                //            .empty
							
							.databuff(databuff),    
							.validout(validbuff), 
							.readybuff(readybuff),  
							.sopout(sopbuff),    
							.eopout(eopbuff),     
							.emptybuff(emptybuff),
							.readytx(readytx)                //            .ready	
						);
             
 // 10Gbps TRANSMITTER MAC           
	mac_tx tx (
							.clk(link_clk), 
							.stdata(databuff),// input data
							.reset(!RESET_N),
							.c(crclinktx),
							.sopin(sopbuff),
							.eopin(eopbuff),
							.valid(validbuff),
							.ready(readybuff),
							.empty(emptybuff),
												
							.xgmiirev(xgmiirevtx),				
							.xgmii(tx_buff),
							.newcrc(crclinktx),
							.cc(cctx)
						); 
  
  // Xgbe MAC Module
   /* xgbe_mac mac_test(
        .csr_clk_clk (link_clk),         //                    csr_clk.clk
        .csr_reset_reset_n(!RESET_N),               //                  csr_reset.reset_n
        .csr_address (),                     //                        csr.address
        .csr_waitrequest(),                 //                           .waitrequest
        .csr_read (),                        //                           .read
        .csr_readdata (),                    //                           .readdata
        .csr_write (),                       //                           .write
        .csr_writedata (),                   //                           .writedata
        
        .tx_clk_clk (link_clk),                      //                     tx_clk.clk
        .tx_reset_reset_n(!RESET_N),                //                   tx_reset.reset_n
        .avalon_st_tx_startofpacket,      //               avalon_st_tx.startofpacket
        .avalon_st_tx_valid,              //                           .valid
        .avalon_st_tx_data,               //                           .data
        .avalon_st_tx_empty,               //                           .empty
        .avalon_st_tx_ready(),              //                           .ready
        .avalon_st_tx_error,              //                           .error
        .avalon_st_tx_endofpacket,        //                           .endofpacket
        .avalon_st_pause_data,            //            avalon_st_pause.data
        .xgmii_tx_data,                   //                   xgmii_tx.data
        .avalon_st_txstatus_valid,        //         avalon_st_txstatus.valid
        .avalon_st_txstatus_data,         //                           .data
        .avalon_st_txstatus_error,        //                           .error
        
        .rx_clk_clk (link_clk),                      //                     rx_clk.clk
        .rx_reset_reset_n(!RESET_N),                //                   rx_reset.reset_n
        .xgmii_rx_data(xgmii_rx_dc),                   //                   xgmii_rx.data
        .avalon_st_rx_startofpacket,      //               avalon_st_rx.startofpacket
        .avalon_st_rx_endofpacket,        //                           .endofpacket
        .avalon_st_rx_valid,              //                           .valid
        .avalon_st_rx_ready,              //                           .ready
        .avalon_st_rx_data,               //                           .data
        .avalon_st_rx_empty,              //                           .empty
        .avalon_st_rx_error,              //                           .error
        .avalon_st_rxstatus_valid,        //         avalon_st_rxstatus.valid
        .avalon_st_rxstatus_data,         //                           .data
        .avalon_st_rxstatus_error,        //                           .error
        .link_fault_status_xgmii_rx_data  // link_fault_status_xgmii_rx.data
        );*/
endmodule 