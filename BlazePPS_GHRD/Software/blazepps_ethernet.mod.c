#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x14522340, "module_layout" },
	{ 0x6980fe91, "param_get_int" },
	{ 0x3e2e9532, "napi_complete" },
	{ 0xd691cba2, "malloc_sizes" },
	{ 0x973873ab, "_spin_lock" },
	{ 0xff964b25, "param_set_int" },
	{ 0x712aa29b, "_spin_lock_irqsave" },
	{ 0x7d11c268, "jiffies" },
	{ 0x75484296, "netif_rx" },
	{ 0xa13798f8, "printk_ratelimit" },
	{ 0xf12bb3b8, "dev_alloc_skb" },
	{ 0xea147363, "printk" },
	{ 0x7bd0a577, "free_netdev" },
	{ 0xdb3b96d5, "register_netdev" },
	{ 0xb4390f9a, "mcount" },
	{ 0xaf8d5e94, "netif_receive_skb" },
	{ 0x3229caeb, "skb_push" },
	{ 0x4b07e779, "_spin_unlock_irqrestore" },
	{ 0x1902adf, "netpoll_trap" },
	{ 0x2689e860, "netif_napi_add" },
	{ 0xbb0c1c6f, "__napi_schedule" },
	{ 0xf0fdf6cb, "__stack_chk_fail" },
	{ 0xafbc0d15, "alloc_netdev_mq" },
	{ 0xd55704ee, "eth_type_trans" },
	{ 0x7f8bdd3a, "ether_setup" },
	{ 0x2044fa9e, "kmem_cache_alloc_trace" },
	{ 0x37a0cba, "kfree" },
	{ 0x236c8c64, "memcpy" },
	{ 0x73618816, "unregister_netdev" },
	{ 0xbc0d78f9, "__netif_schedule" },
	{ 0xde0cf25, "consume_skb" },
	{ 0x207b7e2c, "skb_put" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "74EDF20C1CEA303821712B8");

static const struct rheldata _rheldata __used
__attribute__((section(".rheldata"))) = {
	.rhel_major = 6,
	.rhel_minor = 6,
};
