/* BlazePPS Ethernet Driver Header 
 *
 * Authors: 
 *	Valeh Valiollahpour Amiri (vv2252@columbia.edu)
 *	Christopher Campbell (cc3769@columbia.edu)
 *
 * Last Modified:
 *	April 23rd, 2015
 */

extern struct net_device *blazedrv;

/* Macro for debug printk's */
#define PDEBUG(fmt, args...) printk( KERN_DEBUG "blaze: " fmt, ## args)

#define NUM_DEVS 2

/* These are the flags in the statusword */
#define BLAZE_RX_INTR 0x0001
#define BLAZE_TX_INTR 0x0002

/* Default use of napi - alternatively it could be declared as module_param and given as parameter at module load time */
#define USE_NAPI 0

/* Default size of the packet pool - alternatively it could be declared as module_param and given as parameter at module load time */
#define POOL_SZ 8

/* Default use of lockup - alternatively it could be declared as module_param and given as parameter at module load time */
#define LOCKUP 0

/* Default timeout period - alternatively it could be declared as module_param and given as parameter at module load time */
#define BLAZE_TIMEOUT 5   /* In jiffies */
